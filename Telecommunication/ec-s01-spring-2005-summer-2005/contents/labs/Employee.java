//Employee is an interface and has all methods defaulted to abstract
//All comments are meant to describe what a concret version of the method would do

public interface Employee {
	
	//This method returns the employee's wage per hour
	double getWage();
	
	//This method sets the employee's hourly wage
	void setWage(double wage1);
	
	//This method returns 1.5 times the regular wage
	double getOvertimeWage();
	
	//This method takes in a certain number of hours and computes the pay, taking into account overtime pay
	void totalDailyPay (double hours);
}