import java.util.*;

public class Course {
    private String name;
    private ArrayList students;
    private ArrayList studentGrades;

    public Course(String n) {
	this.name = n;
	this.students = new ArrayList();
	this.studentGrades = new ArrayList();
    }
    
    public String getCourseName() {
	return name;
    }
    
    public void addStudent(Student stud) {
	students.add(stud);
	studentGrades.add(new Double(0.0));
	stud.addCourse(this);
    }
    
    public void addPoints(double points, String name) {
	for (int i = 0; i < students.size(); i++) 
	    if (((Student)(students.get(i))).getName().equals(name))
		studentGrades.set
		    (i,new Double(((Double)studentGrades.get(i)).doubleValue()+points));
    }
    
    public double studentScore(Student stud) {
	for (int i = 0; i < students.size(); i++)
	    if (stud == (Student)students.get(i))
		return ((Double)studentGrades.get(i)).doubleValue();

	return 0.0;
    }
    
    public double averageAll() {
	double totalScore = 0;
	for (int i = 0; i < studentGrades.size(); i++) 
	    totalScore += ((Double)studentGrades.get(i)).doubleValue();
	return totalScore/studentGrades.size();
    }

    public void report() {
	for (int i = 0; i < students.size(); i++) 
	    System.out.println(((Student) students.get(i)).toString());
    }
}