import java.awt.event.*;
/**
 * This class listens to buttons that have been 
 * implemented in the SwingEmployees application
 */
class EmployeeListener implements ActionListener {
   
   // FIELDS 
   // private field that keeps a handle on the
   // SwingEmployee that is using this listener
   private SwingEmployees semp;
    
    // Constructor
   public EmployeeListener(SwingEmployees s) { 
   	   semp = s; 
   }

   // the actionPerformed method
   public void actionPerformed(ActionEvent e) {
 	   // get the identity of the button that was pressed
 	   String buttonPressed = e.getActionCommand();
 	   
 	   // dispatch to the right method call based on the
 	   // button pressed
 	   if (buttonPressed.equals("Add Employee")){
 	   		// do the addition
 	   		semp.addPressed();
   			// the method in SwingEmployee is therefore 
   			// addPressed() for this button		
 	   } else if (buttonPressed.equals("Print Employee Names")) {
 	   		//call the print method
 	   		semp.printPressed();
 	   } else if (buttonPressed.equals("Clear")) {
 	   		//call the clear method
 	   		semp.clearPressed();
 	   }

			
       }
   }
