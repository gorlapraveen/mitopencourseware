/**
 * AITI Java Laboratory 4:
 * Gradebook: Methods
 */
import java.io.*;

public class GradeBook {
    public static void main(String args[]) throws Exception {
       	/** 'scores' stores a student's score */
	double[] scores; // = {82.4, 72.6, 90, 96.8, 86.1};

	BufferedReader reader = 
	    new BufferedReader(new InputStreamReader(System.in));

	System.out.print("How many scores to enter? ");
	int size = Integer.parseInt(reader.readLine());
	
	scores = new double[size];
	
	System.out.println("Entering " +  size + " scores.");
	System.out.println("Type each score and press enter.");
	for (int i=0; i<size; i++) {
	    System.out.print("Score #" + (i+1) + ": ");
	    scores[i] = Double.parseDouble(reader.readLine());
	}

	printScores(scores);

	System.out.println("Average score: " + aveScore(scores) + 
			   " Maximum score: " + maxScore(scores));
	printGrades(scores);
	histogram(scores);
    }

    /* Lab 4, Part 2(a) */
    public static void printScores(double[] scores) {
	System.out.println("The scores are: ");
	for(int i = 0; i < scores.length; i++) 
	    System.out.print(scores [i]+" ");
	System.out.println();
    }

    /* Lab 4, Part 2(b) */
    public static double aveScore(double[] scores) {
	double total = 0.0;
	for(int i = 0; i < scores.length; i++) 
	    total += scores[i];
	return total/scores.length;
    }

    /* Lab 4, Part 2(c) */
    public static double maxScore(double[] scores) {
	double max = 0.0;
	for(int i = 0; i < scores.length; i++) 
	    max = (max > scores[i])?max:scores[i];
	return max;
    }

    /* Lab 4, Part 2(d) */
    public static char letterGrade(double score) {
	if (score >= 90) 
	    return 'A';
	else if (score >= 80)
	    return 'B';
	else if (score >= 70)
	    return 'C';
	else if (score >= 60)
	    return 'D';
	else 
	    return 'F';
    }

    /* Lab 4, Part 2(e) */
    public static void printGrades(double[] scores) {
	System.out.print("Letter Grades: ");
	for (int i=0; i<scores.length; i++) 
	    System.out.print(letterGrade(scores[i]) + " ");
	System.out.println();
    }
   
    /* Lab 4, Part 2(f) */
    public static void histogram(double[] scores) {
	int[] gradeCounts = new int[5];
	
	/* Tally up how many of each grade there are */
	for (int i = 0; i< scores.length; i++) 
	    if (letterGrade(scores[i]) < 'F')
		gradeCounts[(int)(letterGrade(scores[i])-'A')]++;
	    else 
		gradeCounts[4]++;

	for (int i=0; i<gradeCounts.length; i++) {
	    if (i == 0) System.out.print("A: ");
	    else if (i == 1) System.out.print("B: ");
	    else if (i == 2) System.out.print("C: ");
	    else if (i == 3) System.out.print("D: ");
	    else System.out.print("F: ");

	    /* Print an 'X' for each instance of a particular grade */
	    for (int j=0; j<gradeCounts[i]; j++)
		System.out.print("X");
	    System.out.println();
	}
    }
}