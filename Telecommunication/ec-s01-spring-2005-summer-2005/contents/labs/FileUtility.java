/**
 * AWT Sample application
 *
 * @author 
 * @version 1.00 05/07/05
 */
import java.io.*;
import java.util.*;


 
public class FileUtility {
    
    public static void displayFile(String filename) {
    	BufferedReader lineRead = null;
    	String token; 
    	
    	try {
    		lineRead = new BufferedReader (new FileReader (filename));
    		String line = lineRead.readLine();
    		StringTokenizer st = new StringTokenizer(line);
    		
    		while (st.hasMoreTokens()) {
    			token = st.nextToken();
    			System.out.println(token);
    			 
    		}
    	}
    	
    	catch (IOException e){
    		System.out.println("Could not read from file");
    	}
    	
    	if (lineRead != null) {
    		try {lineRead.close();
    		}
    		catch(IOException e){}
    	}
    }
    
    
    
    public static void writeToFile(String filetocreate) {
    	BufferedWriter h = null;
    	
    	try {
    	
    	File k = new File(filetocreate);
    	FileWriter g = new FileWriter(k);
    	 h = new BufferedWriter(g);
    	
    	h.write("Robert Leke " + "Java is tough but I am loving it");
    	}
    	
    	catch(IOException e) {
    		System.out.println("Could not create and write to file");
    	}
    	
    	if (h != null) {
    		try {
    			h.close();
    		}
    		catch(IOException e) {}
    	}
    }
    
    public static void main (String[] args) {
    	writeToFile("C:\\bwork.txt");
    	FileUtility.displayFile("C:\\bwork.txt");
    }
}




