import java.util.*;
import java.text.DateFormat;

public class I18N {

    static void printMessages(Locale currentLocale) {
	DateFormat dateFormatter = DateFormat.getDateInstance
	    (DateFormat.DEFAULT, currentLocale);

	ResourceBundle labels = 
	    ResourceBundle.getBundle("Labels", currentLocale);
	System.out.println("Current Locale is " + 
			   currentLocale.getDisplayName());
	System.out.println(labels.getString("hello"));
	System.out.println(labels.getString("thanks"));
	System.out.println("Date: " + dateFormatter.format(new Date()));
    }
    
    static public void main(String[] args){
	Locale english = new Locale("en");
	Locale spanish = new Locale("es");
	Locale swahili = new Locale("sw");

	printMessages(english);
	printMessages(spanish);
	printMessages(swahili);
    }
}
