/**
 * <b>Student</b><br>
 * <br>
 * Students are constructed by giving the student's name to the Student(String) constructor.
 * Student have a running point total, initialized to zero, that is increased using the 
 * addPoints(int) method. This value is returned by the getPoints() method.
 * <br>
 * <br>
 * The Student class overrides Object.toString() and returns output of the form:<bt>
 * <i>name points</i> 
 * 
 * 
 * @version 1.0
 * @author AITI Staff
 */
public class Student {
    
	/**
	 * Comment for <code>name</code>:
	 * Private variable storing this Student's name.
	 */
	private String name;
    
	
	/**
	 * Comment for <code>points</code>:
	 * Private variable containing this Student's point total.
	 */
	private double points;
    
    /**
     * Creates a new student with the given name.
     * 
     * @param name The name of this Student.
     */
	public Student(String name) {
    	this.name = name;
    	this.points = 0;
    }
    
    /**
     * Adds the given number of points to this Student's total.
     * 
     * @param newPoints Number of points to add to this Student's total
     */
    public void addPoints(double newPoints) { this.points += newPoints; }
    
    /**
     * Returns the total points this student has earned.
     * 
     * @return Returns the number of points earned by this student.
     */
    public double getPoints() { return this.points; }

    /**
     * Returns a String representation of this Student of the form <i>name points</i>.
     * 
     * @return A String representation of this Student of the form <i>name points</i>.  
     * @see java.lang.Object#toString()
     */
    public String toString() { 
    		return this.name + " " + this.getPoints();
    }
}

/**
 * <b>Course</b><br>
 * <br>
 * Courses are constructed by passing them an array of Students enrolled in the course. 
 * At any time, the course can return the average student score using the average() method.
 * New scores are added to an enrolled student's total using the checkOff(Student, double) method. 
 * Finally, the report() method displays a report to the console of the form 
 * <i>student.toString() Pass/Fail</i>, where the student passes if they have a score higher than 
 * the average.
 * @author AITI Staff
 */
class Course {
    /**
     * Comment for <code>students</code>: private variable containin this course's students.
     */
    private Student[] students;

    /**
     * Creates a new Course that the given students are taking. These students are considered
     * enrolled in the course.
     * 
     * @param students The students enrolled in this Course.
     */
    public Course(Student[] students) { this.students = students; }

    
    /**
     * Returns the current average total score over all the students enrolled in this course
     * 
     * @return The current average total score over all the students enrolled in this Course.
     */
    public double average() {
	double total = 0;
	for (int i = 0; i < students.length; i++) 
	    total += students[i].getPoints();
	
	return total/students.length;
    }
    
    /**
     * This is a private utility function that returns whether a student is in the course.
     * 
     * @param student The Student to test whether they are in the course.
     * @return A boolean representing whether the given student is enrolled in the course.
     */
    private boolean enrolled(Student student) {
	for (int i = 0; i<students.length; i++) 
	    if (students[i] == student)
		return true;
	
	return false;
    }

    /**
     * This method takes a Student and a score as input. If the student is enrolled in the course
     * (that is, they were in the array passed to the constructor), then this method increments that 
     * student's point total and returns true. Otherwise, if the student is not enrolled, we return 
     * false.
     * 
     * @param student The student whose point total we will attempt to increment.
     * @param score The score to attempt to add to the student's total.
     * @return True if the student was enrolled and we increased their total, and false otherwise.
     */
    public boolean checkOff(Student student, double score) {	
	if (!enrolled(student)) return false;
	student.addPoints(score);
	return true;
	
    }

    /**
     * Prints a report of the following form to the screen:
     * <pre>
     * -----------
     * Student.toString() Pass/fail
     * Student.toString() Pass/fail
     * ...
     * Student.toString() Pass/fail
     * </pre>
     * 
     * In other words, this method outputs a seperator, then each Student's name followed by 
     * whether they passed or failed the course on a seperate line. Students will pass the 
     * course if their total is higher than the Course average.
     * 
     */
    public void report() {
	double average = this.average();
	System.out.println("-----------");
	System.out.println("Course Average: " + this.average());
	for (int i = 0; i < students.length; i++) 
	    System.out.println
		(students[i] + ": " + 
		 ((students[i].getPoints() > average)?"Pass":"Fail"));
	System.out.println("-----------");
    }

}