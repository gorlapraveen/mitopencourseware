public class EmployeeAlreadyAdded extends Exception{

	public EmployeeAlreadyAdded(){
		System.out.println("Employee has already been added!");
	}
	
}
