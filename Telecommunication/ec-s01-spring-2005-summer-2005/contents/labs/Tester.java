/**
 * This class will compare two objects.
 * If they differ, it will output an error message and halt. 
 */
package aiti;

public class Tester {
    public static void compare(Object expected, Object received) {
	if (expected.equals(received)) return;
	else outputError(expected, received);
    }

    private static void outputError(Object expected, Object received) {
	(new Error("Expected: " + expected + " Received: " 
		   + received + " Exiting now.")).printStackTrace();
	System.exit(0);
    }
}