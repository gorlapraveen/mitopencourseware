import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;

public class SwingEmployees extends JFrame{
	
	//declare two JPanels to add 3 buttons on one and a textArea in another
	private JPanel buttonPanel;
	private JPanel textPanel;
	
	//declare 3 buttons
	private JButton addButton;
	private JButton printButton;
	private JButton clearButton;
	
	//declare textArea
	private JTextArea textArea;
	
	//declare the employee ArrayList
	private ArrayList employees;
	
	public SwingEmployees(){
		
		//initialize employees
		employees = new ArrayList();
		
		//declare and initialize the container, set layout, background, title, and size
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		cp.setBackground(Color.white);
		setTitle("SwingEmployees");
		setSize(500,500); 
		
		//initialize the buttonPanel, set background and position
		buttonPanel = new JPanel();
		buttonPanel.setBackground(Color.red);
		cp.add(buttonPanel, BorderLayout.NORTH);
		
		//initialize the textPanel, set background and position
		textPanel = new JPanel();
		textPanel.setBackground(Color.green);
		cp.add(textPanel, BorderLayout.CENTER);
		
		//initialize the buttons
		addButton = new JButton ("Add Employee");
		printButton = new JButton ("Print Employee Names");
		clearButton = new JButton ("Clear");
	
		//add buttons to panel
		buttonPanel.add(addButton);
		buttonPanel.add(printButton);
		buttonPanel.add(clearButton);
		
		//initialize textArea, set background, and add
		textArea = new JTextArea();
		textArea.setForeground(Color.blue);
		textArea.setBackground(Color.green);
		textPanel.add(textArea);
		
		//"connect" the buttons to their events
		EmployeeListener el = new EmployeeListener(this);
		addButton.addActionListener(el);
		printButton.addActionListener(el);
		clearButton.addActionListener(el);
		
		
	}
	
	public void addPressed() {
		//prompt the user for an employee name;
		String emp = JOptionPane.showInputDialog("Enter employee name to add");
		//add the names to the ArrayList
		employees.add(emp);
	}
	
	public void printPressed() {
		//make a loop to print the names of the array
		for(int i = 0; i<employees.size(); i++)
			textArea.append((String)employees.get(i)+"\n");	
	}
	
	public void clearPressed() {
		//clears the textArea
		textArea.setText(null);
	}
	
	public static void main(String[] args) {
		SwingEmployees test = new SwingEmployees();
		test.setVisible(true);
	}
}