public class AttachmentStudent extends Student implements Employee {
	
	//An array that takes 4 grades representing an average of 4 courses
	//It is important to initialize each field
	double[] avg = new double[4];
	double regHours=0;
	double overHours=0;
	double wage =8;
	double pay = 0;
	
	char letterGrade;
	
	//The constructor will take in 3 arguments
	//Two arguments will be used to call the superclass, while the toher will be an array of grades
	public AttachmentStudent (String n, String c, double[] grades) {
		
		//We can call the Student superclass constructor
		super(n,c);
		
		avg=grades;
		
		//We give the variable letterGrade a value so that we can access it from the hired method
		letterGrade= gradeAverage(grades);
	}
	
	//this method takes in a letterGrade and returns true if the grade is an A, false otherwise
	boolean hired () {
		
		if (letterGrade == 'A')
			return true;
		else
			return false;
	}
	
	//This method sets the regular hours and overtime hours
	void setHours (double hours) {
		
		if (hours > 8) {
			regHours= 8;
			overHours= hours-8;
		} else
			regHours = hours;
			
		this.totalDailyPay(hours);
	}
	
	//This returns a double array which holds the regular hours and overtime hours
	public double[] getHours() {
		
		//Only returns a double array with values if the student is hired
		if (this.hired()) {
			double[] hoursArray={regHours, overHours};
			return hoursArray;
		} else
		return null;
		
	}
	
	//Sets the wage to a the inputted value
	public void setWage(double wage1) {
		wage=wage1;
	}

	//Returns the regular wage
	public double getWage() {
		return wage;
	}
	
	//Returns the overtime wage
	public double getOvertimeWage(){
		return wage*1.5;
	}
	
	//Calculates pay using the wage and hours
	public void totalDailyPay(double hours) {
				
		pay = this.getWage()*regHours;
		pay += this.getOvertimeWage()*overHours;
		
	}
	
	//Returns a lettergrade depending on the average of grades in the inputted array
	public char gradeAverage (double[] grades) {
		
		//First compute the average
		double sum=0;
		for (int i=0; i<grades.length; i++) {
			sum += grades[i];			
		}
		double average = sum/grades.length;
		
		//If the average is >= 85, return an 'A', otherwise return 'B'
		if (average >= 85) {
			return 'A';
		} else
			return 'B';
	}
	
	//This allows a user to get information by calling: System.out.println(<object>);
	public String toString() {
		
		//The following will help us convert true/false to yes/no in the hired method
		String hire;
				
		if (this.hired())
			hire="Yes";
		else
			hire="No";
		
		//Now we will construct the string 'a' that we will return. We make new lines by "\n"
		String a = "Name: " + super.getName() + "\n";
		a += "Course: " + super.getCourse() + "\n";
		a += "Hired: " + hire + "\n";
		a += "Wage: " + this.getWage() + "\n";
		a += "Total Pay: " + pay;
		
		return a;
	}
		
	
}