public class Lab8Test{


public static void main (String[] args) {
	
	double[] grades1 = {98.5, 99.6, 95.2, 90.3};
	double[] grades2 = {98, 99.3, 95, 90.0};
	double[] grades3 = {78.5, 69, 85, 80};
		
	AttachmentStudent s1 = new AttachmentStudent ("Zahir", "EECS", grades1);
	AttachmentStudent s2 = new AttachmentStudent ("Curts", "MechE", grades2);
	AttachmentStudent s3 = new AttachmentStudent ("Robs", "EE", grades3);
	
	s1.setWage(10);
	s2.setWage(6);
	
	s1.setHours(12);
	s2.setHours(9);
	s3.setHours(6);
	
	System.out.println(s1.getName());
	System.out.print("The wage earned is: KSh ");
	System.out.println(s1.getWage());
	System.out.print("The overtime wage earned is: KSh ");
	System.out.println(s1.getOvertimeWage());	
	System.out.println();
	
	System.out.println(s1);
	System.out.println();
	
	
	System.out.println(s2.getName());
	double[] hours2= s2.getHours(); 
	System.out.println("The number of regular hours worked is: "+ hours2[0] + " hrs.");
	System.out.println("The number of overtime hours worked is: " + hours2[1] + " hrs.");
	System.out.println();
	
	System.out.println(s2);
	System.out.println();
	
	
	System.out.println(s3);
	System.out.println();	
}

}