/**
 * MIT-AITI Kenya 2005 Lab 2 Solutions
 *
 */

class UsingControlStructures {
    public static void main(String[] args) {
	/* Lab 2, Parts 3-4: These should test out all the cases */
	int age = 26;
	int schoolAge = 6;
	int votingAge = 18;
	int presidentAge = 35;
	int retirementAge = 65;

	if (age < schoolAge) 
	    System.out.println("Too young.");
	if (age >= votingAge)
	    System.out.println("Remember to vote.");
	if (age < presidentAge)
	    System.out.println("You can't be president.");
	else
	    System.out.println("Vote for me.");
	if (age > retirementAge)
	    System.out.println("Too old.");

	System.out.println("----------------------");

	/* Lab 2, Part 5 solution */
	for (int i = 40; i>=0; i--) if (i%3 == 0) System.out.println(i);

	System.out.println("----------------------");

	/* Lab 2, Part 6 solution */
	for (int i = 6; i< 30; i++)
	    if ((i%2 != 0) && (i%3 != 0) && (i%5 != 0)) System.out.println(i);	
    }


}