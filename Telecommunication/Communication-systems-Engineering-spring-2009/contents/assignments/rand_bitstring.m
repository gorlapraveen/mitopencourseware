% Greg Kuperman
% Problem Set 6

function bs = rand_bitstring(n,p)
% generate random bit string of size n with distribution p: 0 <= p <= 1

bs = [];

for i=1:n
    r = rand(1);
    if r > p
        bit = 0;
    else
        bit = 1;
    end
    bit = dec2bin(bit);
    bs = [bs bit];
end
