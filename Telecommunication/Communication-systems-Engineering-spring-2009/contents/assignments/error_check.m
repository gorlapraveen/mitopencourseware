% Greg Kuperman
% Problem Set 6

function [num_errors,error_rate] = error_check(B1,B2)
% take in bitstring B1 and B2, return number of bit errors and error rate

if (length(B1) ~= length(B2))
    error('lengths of two bit-strings need to be equal');
end

num_errors = 0;

for(i=1:length(B1))
    if (B1(i) ~= B2(i))
        num_errors = 1 + num_errors;
    end
end

error_rate = num_errors / length(B1);
