1
00:00:04,800 --> 00:00:06,550
JANET RANKIN: One
active learning strategy

2
00:00:06,550 --> 00:00:10,690
that I use in 595
are called mud cards.

3
00:00:10,690 --> 00:00:16,300
And they are a really
easy activity to use.

4
00:00:16,300 --> 00:00:19,900
Anyone can implement them,
and it's a really low barrier.

5
00:00:19,900 --> 00:00:21,940
So I like to introduce
that early in the class

6
00:00:21,940 --> 00:00:24,580
so that students can see
that it doesn't necessarily

7
00:00:24,580 --> 00:00:26,849
disrupt the class flow.

8
00:00:26,849 --> 00:00:28,390
It doesn't really
take a lot of prep.

9
00:00:28,390 --> 00:00:29,848
It doesn't take a
lot of resources.

10
00:00:29,848 --> 00:00:34,960
So a mud card is, at
minimum, is an index card.

11
00:00:34,960 --> 00:00:38,230
And at the end of
every class, I ask

12
00:00:38,230 --> 00:00:41,530
them to write down on the
index card what they might want

13
00:00:41,530 --> 00:00:44,710
to know more about, what
they're still confused about,

14
00:00:44,710 --> 00:00:47,320
or something that they
found particularly

15
00:00:47,320 --> 00:00:48,880
compelling or interesting.

16
00:00:48,880 --> 00:00:51,567
I ask them to specify that so
that I know if they just say,

17
00:00:51,567 --> 00:00:53,650
active learning, I don't
know whether they thought

18
00:00:53,650 --> 00:00:54,820
it was really interesting
or whether they

19
00:00:54,820 --> 00:00:56,110
thought it was confusing.

20
00:00:56,110 --> 00:00:57,820
So I asked them to specify.

21
00:00:57,820 --> 00:01:01,300
But we use we use that because--

22
00:01:01,300 --> 00:01:05,140
and then I also talk to them
about why it's so effective.

23
00:01:05,140 --> 00:01:09,490
They're able to anonymously
identify what they understood

24
00:01:09,490 --> 00:01:11,530
or what they're
still confused about.

25
00:01:11,530 --> 00:01:13,970
I can go back to my
office afterwards,

26
00:01:13,970 --> 00:01:15,340
I can look through the cards.

27
00:01:15,340 --> 00:01:18,460
I can sort them out really
quickly, even in a large class,

28
00:01:18,460 --> 00:01:20,590
and I've used them in
a class of 100 people,

29
00:01:20,590 --> 00:01:23,200
even though 595
is just around 15.

30
00:01:23,200 --> 00:01:26,440
And generally speaking, the
cards fall in three categories.

31
00:01:26,440 --> 00:01:29,140
You get three main problems.

32
00:01:29,140 --> 00:01:31,180
And so you know that
maybe you didn't

33
00:01:31,180 --> 00:01:33,790
do a very good job of
helping students understand

34
00:01:33,790 --> 00:01:36,910
a particular topic, or that
students are really interested

35
00:01:36,910 --> 00:01:39,160
and want more information
about something.

36
00:01:39,160 --> 00:01:42,440
I know that really
easily, really quickly.

37
00:01:42,440 --> 00:01:45,820
So I can make a decision to
either get more resources,

38
00:01:45,820 --> 00:01:50,350
write something up that I can
post on our course website,

39
00:01:50,350 --> 00:01:51,970
or I can come to
class the next time

40
00:01:51,970 --> 00:01:54,170
and say, hey, looks
like most of you

41
00:01:54,170 --> 00:01:57,450
were confused about X.
Let's talk a little bit more

42
00:01:57,450 --> 00:01:57,950
about that.

43
00:01:57,950 --> 00:02:00,520
And I can prepare
ahead of time and be

44
00:02:00,520 --> 00:02:03,430
totally ready to go in
with multiple explanations

45
00:02:03,430 --> 00:02:05,080
or multiple examples
of whatever it

46
00:02:05,080 --> 00:02:06,580
is people are confused about.

47
00:02:06,580 --> 00:02:09,102
I looked over some of the mud
cards we had from last time

48
00:02:09,102 --> 00:02:11,560
and there were some really good
questions, some really good

49
00:02:11,560 --> 00:02:12,060
points.

50
00:02:12,060 --> 00:02:14,470
Someone asked if all those
learning theories that we

51
00:02:14,470 --> 00:02:16,450
discussed were equally valid.

52
00:02:16,450 --> 00:02:19,570
They get they get
targeted feedback based

53
00:02:19,570 --> 00:02:21,640
on what they don't understand.

54
00:02:21,640 --> 00:02:23,740
And it's really easy.

55
00:02:23,740 --> 00:02:26,530
It takes two minutes
at the end of class.

56
00:02:26,530 --> 00:02:28,692
I have a colleague who
calls them tickets to leave,

57
00:02:28,692 --> 00:02:31,150
meaning he doesn't let people
leave the class until they've

58
00:02:31,150 --> 00:02:33,310
handed him an index card
with something on it.

59
00:02:33,310 --> 00:02:35,290
But they're very,
very effective.

60
00:02:35,290 --> 00:02:38,110
I believe they were first used
in the Aero-Astro department

61
00:02:38,110 --> 00:02:42,730
here at MIT, but they're
used all over the place now

62
00:02:42,730 --> 00:02:44,980
and they're extremely,
extremely effective.

63
00:02:44,980 --> 00:02:46,900
For instructors
that are thinking

64
00:02:46,900 --> 00:02:49,720
about using mud cards
in their classes,

65
00:02:49,720 --> 00:02:53,050
I would say the biggest
issue or the biggest concern

66
00:02:53,050 --> 00:02:57,220
would be to make sure that you
use them early in the semester

67
00:02:57,220 --> 00:02:58,810
and that you use them often.

68
00:02:58,810 --> 00:03:00,940
If you use them one
day and then you

69
00:03:00,940 --> 00:03:04,720
don't use them for another two
weeks or even another week,

70
00:03:04,720 --> 00:03:07,420
students really won't
get in the pattern

71
00:03:07,420 --> 00:03:10,180
of filling them out and
filling them out thoughtfully.

72
00:03:10,180 --> 00:03:12,790
And they won't see the
utility, because you really

73
00:03:12,790 --> 00:03:16,740
need to come back the next time
with useful information saying,

74
00:03:16,740 --> 00:03:18,280
I looked at these cards.

75
00:03:18,280 --> 00:03:19,780
I understand you don't get this.

76
00:03:19,780 --> 00:03:20,920
Let me help you.

77
00:03:20,920 --> 00:03:24,460
And that act really
encourages students

78
00:03:24,460 --> 00:03:26,860
to keep filling them
out, and it's just

79
00:03:26,860 --> 00:03:29,340
a win-win for everyone.