1
00:00:04,800 --> 00:00:07,030
JANET RANKIN: The
think-pair-share strategy

2
00:00:07,030 --> 00:00:10,170
is one of the classic
active learning strategies

3
00:00:10,170 --> 00:00:13,120
that one can use
in one's classes.

4
00:00:13,120 --> 00:00:15,730
And it has, as you might
guess, three parts-- the think,

5
00:00:15,730 --> 00:00:17,410
the pair, and the share.

6
00:00:17,410 --> 00:00:22,900
Often there's a collapsing
of the pair and the share.

7
00:00:22,900 --> 00:00:25,660
But in general, you
first give students

8
00:00:25,660 --> 00:00:29,440
time to think about a
question or a situation

9
00:00:29,440 --> 00:00:32,920
or some other scenario that
you want them to think about.

10
00:00:32,920 --> 00:00:37,217
And you give them time to think
actively but alone about that.

11
00:00:37,217 --> 00:00:39,550
And often you can ask them
to write down their thoughts.

12
00:00:39,550 --> 00:00:41,980
But then at the end of
that short period of time,

13
00:00:41,980 --> 00:00:44,470
whether it's three
minutes or five minutes,

14
00:00:44,470 --> 00:00:46,210
you have them pair up.

15
00:00:46,210 --> 00:00:51,130
And if it's a really big class
or the numbers of students

16
00:00:51,130 --> 00:00:53,500
in your class warrant it,
you can have them triple up,

17
00:00:53,500 --> 00:00:55,030
it doesn't have to be a pair.

18
00:00:55,030 --> 00:00:57,610
And they discuss whatever
it is they came up with.

19
00:00:57,610 --> 00:01:00,580
It's just an opportunity
for them to just say here,

20
00:01:00,580 --> 00:01:01,840
this is what I thought of.

21
00:01:01,840 --> 00:01:03,160
What did you think about it?

22
00:01:03,160 --> 00:01:04,750
What did you think
about what I said?

23
00:01:04,750 --> 00:01:07,420
So it's just an interactive
opportunity for the students

24
00:01:07,420 --> 00:01:09,340
to hear from their peers.

25
00:01:09,340 --> 00:01:13,330
And then the key part when
they're talking to each other

26
00:01:13,330 --> 00:01:15,910
is that the instructor
should walk around

27
00:01:15,910 --> 00:01:18,820
the room and kind of eavesdrop
on what the students are

28
00:01:18,820 --> 00:01:19,330
saying.

29
00:01:19,330 --> 00:01:22,270
So you want to understand
the kinds of things

30
00:01:22,270 --> 00:01:24,550
they're talking about, maybe
not the specific details,

31
00:01:24,550 --> 00:01:26,530
but the kinds of things
they're talking about.

32
00:01:26,530 --> 00:01:29,680
And pretty quickly you
can learn if students

33
00:01:29,680 --> 00:01:33,280
are on the right track or if
there's a big misconception

34
00:01:33,280 --> 00:01:36,250
that maybe they
might be reinforcing

35
00:01:36,250 --> 00:01:38,030
each other's misconceptions.

36
00:01:38,030 --> 00:01:39,940
So you want to know
that as the instructor.

37
00:01:39,940 --> 00:01:42,520
So that's why if possible,
to walk around the room

38
00:01:42,520 --> 00:01:45,340
and just overhear what
people are saying,

39
00:01:45,340 --> 00:01:48,790
that can be a really important
part of that pair step.

40
00:01:48,790 --> 00:01:51,970
And then for the share,
you have them report out.

41
00:01:51,970 --> 00:01:53,390
So this does two things.

42
00:01:53,390 --> 00:01:58,660
One, it lets you bring to light
any specific comments that you

43
00:01:58,660 --> 00:02:00,800
overheard during
the pair section.

44
00:02:00,800 --> 00:02:03,430
You can bring it up
with the whole class,

45
00:02:03,430 --> 00:02:05,800
because there can be
some really important,

46
00:02:05,800 --> 00:02:08,979
wonderful observations the
students make within the pair.

47
00:02:08,979 --> 00:02:11,800
And if you don't share that, or
if you don't create a situation

48
00:02:11,800 --> 00:02:13,780
where that can be shared
with the bigger class,

49
00:02:13,780 --> 00:02:17,170
then everybody misses out
except for those three people.

50
00:02:17,170 --> 00:02:20,590
So it gives you an
opportunity to do that.

51
00:02:20,590 --> 00:02:22,540
It also gives you an
opportunity to globally

52
00:02:22,540 --> 00:02:24,610
clear up any
misconceptions that you've

53
00:02:24,610 --> 00:02:28,370
heard that have bubbled up
from this pair exercise.

54
00:02:28,370 --> 00:02:30,340
The other thing it
does is if there's

55
00:02:30,340 --> 00:02:33,250
a group of two or
three or even four,

56
00:02:33,250 --> 00:02:37,330
but I think three
is probably optimal,

57
00:02:37,330 --> 00:02:40,060
the group reports as a group.

58
00:02:40,060 --> 00:02:41,870
It doesn't report
as an individual.

59
00:02:41,870 --> 00:02:43,870
So if a student is
a little insecure

60
00:02:43,870 --> 00:02:47,410
or may not really want to
share what he or she thinks

61
00:02:47,410 --> 00:02:50,500
at a personal level or
at an individual level,

62
00:02:50,500 --> 00:02:54,290
being able to share out
as a group is a bit safer,

63
00:02:54,290 --> 00:02:56,360
is a bit more comfortable
for many students.

64
00:02:56,360 --> 00:02:59,270
So that's what the
share part does.

65
00:02:59,270 --> 00:03:02,230
And then it just gets
the whole group together,

66
00:03:02,230 --> 00:03:07,270
and it lets you kind of sum
up the smaller conversations

67
00:03:07,270 --> 00:03:09,420
with the larger group.