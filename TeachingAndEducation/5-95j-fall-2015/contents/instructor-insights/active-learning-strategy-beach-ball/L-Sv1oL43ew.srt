1
00:00:04,870 --> 00:00:06,920
JANET RANKIN: The
beach ball strategy

2
00:00:06,920 --> 00:00:10,250
is a way to get
students who might not

3
00:00:10,250 --> 00:00:13,580
want to answer questions
or be a little less

4
00:00:13,580 --> 00:00:18,350
outgoing in their answering of
questions to answer questions.

5
00:00:18,350 --> 00:00:20,480
So there's a few different
ways to use the beach

6
00:00:20,480 --> 00:00:21,590
ball, actually.

7
00:00:21,590 --> 00:00:24,500
And one is you just
pose a question

8
00:00:24,500 --> 00:00:28,470
that has enough variation
in the types of responses.

9
00:00:28,470 --> 00:00:31,331
So you wouldn't say
OK, what's 2 plus 2

10
00:00:31,331 --> 00:00:33,080
as an example for the
beach ball question.

11
00:00:33,080 --> 00:00:36,350
You would have to have something
that was a little bit richer.

12
00:00:36,350 --> 00:00:40,130
And then you throw the
ball at one student

13
00:00:40,130 --> 00:00:43,580
or up into the lecture hall,
if it's a big lecture hall.

14
00:00:43,580 --> 00:00:46,910
And whoever catches
it has to respond.

15
00:00:46,910 --> 00:00:50,270
And that response, you
share it with the group,

16
00:00:50,270 --> 00:00:52,500
comment on it if you
want to comment on it.

17
00:00:52,500 --> 00:00:55,130
And then that person who gave
the first response throws

18
00:00:55,130 --> 00:00:56,810
the ball to somebody else.

19
00:00:56,810 --> 00:00:59,540
And then whoever catches
the ball that time responds,

20
00:00:59,540 --> 00:01:01,700
et cetera.

21
00:01:01,700 --> 00:01:06,060
You can use it coupled with
a picture prompt or a graph.

22
00:01:06,060 --> 00:01:09,950
So if you show a graph
or an image and you say,

23
00:01:09,950 --> 00:01:11,930
tell me something you
observe about this image,

24
00:01:11,930 --> 00:01:13,790
and then you throw the
ball and the person

25
00:01:13,790 --> 00:01:16,484
catches it first says one thing
they observe, and they throw it

26
00:01:16,484 --> 00:01:17,900
and the next person
says something

27
00:01:17,900 --> 00:01:19,760
that they observed about
the picture prompt.

28
00:01:19,760 --> 00:01:23,630
So that gives a
foundation and it opens up

29
00:01:23,630 --> 00:01:27,186
space for lots of responses.

30
00:01:27,186 --> 00:01:28,560
The beach ball
does a few things.

31
00:01:28,560 --> 00:01:30,800
One is that as the
instructor, you

32
00:01:30,800 --> 00:01:33,632
don't have to be the one
that calls on the students

33
00:01:33,632 --> 00:01:35,090
all the time, so
the students don't

34
00:01:35,090 --> 00:01:37,520
feel like you're picking
on them, that you're

35
00:01:37,520 --> 00:01:39,200
the one that's cold
calling on them.

36
00:01:39,200 --> 00:01:41,850
And in fact you can throw on
the beach ball completely blind

37
00:01:41,850 --> 00:01:43,850
the first time, close
your eyes, throw the ball,

38
00:01:43,850 --> 00:01:47,420
and so nobody really feels
that you've called on them.

39
00:01:47,420 --> 00:01:49,610
And then subsequently,
it's other students that

40
00:01:49,610 --> 00:01:50,990
are throwing the
ball, so there's

41
00:01:50,990 --> 00:01:56,240
no issue that you're the one
that's picking on the students.

42
00:01:56,240 --> 00:01:58,040
The other thing is
it's kind of fun,

43
00:01:58,040 --> 00:02:03,380
you're throwing the ball around,
so it's hard to get upset,

44
00:02:03,380 --> 00:02:06,230
it's hard to get as
a student nervous

45
00:02:06,230 --> 00:02:08,330
that you're going to have
to answer the question.

46
00:02:08,330 --> 00:02:10,280
And in fact, you
could dodge the ball

47
00:02:10,280 --> 00:02:13,160
if you really didn't want
to answer the question.

48
00:02:13,160 --> 00:02:15,800
So it's a great way to
get students talking.

49
00:02:15,800 --> 00:02:18,250
And in general, it's pretty fun.

50
00:02:18,250 --> 00:02:20,510
And at first I thought
oh, I can't really

51
00:02:20,510 --> 00:02:22,980
use this with grad
students, it's too goofy,

52
00:02:22,980 --> 00:02:26,870
it's too baby or silly.

53
00:02:26,870 --> 00:02:29,000
But actually,
every time I use it

54
00:02:29,000 --> 00:02:30,890
with grad students,
postdocs, everybody

55
00:02:30,890 --> 00:02:32,120
seems perfectly fine with it.

56
00:02:32,120 --> 00:02:34,910
I think it's more to do with
the nature of the question.

57
00:02:34,910 --> 00:02:37,370
If you just ask stupid questions
and throw the ball around,

58
00:02:37,370 --> 00:02:38,080
it'll be stupid.

59
00:02:38,080 --> 00:02:39,980
But if you ask good
questions and it's

60
00:02:39,980 --> 00:02:43,040
a way for students to
give you their answers,

61
00:02:43,040 --> 00:02:45,650
then it seems to
work totally fine.

62
00:02:45,650 --> 00:02:46,880
That's the general way.

63
00:02:46,880 --> 00:02:51,170
Sometimes I'll actually write
questions on the beach ball

64
00:02:51,170 --> 00:02:54,450
and then throw the
ball into the class.

65
00:02:54,450 --> 00:02:56,600
And then students
are loosely supposed

66
00:02:56,600 --> 00:02:58,760
to answer the question
that's in front of them

67
00:02:58,760 --> 00:03:00,060
when they catch the ball.

68
00:03:00,060 --> 00:03:01,730
So that's another way to do it.

69
00:03:01,730 --> 00:03:06,880
But it's a nice, inexpensive,
fairly flexible technique.