1
00:00:05,192 --> 00:00:07,150
JANET RANKIN: Another
active learning technique

2
00:00:07,150 --> 00:00:09,430
that can be very, very
effective in supporting

3
00:00:09,430 --> 00:00:11,710
student learning is the jigsaw.

4
00:00:11,710 --> 00:00:16,640
And it gets its name from
the activity involved

5
00:00:16,640 --> 00:00:18,490
when you do a jigsaw puzzle.

6
00:00:18,490 --> 00:00:20,620
And usually, most people
when they do a jigsaw

7
00:00:20,620 --> 00:00:22,070
will look for the--

8
00:00:22,070 --> 00:00:24,040
they'll find all
the edge pieces,

9
00:00:24,040 --> 00:00:27,010
or they'll find all
the blue pieces,

10
00:00:27,010 --> 00:00:28,930
or they'll find
all the pieces that

11
00:00:28,930 --> 00:00:32,200
have a certain image on them.

12
00:00:32,200 --> 00:00:35,290
You do the homogeneous
aspects of the exercise.

13
00:00:35,290 --> 00:00:37,300
So you get all the edge
pieces, the blue pieces,

14
00:00:37,300 --> 00:00:39,880
the particular image pizzas.

15
00:00:39,880 --> 00:00:42,070
But eventually, you have
to take those pieces

16
00:00:42,070 --> 00:00:43,180
and put them together.

17
00:00:43,180 --> 00:00:44,680
You have to link
them up with pieces

18
00:00:44,680 --> 00:00:46,520
that don't look quite
like those pieces,

19
00:00:46,520 --> 00:00:49,190
so that's the heterogeneous
component of it.

20
00:00:49,190 --> 00:00:52,480
And so what you
do in a jigsaw is

21
00:00:52,480 --> 00:00:55,570
you ask students to become
experts, if you will,

22
00:00:55,570 --> 00:00:59,890
homogeneous experts,
in a particular area,

23
00:00:59,890 --> 00:01:01,390
on a particular topic.

24
00:01:01,390 --> 00:01:04,950
And you can do this by giving
them a reading in class,

25
00:01:04,950 --> 00:01:07,480
or you can do this by asking
them to reflect on something

26
00:01:07,480 --> 00:01:10,870
that you've addressed
in a previous class

27
00:01:10,870 --> 00:01:13,630
or that they've addressed
in a previous homework.

28
00:01:13,630 --> 00:01:17,470
But they sit together,
or they group

29
00:01:17,470 --> 00:01:19,870
together in this, what
I call, homogeneous

30
00:01:19,870 --> 00:01:24,010
group, where everyone is talking
about the same particular

31
00:01:24,010 --> 00:01:25,420
topic.

32
00:01:25,420 --> 00:01:29,680
And they just discuss that
topic and they become experts

33
00:01:29,680 --> 00:01:31,750
in that homogeneous topic.

34
00:01:31,750 --> 00:01:34,480
And then you can have three
or four different topics,

35
00:01:34,480 --> 00:01:38,380
whatever, depending on
the size of the class.

36
00:01:38,380 --> 00:01:40,300
And you have these
local groups of experts,

37
00:01:40,300 --> 00:01:43,090
like the edge pieces, and the
blue pieces, and the picture

38
00:01:43,090 --> 00:01:44,890
pieces.

39
00:01:44,890 --> 00:01:47,470
And then after giving them
a period of time, maybe five

40
00:01:47,470 --> 00:01:49,750
minutes, 10 minutes,
depending on the class,

41
00:01:49,750 --> 00:01:52,690
you break them up into
heterogeneous groups.

42
00:01:52,690 --> 00:01:55,765
So you get one with one
representative from Group A,

43
00:01:55,765 --> 00:01:58,630
goes with one
representative from Group B,

44
00:01:58,630 --> 00:02:00,370
and one representative
from Group C,

45
00:02:00,370 --> 00:02:02,440
and one representative
from group D.

46
00:02:02,440 --> 00:02:04,870
And now you have a new
heterogeneous group that has

47
00:02:04,870 --> 00:02:06,490
an A, a B, a C, and a D in it.

48
00:02:06,490 --> 00:02:10,120
Or if we use the
puzzle analogy, you

49
00:02:10,120 --> 00:02:12,340
have a group that has
some person that's

50
00:02:12,340 --> 00:02:14,890
the edge expert, some person
that's the picture expert,

51
00:02:14,890 --> 00:02:17,990
and some person that's
the blue piece expert.

52
00:02:17,990 --> 00:02:19,240
And they're all in a group.

53
00:02:19,240 --> 00:02:21,550
And now they share what
they know individually

54
00:02:21,550 --> 00:02:25,330
with each other and then
they combine that expertise,

55
00:02:25,330 --> 00:02:29,090
they synthesize that expertise,
to make something bigger

56
00:02:29,090 --> 00:02:34,210
to come up with a more global
understanding of the problem.

57
00:02:34,210 --> 00:02:37,750
And then each group does
that in their own way.

58
00:02:37,750 --> 00:02:41,410
So the heterogeneous groups then
form a more holistic, or a more

59
00:02:41,410 --> 00:02:44,670
heterogeneous, solution
to the problem,

60
00:02:44,670 --> 00:02:47,170
as if they're putting together
the different kinds of puzzle

61
00:02:47,170 --> 00:02:48,190
pieces.

62
00:02:48,190 --> 00:02:51,160
And then again, as with
the other activities,

63
00:02:51,160 --> 00:02:54,670
we ask them to then report out.

64
00:02:54,670 --> 00:02:57,280
Before that I usually
will circulate in the room

65
00:02:57,280 --> 00:02:59,110
the same way I do
with the pair share

66
00:02:59,110 --> 00:03:01,330
to hear what each of
the groups are saying,

67
00:03:01,330 --> 00:03:03,850
to see if any groups are
having particular issues,

68
00:03:03,850 --> 00:03:06,070
or if they have some
particular breakthroughs that I

69
00:03:06,070 --> 00:03:10,510
want to make sure they share
with the class as a whole.

70
00:03:10,510 --> 00:03:12,160
There are some
logistical issues.

71
00:03:12,160 --> 00:03:15,360
One, you want to make sure that
the students really can become

72
00:03:15,360 --> 00:03:17,360
these homogeneous experts.

73
00:03:17,360 --> 00:03:19,480
So that if you have a
group of four people,

74
00:03:19,480 --> 00:03:22,270
that they've all really come
up to speed on the topic.

75
00:03:22,270 --> 00:03:26,320
So ideally, you would give
a pre-class assignment

76
00:03:26,320 --> 00:03:27,250
and you'd say, yes.

77
00:03:27,250 --> 00:03:29,110
I need everybody to
read this article

78
00:03:29,110 --> 00:03:31,212
and to be able to explain
this point, this point,

79
00:03:31,212 --> 00:03:32,920
and this point, and
you would assign that

80
00:03:32,920 --> 00:03:35,710
to that particular
group of students.

81
00:03:35,710 --> 00:03:38,970
And then you'd do the same
with a few other topics.

82
00:03:38,970 --> 00:03:42,730
You're assuming then that the
students have read the paper,

83
00:03:42,730 --> 00:03:44,380
or have read the
article, whatever

84
00:03:44,380 --> 00:03:46,720
it is you want them to read,
and that they understand it

85
00:03:46,720 --> 00:03:49,000
to the level that you
hope they understood it.

86
00:03:49,000 --> 00:03:51,580
Sometimes you don't have
time for that or it just

87
00:03:51,580 --> 00:03:53,470
doesn't work out,
so you give them

88
00:03:53,470 --> 00:03:56,750
something to read within class.

89
00:03:56,750 --> 00:03:58,120
And again, that can be tricky.

90
00:03:58,120 --> 00:04:00,610
Students may not have the
same level of understanding,

91
00:04:00,610 --> 00:04:03,100
so groups may be a
little bit spotty.

92
00:04:03,100 --> 00:04:05,350
The other thing that can
happen with the jigsaw, which

93
00:04:05,350 --> 00:04:09,340
is a really silly problem but
sometimes it just trips you

94
00:04:09,340 --> 00:04:14,830
up, is if you have, depending
on the number of students,

95
00:04:14,830 --> 00:04:16,810
you have to kind of
think on your feet

96
00:04:16,810 --> 00:04:20,200
about how many students are
in the homogeneous group

97
00:04:20,200 --> 00:04:22,870
versus how many students are
in the heterogeneous group.

98
00:04:22,870 --> 00:04:25,320
If it's a perfect
square, it works fine.

99
00:04:25,320 --> 00:04:29,560
So if you've got nine students,
or 16 students, or 25 students,

100
00:04:29,560 --> 00:04:33,219
you know you have five
groups in one situation

101
00:04:33,219 --> 00:04:35,260
and then five groups in
the other, that means you

102
00:04:35,260 --> 00:04:41,300
have to have the same number of
groups as heterogeneous topics.

103
00:04:41,300 --> 00:04:45,790
So if I went A, B, C, D, I
have to then have four groups

104
00:04:45,790 --> 00:04:48,690
because I had to have four A's,
four B's, four C's, and four

105
00:04:48,690 --> 00:04:49,190
D's.

106
00:04:49,190 --> 00:04:52,720
So sometimes just the number
of topics and the number

107
00:04:52,720 --> 00:04:54,430
of students that you
have in your class

108
00:04:54,430 --> 00:04:56,050
makes it kind of
hard to make groups

109
00:04:56,050 --> 00:04:58,420
that don't have extra
people or aren't short

110
00:04:58,420 --> 00:04:59,710
a particular expert.

111
00:04:59,710 --> 00:05:02,650
And that's just something
you want to plan beforehand.

112
00:05:02,650 --> 00:05:04,304
And if somebody
doesn't come to class,

113
00:05:04,304 --> 00:05:06,720
if you know you have 25 students
in your class and someone

114
00:05:06,720 --> 00:05:10,560
doesn't show up, then you
have to kind of deal with it

115
00:05:10,560 --> 00:05:11,520
on the fly.

116
00:05:11,520 --> 00:05:15,560
But that can be the trickiest
thing with the jigsaw,

117
00:05:15,560 --> 00:05:18,170
is just getting the
number of students right.