1
00:00:04,765 --> 00:00:06,140
JANET RANKIN: The
lightning round

2
00:00:06,140 --> 00:00:10,910
is a nice interactive technique
that I use in my class.

3
00:00:10,910 --> 00:00:13,730
And what we do is
I divide the group

4
00:00:13,730 --> 00:00:17,060
into usually two
teams, two sides,

5
00:00:17,060 --> 00:00:18,920
and I have them face each other.

6
00:00:18,920 --> 00:00:22,250
So you can have them face each
other in two parallel lines,

7
00:00:22,250 --> 00:00:25,010
or you can have an inner
circle and an outer circle,

8
00:00:25,010 --> 00:00:29,120
as long as one person is
facing only one other person.

9
00:00:29,120 --> 00:00:33,680
And then you can
do it on a topic

10
00:00:33,680 --> 00:00:36,320
similar to the debate where
there's really two sides

11
00:00:36,320 --> 00:00:39,200
and you ask the one side to
take one side and the other side

12
00:00:39,200 --> 00:00:40,640
to take another side.

13
00:00:40,640 --> 00:00:43,760
But the difference with this is
that the communication is only

14
00:00:43,760 --> 00:00:45,680
between the student
and the student

15
00:00:45,680 --> 00:00:48,950
that he or she is
facing, and they only

16
00:00:48,950 --> 00:00:52,460
have one or two minutes
for the interaction.

17
00:00:52,460 --> 00:00:56,810
So one student gets to speak
for say, it's a minute,

18
00:00:56,810 --> 00:00:59,810
and the other student gets
to respond for a minute.

19
00:00:59,810 --> 00:01:03,110
And then after
that's over, I ask

20
00:01:03,110 --> 00:01:05,510
one line to shift,
or one circle to take

21
00:01:05,510 --> 00:01:08,090
a step to the right or the
left, and now each person

22
00:01:08,090 --> 00:01:11,120
has a new partner
that they speak with.

23
00:01:11,120 --> 00:01:13,350
Generally speaking,
if you had one row

24
00:01:13,350 --> 00:01:16,490
be the pro and the other
row be the con, then

25
00:01:16,490 --> 00:01:19,770
for the next iteration,
you would switch roles.

26
00:01:19,770 --> 00:01:21,430
So there would be
the con and the pro.

27
00:01:21,430 --> 00:01:23,480
The advantages of
this technique are

28
00:01:23,480 --> 00:01:27,320
that the students get to hear
a lot of different opinions

29
00:01:27,320 --> 00:01:29,160
from a lot of
different students.

30
00:01:29,160 --> 00:01:31,520
The fact that it takes
only two minutes at most

31
00:01:31,520 --> 00:01:36,740
for each interaction means that
you can do it several times,

32
00:01:36,740 --> 00:01:37,964
four times, five times.

33
00:01:37,964 --> 00:01:40,130
If you do it five times,
it's only about 10 minutes,

34
00:01:40,130 --> 00:01:44,090
maybe 13 minutes with
some time to translate.

35
00:01:44,090 --> 00:01:46,970
So it's a pretty quick exercise.

36
00:01:46,970 --> 00:01:49,997
And by doing this
lightning round,

37
00:01:49,997 --> 00:01:51,830
students can get many
different perspectives

38
00:01:51,830 --> 00:01:53,450
from other students.

39
00:01:53,450 --> 00:01:56,630
The other kind of question you
can ask with a lightning round

40
00:01:56,630 --> 00:01:59,570
is you can ask a question, maybe
you give students a few minutes

41
00:01:59,570 --> 00:02:02,390
beforehand to do a
calculation or to solve

42
00:02:02,390 --> 00:02:04,220
a particular kind of problem.

43
00:02:04,220 --> 00:02:06,530
And then you say, OK,
you have one minute

44
00:02:06,530 --> 00:02:08,850
to explain how you did the
problem to your partner.

45
00:02:08,850 --> 00:02:11,690
And your partner has one minute
to explain how he or she did

46
00:02:11,690 --> 00:02:13,700
the problem to you.

47
00:02:13,700 --> 00:02:17,270
And again, that lets students
see other students' approaches,

48
00:02:17,270 --> 00:02:21,350
and it lets them think
about their own approach

49
00:02:21,350 --> 00:02:23,474
in the context of the
other person's approach.

50
00:02:23,474 --> 00:02:24,890
You also tell them
they're allowed

51
00:02:24,890 --> 00:02:27,440
to change the way they
think about the problem,

52
00:02:27,440 --> 00:02:30,560
or to think about the response,
based on what they've heard

53
00:02:30,560 --> 00:02:32,960
in their previous pairing.

54
00:02:32,960 --> 00:02:35,720
So it lets students
kind of iterate

55
00:02:35,720 --> 00:02:41,360
and develop their opinions
kind of as they go.

56
00:02:41,360 --> 00:02:43,640
A couple of disadvantages
of the technique

57
00:02:43,640 --> 00:02:45,562
are that it's very, very noisy.

58
00:02:45,562 --> 00:02:47,270
So students are speaking
with each other.

59
00:02:47,270 --> 00:02:49,250
They're really only
about a foot apart,

60
00:02:49,250 --> 00:02:51,043
and everyone is speaking
at the same time.

61
00:02:51,043 --> 00:02:52,501
STUDENT: I originally
thought there

62
00:02:52,501 --> 00:02:53,920
was going to be an
increase, but then I

63
00:02:53,920 --> 00:02:55,970
learned that in the 1870's,
there was like, a mini ice age.

64
00:02:55,970 --> 00:02:57,740
JANET RANKIN: So
depending on the class,

65
00:02:57,740 --> 00:02:59,877
depending on the
personalities, depending

66
00:02:59,877 --> 00:03:01,460
on the people in the
class, it may not

67
00:03:01,460 --> 00:03:02,810
be such a great technique.

68
00:03:02,810 --> 00:03:07,400
But the advantage is that
it really gets everybody up.

69
00:03:07,400 --> 00:03:12,050
It gets everybody moving, even
people that are really shy.

70
00:03:12,050 --> 00:03:14,810
There's virtually nobody that
will stand there for a minute

71
00:03:14,810 --> 00:03:16,490
and not say anything.

72
00:03:16,490 --> 00:03:17,930
Even people that
are extremely shy

73
00:03:17,930 --> 00:03:20,630
are generally willing to
talk with one other person.

74
00:03:20,630 --> 00:03:22,970
And since you
can't hear anything

75
00:03:22,970 --> 00:03:25,100
about what's going
on in other groups

76
00:03:25,100 --> 00:03:28,100
or even the instructor
can't hear what's going on,

77
00:03:28,100 --> 00:03:31,880
it's a fairly safe
conversational situation.

78
00:03:31,880 --> 00:03:34,640
So that is an advantage of it.

79
00:03:34,640 --> 00:03:36,080
The thing I like
about it the most

80
00:03:36,080 --> 00:03:37,970
is that it gets students up.

81
00:03:37,970 --> 00:03:40,790
It gives them the opportunity
to formulate an argument

82
00:03:40,790 --> 00:03:42,844
and to listen to the
argument of others

83
00:03:42,844 --> 00:03:44,510
and then put their
opinion, or argument,

84
00:03:44,510 --> 00:03:47,180
or answer in the context
of other answers.

85
00:03:47,180 --> 00:03:49,340
And it lets them hear the
opinions and responses

86
00:03:49,340 --> 00:03:53,972
of a lot of their peers in
a very short period of time.

87
00:03:53,972 --> 00:03:55,430
After I finish the
lightning round,

88
00:03:55,430 --> 00:03:58,640
I always give students a
minute, or two minutes,

89
00:03:58,640 --> 00:04:01,280
three minutes to sort
of sit down and reflect

90
00:04:01,280 --> 00:04:05,510
on the exercise,
what they learned,

91
00:04:05,510 --> 00:04:09,200
if their answer changed, how
it changed, why it changed.

92
00:04:09,200 --> 00:04:13,610
And then we usually do a
large group report out to say,

93
00:04:13,610 --> 00:04:16,160
I thought this going in,
but I heard from so-and-so

94
00:04:16,160 --> 00:04:17,540
and this is what I think now.

95
00:04:17,540 --> 00:04:19,339
So we do a little
bit of a report out.

96
00:04:19,339 --> 00:04:22,670
Also because of the fact that as
the instructor, since you can't

97
00:04:22,670 --> 00:04:24,230
hear with students
are saying, you

98
00:04:24,230 --> 00:04:27,230
want to know if there's some
big misconception that's

99
00:04:27,230 --> 00:04:29,780
been propagated
during the activity,

100
00:04:29,780 --> 00:04:31,300
and you would want
to address that.

101
00:04:31,300 --> 00:04:34,340
So the report out
for that exercise

102
00:04:34,340 --> 00:04:36,880
can be particularly important.