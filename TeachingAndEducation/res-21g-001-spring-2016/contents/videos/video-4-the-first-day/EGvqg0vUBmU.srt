1
00:00:13,490 --> 00:00:18,116
GUOLONG: That is Friday in one
of the classrooms of Building

2
00:00:18,116 --> 00:00:19,512
26, 11:00 AM.

3
00:00:19,512 --> 00:00:21,470
MARZIEH: So I was quite nervous.

4
00:00:21,470 --> 00:00:24,311
SHAWN: The first day is a
little bit nervous for me.

5
00:00:24,311 --> 00:00:25,810
CARLA: I was nervous.

6
00:00:25,810 --> 00:00:27,899
I'm not going to deny it.

7
00:00:27,899 --> 00:00:29,940
NARRATOR: Facing your
students for the first time

8
00:00:29,940 --> 00:00:31,870
can be a little
scary, but it's also

9
00:00:31,870 --> 00:00:34,220
an opportunity to establish
that your class will

10
00:00:34,220 --> 00:00:35,890
be user-friendly.

11
00:00:35,890 --> 00:00:37,580
BILLY: I think the
first day of class

12
00:00:37,580 --> 00:00:42,170
really sets the tone for
the rest of the semester.

13
00:00:42,170 --> 00:00:44,390
NARRATOR: How can you
set the right tone?

14
00:00:44,390 --> 00:00:47,380
As they make their way to your
classroom on the first day,

15
00:00:47,380 --> 00:00:49,956
what qualities are your
students hoping for?

16
00:00:49,956 --> 00:00:52,330
BILLY: Being knowledgeable
and understanding the concept.

17
00:00:52,330 --> 00:00:56,000
ADRIAN: And you need to feel
that you can approach them.

18
00:00:56,000 --> 00:00:57,750
CHANDLER: Have just
an air of confidence.

19
00:00:57,750 --> 00:00:59,820
ADRIAN: Being easy
to understand.

20
00:00:59,820 --> 00:01:01,311
BILLY: And being enthusiastic.

21
00:01:01,311 --> 00:01:03,019
CHANDLER: Being there,
having a presence,

22
00:01:03,019 --> 00:01:04,330
having a lesson plan ready.

23
00:01:04,330 --> 00:01:06,780
CAROL: They're well-organized.

24
00:01:06,780 --> 00:01:08,520
NARRATOR: That may
sound like a lot.

25
00:01:08,520 --> 00:01:10,440
Let's start with
approachability.

26
00:01:10,440 --> 00:01:14,840
ADRIAN: That is important that
you feel accepted and welcomed

27
00:01:14,840 --> 00:01:16,942
in the environment.

28
00:01:16,942 --> 00:01:18,400
NARRATOR: Being
approachable can be

29
00:01:18,400 --> 00:01:21,920
as simple as facing the class
and smiling-- even if you feel

30
00:01:21,920 --> 00:01:24,200
like hiding behind your papers.

31
00:01:24,200 --> 00:01:26,590
And making eye contact.

32
00:01:26,590 --> 00:01:29,760
BILLY: Making eye contact
just is a personal connection

33
00:01:29,760 --> 00:01:33,479
to the person you are talking
to-- the audience in general.

34
00:01:33,479 --> 00:01:36,710
So students feel like the
explanation you are making

35
00:01:36,710 --> 00:01:37,432
is for them.

36
00:01:37,432 --> 00:01:39,390
CAROL: It is an important
form of communication

37
00:01:39,390 --> 00:01:40,300
for them to do that.

38
00:01:40,300 --> 00:01:42,360
It definitely holds the
students' attention.

39
00:01:42,360 --> 00:01:44,984
BILLY: In terms of
international TAs, in general,

40
00:01:44,984 --> 00:01:46,400
there may be some
misunderstanding

41
00:01:46,400 --> 00:01:50,020
here and there, but I think
it's important to make

42
00:01:50,020 --> 00:01:54,270
the students comfortable
asking for the TA

43
00:01:54,270 --> 00:01:56,300
to repeat any word
or any sentence.

44
00:01:56,300 --> 00:01:59,840
CAROL: I think at the
beginning of the semester,

45
00:01:59,840 --> 00:02:06,080
they should maybe say that
it's OK for the students

46
00:02:06,080 --> 00:02:09,639
to ask for clarifications
if something isn't clear.

47
00:02:09,639 --> 00:02:13,460
I think that's generally
a good thing for all TAs

48
00:02:13,460 --> 00:02:15,040
to welcome any questions.

49
00:02:15,040 --> 00:02:19,010
ADRIAN: Show that
you are open and you

50
00:02:19,010 --> 00:02:22,550
care about the success
of the students

51
00:02:22,550 --> 00:02:26,360
that you are trying to teach.

52
00:02:26,360 --> 00:02:27,930
NARRATOR: Enthusiasm
is another way

53
00:02:27,930 --> 00:02:31,074
to show your students your
class will be friendly.

54
00:02:31,074 --> 00:02:33,490
BILLY: Being enthusiastic and
showing that you're actually

55
00:02:33,490 --> 00:02:34,865
passionate about
the material you

56
00:02:34,865 --> 00:02:39,730
are teaching-- that also gives
a little more comfortable

57
00:02:39,730 --> 00:02:41,180
atmosphere during recitation.

58
00:02:41,180 --> 00:02:44,560
CAROL: I think TAs who are
excited about what they're

59
00:02:44,560 --> 00:02:46,930
teaching-- or if
what they're teaching

60
00:02:46,930 --> 00:02:49,440
is very specific
to their research--

61
00:02:49,440 --> 00:02:51,976
you can tell that
they want to teach it

62
00:02:51,976 --> 00:02:53,850
and they want you to
learn it because they're

63
00:02:53,850 --> 00:02:54,840
very excited about it.

64
00:02:54,840 --> 00:02:59,290
Then the student is probably
going to be more engaged.

65
00:02:59,290 --> 00:03:00,970
NARRATOR: And before
any other quality,

66
00:03:00,970 --> 00:03:03,220
students say they
want knowledge,

67
00:03:03,220 --> 00:03:05,746
but they're also
confident you'll have it.

68
00:03:05,746 --> 00:03:07,120
CAROL: I think I
personally trust

69
00:03:07,120 --> 00:03:12,840
that MIT has selected fine
TAs to teach us the material.

70
00:03:12,840 --> 00:03:14,420
NARRATOR: So on the
first day, rather

71
00:03:14,420 --> 00:03:16,410
than trying to
impress the students,

72
00:03:16,410 --> 00:03:18,940
stick with the simple
explanation of why you are

73
00:03:18,940 --> 00:03:21,930
a good fit to teach the class.

74
00:03:21,930 --> 00:03:26,140
GUOLONG: I'm a third year grad
student in the department of

75
00:03:26,140 --> 00:03:27,090
[INAUDIBLE].

76
00:03:27,090 --> 00:03:30,360
I'm working with Professor
Oppenheim, who is also

77
00:03:30,360 --> 00:03:33,700
the lecturer of this class.

78
00:03:33,700 --> 00:03:36,075
NARRATOR: Students also want
TAs to show self confidence.

79
00:03:36,075 --> 00:03:37,824
CAROL: When I think
of confidence in a TA,

80
00:03:37,824 --> 00:03:40,800
I think of someone who is
very-- they come off as, OK, I

81
00:03:40,800 --> 00:03:42,650
know this material
better than you,

82
00:03:42,650 --> 00:03:44,090
and I'm going to
teach it to you.

83
00:03:44,090 --> 00:03:50,040
So often, I think TAs who speak
not super fast and just pretty

84
00:03:50,040 --> 00:03:53,430
articulate and slower, so
that their point gets across,

85
00:03:53,430 --> 00:03:57,027
usually I feel more
sure that they're pretty

86
00:03:57,027 --> 00:03:58,110
good at explaining things.

87
00:03:58,110 --> 00:04:00,950
BILLY: The speed you have when
you talk in a conversation,

88
00:04:00,950 --> 00:04:02,555
just divide that by two.

89
00:04:02,555 --> 00:04:04,680
And just because you're
nervous in front of people,

90
00:04:04,680 --> 00:04:06,470
the speed will
increase either way.

91
00:04:06,470 --> 00:04:09,120
So, speaking a little
bit more slowly

92
00:04:09,120 --> 00:04:13,170
helps a lot because you
can also have the time

93
00:04:13,170 --> 00:04:14,700
to think about
what you're saying,

94
00:04:14,700 --> 00:04:17,300
and the audience and the
students you're speaking to

95
00:04:17,300 --> 00:04:19,519
will have a better time
understanding, especially

96
00:04:19,519 --> 00:04:22,870
if pronunciation or if
you have a certain accent.

97
00:04:22,870 --> 00:04:24,700
NARRATOR: So, speed
makes a difference

98
00:04:24,700 --> 00:04:26,840
in how students see you.

99
00:04:26,840 --> 00:04:28,830
What about volume?

100
00:04:28,830 --> 00:04:31,220
SHAWN: In a big
classroom, I definitely

101
00:04:31,220 --> 00:04:33,960
have to remind myself
that I have to raise

102
00:04:33,960 --> 00:04:35,830
my voice a little bit louder.

103
00:04:35,830 --> 00:04:37,820
NARRATOR: Speaking
too quietly can also

104
00:04:37,820 --> 00:04:39,659
give the impression
of nervousness.

105
00:04:39,659 --> 00:04:41,430
GUOLONG: This week's
lecture focuses

106
00:04:41,430 --> 00:04:45,490
on sampling rate conversion.

107
00:04:45,490 --> 00:04:47,780
NARRATOR: A stronger
voice projects confidence

108
00:04:47,780 --> 00:04:50,070
and commands attention.

109
00:04:50,070 --> 00:04:51,850
GUOLONG: This week's
lecture focuses

110
00:04:51,850 --> 00:04:55,014
on sampling rate conversion.

111
00:04:55,014 --> 00:04:56,680
NARRATOR: Getting to
the classroom early

112
00:04:56,680 --> 00:04:59,070
can also help you to feel
more confident, because you

113
00:04:59,070 --> 00:05:03,260
can get to know the students
before you start teaching.

114
00:05:03,260 --> 00:05:05,110
And planning ahead
not only makes

115
00:05:05,110 --> 00:05:07,640
you feel more
confident, it also makes

116
00:05:07,640 --> 00:05:10,280
students confident in you.

117
00:05:10,280 --> 00:05:13,860
CAROL: So, I think
one TA that I had--

118
00:05:13,860 --> 00:05:17,380
when I think of a user friendly
class, I think of his class,

119
00:05:17,380 --> 00:05:22,560
because every day he had
his recitation planned

120
00:05:22,560 --> 00:05:24,480
to the minute-- which
is pretty ridiculous--

121
00:05:24,480 --> 00:05:28,612
but it actually was very
logical in a way that

122
00:05:28,612 --> 00:05:29,695
helped the students learn.

123
00:05:29,695 --> 00:05:34,740
So he would pretty much write
the agenda on the blackboard.

124
00:05:34,740 --> 00:05:36,666
Like from minute
zero to minute five,

125
00:05:36,666 --> 00:05:37,790
we're going to take a quiz.

126
00:05:37,790 --> 00:05:39,240
And from minute
five to minute 10,

127
00:05:39,240 --> 00:05:40,950
we're going to talk
about the quiz.

128
00:05:40,950 --> 00:05:43,421
If they seem like
they're well organized--

129
00:05:43,421 --> 00:05:47,040
they have their papers in
a nice, little folder--

130
00:05:47,040 --> 00:05:48,342
I definitely look at that.

131
00:05:48,342 --> 00:05:49,800
CHANDLER: Having
a real lesson plan

132
00:05:49,800 --> 00:05:51,716
and showing that there's
going to productivity

133
00:05:51,716 --> 00:05:53,280
in the recitations is important.

134
00:05:53,280 --> 00:05:54,980
Having someone prepared
shows that they

135
00:05:54,980 --> 00:05:58,560
were thinking about us
when we weren't in class.

136
00:05:58,560 --> 00:06:01,220
NARRATOR: Finally, international
teaching assistants

137
00:06:01,220 --> 00:06:03,600
also stress preparation
and planning

138
00:06:03,600 --> 00:06:06,840
in their advice for a
successful first day.

139
00:06:06,840 --> 00:06:10,080
SHAWN: As an international
teaching assistant,

140
00:06:10,080 --> 00:06:12,620
we have some
disadvantages probably

141
00:06:12,620 --> 00:06:14,840
compared to native speakers.

142
00:06:14,840 --> 00:06:19,500
But if we are prepared,
if we show our enthusiasm

143
00:06:19,500 --> 00:06:21,800
towards the subject,
I think the students

144
00:06:21,800 --> 00:06:23,045
will feel them as well.

145
00:06:23,045 --> 00:06:24,670
CARLA: Just be prepared.

146
00:06:24,670 --> 00:06:27,992
That's what speaks scores about
you-- if you're very prepared.

147
00:06:27,992 --> 00:06:31,644
MARZIEH: I went through all
the notes, all the books that

148
00:06:31,644 --> 00:06:33,450
are related to the course.

149
00:06:33,450 --> 00:06:37,940
And I tried to prepare
some good examples.

150
00:06:37,940 --> 00:06:42,982
GUOLONG: I think for that
class, I did over preparation

151
00:06:42,982 --> 00:06:47,825
that if I discussed
everything on my notebook,

152
00:06:47,825 --> 00:06:50,310
it would take two hours
instead of one hour.

153
00:06:50,310 --> 00:06:53,570
So I think never
running out of materials

154
00:06:53,570 --> 00:06:58,889
seems another source
of my confidence.

155
00:06:58,889 --> 00:07:01,180
CARLA: I should be
confident enough just

156
00:07:01,180 --> 00:07:03,080
by the sake of being the TA.

157
00:07:03,080 --> 00:07:05,430
You shouldn't be nervous
because you do know more

158
00:07:05,430 --> 00:07:08,110
about the topic than they do.

159
00:07:08,110 --> 00:07:11,246
MARZIEH: It was challenging,
but in a good way.

160
00:07:11,246 --> 00:07:13,520
SHAWN: Get everything prepared.

161
00:07:13,520 --> 00:07:16,720
Do your best.