1
00:00:16,200 --> 00:00:18,360
GUEST SPEAKER 1: But
we got the propellers

2
00:00:18,360 --> 00:00:20,410
to start spinning
and everything.

3
00:00:20,410 --> 00:00:24,550
But then, the first problem
was that the propellers

4
00:00:24,550 --> 00:00:26,011
weren't spinning fast enough.

5
00:00:26,011 --> 00:00:28,052
GUEST SPEAKER 2: Yes This
is like the motors that

6
00:00:28,052 --> 00:00:29,740
were spinning the propellers.

7
00:00:29,740 --> 00:00:32,000
However, the motors
are too small,

8
00:00:32,000 --> 00:00:35,040
that it cannot provide enough
force for the drone to fly.

9
00:00:35,040 --> 00:00:38,490
So basically, we
disassembled everything

10
00:00:38,490 --> 00:00:41,040
and we get advice
from one of the clubs

11
00:00:41,040 --> 00:00:42,380
on campus, which is MITERS.

12
00:00:42,380 --> 00:00:46,430
And they tell us, our
drone is not going to fly.

13
00:00:46,430 --> 00:00:50,700
But I know people in our
class that have seen it spin.

14
00:00:50,700 --> 00:00:52,250
Like all of the four of us.

15
00:00:52,250 --> 00:00:53,833
Unfortunately, we
weren't able to take

16
00:00:53,833 --> 00:00:55,260
a picture of our initial drone.

17
00:00:55,260 --> 00:00:59,299
But we got inspired
by it, and so, Daren,

18
00:00:59,299 --> 00:01:01,590
do you want to talk about
the transmitter and receiver.

19
00:01:01,590 --> 00:01:03,195
The gyroscope that we
were initially working on.

20
00:01:03,195 --> 00:01:05,019
GUEST SPEAKER 3: OK,
so for the basic drone,

21
00:01:05,019 --> 00:01:06,894
we got the basic sensors,
like the gyroscope.

22
00:01:06,894 --> 00:01:08,460
So we managed to
get data off that

23
00:01:08,460 --> 00:01:10,920
and we tried to plan a simple
way to base on the balance

24
00:01:10,920 --> 00:01:13,320
until hard to
distribute the power

25
00:01:13,320 --> 00:01:15,470
and arrange it so that
if it tilts this way,

26
00:01:15,470 --> 00:01:16,805
the cycles will spin fast.

27
00:01:16,805 --> 00:01:23,024
[INAUDIBLE] I don't know
[INAUDIBLE] I mean, it might.

28
00:01:23,024 --> 00:01:24,710
If the drone is,
again, to the side,

29
00:01:24,710 --> 00:01:27,307
we just add more power to the
other side to compensate for it

30
00:01:27,307 --> 00:01:28,390
and try to keep it stable.

31
00:01:28,390 --> 00:01:30,200
That was the initial plan.

32
00:01:30,200 --> 00:01:32,850
Managed to get the
readings off the gyroscope

33
00:01:32,850 --> 00:01:36,156
so we can actually distribute
the power correctly.

34
00:01:36,156 --> 00:01:39,070
We were trying to control it
with a simple joystick using

35
00:01:39,070 --> 00:01:43,260
RF transceivers, but
we managed to get--

36
00:01:43,260 --> 00:01:45,660
it was working a few days
ago, then it suddenly

37
00:01:45,660 --> 00:01:47,010
decided to break and die.

38
00:01:47,010 --> 00:01:49,340
So we just scrapped
the entire idea

39
00:01:49,340 --> 00:01:51,140
and just went with
direct control.

40
00:01:51,140 --> 00:01:53,953
So for this [INAUDIBLE]
we just tried to--

41
00:01:53,953 --> 00:01:54,453
OK.

42
00:01:54,453 --> 00:01:55,828
As soon as we had
the [INAUDIBLE]

43
00:01:55,828 --> 00:01:58,830
we had the [INAUDIBLE] control.

44
00:01:58,830 --> 00:02:02,109
And I rotate the fan spin, so--

45
00:02:02,109 --> 00:02:03,650
GUEST SPEAKER 2:
Yes, so right now we

46
00:02:03,650 --> 00:02:08,759
have the joystick controlling
so it has four directions.

47
00:02:08,759 --> 00:02:12,700
It is going up.

48
00:02:12,700 --> 00:02:13,200
Like that.

49
00:02:13,200 --> 00:02:15,128
We should plug this in first.

50
00:02:15,128 --> 00:02:16,440
Sorry.

51
00:02:16,440 --> 00:02:22,030
GUEST SPEAKER: So we scrapped
our goal to make a drone,

52
00:02:22,030 --> 00:02:23,550
because it just wouldn't work.

53
00:02:23,550 --> 00:02:25,514
So we just got all the
pieces from our drone

54
00:02:25,514 --> 00:02:27,680
and then put it all together
to make something else.

55
00:02:27,680 --> 00:02:29,000
GUEST SPEAKER 2: Yeah, so the
basic science is the same.

56
00:02:29,000 --> 00:02:31,850
Like we used-- we didn't
use the motor shield,

57
00:02:31,850 --> 00:02:35,240
because we have a motor
driver, which is a chip that

58
00:02:35,240 --> 00:02:37,130
can control the propeller.

59
00:02:37,130 --> 00:02:41,450
So it's the same basic idea
of how the drone works, and we

60
00:02:41,450 --> 00:02:42,560
want to use the joystick.

61
00:02:42,560 --> 00:02:45,050
This is to control the
drone, as well, like up

62
00:02:45,050 --> 00:02:49,310
and down control the speed
and left and right control

63
00:02:49,310 --> 00:02:52,160
the servo motor that is
spinning underneath, which

64
00:02:52,160 --> 00:02:54,000
is our fan right now.

65
00:02:54,000 --> 00:03:00,150
So right now, I'll
control the car.

66
00:03:00,150 --> 00:03:02,540
It now is accelerating the fan.

67
00:03:02,540 --> 00:03:04,140
Sorry, decelerating.

68
00:03:04,140 --> 00:03:05,770
Yeah.

69
00:03:05,770 --> 00:03:07,270
And then it's going
back to the loop

70
00:03:07,270 --> 00:03:12,870
and going to the same speed
now and then accelerates up.

71
00:03:12,870 --> 00:03:16,010
[INAUDIBLE] So now it's
accelerating, right now.

72
00:03:20,040 --> 00:03:22,910
So if you want to turn a
fan right now, just like--

73
00:03:26,074 --> 00:03:28,630
OK, I guess the servo doesn't
like-- it works this morning,

74
00:03:28,630 --> 00:03:29,130
but--

75
00:03:36,090 --> 00:03:37,250
OK, so maybe--

76
00:03:43,140 --> 00:03:43,940
Yeah.

77
00:03:43,940 --> 00:03:46,370
Sorry I didn't really
make the thing work,

78
00:03:46,370 --> 00:03:48,390
but it actually does work--

79
00:03:48,390 --> 00:03:51,800
just like an hour ago, as well.

80
00:03:51,800 --> 00:03:55,420
So we get the common sense
of how a drone works,

81
00:03:55,420 --> 00:03:58,590
even we're maybe
taking too much--

82
00:03:58,590 --> 00:04:00,602
we were envisioning
wanting to build a drone,

83
00:04:00,602 --> 00:04:02,685
but then realized that
there's so much things need

84
00:04:02,685 --> 00:04:03,680
to consider.

85
00:04:03,680 --> 00:04:06,200
But the learning process
teaches us how to build a drone,

86
00:04:06,200 --> 00:04:09,280
and we actually went to MITERS.

87
00:04:11,890 --> 00:04:13,186
Do you want to say?

88
00:04:13,186 --> 00:04:14,894
GUEST SPEAKER 1: OK,
so we went to MITERS

89
00:04:14,894 --> 00:04:17,250
and then we showed them our
original drone design, which

90
00:04:17,250 --> 00:04:19,208
is like-- they looked at
it and they were like,

91
00:04:19,208 --> 00:04:23,140
oh it's not going to work,
because the motor driver can't

92
00:04:23,140 --> 00:04:25,400
take that much power
to fly a drone.

93
00:04:25,400 --> 00:04:29,700
And then we need a really,
really big battery to power

94
00:04:29,700 --> 00:04:31,930
the motors, and we
didn't have the battery

95
00:04:31,930 --> 00:04:34,420
to supply the power
in the first place.

96
00:04:34,420 --> 00:04:37,020
So they're just like,
hey, we have some pieces

97
00:04:37,020 --> 00:04:38,280
of a drone lying around.

98
00:04:38,280 --> 00:04:39,710
You guys should
put one together.

99
00:04:39,710 --> 00:04:40,900
So we put one together.

100
00:04:40,900 --> 00:04:41,780
Here it is.

101
00:04:41,780 --> 00:04:42,986
GUEST SPEAKER 2: Yes.

102
00:04:42,986 --> 00:04:45,150
Well the thing
is, it is too big,

103
00:04:45,150 --> 00:04:48,220
so it's very hard to
control it inside the room.

104
00:04:48,220 --> 00:04:51,180
You need a very big
land of parking lot

105
00:04:51,180 --> 00:04:52,756
or like a grass
land or something,

106
00:04:52,756 --> 00:04:54,550
so that you can actually fly it.

107
00:04:54,550 --> 00:04:59,340
So actually fly it,
and this is the basic--

108
00:04:59,340 --> 00:05:01,820
the transmitter they
lent us to fly it.

109
00:05:01,820 --> 00:05:05,230
And so I will
introduce this part.

110
00:05:05,230 --> 00:05:08,670
So these are the stronger,
brush-less motors.

111
00:05:08,670 --> 00:05:12,330
They can actually spin,
and unfortunately we

112
00:05:12,330 --> 00:05:14,070
break the propellers.

113
00:05:14,070 --> 00:05:16,740
That is initially all,
which is bigger than that.

114
00:05:16,740 --> 00:05:20,970
But we used our propellers, as
well, to assemble to this part,

115
00:05:20,970 --> 00:05:24,520
and we reprogrammed the--
so people in the MITERS

116
00:05:24,520 --> 00:05:27,270
helped us reprogram the
whole speed controllers

117
00:05:27,270 --> 00:05:30,640
and they get a really good
flight controllers that

118
00:05:30,640 --> 00:05:35,584
can control the four motors over
here, and let's see, actually.

119
00:05:35,584 --> 00:05:39,470
[INAUDIBLE] Can we
make it into the fan?

120
00:05:39,470 --> 00:05:43,170
Like I will be really...

121
00:05:43,170 --> 00:05:46,383
GUEST SPEAKER 4: It can
be like a something that

122
00:05:46,383 --> 00:05:48,080
cleans the floor.