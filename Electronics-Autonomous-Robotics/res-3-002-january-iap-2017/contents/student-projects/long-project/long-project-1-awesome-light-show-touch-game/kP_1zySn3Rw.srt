1
00:00:14,910 --> 00:00:16,940
GUEST SPEAKER 1: A
lights game, where

2
00:00:16,940 --> 00:00:24,490
basically if you tap on the red
light, you increase your score.

3
00:00:24,490 --> 00:00:30,110
And each time you tap correctly,
it keeps switching in between.

4
00:00:30,110 --> 00:00:32,040
And so, the original
concept for this game

5
00:00:32,040 --> 00:00:34,592
was a kind of a game sort of
like Guitar Hero or Dance Dance

6
00:00:34,592 --> 00:00:37,050
Revolution, where you have the
music synced with the light.

7
00:00:37,050 --> 00:00:38,750
So you're tapping
them in rhythm.

8
00:00:38,750 --> 00:00:41,640
And if you get then in
rhythm and at the right time,

9
00:00:41,640 --> 00:00:44,790
the right buttons, that's when
you get the highest score.

10
00:00:44,790 --> 00:00:47,400
But we were having a lot
of issues with regards

11
00:00:47,400 --> 00:00:50,449
to just getting this
hardware and software set up,

12
00:00:50,449 --> 00:00:52,740
so we decided to throw it on
the stove for the project.

13
00:00:52,740 --> 00:00:53,600
It's not my plain theory.

14
00:00:53,600 --> 00:00:56,080
We could apply this very
easily to that type of project.

15
00:00:59,130 --> 00:01:01,950
So basically what the
components we have here are,

16
00:01:01,950 --> 00:01:05,440
we have capacitive sensors.

17
00:01:05,440 --> 00:01:08,190
So basically just a wire
where you just touch it,

18
00:01:08,190 --> 00:01:11,390
it just changes the
capacitance of the circuit

19
00:01:11,390 --> 00:01:14,470
so you can just register that.

20
00:01:14,470 --> 00:01:16,836
They're pretty receptive,
pretty receptive.

21
00:01:16,836 --> 00:01:18,660
Lost the [INAUDIBLE] somewhere.

22
00:01:18,660 --> 00:01:19,700
GUEST SPEAKER 2: And
they're in rainbow order.

23
00:01:19,700 --> 00:01:21,565
You hit them in rainbow
order, so you have the ability

24
00:01:21,565 --> 00:01:22,273
to project ahead.

25
00:01:22,273 --> 00:01:24,980
You know it's going to go
here, here, here, here, here.

26
00:01:24,980 --> 00:01:26,010
No problem.

27
00:01:26,010 --> 00:01:28,010
GUEST SPEAKER 1: See, you
follow the orange one,

28
00:01:28,010 --> 00:01:30,100
you see that one is
what becomes red next.

29
00:01:30,100 --> 00:01:31,766
GUEST SPEAKER 3: Oh,
you told them the--

30
00:01:31,766 --> 00:01:32,679
[LAUGHTER]

31
00:01:32,679 --> 00:01:33,530
--secret.

32
00:01:33,530 --> 00:01:36,030
GUEST SPEAKER 4: And one of the
cool things that we realized

33
00:01:36,030 --> 00:01:40,980
is that, upon making this,
that actually having LED set up

34
00:01:40,980 --> 00:01:45,310
farther away from
the acrylic covering

35
00:01:45,310 --> 00:01:48,606
actually makes the
squares seem brighter

36
00:01:48,606 --> 00:01:50,400
because the light
kind of diffuses.

37
00:01:50,400 --> 00:01:54,300
As opposed to being
right next to the acrylic

38
00:01:54,300 --> 00:01:55,862
where you just see the LEDs.

39
00:01:55,862 --> 00:01:57,540
But if you back
off, you actually

40
00:01:57,540 --> 00:02:00,340
get a lot more illumination.

41
00:02:00,340 --> 00:02:01,400
One of the cool lessons.

42
00:02:04,410 --> 00:02:07,470
GUEST SPEAKER 1: We would love
to give a lot of credit to Tay.

43
00:02:07,470 --> 00:02:11,190
He was the guy in our group who
made this acrylic with a laser

44
00:02:11,190 --> 00:02:14,559
printer, and put the wood
so that each of the squares

45
00:02:14,559 --> 00:02:15,600
were in their own places.

46
00:02:15,600 --> 00:02:17,058
He just made this
beautiful set up.

47
00:02:17,058 --> 00:02:18,880
Unfortunately, he's
not here today.

48
00:02:18,880 --> 00:02:21,320
GUEST SPEAKER 4: Also a lot
of the soldering back here.

49
00:02:21,320 --> 00:02:24,164
There's a lot of
soldering that's going on.

50
00:02:24,164 --> 00:02:25,080
These are really cool.

51
00:02:25,080 --> 00:02:27,660
Also, they sell LEDs.

52
00:02:27,660 --> 00:02:32,105
These LEDs were actually one
series of LEDs down the strip.

53
00:02:32,105 --> 00:02:35,740
So there's like 150
LEDs on a couple

54
00:02:35,740 --> 00:02:39,119
of meters of this kind of a--

55
00:02:39,119 --> 00:02:40,160
GUEST SPEAKER 5: A spool.

56
00:02:40,160 --> 00:02:41,651
GUEST SPEAKER 4: On a spool.

57
00:02:41,651 --> 00:02:46,200
And you can actually control
each one individually

58
00:02:46,200 --> 00:02:47,975
with Arduino.

59
00:02:47,975 --> 00:02:50,830
Just be careful not to
power up all of the,

60
00:02:50,830 --> 00:02:53,780
because you'll actually
pass a lot of current

61
00:02:53,780 --> 00:02:54,600
through your USB.

62
00:02:54,600 --> 00:02:57,920
Probably damage
your USB Arduino.

63
00:02:57,920 --> 00:03:00,330
One of the things that
we learned about wiring

64
00:03:00,330 --> 00:03:02,390
and knowing how much
current's going through.

65
00:03:02,390 --> 00:03:03,418
GUEST SPEAKER 1:
Luckily, we learned--

66
00:03:03,418 --> 00:03:05,250
AUDIENCE: Are you going
to continue to USB, or?

67
00:03:05,250 --> 00:03:06,958
GUEST SPEAKER 1: No,
we learned this one.

68
00:03:06,958 --> 00:03:09,390
GUEST SPEAKER 5: Luckily Andrew
had the magic line of code

69
00:03:09,390 --> 00:03:11,630
which allows you to just limit
the total amount of current

70
00:03:11,630 --> 00:03:12,421
you're putting out.

71
00:03:12,421 --> 00:03:14,610
So even if you ask it to
draw a ton of current,

72
00:03:14,610 --> 00:03:15,889
you just dim LEDs.

73
00:03:15,889 --> 00:03:18,180
GUEST SPEAKER 4: We were
actually warned about a minute

74
00:03:18,180 --> 00:03:21,870
before we were going to try
it, so it saved a laptop

75
00:03:21,870 --> 00:03:23,500
from being destroyed.

76
00:03:23,500 --> 00:03:25,600
So it was a big lesson.

77
00:03:25,600 --> 00:03:28,110
GUEST SPEAKER 3: Anybody
want to have a try?

78
00:03:28,110 --> 00:03:31,697
GUEST SPEAKER 1: Well, firstly,
do you want to say anything?

79
00:03:31,697 --> 00:03:33,780
GUEST SPEAKER 3: That was
about the right summary.

80
00:03:33,780 --> 00:03:35,480
And we will maybe
attempt to continue

81
00:03:35,480 --> 00:03:37,430
to add more features,
including timing

82
00:03:37,430 --> 00:03:39,290
so you get more
points for ending it

83
00:03:39,290 --> 00:03:41,790
in the rhythm with
lights or music.

84
00:03:41,790 --> 00:03:44,340
We were originally going to
include some of the processing

85
00:03:44,340 --> 00:03:47,095
code that the Arduino
team used last time

86
00:03:47,095 --> 00:03:48,720
to make some lights
that flash and sync

87
00:03:48,720 --> 00:03:49,943
with the beat of the music.

88
00:03:49,943 --> 00:03:51,780
But due to technical
difficulties

89
00:03:51,780 --> 00:03:54,250
with wiring, and
capacity, et cetera.

90
00:03:54,250 --> 00:03:57,020
That's going to be
in a later version.

91
00:03:57,020 --> 00:03:59,430
GUEST SPEAKER 1: Yeah,
so, that's our project.

92
00:03:59,430 --> 00:04:00,870
[APPLAUSE]

93
00:04:05,910 --> 00:04:07,120
Yeah?

94
00:04:07,120 --> 00:04:10,540
AUDIENCE: You mentioned an
acrylic piece was printed.