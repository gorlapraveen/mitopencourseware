1
00:00:17,290 --> 00:00:22,724
[APPLAUSE]

2
00:00:29,146 --> 00:00:30,628
GUEST SPEAKER: Thank you.

3
00:00:30,628 --> 00:00:32,122
[APPLAUSE]

4
00:00:32,122 --> 00:00:34,580
AUDIENCE: What [INAUDIBLE] did
you guys use different color

5
00:00:34,580 --> 00:00:36,080
LEDs?

6
00:00:36,080 --> 00:00:37,504
GUEST SPEAKER: We
used new pixel.

7
00:00:37,504 --> 00:00:38,800
AUDIENCE: To add the color?

8
00:00:38,800 --> 00:00:39,644
GUEST SPEAKER: Yeah.

9
00:00:39,644 --> 00:00:43,470
As well as a couple of
other ones and then we--

10
00:00:43,470 --> 00:00:44,400
AUDIENCE: [INAUDIBLE]?

11
00:00:44,400 --> 00:00:45,585
GUEST SPEAKER: Yep.

12
00:00:45,585 --> 00:00:46,960
AUDIENCE: I don't
know if this is

13
00:00:46,960 --> 00:00:50,180
a question you guys
can answer, but do you

14
00:00:50,180 --> 00:00:53,290
guys think that if the paper,
the surfaced in which things

15
00:00:53,290 --> 00:00:56,910
are being projected
through or reflected

16
00:00:56,910 --> 00:01:01,150
is either red, blue, or green,
could you hide some things

17
00:01:01,150 --> 00:01:02,960
and show some others,
you know what I mean?

18
00:01:02,960 --> 00:01:05,540
GUEST SPEAKER: To
an extent, I think.

19
00:01:05,540 --> 00:01:08,520
But it depends also what kind
of material you would use.

20
00:01:08,520 --> 00:01:09,450
AUDIENCE: Right.

21
00:01:09,450 --> 00:01:10,908
GUEST SPEAKER: To
an extent, but it

22
00:01:10,908 --> 00:01:14,710
wouldn't be as effective
as basically using

23
00:01:14,710 --> 00:01:17,110
the codes that turn on and off.

24
00:01:17,110 --> 00:01:20,460
[INAUDIBLE] Yeah.