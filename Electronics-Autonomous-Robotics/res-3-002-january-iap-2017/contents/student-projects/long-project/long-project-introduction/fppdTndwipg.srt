1
00:00:15,194 --> 00:00:16,110
KYLE KEANE: All right.

2
00:00:16,110 --> 00:00:16,609
Cool.

3
00:00:16,609 --> 00:00:20,050
So projects, presentations
are going to happen.

4
00:00:20,050 --> 00:00:22,710
And we're going to start
with the video games,

5
00:00:22,710 --> 00:00:27,270
and then we're going to take
a cool break in the middle

6
00:00:27,270 --> 00:00:33,960
to play with a collaborative,
crowdsourced, cloud-based robot

7
00:00:33,960 --> 00:00:37,380
that's going to try to follow
all of our collective input

8
00:00:37,380 --> 00:00:40,590
in order to grab
a piece of paper,

9
00:00:40,590 --> 00:00:43,130
that Abhinav, the TA
for the Arduino class,

10
00:00:43,130 --> 00:00:47,650
has been building
in his free time.

11
00:00:47,650 --> 00:00:49,410
That's a good embarrassed face.

12
00:00:49,410 --> 00:00:51,390
So that's a way to
model embarrassment.

13
00:00:51,390 --> 00:00:53,973
I appreciate that, because we're
all going to feel embarrassed

14
00:00:53,973 --> 00:00:55,440
when we present, and that's OK.

15
00:00:55,440 --> 00:00:58,530
I feel embarrassed right now
trying to get your attention

16
00:00:58,530 --> 00:01:00,180
and pull you away
from your projects,

17
00:01:00,180 --> 00:01:03,040
because I know that that's what
you want to be doing right now.

18
00:01:03,040 --> 00:01:04,019
So that's OK.

19
00:01:04,019 --> 00:01:06,750
We're all going to
feel embarrassed.

20
00:01:06,750 --> 00:01:09,180
But we're going to
share, and not in order

21
00:01:09,180 --> 00:01:12,990
to impress everyone with
how amazing our project was,

22
00:01:12,990 --> 00:01:15,960
but to open up a dialogue
about what we learned

23
00:01:15,960 --> 00:01:17,830
and what other people
can learn from us.

24
00:01:17,830 --> 00:01:21,030
So the point of coming up
and sharing your presentation

25
00:01:21,030 --> 00:01:23,250
is not to get accolades
from your friends

26
00:01:23,250 --> 00:01:25,470
about how amazingly
brilliant you are.

27
00:01:25,470 --> 00:01:28,140
It's to share something, so
that if someone's interest gets

28
00:01:28,140 --> 00:01:30,100
piqued, they can go, oh, cool!

29
00:01:30,100 --> 00:01:31,800
How did you do that thing?

30
00:01:31,800 --> 00:01:33,482
And then you have a
chance to teach them

31
00:01:33,482 --> 00:01:35,190
about that really
specific thing that you

32
00:01:35,190 --> 00:01:39,430
did, if you were able to make a
really cute thing on the screen

33
00:01:39,430 --> 00:01:43,080
move, or if you're able to make
a really cool video game work,

34
00:01:43,080 --> 00:01:47,100
or if you're able to make a fun
Arduino electronics project.

35
00:01:47,100 --> 00:01:49,800
This is your chance to
teach by sharing what you do

36
00:01:49,800 --> 00:01:52,720
and inviting other people
to ask questions about it.

37
00:01:52,720 --> 00:01:54,870
So this is not about
impressing each other.

38
00:01:54,870 --> 00:01:56,620
Most of you will never--

39
00:01:56,620 --> 00:01:58,770
which way are you
trying to shift me?

40
00:01:58,770 --> 00:01:59,390
Oh, yeah.

41
00:01:59,390 --> 00:02:00,600
You guys.

42
00:02:00,600 --> 00:02:02,820
Most of you won't
talk after this class,

43
00:02:02,820 --> 00:02:04,830
so don't worry if you do
something embarrassing.

44
00:02:04,830 --> 00:02:07,140
You won't see most
of these people.

45
00:02:07,140 --> 00:02:09,565
The ones that you do,
exchange emails with them

46
00:02:09,565 --> 00:02:11,190
and talk to them
after, and they're not

47
00:02:11,190 --> 00:02:13,650
going to care if you do
something silly up here.

48
00:02:13,650 --> 00:02:16,320
So I just want to
set the expectation

49
00:02:16,320 --> 00:02:20,070
that this is not a competitive
presentation environment.

50
00:02:20,070 --> 00:02:22,710
We're not getting
venture capital.

51
00:02:22,710 --> 00:02:26,162
There's nothing other than
a learning opportunity here.

52
00:02:26,162 --> 00:02:30,510
ANDREW RINGLER: As Kyle
said, we're not competing.

53
00:02:30,510 --> 00:02:35,100
Hopefully you had fun
and learned something

54
00:02:35,100 --> 00:02:36,270
over the last two weeks.

55
00:02:36,270 --> 00:02:41,070
That's teachers'
only goal, to let you

56
00:02:41,070 --> 00:02:43,780
have fun and learn something.

57
00:02:43,780 --> 00:02:44,280
So yeah.

58
00:02:44,280 --> 00:02:46,440
I think the point
of this two weeks

59
00:02:46,440 --> 00:02:49,200
is to get your feet
wet with Arduino

60
00:02:49,200 --> 00:02:55,640
and see what it's like to plan
a project and try to build it,

61
00:02:55,640 --> 00:03:00,970
and then figure out how
to do it better next time.

62
00:03:00,970 --> 00:03:03,660
So that's what we do.

63
00:03:03,660 --> 00:03:07,970
But that said, all your
groups did amazing.

64
00:03:07,970 --> 00:03:10,560
You did so much work,
which is awesome,

65
00:03:10,560 --> 00:03:15,000
and we're all excited to see
what you have to present.

66
00:03:15,000 --> 00:03:17,910
Thank you.

67
00:03:17,910 --> 00:03:19,650
All right.

68
00:03:19,650 --> 00:03:20,921
KYLE KEANE: You're done?

69
00:03:20,921 --> 00:03:22,170
ANDREW RINGLER: Yes, I'm done.

70
00:03:22,170 --> 00:03:22,640
KYLE KEANE: All right, cool.

71
00:03:22,640 --> 00:03:23,525
ANDREW RINGLER: Come on in.

72
00:03:23,525 --> 00:03:24,441
KYLE KEANE: All right.

73
00:03:24,441 --> 00:03:27,055
So the first group to
present is going to--

74
00:03:27,055 --> 00:03:29,110
I'll let them come
up and introduce.

75
00:03:29,110 --> 00:03:30,870
So the video game
groups, when you

76
00:03:30,870 --> 00:03:35,610
come up, take the microphone,
introduce your game,

77
00:03:35,610 --> 00:03:37,140
demo your game--

78
00:03:37,140 --> 00:03:39,960
or introduce your game,
talk about anything

79
00:03:39,960 --> 00:03:42,330
that you feel inspired to
talk about, if there is a fun,

80
00:03:42,330 --> 00:03:45,480
interesting learning experience,
or if something gets you really

81
00:03:45,480 --> 00:03:47,970
excited, talk about that.

82
00:03:47,970 --> 00:03:49,530
Demo your game.

83
00:03:49,530 --> 00:03:51,810
And then we're going to
open it up to questions.

84
00:03:51,810 --> 00:03:54,150
As an audience
member, the best thing

85
00:03:54,150 --> 00:03:56,970
that you can do to support
someone who's presenting

86
00:03:56,970 --> 00:04:01,470
is to make occasional eye
contact and pay some attention,

87
00:04:01,470 --> 00:04:04,661
and then at the end be able to
ask a question that reflects--

88
00:04:04,661 --> 00:04:05,160
I know.

89
00:04:05,160 --> 00:04:07,355
I'm very explicit,
but you'd be amazed

90
00:04:07,355 --> 00:04:08,730
at how many people
don't make eye

91
00:04:08,730 --> 00:04:11,070
contact with the presenters.

92
00:04:11,070 --> 00:04:13,620
And then ask
questions at the end,

93
00:04:13,620 --> 00:04:15,230
which reflects something like--

94
00:04:15,230 --> 00:04:16,230
that looked really cool.

95
00:04:16,230 --> 00:04:17,250
How did you do that?

96
00:04:17,250 --> 00:04:19,440
And what you're doing is
you're setting them up

97
00:04:19,440 --> 00:04:22,680
to look smart and share
something that they're

98
00:04:22,680 --> 00:04:24,990
passionate about
and feel supported.

99
00:04:24,990 --> 00:04:28,050
And you, too, will have
your chance to be up here.

100
00:04:28,050 --> 00:04:29,550
And I guarantee
that you will want

101
00:04:29,550 --> 00:04:31,960
to have an audience
that makes eye contact

102
00:04:31,960 --> 00:04:33,930
and then asks you
questions about things that

103
00:04:33,930 --> 00:04:37,140
were interesting in your project
so that you, too, can feel

104
00:04:37,140 --> 00:04:40,562
supported and interesting
and like people

105
00:04:40,562 --> 00:04:41,770
care about what you're doing.

106
00:04:41,770 --> 00:04:45,840
Because a lot of this class was
two weeks of passion projects.

107
00:04:45,840 --> 00:04:47,670
We don't have a
lot of structure,

108
00:04:47,670 --> 00:04:50,700
because I think that
you're here for two weeks,

109
00:04:50,700 --> 00:04:53,040
and you should opt into the
project that you want to do,

110
00:04:53,040 --> 00:04:55,290
and you should find people
that you want to work with.

111
00:04:55,290 --> 00:04:57,206
So a lot of this
work that you've

112
00:04:57,206 --> 00:04:58,830
done over the last
two weeks represents

113
00:04:58,830 --> 00:05:00,600
a personal investment.

114
00:05:00,600 --> 00:05:03,060
And so respect
that in each other.

115
00:05:03,060 --> 00:05:05,670
Everyone who's up
here presenting

116
00:05:05,670 --> 00:05:09,210
put a lot of their own
interest and passion into this.

117
00:05:09,210 --> 00:05:14,580
So yeah-- support,
support, support.

118
00:05:14,580 --> 00:05:18,050
So, first group, are
you ready to present?

119
00:05:18,050 --> 00:05:18,940
All right.

120
00:05:18,940 --> 00:05:20,140
Here you go.

121
00:05:20,140 --> 00:05:21,990
[CLAPPING]