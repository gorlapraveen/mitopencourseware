1
00:00:14,880 --> 00:00:17,360
AUDIENCE: That was awesome,
but my card was stolen.

2
00:00:17,360 --> 00:00:19,369
[LAUGHING]

3
00:00:19,369 --> 00:00:21,910
GUEST SPEAKER 1: So this is the
Soil Moisture Inversion Level

4
00:00:21,910 --> 00:00:25,481
Estimator, or SMILE,
for short, and it

5
00:00:25,481 --> 00:00:27,610
will keep your plants happy.

6
00:00:27,610 --> 00:00:30,530
GUEST SPEAKER 2: By telling you
when your soil is too wet, too

7
00:00:30,530 --> 00:00:32,808
dry, or just right.

8
00:00:32,808 --> 00:00:37,890
So it uses an RGB LED, and
any soil moisture sensor.

9
00:00:37,890 --> 00:00:41,700
GUEST SPEAKER 1: And
here's how it works,

10
00:00:41,700 --> 00:00:48,120
GUEST SPEAKER 2: So just water
turns blue, and that's too wet.

11
00:00:48,120 --> 00:00:52,578
GUEST SPEAKER 1: It says "Gone
Fishing" in the silly monitor.

12
00:00:52,578 --> 00:00:54,522
[LAUGHING]

13
00:01:03,290 --> 00:01:06,109
GUEST SPEAKER 2: Moist
soil will turn green

14
00:01:06,109 --> 00:01:07,400
if you stick it in all the way.

15
00:01:07,400 --> 00:01:08,790
And then it says--

16
00:01:08,790 --> 00:01:09,710
what does it say?

17
00:01:09,710 --> 00:01:10,590
GUEST SPEAKER 2: I feel good.

18
00:01:10,590 --> 00:01:12,090
GUEST SPEAKER 2: I
feel good, so you

19
00:01:12,090 --> 00:01:13,530
don't need to water your plant.

20
00:01:13,530 --> 00:01:19,234
And then completely dry
soil is red, and it says--

21
00:01:19,234 --> 00:01:20,810
GUEST SPEAKER 1: I'm thirsty.

22
00:01:20,810 --> 00:01:22,940
GUEST SPEAKER 2:
Water me, I'm thirsty.