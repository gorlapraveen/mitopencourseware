1
00:00:15,350 --> 00:00:18,860
KYLE KEANE: So today,
we're going to do something

2
00:00:18,860 --> 00:00:23,810
where we talk about our name.

3
00:00:23,810 --> 00:00:26,908
Renew icebreakery thing
for like 15-- well, no,

4
00:00:26,908 --> 00:00:27,950
there's a lot of people--

5
00:00:27,950 --> 00:00:30,150
20 minutes.

6
00:00:30,150 --> 00:00:31,630
Name, what brought you here.

7
00:00:35,990 --> 00:00:38,970
And, oh yeah, right off the bat
I'll say the more you all talk,

8
00:00:38,970 --> 00:00:40,040
the less I talk.

9
00:00:40,040 --> 00:00:42,420
So you'll get sick of
my voice, I promise.

10
00:00:42,420 --> 00:00:44,990
I am very monotonous at times.

11
00:00:44,990 --> 00:00:47,900
And so, the more you guys
participate by saying words,

12
00:00:47,900 --> 00:00:50,100
the less you have
to hear my voice.

13
00:00:50,100 --> 00:00:53,670
So I would like three
things that we would

14
00:00:53,670 --> 00:00:54,920
like to know about each other.

15
00:00:57,630 --> 00:01:00,970
So if you could say--

16
00:01:00,970 --> 00:01:03,239
I need three more bullet
points about things

17
00:01:03,239 --> 00:01:04,239
that we're going to say.

18
00:01:06,774 --> 00:01:08,190
This is the moment
where you talk,

19
00:01:08,190 --> 00:01:10,142
otherwise I just
have a compulsion.

20
00:01:10,142 --> 00:01:11,600
I want to hear my
voice constantly,

21
00:01:11,600 --> 00:01:13,360
that's why I'm in
front of a classroom.

22
00:01:13,360 --> 00:01:14,630
AUDIENCE: Favorite video game.

23
00:01:14,630 --> 00:01:15,620
KYLE KEANE: Favorite video game.

24
00:01:15,620 --> 00:01:16,120
Cool.

25
00:01:16,120 --> 00:01:18,470
Oh, that's so well-themed.

26
00:01:18,470 --> 00:01:19,300
Nice.

27
00:01:19,300 --> 00:01:21,410
All right.

28
00:01:21,410 --> 00:01:22,260
No, honestly.

29
00:01:22,260 --> 00:01:25,310
We had one handed food during
the seminar I was teaching,

30
00:01:25,310 --> 00:01:28,460
but, yeah, favorite video
game is really on point.

31
00:01:31,300 --> 00:01:33,259
I can write, I promise.

32
00:01:33,259 --> 00:01:36,170
There we go.

33
00:01:36,170 --> 00:01:37,850
All right, favorite video game.

34
00:01:42,970 --> 00:01:43,810
I can do this.

35
00:01:43,810 --> 00:01:45,300
AUDIENCE: Programming
background?

36
00:01:45,300 --> 00:01:48,880
KYLE KEANE: Programming
background, sure.

37
00:01:48,880 --> 00:01:52,848
Do we want to know
languages or--

38
00:01:52,848 --> 00:01:55,375
AUDIENCE: Either languages,
amount of rough-- amount

39
00:01:55,375 --> 00:01:56,050
of experience.

40
00:01:56,050 --> 00:01:57,496
Things like that.

41
00:01:57,496 --> 00:01:58,460
KYLE KEANE: All right.

42
00:02:08,590 --> 00:02:14,000
So something about your
general level of experience.

43
00:02:14,000 --> 00:02:16,510
And if you have nothing
to say about that,

44
00:02:16,510 --> 00:02:23,000
then just say [INAUDIBLE]----
and that's perfectly fine.

45
00:02:23,000 --> 00:02:26,800
That gives us a good read
on what's happening there.

46
00:02:26,800 --> 00:02:27,886
One more bullet point.

47
00:02:27,886 --> 00:02:30,010
Are these called bullet
points when they're dashes?

48
00:02:30,010 --> 00:02:31,403
Maybe they're just
called dashes.

49
00:02:31,403 --> 00:02:32,090
AUDIENCE: Close enough.

50
00:02:32,090 --> 00:02:33,371
KYLE KEANE: Falsima, was it?

51
00:02:33,371 --> 00:02:34,329
AUDIENCE: Close enough.

52
00:02:34,329 --> 00:02:35,371
KYLE KEANE: Close enough.

53
00:02:35,371 --> 00:02:36,629
Oh, yes, that's good.

54
00:02:36,629 --> 00:02:38,920
AUDIENCE: You can call them
bullets-- squashed bullets.

55
00:02:38,920 --> 00:02:40,170
KYLE KEANE: Squashed bullets--

56
00:02:40,170 --> 00:02:40,820
I like that.

57
00:02:40,820 --> 00:02:41,320
All right.

58
00:02:41,320 --> 00:02:42,625
One more fun thing.

59
00:02:42,625 --> 00:02:45,520
AUDIENCE: How about what
you might do with Unity?

60
00:02:45,520 --> 00:02:46,228
KYLE KEANE: Cool.

61
00:02:46,228 --> 00:02:46,728
All right.

62
00:02:46,728 --> 00:02:47,690
What do you want to do?

63
00:02:47,690 --> 00:02:49,773
Well, if you think that
that's different than what

64
00:02:49,773 --> 00:02:51,296
brought you here.

65
00:02:51,296 --> 00:02:52,670
AUDIENCE: All
right, fair enough.

66
00:02:52,670 --> 00:02:52,980
KYLE KEANE: Yeah.

67
00:02:52,980 --> 00:02:53,780
All right.

68
00:02:53,780 --> 00:02:56,489
I like it, but all
right, so-- the vision

69
00:02:56,489 --> 00:02:57,520
for the type of game.

70
00:02:57,520 --> 00:02:59,353
Because some of you may
have come with this,

71
00:02:59,353 --> 00:03:01,300
oh my god, I have this
amazing side scroller

72
00:03:01,300 --> 00:03:04,750
that involves pajamas
and bed sheets.

73
00:03:04,750 --> 00:03:07,740
So you can feel free
to share that here.

74
00:03:07,740 --> 00:03:16,400
What type of game or
what you want to do.

75
00:03:20,060 --> 00:03:22,579
I know that it's way too
small, but that's all right.

76
00:03:22,579 --> 00:03:25,484
We'll get it from example,
once each other starts sharing.

77
00:03:25,484 --> 00:03:26,651
AUDIENCE: Where you're from.

78
00:03:26,651 --> 00:03:27,942
KYLE KEANE: Where you're from--

79
00:03:27,942 --> 00:03:28,610
I love that one.

80
00:03:28,610 --> 00:03:29,776
There's so many fun things--

81
00:03:29,776 --> 00:03:31,550
I'm from Southern California.

82
00:03:31,550 --> 00:03:33,850
And when I was in
Southern California,

83
00:03:33,850 --> 00:03:35,600
most people were from
Southern California.

84
00:03:35,600 --> 00:03:38,090
But now that I am in
Cambridge, I get so excited.

85
00:03:38,090 --> 00:03:39,440
I'm like, that person--

86
00:03:39,440 --> 00:03:41,810
I had a freshman in
my seminar this year

87
00:03:41,810 --> 00:03:44,210
that was from my neighboring
city in Southern California.

88
00:03:44,210 --> 00:03:45,730
It was so exciting.

89
00:03:45,730 --> 00:03:46,230
All right.

90
00:03:46,230 --> 00:03:48,340
So-- AUDIENCE:
[INAUDIBLE] KYLE KEANE:

91
00:03:48,340 --> 00:03:50,530
It is Chino Hills,
California, which

92
00:03:50,530 --> 00:03:54,045
is a tiny weird little city
that is not really connected

93
00:03:54,045 --> 00:03:55,707
to many other cities.

94
00:03:55,707 --> 00:03:57,040
AUDIENCE: Near LA, I'm guessing.

95
00:03:57,040 --> 00:03:59,240
KYLE KEANE: It's in
between LA and San Diego

96
00:03:59,240 --> 00:04:01,690
and out into the desert.