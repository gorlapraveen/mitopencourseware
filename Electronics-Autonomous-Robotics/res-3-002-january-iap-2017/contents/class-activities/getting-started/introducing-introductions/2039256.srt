1
00:00:14,884 --> 00:00:15,800
KYLE KEANE: All right.

2
00:00:15,800 --> 00:00:22,599
So on a scale of zero to 10, how
energetic is everyone feeling?

3
00:00:22,599 --> 00:00:24,390
I'm going to count to
three and then-- hey,

4
00:00:24,390 --> 00:00:27,200
is that a super familiar face?

5
00:00:27,200 --> 00:00:28,880
Hey.

6
00:00:28,880 --> 00:00:30,700
Howdy.

7
00:00:30,700 --> 00:00:33,380
Yeah, so on the
count of three, I

8
00:00:33,380 --> 00:00:36,080
want everyone to say a
number between zero and 10

9
00:00:36,080 --> 00:00:41,316
about how much energy you're
feeling about being here today.

10
00:00:41,316 --> 00:00:42,440
So I don't want to project.

11
00:00:42,440 --> 00:00:44,266
I'm feeling a little
bit low energy.

12
00:00:44,266 --> 00:00:45,390
So I don't want to project.

13
00:00:45,390 --> 00:00:46,765
So if you guys
feel good, tell me

14
00:00:46,765 --> 00:00:49,880
you feel good so that I know
that my internal state is not

15
00:00:49,880 --> 00:00:51,380
your state.

16
00:00:51,380 --> 00:00:53,630
It's really important to
gauge these things sometimes.

17
00:00:53,630 --> 00:00:54,270
All right.

18
00:00:54,270 --> 00:00:55,850
So 1, 2, 3.

19
00:00:55,850 --> 00:00:59,420
[INTERPOSING VOICES]
KYLE KEANE: All right.

20
00:00:59,420 --> 00:01:03,690
There was a gradient in this
direction, but all right.

21
00:01:03,690 --> 00:01:04,190
That's good.

22
00:01:04,190 --> 00:01:06,200
Nobody went down to three,
which is about where I am.

23
00:01:06,200 --> 00:01:08,150
Unless there was any threes
out there-- if there are,

24
00:01:08,150 --> 00:01:09,066
come talk to me after.

25
00:01:09,066 --> 00:01:10,170
There's support services.

26
00:01:10,170 --> 00:01:14,760
And we can go together and
get a little bumped up.

27
00:01:14,760 --> 00:01:21,470
But yeah, so again, I want
social interaction today.

28
00:01:21,470 --> 00:01:26,010
So we're going to do something
quicker than yesterday,

29
00:01:26,010 --> 00:01:31,085
but I want people to
introduce yourself and say

30
00:01:31,085 --> 00:01:34,610
a random animal and make that
animal's noise if you can,

31
00:01:34,610 --> 00:01:36,330
or attempt to.

32
00:01:36,330 --> 00:01:44,330
So I'm Kyle and-- it
doesn't even have to--

33
00:01:44,330 --> 00:01:46,982
I'm trying to make it
rhyme or have alliteration.

34
00:01:46,982 --> 00:01:48,190
Doesn't even have to be that.

35
00:01:48,190 --> 00:01:50,930
I'm going to say
Kyle, cat, meow.

36
00:01:50,930 --> 00:01:53,450
That's most-- OK, meow.

37
00:01:53,450 --> 00:01:55,000
That's more like
an actual cat says.

38
00:01:55,000 --> 00:01:57,759
That's not what we
represent cats saying.

39
00:01:57,759 --> 00:02:00,560
Yeah, so try to look
around and figure out

40
00:02:00,560 --> 00:02:04,640
people's names and maybe
one other thing, something

41
00:02:04,640 --> 00:02:10,009
personal about us that we can
get to know some type of thing.

42
00:02:10,009 --> 00:02:10,892
And what's a good--

43
00:02:10,892 --> 00:02:12,600
AUDIENCE: What we want
to eat for dinner.

44
00:02:12,600 --> 00:02:12,990
KYLE KEANE: Great.

45
00:02:12,990 --> 00:02:13,250
All right.

46
00:02:13,250 --> 00:02:15,166
What you're hoping to
have for dinner tonight.

47
00:02:15,166 --> 00:02:16,550
Thank you.

48
00:02:16,550 --> 00:02:20,564
All right, so Kyle,
probably Thai food,

49
00:02:20,564 --> 00:02:21,980
but that's because
it's my default

50
00:02:21,980 --> 00:02:25,250
and I never know what
else to do, so Thai food.

51
00:02:25,250 --> 00:02:25,910
All right.

52
00:02:25,910 --> 00:02:27,759
So with you guys--

53
00:02:27,759 --> 00:02:28,550
AUDIENCE: I'm Mark.

54
00:02:28,550 --> 00:02:30,942
I'm probably going to end up
having pasta tonight, maybe

55
00:02:30,942 --> 00:02:31,680
Soylent.

56
00:02:31,680 --> 00:02:32,180
Who knows?

57
00:02:32,180 --> 00:02:32,680
We'll see.

58
00:02:32,680 --> 00:02:38,642
And [INTERPOSING VOICES]
KYLE KEANE: horrified.

59
00:02:38,642 --> 00:02:40,121
AUDIENCE: You really
drink Soylent?

60
00:02:40,121 --> 00:02:42,353
AUDIENCE: I survived off of
that for weeks at a time.

61
00:02:42,353 --> 00:02:44,312
AUDIENCE: Just like just
Soylent, nothing else?

62
00:02:44,312 --> 00:02:44,764
AUDIENCE: Just Soylent.

63
00:02:44,764 --> 00:02:45,264
Yeah.

64
00:02:45,264 --> 00:02:47,386
KYLE KEANE: Anybody not
know what Soylent is?

65
00:02:47,386 --> 00:02:51,450
[INTERPOSING VOICES]
AUDIENCE: Not Soylent Green.

66
00:02:51,450 --> 00:02:52,700
KYLE KEANE: Not Soylent Green.

67
00:02:52,700 --> 00:02:53,469
No.

68
00:02:53,469 --> 00:02:55,427
AUDIENCE: It's like a
better version of Ensure.

69
00:02:55,427 --> 00:02:56,136
AUDIENCE: Oh, OK.

70
00:02:56,136 --> 00:02:56,886
KYLE KEANE: Right.

71
00:02:56,886 --> 00:02:59,480
But it has all the nutrients
that a human needs to survive.

72
00:02:59,480 --> 00:03:02,560
And you just drink it and you
never have to worry about food.

73
00:03:02,560 --> 00:03:05,720
But-- AUDIENCE: It's
easy and it's cheap.

74
00:03:05,720 --> 00:03:09,180
Those are all the requirements
a student should--

75
00:03:09,180 --> 00:03:11,140
and my favorite animal
is the prairie dog.

76
00:03:11,140 --> 00:03:12,848
I don't really know
what sound they make,

77
00:03:12,848 --> 00:03:14,510
but I think it's
like [CLICKING]....

78
00:03:14,510 --> 00:03:15,560
KYLE KEANE: Aw.

79
00:03:15,560 --> 00:03:18,860
That was nice.

80
00:03:18,860 --> 00:03:20,040
Cool.

81
00:03:20,040 --> 00:03:21,140
All right.

82
00:03:21,140 --> 00:03:22,790
Thank you.

83
00:03:22,790 --> 00:03:25,625
So today, at some point, you're
going to break up into groups.

84
00:03:25,625 --> 00:03:27,500
And at some point, I'm
going to do a pop quiz

85
00:03:27,500 --> 00:03:28,370
and you're going
to have to tell me

86
00:03:28,370 --> 00:03:30,130
the names of the
people in your group,

87
00:03:30,130 --> 00:03:31,766
because I forgot to
do that yesterday

88
00:03:31,766 --> 00:03:33,140
and I wonder how
many of you know

89
00:03:33,140 --> 00:03:35,710
the names of the people
that were in your group.

90
00:03:35,710 --> 00:03:39,200
I'm not going to ask
you about yesterday,

91
00:03:39,200 --> 00:03:41,930
but today I will
ask you about that.

92
00:03:41,930 --> 00:03:44,780
So I mean, some of you
may have known the names.

93
00:03:44,780 --> 00:03:47,230
Thumbs up if you did.