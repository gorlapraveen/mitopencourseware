1
00:00:19,770 --> 00:00:21,930
ANDREW RINGLER: It's like
a file sharing system,

2
00:00:21,930 --> 00:00:25,860
like Dropbox, except you
manually take care of the files

3
00:00:25,860 --> 00:00:28,080
yourself and you worry
about merge conflicts.

4
00:00:28,080 --> 00:00:31,500
So if in Dropbox, you
and your collaborator

5
00:00:31,500 --> 00:00:34,920
added a document at the same
time, it tries to save both

6
00:00:34,920 --> 00:00:36,750
and it goes, aah,
something's wrong.

7
00:00:36,750 --> 00:00:39,920
And it makes one that
says "conflicted copy."

8
00:00:39,920 --> 00:00:45,186
And GitHub does a similar
thing but then it hands it back

9
00:00:45,186 --> 00:00:46,560
to you and it
says, I'm not going

10
00:00:46,560 --> 00:00:48,030
to save that
conflicted copy, you

11
00:00:48,030 --> 00:00:51,240
need to tell me what to do to
resolve that so that I only

12
00:00:51,240 --> 00:00:55,080
have one version of each file
and it's the master version.

13
00:00:55,080 --> 00:00:57,180
So GitHub, in a
nutshell, is just

14
00:00:57,180 --> 00:00:59,190
a way to share files
with other people

15
00:00:59,190 --> 00:01:02,520
collaboratively without
overwriting other people's

16
00:01:02,520 --> 00:01:05,400
stuff and without having
conflicts happen inside

17
00:01:05,400 --> 00:01:08,544
of the cloud where you're
able to share this document.

18
00:01:08,544 --> 00:01:09,960
So what I'm going
to go over today

19
00:01:09,960 --> 00:01:14,789
is GitHub and then briefly how
to add pages and upload them.

20
00:01:18,090 --> 00:01:21,520
So what is GitHub?

21
00:01:21,520 --> 00:01:25,320
Does anyone want to
explain what GitHub is?

22
00:01:25,320 --> 00:01:25,940
Or Git?

23
00:01:34,140 --> 00:01:35,005
The wild guess.

24
00:01:38,870 --> 00:01:41,670
OK.

25
00:01:41,670 --> 00:01:44,690
GitHub is a few things.

26
00:01:44,690 --> 00:01:55,770
GitHub is a web-based platform,
a website for sharing code,

27
00:01:55,770 --> 00:01:57,250
sharing software code--

28
00:01:57,250 --> 00:02:00,030
the source code of software.

29
00:02:00,030 --> 00:02:06,210
So it's created for developers
to collaborate on code.

30
00:02:06,210 --> 00:02:09,570
A lot of code on there
is open source code--

31
00:02:09,570 --> 00:02:13,140
code that the source code is
freely available to look at.

32
00:02:18,130 --> 00:02:19,560
So basically, it's
a website where

33
00:02:19,560 --> 00:02:24,120
you can navigate
other people's code,

34
00:02:24,120 --> 00:02:26,880
you can collaborate
on the same project.

35
00:02:32,192 --> 00:02:35,485
And you can also just look
at other people's projects

36
00:02:35,485 --> 00:02:38,215
so you can submit bug
reports to projects.

37
00:02:44,370 --> 00:02:50,200
So GitHub is the
sort of community.

38
00:02:50,200 --> 00:02:52,950
The other thing it is is
it's a piece of software

39
00:02:52,950 --> 00:02:56,270
based on a technology
called Git,

40
00:02:56,270 --> 00:03:06,120
G-I-T. Git is a piece
of software that

41
00:03:06,120 --> 00:03:13,970
lets you collaborate, basically
lets you share source code.

42
00:03:13,970 --> 00:03:18,920
And GitHub kind of extends
that by making a pretty website

43
00:03:18,920 --> 00:03:21,515
and a few other things.

44
00:03:29,130 --> 00:03:33,145
So Git is a piece of software
called version control.

45
00:03:33,145 --> 00:03:37,820
Has anyone heard
that term before?

46
00:03:37,820 --> 00:03:38,977
Yes?

47
00:03:38,977 --> 00:03:41,310
Does anyone want to explain
that term-- version control?

48
00:03:45,850 --> 00:03:50,860
It is a very fancy way of
saying keeping a history

49
00:03:50,860 --> 00:03:53,760
and storing multiple
versions of things.

50
00:03:53,760 --> 00:03:59,240
So for example,
say, you are working

51
00:03:59,240 --> 00:04:01,130
on some sort of document--

52
00:04:03,800 --> 00:04:06,830
maybe a really important essay.

53
00:04:06,830 --> 00:04:11,840
So this is my really
important essay.

54
00:04:11,840 --> 00:04:16,959
"My essay on bugs by Andrew."

55
00:04:19,600 --> 00:04:24,520
So you're working on this
essay, you save the essay.

56
00:04:31,730 --> 00:04:42,170
Then you make some
changes, you save it again.

57
00:04:42,170 --> 00:04:45,470
And then you're like, oh,
what was the first version

58
00:04:45,470 --> 00:04:47,330
of the essay I did?

59
00:04:47,330 --> 00:04:50,330
It looks so different now.

60
00:04:50,330 --> 00:04:51,880
So one of the
things you'll do is

61
00:04:51,880 --> 00:05:02,080
once you have a
version you like,

62
00:05:02,080 --> 00:05:05,770
maybe you'll take that version
and let's make a copy of it.

63
00:05:11,710 --> 00:05:13,350
Let's call it like v1--

64
00:05:13,350 --> 00:05:13,930
version 1.

65
00:05:17,826 --> 00:05:20,950
Now let's start
working on version 2.

66
00:05:20,950 --> 00:05:24,370
So if we mess something up, we
still have the first version.

67
00:05:34,520 --> 00:05:36,460
And then it's
getting really good.

68
00:05:36,460 --> 00:05:43,580
So then at this point,
we save our version 2.

69
00:05:49,110 --> 00:05:51,750
Let's call this one Final.

70
00:06:01,140 --> 00:06:05,830
So as you work on multiple
versions of the document,

71
00:06:05,830 --> 00:06:10,330
you want to save different
versions in case you mess up.

72
00:06:14,479 --> 00:06:16,395
You might have done that
with an essay before.

73
00:06:19,300 --> 00:06:21,707
You might submit an
essay to professor.

74
00:06:21,707 --> 00:06:23,290
They give you back
a different version

75
00:06:23,290 --> 00:06:25,840
with corrected changes--

76
00:06:25,840 --> 00:06:28,810
an annotated version.

77
00:06:28,810 --> 00:06:32,530
So version control in
software is just a fancy way

78
00:06:32,530 --> 00:06:34,330
of saying the same thing--

79
00:06:34,330 --> 00:06:39,940
it's making multiple
versions of things

80
00:06:39,940 --> 00:06:44,312
that you can go
back to and look at.

81
00:06:44,312 --> 00:06:47,020
The thing with Git, though,
is you don't actually

82
00:06:47,020 --> 00:06:48,550
have to do what I just did.

83
00:06:48,550 --> 00:06:50,320
It does all that
automatically for you.

84
00:07:00,060 --> 00:07:02,010
And that's similar to--

85
00:07:02,010 --> 00:07:07,500
has anyone heard of Time
Machine for your Apple?

86
00:07:07,500 --> 00:07:16,050
Time Machine on OS X is software
product that basically backs up

87
00:07:16,050 --> 00:07:19,830
your computer every hour, and
then if something messes up,

88
00:07:19,830 --> 00:07:22,320
you can restore your computer
to an earlier version.

89
00:07:27,700 --> 00:07:30,520
It has a nice little interface
like with a little time

90
00:07:30,520 --> 00:07:31,570
machine.

91
00:07:31,570 --> 00:07:34,750
You can go back and look
at previous versions

92
00:07:34,750 --> 00:07:35,510
of your computer.

93
00:07:38,960 --> 00:07:40,930
GitHub is really similar.

94
00:07:40,930 --> 00:07:46,670
It's very similar, except
it's tuned for working well

95
00:07:46,670 --> 00:07:49,909
with source code, so it
has a lot of other features

96
00:07:49,909 --> 00:07:54,740
that are useful for
software developers.

97
00:07:54,740 --> 00:07:56,990
So if you're programming,
eventually you

98
00:07:56,990 --> 00:08:00,140
will have to learn
Git at some point.

99
00:08:00,140 --> 00:08:03,710
And GitHub is like
the most popular way

100
00:08:03,710 --> 00:08:07,430
of using Git, that's
why people treat them

101
00:08:07,430 --> 00:08:08,690
as almost the same thing.

102
00:08:12,610 --> 00:08:15,920
So let's take a look.

103
00:08:15,920 --> 00:08:23,012
The way GitHub
organizes things is

104
00:08:23,012 --> 00:08:27,370
it has these things
called repositories.

105
00:08:27,370 --> 00:08:30,770
And repository, you
could think of it

106
00:08:30,770 --> 00:08:35,030
as like a folder or a project.

107
00:08:35,030 --> 00:08:38,909
So every different project you'd
want to different repository.

108
00:08:38,909 --> 00:08:52,960
So for this course website we
have one called mens-et-manus.

109
00:08:56,000 --> 00:09:00,059
So where is that?

110
00:09:00,059 --> 00:09:01,640
Ahh, here it is.

111
00:09:06,480 --> 00:09:10,200
One of the things GitHub does
is it provides this fancy home

112
00:09:10,200 --> 00:09:15,000
page for your projects, so
you can document your projects

113
00:09:15,000 --> 00:09:16,110
on the homepage.

114
00:09:23,680 --> 00:09:25,830
GitHub has a few ways
of interacting with it.

115
00:09:25,830 --> 00:09:31,140
You can look at your files from
the web interface, GitHub.com.

116
00:09:31,140 --> 00:09:34,510
There is also
text-based interfaces

117
00:09:34,510 --> 00:09:38,171
and a visual interface
of GitHub Desktop, which

118
00:09:38,171 --> 00:09:38,920
we'll use shortly.

119
00:09:43,790 --> 00:09:47,100
A few things to note.

120
00:09:47,100 --> 00:09:50,040
So this is the current,
latest version of your files.

121
00:09:50,040 --> 00:09:53,000
This is a view of
the latest version.

122
00:09:56,290 --> 00:09:58,800
Unlike Time Machine, it
doesn't automatically

123
00:09:58,800 --> 00:10:00,510
create a new version every hour.

124
00:10:03,130 --> 00:10:06,940
And if you used
Dropbox, Dropbox creates

125
00:10:06,940 --> 00:10:10,010
a new version of your file
every time you change it.

126
00:10:10,010 --> 00:10:15,370
GitHub does not
do that because it

127
00:10:15,370 --> 00:10:20,330
wants you to explicitly say when
you've made a working change.

128
00:10:20,330 --> 00:10:23,770
Because if you're changing
a piece of software,

129
00:10:23,770 --> 00:10:25,790
you might change
three or four files

130
00:10:25,790 --> 00:10:31,090
until you have a working next
version of your software.

131
00:10:31,090 --> 00:10:32,890
So you basically
have to tell Git

132
00:10:32,890 --> 00:10:37,000
every time you have a
new version that works.

133
00:10:37,000 --> 00:10:38,410
And that's called the commit.

134
00:10:38,410 --> 00:10:41,960
So the commits are
basically the history

135
00:10:41,960 --> 00:10:48,400
of every time you've made a
change and told GitHub about it

136
00:10:48,400 --> 00:10:49,830
AUDIENCE: Excuse me.

137
00:10:49,830 --> 00:10:53,670
I would like to know if
you made an account there

138
00:10:53,670 --> 00:10:58,790
and put the files for the
course, uploaded them there.

139
00:10:58,790 --> 00:10:59,880
ANDREW RINGLER: Yes.

140
00:10:59,880 --> 00:11:00,380
Yes

141
00:11:00,380 --> 00:11:04,682
AUDIENCE: And if we sign, we can
view this if we search for it.

142
00:11:04,682 --> 00:11:06,594
Do we have to have access?

143
00:11:06,594 --> 00:11:08,040
Do you have to give us access?

144
00:11:08,040 --> 00:11:10,440
ANDREW RINGLER: I do, yes.

145
00:11:10,440 --> 00:11:12,390
You'll notice a few things.

146
00:11:12,390 --> 00:11:16,500
There's a little lock
icon and there's also

147
00:11:16,500 --> 00:11:19,908
this text that says "private."

148
00:11:19,908 --> 00:11:22,350
So yes, until I add
you, you will not

149
00:11:22,350 --> 00:11:23,485
be able to see this URL.

150
00:11:27,177 --> 00:11:31,110
AUDIENCE: Are you going to?

151
00:11:31,110 --> 00:11:32,660
ANDREW RINGLER: Twist my arm.

152
00:11:32,660 --> 00:11:33,160
Yes.

153
00:11:33,160 --> 00:11:34,868
AUDIENCE: No, I wonder
if we are supposed

154
00:11:34,868 --> 00:11:37,350
to use anything in there.

155
00:11:37,350 --> 00:11:41,790
ANDREW RINGLER:
Yes, at some point

156
00:11:41,790 --> 00:11:45,370
I will ask you all to
email me your GitHub user

157
00:11:45,370 --> 00:11:46,810
names and [INAUDIBLE].

158
00:11:50,210 --> 00:11:50,710
OK.

159
00:11:53,460 --> 00:11:55,990
So the commits are
basically the history.

160
00:11:55,990 --> 00:11:57,890
It's good to name them well.

161
00:11:57,890 --> 00:12:01,511
So I said, "Link to electronic
books and schedule."

162
00:12:07,310 --> 00:12:09,680
That's when I added this
schedule to the website.

163
00:12:21,450 --> 00:12:23,450
"Added a group
project example"--

164
00:12:23,450 --> 00:12:24,570
that was five hours ago.

165
00:12:28,710 --> 00:12:32,380
So that's when I added this
example of group project.

166
00:12:32,380 --> 00:12:39,590
So I basically made changes
and notated the change.

167
00:12:39,590 --> 00:12:44,300
So you can view the whole
history of stuff on GitHub.

168
00:12:44,300 --> 00:12:48,170
There's also a ton of other
features which we don't use.

169
00:12:48,170 --> 00:12:51,800
You could submit bug
reports on their issues.

170
00:12:51,800 --> 00:12:53,690
You can create a wiki
for your project,

171
00:12:53,690 --> 00:12:56,460
which most people do not do.

172
00:13:04,200 --> 00:13:05,610
All right.

173
00:13:05,610 --> 00:13:09,420
The web interface--
you only really want

174
00:13:09,420 --> 00:13:11,980
to use it as a
view of your files.

175
00:13:11,980 --> 00:13:13,880
You don't actually
upload files there.

176
00:13:16,490 --> 00:13:19,230
There is this Upload button
and Create a New File button,

177
00:13:19,230 --> 00:13:23,610
but you don't want to use that.

178
00:13:23,610 --> 00:13:27,000
What we actually do want
to use is this program

179
00:13:27,000 --> 00:13:28,635
called GitHub Desktop.

180
00:13:32,970 --> 00:13:43,310
GitHub Desktop is
similar to Dropbox.

181
00:13:43,310 --> 00:13:45,220
Has anyone not heard of Dropbox?

182
00:13:48,040 --> 00:13:50,100
Has anyone heard of Dropbox?

183
00:13:50,100 --> 00:13:52,600
AUDIENCE: Yes.

184
00:13:52,600 --> 00:13:57,070
ANDREW RINGLER: Yes, it had
an IPO, it's a big company.

185
00:13:57,070 --> 00:14:04,410
So Dropbox-- yes, here you go.

186
00:14:04,410 --> 00:14:06,170
So Dropbox has a few things.

187
00:14:06,170 --> 00:14:10,950
It has a sleek little program
that runs continuously.

188
00:14:10,950 --> 00:14:21,550
And what it does is it takes
a folder on your hard disk.

189
00:14:21,550 --> 00:14:23,660
And any file you
put in it, Dropbox

190
00:14:23,660 --> 00:14:26,300
will upload it to the cloud.

191
00:14:26,300 --> 00:14:28,290
And if you have Dropbox
on two computers,

192
00:14:28,290 --> 00:14:30,270
it will automatically
sync them so you can

193
00:14:30,270 --> 00:14:31,561
have two folders look the same.

194
00:14:33,970 --> 00:14:42,270
GitHub Desktop is very similar
to that, except it does not

195
00:14:42,270 --> 00:14:45,450
do anything automatically.

196
00:14:45,450 --> 00:14:48,450
If you drop files in a
folder, it will detect them,

197
00:14:48,450 --> 00:14:52,240
but it won't automatically
push them up to the cloud.

198
00:14:52,240 --> 00:14:55,290
You have to do that manually.

199
00:14:55,290 --> 00:14:57,560
The reason is--
what I said before--

200
00:14:57,560 --> 00:15:01,170
is because you need
to really make sure

201
00:15:01,170 --> 00:15:04,780
this is a change that
won't break the software,

202
00:15:04,780 --> 00:15:17,150
so forcing developers
to write out the change,

203
00:15:17,150 --> 00:15:20,410
helps make the software
easier to maintain.

204
00:15:20,410 --> 00:15:25,040
OK, so in a moment--

205
00:15:29,920 --> 00:15:31,860
I'll run through this quickly.

206
00:15:36,880 --> 00:15:39,620
Let me tell you all the
steps that have to happen.

207
00:15:42,500 --> 00:15:50,910
In a moment, on your own,
you will create an account

208
00:15:50,910 --> 00:15:52,000
on GitHub.com.

209
00:15:52,000 --> 00:15:52,870
It's a free account.

210
00:15:52,870 --> 00:15:54,494
You can use any email
address you want.

211
00:15:57,974 --> 00:15:59,515
You'll then download
GitHub Desktop--

212
00:16:02,080 --> 00:16:03,390
that is also free software.

213
00:16:15,340 --> 00:16:20,150
Once you've done that, you
can log into GitHub Desktop

214
00:16:20,150 --> 00:16:27,250
and sync your cloud account with
this piece of software locally.

215
00:16:31,620 --> 00:16:35,140
Once you log in,
you basically then

216
00:16:35,140 --> 00:16:37,660
choose which repositories
you want to track.

217
00:16:40,700 --> 00:16:47,696
And to do that, we hit the
Plus button, we say Clone.

218
00:16:57,090 --> 00:16:59,100
Then we search for
the repository.

219
00:17:02,130 --> 00:17:05,260
We clone it.

220
00:17:05,260 --> 00:17:09,108
We choose a location to save it.

221
00:17:09,108 --> 00:17:10,021
I'll run.

222
00:17:10,021 --> 00:17:11,520
through this really
quickly and then

223
00:17:11,520 --> 00:17:14,810
I'll step back for
everyone to do it.

224
00:17:18,670 --> 00:17:20,510
So what it's doing,
a clone is basically

225
00:17:20,510 --> 00:17:22,930
making a copy of the repository.

226
00:17:22,930 --> 00:17:28,260
So there's a version in my
cloud at GitHub in California,

227
00:17:28,260 --> 00:17:29,476
I guess.

228
00:17:29,476 --> 00:17:32,550
So we're downloading that.

229
00:17:32,550 --> 00:17:36,450
Once we downloaded it, we
could see a very similar view

230
00:17:36,450 --> 00:17:39,200
to what we saw online.

231
00:17:39,200 --> 00:17:41,310
Here "Link to electronic
books and schedule"--

232
00:17:41,310 --> 00:17:43,200
that's same thing we
saw on the web page.

233
00:17:46,690 --> 00:17:50,730
We'll say, "No
uncommitted changes."

234
00:17:50,730 --> 00:17:52,290
Great.

235
00:17:52,290 --> 00:18:04,540
As I mentioned, if you
drop a file in the folder--

236
00:18:04,540 --> 00:18:09,010
GitHub Desktop has created
this folder for me, just

237
00:18:09,010 --> 00:18:09,985
like a normal folder.

238
00:18:12,500 --> 00:18:15,860
And these are all
the files in it.

239
00:18:15,860 --> 00:18:22,510
So if we drag a new file into
it, just like with Dropbox--

240
00:18:22,510 --> 00:18:26,800
let's drag up the final
version of an essay.

241
00:18:26,800 --> 00:18:32,130
So I dragged it into
our repository folder

242
00:18:32,130 --> 00:18:35,850
locally on my hard drive.

243
00:18:35,850 --> 00:18:38,190
If we search back
to GitHub Desktop,

244
00:18:38,190 --> 00:18:39,640
it's detected that change.

245
00:18:44,250 --> 00:18:45,940
Then we want to commit.

246
00:18:45,940 --> 00:18:51,630
So I said, "Added my
final essay version."

247
00:18:51,630 --> 00:18:53,670
You always have to write
out a commit message.

248
00:18:53,670 --> 00:18:57,860
It won't let you commit
without a message.

249
00:18:57,860 --> 00:19:00,500
So I said, "Added
final version."

250
00:19:00,500 --> 00:19:01,390
I'll say Commit.

251
00:19:05,630 --> 00:19:11,360
That actually committed
locally on your computer.

252
00:19:11,360 --> 00:19:13,390
So it creates the whole history.

253
00:19:13,390 --> 00:19:15,040
Then we have to
do another step--

254
00:19:15,040 --> 00:19:20,390
sync-- which syncs my
history on my laptop

255
00:19:20,390 --> 00:19:28,160
with the universal history
on GitHub.com in California.

256
00:19:28,160 --> 00:19:30,352
So once I hit Sync--

257
00:19:30,352 --> 00:19:38,986
if we go back to the
web page and reload it,

258
00:19:38,986 --> 00:19:41,270
we can see the essay is up here.

259
00:19:43,780 --> 00:19:46,980
And we now have 566 commits.

260
00:19:46,980 --> 00:19:47,930
The last one--

261
00:19:47,930 --> 00:19:49,640
"Andrew Ringler
committed a minute ago,"

262
00:19:49,640 --> 00:19:51,279
and my file essay version.

263
00:19:59,860 --> 00:20:06,860
That's basically the idea of the
whole flow of files on GitHub.

264
00:20:09,840 --> 00:20:12,570
Any questions so far?