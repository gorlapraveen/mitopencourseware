1
00:00:04,950 --> 00:00:07,920
[THEME MUSIC]

2
00:00:09,566 --> 00:00:11,690
DIEGO MENDOZA HALLIDAY:
The purpose of this project

3
00:00:11,690 --> 00:00:19,850
was to have the iCub robot
play a tune in the piano,

4
00:00:19,850 --> 00:00:22,200
replicating something
that it heard.

5
00:00:22,200 --> 00:00:24,800
So what I will be doing
is, I will be playing

6
00:00:24,800 --> 00:00:29,210
a melody in the piano here.

7
00:00:29,210 --> 00:00:33,080
And the iCub is supposed
to hear the melody

8
00:00:33,080 --> 00:00:37,040
and through an auditory
system extract information

9
00:00:37,040 --> 00:00:44,270
about the sound regarding the
melody's notes and the timing

10
00:00:44,270 --> 00:00:45,500
of the notes.

11
00:00:45,500 --> 00:00:52,010
And then produce motor
commands, that in his keyboard,

12
00:00:52,010 --> 00:00:54,770
will replicate the
tune that I play.

13
00:00:54,770 --> 00:00:56,660
So here we go.

14
00:00:56,660 --> 00:00:58,760
[PLAYS SEQUENCE OF NOTES]

15
00:01:30,860 --> 00:02:32,360
[ROBOT PLAYS SAME SEQUENCE OF
 NOTES]

16
00:02:32,360 --> 00:02:34,760
Hey, hey!

17
00:02:34,760 --> 00:02:36,610
Happy birthday.