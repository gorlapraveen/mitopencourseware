Program 2: write a program to: 
-	Print a message on the LCD screen when it first starts up 
-	Continuously print out the reading of an analog sensor 
-	If a specific digital switch is hit, the program should 
	--	Print some different message (stop printing the analog sensor) 
	--	Turn on a motor for 5 seconds, off for 2 seconds, and on at half speed for 5 seconds. 
	--	Return to printing out the analog sensor reading 
-	If a different digital switch is hit, the program should print a goodbye message and exit. 