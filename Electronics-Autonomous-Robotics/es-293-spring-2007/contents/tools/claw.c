#define motora 0
#define motorb 1
#define motorc 2
#define motord 3

#define heightmotor 2
#define gripmotor 3

#define controla 0 
#define controlb 1
#define controlc 2 
#define controld 3

#define deadzone 50

#define ircode_cd_nexttrack 180
#define ircode_cd_prevtrack 179 
#define ircode_cd_volumeup 18 
#define ircode_cd_volumedown 19 
#define ircode_cd_stop 184


void main() 
{

  int a, b, c, d;
  int data = 0;
  sony_init(1);

  while(1) {

    printf("ESG robotics IR   (press START)\n");

    while (start_button() == 0) {

      data = ir_data(1);
    
      if (data != 0) {
	printf("IR: %d\n", data);
      }

      if (data == ircode_cd_volumeup) {
	motor(heightmotor,80);
      }
      if (data == ircode_cd_volumedown) {
	motor(heightmotor,-80);
      }
      if (data == ircode_cd_stop) {
	ao();
      }
      if (data == ircode_cd_nexttrack) {
	motor(gripmotor,80);
      }
      if (data == ircode_cd_prevtrack) {
	motor(gripmotor,-80);
      }

    }

    ao();

    printf("ESG robotics pot  (press STOP)\n");

    while (stop_button() == 0) {

      a = analog(controla) - 128;     /* now it's from -128 to 127 */
      b = analog(controlb) - 128;     /* now it's from -128 to 127 */
      c = analog(controlc) - 128;     /* now it's from -128 to 127 */
      d = analog(controld) - 128;     /* now it's from -128 to 127 */


      /* make a zone in the middle of the sensor where the motor's off */

      if ((a <= deadzone) && (a >= -deadzone)) { 
	motor(motora,0); 
      }
      else {
	if (a < 0) { 
	  a = a + 28;
	  motor(motora,a);
	}
	else {
	  a = a - 27;
	  motor(motora,a);
	}
      }
      if ((b <= deadzone) && (b >= -deadzone)) { 
	motor(motorb,0); 
      }
      else {
	if (b < 0) { 
	  b = b + 28;
	  motor(motorb,b);
	}
	else {
	  b = b - 27;
	  motor(motorb,b);
	}
      }
      if ((c <= deadzone) && (c >= -deadzone)) { 
	motor(motorc,0); 
      }
      else {
	if (c < 0) { 
	  c = c + 28;
	  motor(motorc,c);
	}
	else {
	  c = c - 27;
	  motor(motorc,c);
	}
      }
      if ((d <= deadzone) && (d >= -deadzone)) { 
	motor(motord,0); 
      }
      else {
	if (d < 0) { 
	  d = d + 28;
	  motor(motord,d);
	}
	else {
	  d = d - 27;
	  motor(motord,d);
	}
	
      }
    }




    ao();           /* turn all motors off */

  }

}
