void main() {
  start_process(beeper());
  while(1)
    motor(3, (int) (100.0 * sin(seconds())));
}

void beeper() {
  float start = seconds();
  while (seconds() - start < 20.0)
    beep();
}
