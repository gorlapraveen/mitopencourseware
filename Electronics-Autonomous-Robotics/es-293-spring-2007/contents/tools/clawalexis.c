
/* place your #defines and global variable declarations here */

#define clawmotor 3
#define clawsensor 7
#define clawheightsensor 0
#define heightmotor 1
void program()
{

  /* declare your variables here */

  /* your program goes here */ 
}

/* put your functions and subroutines here */
void openclaw()
{
 int a;
  a=digital(clawsensor);
  while (a==0){
    a=digital(clawsensor);  
    motor(clawmotor,40);
  }
  motor(clawmotor,0);
}

void clawheight(int target)
{

  /* 66(up)-107(down) +=up */
int a;
while (a<target){
  a=analog(clawheightsensor);
  motor(heightmotor,-40);
    }
while (a>target){
  a=analog(clawheightsensor);
  motor(heightmotor,40);
}
  motor(heightmotor,0);
}

void clawheight2(int height)

{
int a;
int target;
 /* 66(up)-107(down) +=up */

if (height<0){
  target=105;
}
else{
  if (height>10){
    target=67;
  }
  else{
    if (height>=0||height<=10){
      target=105-+38*height/10;
}
   
  }}

 


while (a<target){
  a=analog(clawheightsensor);
  motor(heightmotor,-40);
    }
while (a>target){
  a=analog(clawheightsensor);
  motor(heightmotor,40);
}
  motor(heightmotor,0);
}
/*********************************************************************/
/********* FROM HERE ON ARE MENU ROUTINES: LEAVE AS IS ***************/
/*********************************************************************/

void main()
{
  while (1==1) {
    ao();
    menu();  
    ao();
    program();
  }

}

char menu_choices[5][32] = {"Any button = run user program","MOTOR: Start=FWD  0  : Stop=RVS","MOTOR: Start=FWD  1  : Stop=RVS","MOTOR: Start=FWD  2  : Stop=RVS","MOTOR: Start=FWD  3  : Stop=RVS"};


int menu_motorstate = 0;					      
int menu_choice = 9;						      
int menu_motorpower = 80;					      

void menu() {

  while (1) {

    while (stop_button() || start_button()) {}
    sleep(0.05);
    while (stop_button() || start_button()) {}
    sleep(0.05);
    while (stop_button() || start_button()) {}
    sleep(0.05);
    while (stop_button() || start_button()) {}
    
    menu_choice = select_string(menu_choices, 5);

    if ((menu_choice == -1) || (menu_choice == 0)) {
      printf("\n\n");
      return;
    }
    else {
      if (menu_choice == 1) {
	motor(0, menu_motorpower);
      }
      if (menu_choice == 2) {
	motor(0, -menu_motorpower);
      }
      if (menu_choice == 3) {
	motor(1, menu_motorpower);
      }
      if (menu_choice == 4) {
	motor(1, -menu_motorpower);
      }
      if (menu_choice == 5) {
	motor(2, menu_motorpower);
      }
      if (menu_choice == 6) {
	motor(2, -menu_motorpower);
      }
      if (menu_choice == 7) {
	motor(3, menu_motorpower);
      }
      if (menu_choice == 8) {
	motor(3, -menu_motorpower);
      }

      while (stop_button() || start_button()) {}
      
      ao();

    }
    
  }

}

int select_string(char choices[][],int n)
{

  int selection,last_selection=-1,coarseness;
  if(n>_array_size(choices))
    n=_array_size(choices);

  coarseness=255/(n-1);

  while(!(stop_button() || start_button()))
    {
      selection=(knob())/coarseness;
      if(selection!=last_selection)
	{
	  printf("%s\n",choices[selection]);
	  sleep(0.1);
	  last_selection=selection;
	}
    }
  if (start_button()) {
    return selection * 2 - 1;
  }
  else {
    return selection * 2;
  }
}



