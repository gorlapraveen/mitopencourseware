void main() {
  int count;
  long last;
  ir_receive_on();
  set_ir_receive_frequency(125);

  while (1) {
    count = 100;
    last = mseconds();
    while (count--) {
      msleep(3L);
      bit_clear(0x0e, 0x80);
      msleep(3L);
      bit_set(0x0e, 0x80);	/* turn motor on */
    }
    printf("Receiving at %d with %d\n", ir_counts(15), mseconds() - last);
  }
}
