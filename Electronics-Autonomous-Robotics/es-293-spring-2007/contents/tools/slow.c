int main() {
  while (1) {
    if (start_button())
      motor(3, knob() / 3);
    else if (!stop_button())
      motor(3, 0);
    
    if (stop_button())
      motor(3, -knob() / 3);
    else if (!start_button())
      motor(3, 0);
  }
}
