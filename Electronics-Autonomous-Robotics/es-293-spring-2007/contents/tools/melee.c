void main() {
  int i;

  while(1) {
    /* Transmit Start Sequence (.26 seconds) */
    transmit_ir(.1);
    sleep(.1);
    transmit_ir(.02);
    sleep(.02);
    transmit_ir(.02);

    /* Transmit Robot A loc (.32 x 2 + .1 = .74 seconds) */
    transmitLoc(teama());
    sleep(.1);
    transmitLoc(teamb());
  }
}

void transmitLoc(int loc) {
  int i;

  for (i = 0; i < 8; i++) {
    if (loc && 0x1)
      transmit_ir(.02);
    else
      sleep(.02);
    
    loc >>= 1;
  }
}  
