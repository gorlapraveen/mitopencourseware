/*  plug a digital switch into port 7 to turn on/off */

#define powerswitch 7

#define leftmotor 0
#define rightmotor 1

float maxspeed = 85.0;   /* tweak according to battery level, etc... *

void main()
{

  printf("Hi, push my button.\n");
  
  while (digital(powerswitch)==0)
    ;                                 /* empty while... waits here
                                         until powerswitch==1 */

  printf("Let's go!\n");

  while (digital(powerswitch)==1)
     ;                                /* waits until switch is released
                                         to start moving so there aren't
                                         any false positives in the
                                         below check of powerswitch */

  while (digital(powerswitch)==0)     /* loop until switch pressed again */
    {
      go_forward((int) maxspeed);     /* call sub go_forward */
    }

  
  off(leftmotor);
  off(rightmotor);
  
  printf("\n\nGoodbye.\n");
  
}


void go_forward(int tspeed)       
{
  motor(leftmotor,tspeed);
  motor(rightmotor,tspeed);
}





