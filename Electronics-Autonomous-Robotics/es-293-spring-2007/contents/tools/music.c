/* Format of string:
   note, length, ... */

float scales[13][7] =
{{0.0, 55.000, 110.000, 220.000, 440.000, 880.000, 1760.000},
 {0.0, 58.270, 116.541, 233.082, 466.164, 932.328, 1864.655},
 {0.0, 61.735, 123.471, 246.942, 493.883, 987.767, 1975.533},
 {0.0, 65.406, 130.813, 261.626, 523.251, 1046.502, 2093.05},
 {0.0, 69.296, 138.591, 277.183, 554.365, 1108.731, 2217.461},
 {0.0, 73.416, 146.832, 293.665, 587.330, 1174.659, 2349.318},
 {0.0, 77.782, 155.563, 311.127, 622.254, 1244.508, 2489.016},
 {0.0, 82.407, 164.814, 329.628, 659.255, 1318.510, 2637.020},
 {0.0, 87.307, 174.614, 349.228, 698.456, 1396.913, 2793.826},
 {0.0, 92.499, 184.997, 369.994, 739.989, 1479.978, 2959.955},
 {0.0, 97.999, 195.998, 391.995, 783.991, 1567.982, 3135.963},
 {0.0, 103.826, 207.652, 415.305, 830.609, 1661.219, 3322.438},
 {0.0, 110.000, 220.000, 440.000, 880.000, 1760.000, 3520.000}};
int scaleloc[7] = {0, 2, 3, 5, 7, 8, 10};

void main() {
  play(20.0, "as");
  printf("Done!\n");
}

int play(float tempo, char music[]) {
  char *note;
  int tone;
  int octave;
  int loc;
  float duration;

  for (note = music; note; note = strchr(note, ' ')) {
    /* find tone */
    loc = 0;
    if (note[0] >= 'a' && note[0] <= 'g') {
      tone = scaleloc[note[0] - 'a'];
      if (note[1] == '#') {
	tone++;
	loc++;
      }
      if (note[1] == 'b') {
	tone--;
	loc++;
      }
      loc++;
    } else
      tone = 0;

    /* find octave */
    if (music[loc] > '0' && music[loc] < '7') {
      octave = music[loc] - '0';
      loc++;
    } else
      octave = 3;
    if (note < 0) {
      octave--;
      note+=13;
    }

    /* find duration */
    duration = 0.0;
    if (music[loc] == 'w')
      duration = 1.0;
    else if (music[loc] == 'h')
      duration = .5;
    else if (music[loc] == 'q')
      duration = .25;
    else if (music[loc] == 'e')
      duration = .125;
    else if (music[loc] == 's')
      duration = .0625;
    else if (music[loc] == 't')
      duration = .03125;
    if (music[loc + 1] == '.') {
      duration *= 1.5;
      loc++;
    }

    tone(scales[note][octave], .5 * duration);
    sleep(.5 * duration);
  }
}
  
char *strchr(char *str, char chr) {
  
