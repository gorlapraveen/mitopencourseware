int main()
{
  printf ("adjust wheels\n");
  while (!(start_button())){
    motor(1,knob()-128);
  }
  off(1);
  motor(2,100); /*goes forwards*/
  while (1){  /*loop!*/
    int k;
    int m;
    printf ("forwards\n");
    if (digital (11)) {  /*the wimpy sensor*/
      int i;              /*variable, just a variable*/
      printf ("sensor one\n");
      for (i=0; i<5; i++){ /*loops that inches robot forwards*/
	motor (2,100);     /*turns motor on*/
	msleep(20l);      /*lets motor inch forwards*/
	if (digital (12)) {  /*if hard motor is pressed*/
	  printf("immoveable object\n");
	  off(1);         /*stops going forwards*/
	  motor(1,-30);  /*turns*/
	  msleep(100l);   /*for a little time*/
	  off(1);        /*stops turning*/
	  motor(2,-100);  /*backs up*/
	  msleep(100l);   /*for a little while*/
	  if (!(digital (7))){/*checks to see if it hit anything*/
	    off(2);    /*and turns off motor if it did.*/
	  }
	  off(2);        /*stops backing up*/
	  motor(1,30);   /*turns back to netrual*/
	  msleep(100l);   /*takes a little while*/
	  off(1);        /*stops turning*/
	  motor(2,100);   /*continues proceeding forwards*/
	  break;
	}
      }
      if (i==5){          /*if heavy sensor wasn't pressed*/
	printf ("moveable object\n");
	off(2);       /*stops going forwards*/
	motor(3,30);  /*opens claws*/
	msleep(400l);  /*for a little while*/
	off(3);       /*stops opening*/
	motor(2,100);  /*goes forwards*/
	msleep(100l);  /*only a bit*/
	off(2);       /*stops going forwards*/
	motor(3,-30); /*closes claws*/
	msleep(400l);  /*takes a little bit*/
	off (3);      /*stops closing them*/
	motor(0,-100);  /*arm lifts up*/
	motor(1,-30);  /*turns*/
	msleep(100l);   /*for a little time*/
	off(1);        /*stops turning*/
	for (i=0; i<5; i++){
	  motor(2,-100);  /*backs up*/
	  msleep(100l);   /*for a little while*/
	  if (!(digital (7))){/*checks to see if it hit anything*/
	    off(2);    /*and turns off motor if it did.*/
	    m = i;    /*returns amount backed up*/
	  }
	}
	off(2);        /*makes sure its off*/
	motor(1,30);   /*turns back to netrual*/
	msleep(100l);   /*takes a little while*/
	off(1);        /*stops turning*/
	motor(2,100);   /*goes forwards*/
	msleep(100l);   /*for a little bit*/
	off(2);        /*stops going forwards*/
	off(0);        /*arm lowers to ground*/
	motor(3,30);  /*opens claws*/
	msleep(400l);  /*for a little while*/
	off(3);       /*stops opening*/
	motor(2,-100);  /*backs up*/
	msleep(100l);   /*for a little while*/
	off(2);        /*stops backing up*/
	motor(3,-30);  /*closes claws*/
	msleep(400l);   /*for a little while*/
	off(3);        /*stops opening*/
	motor(1,-30);  /*turns*/
	msleep(100l);   /*for a little time*/
	off(1);        /*stops turning*/
	for (i=0; i<m; i++){
	  motor(2,30);  /*goes forwards as much as its backed up*/
	}
	motor(2,100);  /*goes forwards*/
	msleep(100l);  /*only a bit*/
	off(2);       /*stops going forwards*/
	motor(1,30);   /*turns back to netrual*/
	msleep(100l);   /*takes a little while*/
	off(1);        /*stops turning*/
	motor(2,100);  /*keeps proceeding forwards*/
      }
    }
  }
}


