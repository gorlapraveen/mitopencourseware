float Pi = 3.14159;

int abs(int val) {
  if (val < 0)
    return val * -1;
  else
    return val;
}

void
main() {
  int locx, locy;
  float velx, vely;
  float magntd;
  float radius;
  long time;
  float phi;

  while (1) {
    /* Choose a point and move to it */
    locx = random(240 - 10) + 5;
    locy = random(235 - 10) + 5;

    while (analog(0) != locx && analog(1) != locy) {
      velx = ((float) (locx - analog(0))) / 230.0;
      vely = ((float) (locy - analog(1))) / 230.0;
      magntd = sqrt(velx * velx + vely * vely);
      motor(0, (int) (velx * 30.0 / magntd));
      motor(1, (int) (vely * 30.0 / magntd));
    }
    alloff();

    /* Choose a radius and move to it*/
    radius = .1 * (float) (random(99) + 1);
    printf("x = %d, y = %d, r = %f\n", locx, locy, radius);
    motor(0, 30);
    while (analog(0) < locx + (int) radius);
    off(0);

    /* Make circle */
    time = mseconds();
    while ((phi = (float) (mseconds() - time) / (100.0 * radius)) < 1.0) {
      motor(0, (int) (25.0 * cos(phi * 2.0 * Pi)));
      motor(1, (int) (25.0 * sin(phi * 2.0 * Pi)));
    }
  }
}
