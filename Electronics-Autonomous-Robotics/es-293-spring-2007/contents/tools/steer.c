void main() {
  motor(2, 100);
  
  while (1) {
    if (start_button())
      fd(3);
    if (stop_button())
      bk(3);
    if (!start_button() && !stop_button())
      off(3);
  }
}
