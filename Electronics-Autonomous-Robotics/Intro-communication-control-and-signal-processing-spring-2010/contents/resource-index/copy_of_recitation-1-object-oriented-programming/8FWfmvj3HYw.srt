1
00:00:05,640 --> 00:00:06,910
PROFESSOR: Hi.

2
00:00:06,910 --> 00:00:08,990
Today I'd like to talk to you
about state machines.

3
00:00:08,990 --> 00:00:11,260
State machines are an incredibly
important concept,

4
00:00:11,260 --> 00:00:14,260
both in 6.01, and in the general
sense when you're

5
00:00:14,260 --> 00:00:16,670
going to be doing things like
control theory or artificial

6
00:00:16,670 --> 00:00:17,920
intelligence.

7
00:00:20,250 --> 00:00:24,360
Any further work you do in terms
of computability theory,

8
00:00:24,360 --> 00:00:27,140
state machines are
really important.

9
00:00:27,140 --> 00:00:30,910
So I'm going to review what
we've done so far and talk

10
00:00:30,910 --> 00:00:33,710
about why we now need state
machines to add additional

11
00:00:33,710 --> 00:00:35,580
complexity to the kinds of
models that we're going to

12
00:00:35,580 --> 00:00:38,370
build, and then talk a little
bit about how state machines

13
00:00:38,370 --> 00:00:41,430
are represented in different
domains, and then also talk

14
00:00:41,430 --> 00:00:42,820
about how we're going
to use state

15
00:00:42,820 --> 00:00:45,270
machines in the 6.01 software.

16
00:00:45,270 --> 00:00:48,570
First of all, let's review what
we've learned so far.

17
00:00:48,570 --> 00:00:51,120
We've talked, so far, about
functional, imperative and

18
00:00:51,120 --> 00:00:54,120
object-oriented programming
paradigms.

19
00:00:54,120 --> 00:00:57,310
In functional programming,
everything is a function.

20
00:00:57,310 --> 00:00:59,180
And in imperative programming,
we're allowed to use

21
00:00:59,180 --> 00:01:01,400
functions, but they're also
allowed to have side effects.

22
00:01:01,400 --> 00:01:03,650
And in object-oriented
programming,

23
00:01:03,650 --> 00:01:04,750
everything is an object.

24
00:01:04,750 --> 00:01:07,930
We can actually use the first
two to implement the last one,

25
00:01:07,930 --> 00:01:13,760
and use the first one plus
an idea of maintaining

26
00:01:13,760 --> 00:01:16,190
assignments to variables,
that sort of thing, to

27
00:01:16,190 --> 00:01:17,390
implement this one.

28
00:01:17,390 --> 00:01:21,070
So you can trace the progression
of different kinds

29
00:01:21,070 --> 00:01:25,000
of computer science language
along this paradigm.

30
00:01:25,000 --> 00:01:28,650
But the one thing that none of
these languages on their own

31
00:01:28,650 --> 00:01:32,290
allows us to do is keep a notion
of internal state.

32
00:01:32,290 --> 00:01:36,670
And what I mean by that is that
if we have a system that

33
00:01:36,670 --> 00:01:40,280
we want to model in terms of
the passage of time or keep

34
00:01:40,280 --> 00:01:42,620
track of the evolution of that
system or keep track of some

35
00:01:42,620 --> 00:01:47,300
of the data that has accumulated
over time in that

36
00:01:47,300 --> 00:01:49,510
system, then we certainly can't
do it with functional

37
00:01:49,510 --> 00:01:50,180
programming.

38
00:01:50,180 --> 00:01:52,660
Functional programming takes
one input, generates one

39
00:01:52,660 --> 00:01:57,840
output, and you could generate
a list of codes that took in

40
00:01:57,840 --> 00:02:06,230
every possible situation and
then generated the logical

41
00:02:06,230 --> 00:02:08,940
output, but that would
be a lot of code.

42
00:02:08,940 --> 00:02:11,270
Same thing for imperative and
object-oriented programming.

43
00:02:11,270 --> 00:02:13,920
They alone do not address
everything that we want to

44
00:02:13,920 --> 00:02:17,110
address, which is the ability to
look at everything that has

45
00:02:17,110 --> 00:02:19,770
happened as a consequence of the
passage of time and all of

46
00:02:19,770 --> 00:02:21,560
the data that we've looked at
as a consequence of the

47
00:02:21,560 --> 00:02:27,430
passage of time, synthesize it
in some way, and then generate

48
00:02:27,430 --> 00:02:31,440
whatever was supposed to come
out of that situation.

49
00:02:31,440 --> 00:02:33,120
That is the notion of
internal state.

50
00:02:49,550 --> 00:02:51,750
State machines have been
around for a while.

51
00:02:51,750 --> 00:02:55,790
You might see them refer to as
discrete finite automata.

52
00:02:55,790 --> 00:02:58,020
There are also such things as
continuous state machines, but

53
00:02:58,020 --> 00:03:00,960
we're not going to talk about
those in this course so much.

54
00:03:00,960 --> 00:03:02,920
So we're only talking about
discrete state machines.

55
00:03:02,920 --> 00:03:05,040
And when you see them referred
to as state machines or

56
00:03:05,040 --> 00:03:08,020
discrete finite automata in
literature, especially

57
00:03:08,020 --> 00:03:11,940
mathematics, you'll be looking
for this five set of things.

58
00:03:11,940 --> 00:03:15,510
One is a set of the possible
states you could be in, given

59
00:03:15,510 --> 00:03:17,100
a particular state machine.

60
00:03:17,100 --> 00:03:19,960
One is the set of inputs you
could possibly encounter while

61
00:03:19,960 --> 00:03:21,110
in that state machine.

62
00:03:21,110 --> 00:03:23,690
One is the set of outputs that
that state machine could

63
00:03:23,690 --> 00:03:26,090
possibly generate.

64
00:03:26,090 --> 00:03:30,580
One is a transition function
that looks at pairs of these

65
00:03:30,580 --> 00:03:34,160
values and tells you, based on
which state you're currently

66
00:03:34,160 --> 00:03:39,180
in, what state you will end up
in as a consequence of the

67
00:03:39,180 --> 00:03:43,190
current input, and then also
specify what the output will

68
00:03:43,190 --> 00:03:46,290
be as a consequence of
that transition.

69
00:03:46,290 --> 00:03:50,540
And finally, it'll tell you
where you start out.

70
00:03:50,540 --> 00:03:52,110
That's a lot to absorb.

71
00:03:52,110 --> 00:03:54,300
I'm going to show you a state
transition diagram, which many

72
00:03:54,300 --> 00:03:58,380
of you have probably seen
before, and map these values

73
00:03:58,380 --> 00:04:03,140
to this state transition diagram
in hopes of making

74
00:04:03,140 --> 00:04:04,390
this a little bit
more concrete.

75
00:04:08,710 --> 00:04:11,170
Hopefully at this point, all
of you who have interacted

76
00:04:11,170 --> 00:04:13,000
with an MBTA turnstile.

77
00:04:13,000 --> 00:04:16,010
This is a thing that opens and
closes, and you stick your

78
00:04:16,010 --> 00:04:18,560
RFID card on it, and then
Richard Stallman yells at you

79
00:04:18,560 --> 00:04:20,670
and possibly hands you more
Charlie tickets or

80
00:04:20,670 --> 00:04:22,350
something like that.

81
00:04:22,350 --> 00:04:24,150
It has four states.

82
00:04:24,150 --> 00:04:26,930
One of them is that this
turnstile could be closed, and

83
00:04:26,930 --> 00:04:29,660
it's waiting for people to
interact with it in some way.

84
00:04:29,660 --> 00:04:34,550
It could be open, and I
anthropomorphize the turnstile

85
00:04:34,550 --> 00:04:36,800
as being happy as a consequence
of you putting

86
00:04:36,800 --> 00:04:38,050
money in it.

87
00:04:39,660 --> 00:04:43,240
It could be open and quiet as a
consequence of usually some

88
00:04:43,240 --> 00:04:44,610
sort of other previous
interaction

89
00:04:44,610 --> 00:04:46,290
with another person.

90
00:04:46,290 --> 00:04:49,070
Or it could be open and angry
as a consequence of people

91
00:04:49,070 --> 00:04:50,440
interacting with it when
they should not.

92
00:04:55,050 --> 00:05:00,355
The vertices in this directed
graph are my set of states.

93
00:05:05,180 --> 00:05:07,720
So when you see something like
this, and you're asked to map

94
00:05:07,720 --> 00:05:11,840
it to the mathematical
construct, just grab the names

95
00:05:11,840 --> 00:05:13,200
and say this is my
set of states.

96
00:05:15,860 --> 00:05:17,180
Let's say I start
off in closed.

97
00:05:17,180 --> 00:05:20,040
And actually, it occurs to me
that the other thing that

98
00:05:20,040 --> 00:05:21,510
should be specified
is a start state.

99
00:05:21,510 --> 00:05:22,600
And it's not here.

100
00:05:22,600 --> 00:05:25,780
So let's say the turnstile
starts off as closed.

101
00:05:25,780 --> 00:05:28,490
Usually this is an arrow coming
out of nowhere that

102
00:05:28,490 --> 00:05:31,300
directs into one
of the states.

103
00:05:31,300 --> 00:05:33,670
Sometimes it will be explicitly
indicated by saying

104
00:05:33,670 --> 00:05:35,770
start state, but typically
you'll just see

105
00:05:35,770 --> 00:05:37,510
an arrow from nowhere.

106
00:05:41,480 --> 00:05:46,125
At this point, I don't have
any inputs or outputs.

107
00:05:50,230 --> 00:05:52,440
If I put money in the turnstile,
it constitutes

108
00:05:52,440 --> 00:05:56,100
input to my system.

109
00:05:56,100 --> 00:06:01,840
It's going to end up in my
set of inputs for math.

110
00:06:01,840 --> 00:06:08,040
My transition function looks at
the state I'm in and looks

111
00:06:08,040 --> 00:06:14,700
at the current input and
generates the output and the

112
00:06:14,700 --> 00:06:16,860
next state.

113
00:06:16,860 --> 00:06:22,200
So all of these arrows, in
addition to whatever

114
00:06:22,200 --> 00:06:26,600
information is contained in
the receiving vertex,

115
00:06:26,600 --> 00:06:28,220
specifies my transition
function.

116
00:06:31,580 --> 00:06:36,010
Any transition that's not
specified is not considered in

117
00:06:36,010 --> 00:06:37,240
the function.

118
00:06:37,240 --> 00:06:40,750
This was not part of our
original drawing.

119
00:06:40,750 --> 00:06:44,090
So if I was fed open angry,
there would be no way to get

120
00:06:44,090 --> 00:06:46,880
to open happy.

121
00:06:46,880 --> 00:06:54,030
Likewise, if I was in open angry
and fed money for this

122
00:06:54,030 --> 00:06:57,030
simple system, we're going to
say nothing would happen.

123
00:07:05,440 --> 00:07:11,035
I think at this point it's
become clear how to transform

124
00:07:11,035 --> 00:07:15,560
a state transition diagram
into the mathematical

125
00:07:15,560 --> 00:07:17,120
constructs.

126
00:07:17,120 --> 00:07:18,830
It's good to have the
mathematical constructs,

127
00:07:18,830 --> 00:07:21,200
because they end up being used
in software, which I'll talk

128
00:07:21,200 --> 00:07:22,470
about in a second.

129
00:07:22,470 --> 00:07:25,220
But the first thing I'm going to
do is just walk around the

130
00:07:25,220 --> 00:07:28,420
state transition diagram and
indicate what would be inputs,

131
00:07:28,420 --> 00:07:31,012
what would be outputs,
that sort of thing.

132
00:07:31,012 --> 00:07:38,300
So let's say I walk up to the
turnstile and somebody else

133
00:07:38,300 --> 00:07:41,740
interacts with the turnstile
by exiting.

134
00:07:41,740 --> 00:07:46,070
In this case, exit is the input,
none is the output, the

135
00:07:46,070 --> 00:07:49,390
turnstile doesn't make
any noise, and the

136
00:07:49,390 --> 00:07:52,340
turnstile is open.

137
00:07:52,340 --> 00:07:58,330
If at that point I interact
with the turnstile by

138
00:07:58,330 --> 00:08:06,600
entering, it's going to make
noise, which is the output.

139
00:08:06,600 --> 00:08:09,130
And then the new state is going
to be that the turnstile

140
00:08:09,130 --> 00:08:10,410
is open and angry.

141
00:08:13,900 --> 00:08:16,730
At that point, you and
I know that the

142
00:08:16,730 --> 00:08:18,710
turnstile is going to close.

143
00:08:18,710 --> 00:08:30,190
So this edge indicates that the
only available input at

144
00:08:30,190 --> 00:08:32,510
that point is to do nothing.

145
00:08:32,510 --> 00:08:36,100
Or independent of anything else,
it's going to squawk

146
00:08:36,100 --> 00:08:37,740
again and close.

147
00:08:42,679 --> 00:08:45,535
One more time, here
are the states.

148
00:08:50,270 --> 00:08:54,660
Inputs are the first
of these two pairs.

149
00:08:54,660 --> 00:08:58,500
Outputs, as a consequence of the
transition, is the second

150
00:08:58,500 --> 00:09:01,270
of these two pairs.

151
00:09:01,270 --> 00:09:04,040
And the transition function is
represented by the directed

152
00:09:04,040 --> 00:09:06,740
edge and the new state.

153
00:09:11,010 --> 00:09:13,030
Once you have all those sets
figured out, you can start

154
00:09:13,030 --> 00:09:17,650
talking about how to implement
state machines and software.

155
00:09:17,650 --> 00:09:20,370
We've actually abstracted
this away from you.

156
00:09:20,370 --> 00:09:22,760
You don't have to
deal with it.

157
00:09:22,760 --> 00:09:24,990
But as a consequence, you should
know how to interact

158
00:09:24,990 --> 00:09:33,970
with the 6.01 library.

159
00:09:33,970 --> 00:09:39,340
Let's look at an example of
the state machine class.

160
00:09:39,340 --> 00:09:44,640
I want to build an accumulator,
which at every

161
00:09:44,640 --> 00:09:52,860
time step we'll look at the
input, add it to every other

162
00:09:52,860 --> 00:09:57,680
example of input and output, and
output the new value and

163
00:09:57,680 --> 00:09:58,930
retain it as the new state.

164
00:10:03,020 --> 00:10:06,840
The first thing I need to do is
initialize the accumulator

165
00:10:06,840 --> 00:10:09,040
with a value.

166
00:10:09,040 --> 00:10:11,920
This is our start state, which
is the same as our start state

167
00:10:11,920 --> 00:10:14,700
from the MBTA turnstile, and
also the same as our start

168
00:10:14,700 --> 00:10:17,290
state from the mathematical
construct.

169
00:10:22,340 --> 00:10:27,010
I also want something called
getNext Values which is the

170
00:10:27,010 --> 00:10:29,750
functional equivalent
of the transitions.

171
00:10:32,380 --> 00:10:34,280
Here's our self again from
object oriented programming --

172
00:10:34,280 --> 00:10:35,530
but we don't care about that.

173
00:10:38,800 --> 00:10:41,280
We're going to look at
the current state

174
00:10:41,280 --> 00:10:42,530
and the current input.

175
00:10:46,060 --> 00:10:50,400
Some getNext Values we'll do
some internal data munging,

176
00:10:50,400 --> 00:10:56,320
possibly multiplication by two
or comparing to the previous

177
00:10:56,320 --> 00:10:57,840
input and then having
some conditionals,

178
00:10:57,840 --> 00:10:58,950
that sort of thing.

179
00:10:58,950 --> 00:11:03,040
But there'll be some sort of
very simple function under

180
00:11:03,040 --> 00:11:07,430
getNext Values at least for
the first couple of weeks.

181
00:11:07,430 --> 00:11:09,930
And then we will return
the new state and

182
00:11:09,930 --> 00:11:12,340
the output into tuple.

183
00:11:12,340 --> 00:11:15,010
In this case, the new state
is going to be the linear

184
00:11:15,010 --> 00:11:18,650
combination of the current state
and the current input.

185
00:11:18,650 --> 00:11:22,290
And the output is going to be
the linear combination of the

186
00:11:22,290 --> 00:11:27,020
current state and the
current input.

187
00:11:27,020 --> 00:11:31,670
If I were to draw this
accumulator as a state

188
00:11:31,670 --> 00:11:33,640
transition diagram,
I would do this.

189
00:11:51,340 --> 00:11:52,710
My start state is the
initial value.

190
00:11:59,950 --> 00:12:03,270
If I pass in a new input--

191
00:12:03,270 --> 00:12:04,520
we'll call it input 0 --

192
00:12:11,580 --> 00:12:47,290
both my output and the new
state are going to be the

193
00:12:47,290 --> 00:12:48,830
linear combination of
these two values.

194
00:12:52,730 --> 00:12:59,870
If I made another transition, I
would take whatever my next

195
00:12:59,870 --> 00:13:06,290
input was and add it to the
current state value and return

196
00:13:06,290 --> 00:13:08,335
it out as the output, and
so on and so forth.

197
00:13:11,410 --> 00:13:16,110
I encourage you try this in
Python, or in IDLE and munge

198
00:13:16,110 --> 00:13:17,990
around with it and see what
you can get it to do.

199
00:13:17,990 --> 00:13:22,280
You might have to type add
lib6.01 in order to get the

200
00:13:22,280 --> 00:13:26,320
state machine class, or
initialize using lib6.01 in

201
00:13:26,320 --> 00:13:28,650
order to get the state
machine class.

202
00:13:28,650 --> 00:13:31,170
But otherwise, those should be
enough to get you started with

203
00:13:31,170 --> 00:13:32,560
an introduction to
state machines.

204
00:13:32,560 --> 00:13:35,850
If you're having trouble, I
highly recommend going through

205
00:13:35,850 --> 00:13:37,370
all the examples in
the readings.

206
00:13:37,370 --> 00:13:39,860
They're pretty comprehensive,
and it also includes the

207
00:13:39,860 --> 00:13:41,520
accumulator.

208
00:13:41,520 --> 00:13:42,490
That's all for now.

209
00:13:42,490 --> 00:13:44,800
Next time we'll talk about
linear time varying systems.