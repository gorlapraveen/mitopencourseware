1
00:00:05,360 --> 00:00:07,720
PROFESSOR: Last time we talked
about op-amps and the fact

2
00:00:07,720 --> 00:00:10,120
that they allowed us to
abstract away certain

3
00:00:10,120 --> 00:00:13,070
components of a circuit and
sample particular voltages

4
00:00:13,070 --> 00:00:15,820
from a circuit and then modify
those voltages as we would

5
00:00:15,820 --> 00:00:18,720
like in an LTI fashion.

6
00:00:18,720 --> 00:00:22,030
Today I'd like to talk to you
about some other interesting

7
00:00:22,030 --> 00:00:24,470
things that fall out of the fact
that we're only dealing

8
00:00:24,470 --> 00:00:25,760
with LTI systems.

9
00:00:25,760 --> 00:00:29,840
In particular, Thevenin/Norton
equivalence and superposition.

10
00:00:32,460 --> 00:00:35,700
Thevenin/Norton equivalence is
the idea that if you have a

11
00:00:35,700 --> 00:00:39,100
very complex circuit, but it's
still a LTI circuit, you can

12
00:00:39,100 --> 00:00:43,540
express it using
a linear curve.

13
00:00:43,540 --> 00:00:46,060
And superposition is the idea
that because you're also

14
00:00:46,060 --> 00:00:48,900
dealing with LTI components,
if you're trying to solve a

15
00:00:48,900 --> 00:00:54,260
circuit, you can take the
individual contributions of

16
00:00:54,260 --> 00:00:58,820
independent sources to that
circuit and sum them in order

17
00:00:58,820 --> 00:01:01,845
to find out either the total
current flowing through a

18
00:01:01,845 --> 00:01:04,170
particular component or the
total voltage drop across a

19
00:01:04,170 --> 00:01:07,220
particular component.

20
00:01:07,220 --> 00:01:08,665
At this point, I'll walk
you through both.

21
00:01:16,270 --> 00:01:18,970
Thevenin/Norton equivalence is
an important concept in that

22
00:01:18,970 --> 00:01:20,880
you may have a very complicated
circuit.

23
00:01:20,880 --> 00:01:22,700
And you don't really want
to talk about the entire

24
00:01:22,700 --> 00:01:23,490
complicated circuit.

25
00:01:23,490 --> 00:01:28,220
You just want to sample the
voltage drop or current in a

26
00:01:28,220 --> 00:01:29,470
very particular location.

27
00:01:32,250 --> 00:01:35,090
Because we're dealing with LTI
systems we can actually

28
00:01:35,090 --> 00:01:40,115
express that particular sample
as its relationship between I

29
00:01:40,115 --> 00:01:43,880
and V and possibly whatever
resistive component is

30
00:01:43,880 --> 00:01:50,170
associated with the voltage
drop across that sample.

31
00:01:50,170 --> 00:01:53,530
We can solve for this curve by
looking at the location that

32
00:01:53,530 --> 00:01:56,800
were sampling and finding
the open circuit voltage

33
00:01:56,800 --> 00:01:58,690
associated with that position.

34
00:01:58,690 --> 00:02:02,170
Or if we were to leave the two
terminals from where we are

35
00:02:02,170 --> 00:02:06,250
sampling open, what is the
voltage drop across that

36
00:02:06,250 --> 00:02:10,419
section of our circuit?

37
00:02:10,419 --> 00:02:13,540
That's the point right here,
where the current flowing

38
00:02:13,540 --> 00:02:16,090
through the system is 0.

39
00:02:16,090 --> 00:02:19,550
And there's a given voltage
associated with it.

40
00:02:19,550 --> 00:02:26,180
Likewise if we want to find the
other intercept, we can

41
00:02:26,180 --> 00:02:29,430
close those two terminals by
running a wire across them and

42
00:02:29,430 --> 00:02:32,900
then look at the current that
flows across that wire.

43
00:02:32,900 --> 00:02:34,260
That's the close circuit
current.

44
00:02:41,950 --> 00:02:44,620
The slope is going to tell us
about our resistance, if any.

45
00:02:44,620 --> 00:02:46,960
If we're only dealing with a
voltage source or a current

46
00:02:46,960 --> 00:02:48,990
source, then we're only
going to be dealing

47
00:02:48,990 --> 00:02:50,780
with a straight line.

48
00:02:54,460 --> 00:02:57,300
Once we've solved for these
values, we tend to express

49
00:02:57,300 --> 00:03:02,410
them either as a Thevenin
equivalent circuit or a Norton

50
00:03:02,410 --> 00:03:03,680
equivalent circuit.

51
00:03:03,680 --> 00:03:07,240
And you can convert
between the two.

52
00:03:07,240 --> 00:03:08,490
Let's walk through an example.

53
00:03:13,620 --> 00:03:14,870
Here I've got a simple
circuit.

54
00:03:17,550 --> 00:03:19,750
And I would like to find the
Thevenin equivalent of that

55
00:03:19,750 --> 00:03:23,750
circuit, given that I'm sampling
from above this

56
00:03:23,750 --> 00:03:26,200
resistor and below
this resistor.

57
00:03:28,790 --> 00:03:30,900
The first thing that I'm
going to do is find the

58
00:03:30,900 --> 00:03:32,730
open circuit voltage.

59
00:03:32,730 --> 00:03:38,330
Or if I were sampling from this
point to this point, what

60
00:03:38,330 --> 00:03:42,658
is the voltage drop across this
section of the circuit?

61
00:03:42,658 --> 00:03:43,990
Well, I've got 36 Volts.

62
00:03:46,510 --> 00:03:48,530
There's 12 Ohms of
resistance in the

63
00:03:48,530 --> 00:03:49,885
section that I'm sampling.

64
00:03:49,885 --> 00:03:52,060
And there's 6 Ohms of resistance
outside the section

65
00:03:52,060 --> 00:03:53,380
that I'm sampling.

66
00:03:53,380 --> 00:03:57,200
So 2/3 of my total voltage
drop across this voltage

67
00:03:57,200 --> 00:04:00,570
divider is going to be accounted
for inside my

68
00:04:00,570 --> 00:04:03,910
Thevenin equivalent circuit.

69
00:04:03,910 --> 00:04:07,200
So my Thevenin equivalent
voltage is 24 Volts.

70
00:04:12,290 --> 00:04:15,410
Now I want to go after the
short circuit current.

71
00:04:15,410 --> 00:04:17,589
If I were to draw a wire from
this terminal to this

72
00:04:17,589 --> 00:04:22,480
terminal, I'm curious what
this value would be.

73
00:04:22,480 --> 00:04:26,230
As a consequence of connecting
these two terminals, these

74
00:04:26,230 --> 00:04:28,700
resistors are going to be
completely bypassed.

75
00:04:28,700 --> 00:04:32,880
We don't actually have to drop
any voltage or put any current

76
00:04:32,880 --> 00:04:35,420
through these resistors because
there's actually an

77
00:04:35,420 --> 00:04:39,500
infinitely easier way for
the current to go.

78
00:04:39,500 --> 00:04:41,310
In this case, we're just going
to follow Ohm's law.

79
00:04:44,800 --> 00:04:49,850
36 divided by 6 is 6 Amperes.

80
00:04:49,850 --> 00:04:52,380
But because of the
directionality on this short

81
00:04:52,380 --> 00:04:56,160
circuit current, current in this
system is going to flow

82
00:04:56,160 --> 00:04:57,580
in this direction.

83
00:04:57,580 --> 00:04:58,960
So are short circuit current is

84
00:04:58,960 --> 00:05:00,970
actually negative 6 Amperes.

85
00:05:10,130 --> 00:05:13,160
That just leaves Rth.

86
00:05:13,160 --> 00:05:15,460
Once again we're going
to rely on Ohm's law.

87
00:05:20,760 --> 00:05:26,160
If we divide our voltage
by our current--

88
00:05:26,160 --> 00:05:27,670
you can't have a negative
resistance.

89
00:05:27,670 --> 00:05:29,860
We're actually going to divide
by the negative of this.

90
00:05:33,070 --> 00:05:34,320
We'll get out 4 Ohms.

91
00:05:41,230 --> 00:05:43,750
Let's do it again, this time
with a slightly different

92
00:05:43,750 --> 00:05:45,810
sample point and
find the Norton

93
00:05:45,810 --> 00:05:47,060
equivalent of our circuit.

94
00:05:51,200 --> 00:05:54,345
So if I'm only sampling across
the 9 Ohm resistor--

95
00:05:57,120 --> 00:05:59,970
I personally still solve for
the Thevenin equivalent

96
00:05:59,970 --> 00:06:01,900
voltage first because I
think it's easiest.

97
00:06:06,350 --> 00:06:09,780
I'll just make note
of it over here.

98
00:06:09,780 --> 00:06:12,670
The voltage drop across this
resistor is determined by the

99
00:06:12,670 --> 00:06:14,060
fact that this is a simple
voltage divider.

100
00:06:17,080 --> 00:06:18,440
The other resistors
in this circuit--

101
00:06:18,440 --> 00:06:20,910
7, 2, 9 Ohms.

102
00:06:25,110 --> 00:06:27,200
So the voltage drop across
this resistor is going to

103
00:06:27,200 --> 00:06:30,100
represent half of the total
voltage in our circuit.

104
00:06:33,040 --> 00:06:34,290
That's 18 Volts.

105
00:06:41,150 --> 00:06:43,190
The short circuit current
associated with connecting

106
00:06:43,190 --> 00:06:50,570
these two terminals is 36 Volts
divided by the sum of

107
00:06:50,570 --> 00:06:52,160
these two resistors.

108
00:06:52,160 --> 00:06:54,300
Once again we're going to end
up bypassing this resistor

109
00:06:54,300 --> 00:06:58,610
entirely because, think about
it this way, if there was a

110
00:06:58,610 --> 00:07:04,850
resistor across here with 0
resistance, all the current

111
00:07:04,850 --> 00:07:08,480
would sink through it, or it
would represent the location

112
00:07:08,480 --> 00:07:09,900
for which infinite current
would flow.

113
00:07:19,840 --> 00:07:24,210
36 divided by 9 is 4.

114
00:07:24,210 --> 00:07:26,520
And because we're following in
the opposite direction we're

115
00:07:26,520 --> 00:07:29,110
going to say this is
negative 4 Amperes.

116
00:07:34,000 --> 00:07:41,560
In this orientation, the Norton
equivalent amperage is

117
00:07:41,560 --> 00:07:43,220
actually the negative
of this value.

118
00:07:43,220 --> 00:07:44,875
You sometimes see the arrow
pointing downwards.

119
00:07:54,340 --> 00:07:57,050
This preserves our property
of short circuit current.

120
00:07:57,050 --> 00:07:59,960
If this was short circuited,
Isc would still

121
00:07:59,960 --> 00:08:03,090
be negative 4 Amperes.

122
00:08:03,090 --> 00:08:04,340
And our Vth--

123
00:08:06,610 --> 00:08:10,155
if our Vth was the voltage drop
across these two points--

124
00:08:16,040 --> 00:08:19,550
ha, I haven't solved
for Rth yet.

125
00:08:22,380 --> 00:08:23,470
There are a few ways
to do this.

126
00:08:23,470 --> 00:08:26,870
But I'm going to
use Vth and In.

127
00:08:48,640 --> 00:08:51,890
18 over 4, we can reduce
it to 9 over 2.

128
00:09:00,980 --> 00:09:03,320
And if I were to solve back
out for the voltage drop

129
00:09:03,320 --> 00:09:07,305
across this resistor just as a
consequence of I Norton, I

130
00:09:07,305 --> 00:09:10,520
would get 18 Volts, which is
also the Thevenin equivalent

131
00:09:10,520 --> 00:09:12,000
voltage for this circuit.

132
00:09:16,320 --> 00:09:19,450
That covers Thevenin/Norton
equivalence.

133
00:09:19,450 --> 00:09:22,050
And we're going to talk about
superposition really quickly.

134
00:09:27,050 --> 00:09:30,350
Superposition is yet another
circuit-solving strategy.

135
00:09:30,350 --> 00:09:32,440
It falls out as a consequence
of only working with LTI

136
00:09:32,440 --> 00:09:33,070
components.

137
00:09:33,070 --> 00:09:37,100
And it means that in order to
solve for a given component

138
00:09:37,100 --> 00:09:40,880
current or component voltage,
you can actually find it by

139
00:09:40,880 --> 00:09:43,790
taking the linear combination
of every contribution as a

140
00:09:43,790 --> 00:09:46,290
consequence of independent
sources.

141
00:09:46,290 --> 00:09:47,800
What do I mean when
I say that?

142
00:09:47,800 --> 00:09:50,050
Well, if you have a circuit
like this--

143
00:09:50,050 --> 00:09:51,410
and you might recognize
it from the

144
00:09:51,410 --> 00:09:52,930
NVCC lecture earlier--

145
00:09:56,670 --> 00:10:01,170
you can actually express it as
a linear combination of just

146
00:10:01,170 --> 00:10:05,150
the voltage source and just
the current source.

147
00:10:05,150 --> 00:10:08,120
Note what when I remove the
current source, I end up with

148
00:10:08,120 --> 00:10:11,650
no connection because if you
have 0 current flowing through

149
00:10:11,650 --> 00:10:14,480
a system, it's the
same as if the

150
00:10:14,480 --> 00:10:17,270
connection has been removed.

151
00:10:17,270 --> 00:10:20,760
Note that when I remove the
voltage source, I no longer

152
00:10:20,760 --> 00:10:23,000
have a voltage drop across
that connection, but I

153
00:10:23,000 --> 00:10:24,250
maintain the connection.

154
00:10:29,180 --> 00:10:32,330
If in this example I was
just looking for I1.

155
00:10:32,330 --> 00:10:35,180
I1 would be the linear
combination of the

156
00:10:35,180 --> 00:10:40,570
contribution of I1 in this
particular circuit plus I1 in

157
00:10:40,570 --> 00:10:43,410
this particular circuit.

158
00:10:43,410 --> 00:10:44,920
So this is the expression
that I'm looking for.

159
00:10:51,150 --> 00:10:55,620
In the first circuit, I'm just
going to use V over IR --

160
00:10:55,620 --> 00:10:56,870
or V equals IR --

161
00:11:00,680 --> 00:11:03,060
and I'm going to have 3 Amperes
in this direction.

162
00:11:18,860 --> 00:11:26,600
In this circuit I'm actually
going to have to

163
00:11:26,600 --> 00:11:27,850
use a current divider.

164
00:11:29,910 --> 00:11:32,150
Identify that I have five parts
total to distribute

165
00:11:32,150 --> 00:11:34,060
among these two branches.

166
00:11:34,060 --> 00:11:37,000
Identify that this side is going
to get the inverse of

167
00:11:37,000 --> 00:11:40,370
this side's contribution to the
total parts, or this side

168
00:11:40,370 --> 00:11:45,180
is going to get the proportion
of parts equivalent to this

169
00:11:45,180 --> 00:11:46,430
side's contribution.

170
00:11:48,850 --> 00:11:50,630
And vice versa.

171
00:11:50,630 --> 00:11:53,800
That this side is the side
that I'm interested in.

172
00:11:53,800 --> 00:11:57,150
And so 2/5 of the total current
flowing through this

173
00:11:57,150 --> 00:11:59,650
current divider is going to
end up in this branch.

174
00:12:03,060 --> 00:12:06,050
2/5 of 10 is 4.

175
00:12:06,050 --> 00:12:10,810
It's in the opposite direction
of this current flow.

176
00:12:10,810 --> 00:12:17,550
So I'm actually going to
contribute negative 4 Amperes

177
00:12:17,550 --> 00:12:18,800
from this subcircuit.

178
00:12:27,790 --> 00:12:31,440
This should match the results
from the NVCC lecture.

179
00:12:31,440 --> 00:12:34,690
That's my super fast coverage
of superposition and also

180
00:12:34,690 --> 00:12:36,550
Thevenin/Norton equivalence.

181
00:12:36,550 --> 00:12:38,370
Next time we're going to start
a whole new module.