1
00:00:06,270 --> 00:00:09,980
KENDRA PUGH: Today I'd like
to talk to about poles.

2
00:00:09,980 --> 00:00:12,540
Last time I ended up talking
to you about LTI

3
00:00:12,540 --> 00:00:14,180
representations and
manipulations.

4
00:00:14,180 --> 00:00:17,570
And, in particular, I want to
emphasize the relationship

5
00:00:17,570 --> 00:00:20,870
between feedforward and feedback
systems, in order to

6
00:00:20,870 --> 00:00:23,790
segway us into the poles..

7
00:00:23,790 --> 00:00:27,160
Using what we know about that
relationship, we can find the

8
00:00:27,160 --> 00:00:28,640
base of a geometric sequence.

9
00:00:28,640 --> 00:00:31,240
And that geometric sequence
actually represents the

10
00:00:31,240 --> 00:00:35,220
long-term response of our
system to a unit sample

11
00:00:35,220 --> 00:00:38,690
response or a delta.

12
00:00:38,690 --> 00:00:42,710
That value is also what
we refer to the poles.

13
00:00:42,710 --> 00:00:45,450
At this point, I'll go over how
to solve for them and very

14
00:00:45,450 --> 00:00:47,080
basic properties of them.

15
00:00:47,080 --> 00:00:49,440
So that we can use the
information that we have about

16
00:00:49,440 --> 00:00:52,980
the poles to try to actually
predict the future or look at

17
00:00:52,980 --> 00:00:54,681
the long-term behavior
of our system.

18
00:00:57,980 --> 00:01:01,200
First a quick review.

19
00:01:01,200 --> 00:01:03,290
Last time we talked about
feedforward systems.

20
00:01:03,290 --> 00:01:06,570
And, in particular, I want to
emphasize the fact that if you

21
00:01:06,570 --> 00:01:08,690
have a transient input to your
feedforward system, you're

22
00:01:08,690 --> 00:01:10,680
going to end up with a
transient response.

23
00:01:10,680 --> 00:01:14,550
There's no method by which a
feedforward system can retain

24
00:01:14,550 --> 00:01:17,440
information over more than the
amount of time steps that you

25
00:01:17,440 --> 00:01:20,170
feed information into it.

26
00:01:20,170 --> 00:01:23,170
Feedback systems, on the other
hand, represent a persistent

27
00:01:23,170 --> 00:01:24,940
response to a transient input.

28
00:01:24,940 --> 00:01:27,480
Because you're working with a
feedback system information

29
00:01:27,480 --> 00:01:29,690
that you put in can be reflected
in more than one

30
00:01:29,690 --> 00:01:32,540
time step and possibly multiple
time steps, depending

31
00:01:32,540 --> 00:01:34,410
upon how many delays
you have working in

32
00:01:34,410 --> 00:01:37,710
your feedback system.

33
00:01:37,710 --> 00:01:41,020
Last time I also drew out
the relationship between

34
00:01:41,020 --> 00:01:43,370
feedforward and feedback
systems.

35
00:01:43,370 --> 00:01:46,200
You can actually turn a feedback
system or talk about

36
00:01:46,200 --> 00:01:49,790
a feedback system in terms of
a feedforward system that

37
00:01:49,790 --> 00:01:55,980
takes infinite samples of the
input and feeds them through a

38
00:01:55,980 --> 00:02:00,570
summation that takes infinite
delays through your system.

39
00:02:04,240 --> 00:02:05,960
You can represent
that translation

40
00:02:05,960 --> 00:02:08,660
using a geometric sequence.

41
00:02:08,660 --> 00:02:11,470
The basis of that geometric
sequence is the object that

42
00:02:11,470 --> 00:02:13,630
we're going to use in order
to predict the future.

43
00:02:13,630 --> 00:02:15,320
And that's what we're
talking about when

44
00:02:15,320 --> 00:02:16,570
we talk about poles.

45
00:02:21,470 --> 00:02:23,550
You can have multiple geometric
sequences involved

46
00:02:23,550 --> 00:02:24,740
in actually determining
the long-term

47
00:02:24,740 --> 00:02:27,780
behaviors of your system.

48
00:02:27,780 --> 00:02:31,780
If you only have one, then
things are pretty simple.

49
00:02:31,780 --> 00:02:33,680
You find your system function.

50
00:02:33,680 --> 00:02:37,770
You find the value associated
with p0 in this expression.

51
00:02:37,770 --> 00:02:44,290
It's OK if there's some sort
of scalar on the outside of

52
00:02:44,290 --> 00:02:45,400
this expression.

53
00:02:45,400 --> 00:02:47,525
We're working with linear time
and variance systems, so that

54
00:02:47,525 --> 00:02:51,730
scalar is going to affect
the initial

55
00:02:51,730 --> 00:02:52,950
response to your system.

56
00:02:52,950 --> 00:02:56,150
But in terms of a long
term behavior, it

57
00:02:56,150 --> 00:02:57,080
doesn't matter as much.

58
00:02:57,080 --> 00:02:59,750
So don't worry about
it right now.

59
00:02:59,750 --> 00:03:04,450
Relatedly, if you're solving
for these expressions in

60
00:03:04,450 --> 00:03:07,880
second or higher order systems,
you're going to end

61
00:03:07,880 --> 00:03:10,450
up having to solve partial
fractions.

62
00:03:10,450 --> 00:03:11,480
You can do this.

63
00:03:11,480 --> 00:03:13,220
And in part, one of the reasons
that you would want to

64
00:03:13,220 --> 00:03:15,800
do this is so that you can get
out those scalars, if you're

65
00:03:15,800 --> 00:03:18,960
going to be talking about the
very short-term response to

66
00:03:18,960 --> 00:03:22,780
something, like a
transient input.

67
00:03:22,780 --> 00:03:24,515
We're not going to be
too interested in

68
00:03:24,515 --> 00:03:25,220
those in this course.

69
00:03:25,220 --> 00:03:26,180
We're mostly going
to be talking

70
00:03:26,180 --> 00:03:27,860
about long-term response.

71
00:03:27,860 --> 00:03:32,450
So we can get around the fact
the we're dealing with a

72
00:03:32,450 --> 00:03:37,390
higher order of systems and not
solving partial fractions

73
00:03:37,390 --> 00:03:42,960
by substituting in for an
expression called z, which

74
00:03:42,960 --> 00:03:46,990
actually represents the inverse
power of R and then

75
00:03:46,990 --> 00:03:50,760
solving for the roots
of that equation.

76
00:03:50,760 --> 00:03:56,600
If you substitute z in for 1
over R in this denominator and

77
00:03:56,600 --> 00:04:00,040
then solve for the root
associated with that

78
00:04:00,040 --> 00:04:03,470
expression, you'll get
the same result.

79
00:04:03,470 --> 00:04:05,290
You'll actually end
up out with p0.

80
00:04:09,080 --> 00:04:09,640
All right.

81
00:04:09,640 --> 00:04:14,530
So now we know how to find the
pole or multiple poles, if

82
00:04:14,530 --> 00:04:16,660
we're interested in
multiple poles.

83
00:04:16,660 --> 00:04:17,310
What do we do now?

84
00:04:17,310 --> 00:04:19,529
I still haven't gone over how
to figure out the long-term

85
00:04:19,529 --> 00:04:20,779
behavior of your system.

86
00:04:24,020 --> 00:04:27,250
The first thing you do is look
at the magnitude of all the

87
00:04:27,250 --> 00:04:31,140
poles that you've solved for and
select the poles with the

88
00:04:31,140 --> 00:04:34,580
largest magnitude.

89
00:04:34,580 --> 00:04:39,000
If there are multiple poles with
the same magnitude, then

90
00:04:39,000 --> 00:04:40,250
you'll end up looking
at all of them.

91
00:04:44,680 --> 00:04:48,870
If you have different properties
than the ones here,

92
00:04:48,870 --> 00:04:52,400
you can end up with some, you
know, complex behavior.

93
00:04:52,400 --> 00:04:53,960
I would not worry about
that too much.

94
00:04:53,960 --> 00:04:57,960
Or I would ask a professor
or TA when that happens.

95
00:04:57,960 --> 00:05:02,890
But in the general sense, if
your dominant pole has a

96
00:05:02,890 --> 00:05:06,790
magnitude greater than 1, then
you're going to see long-term

97
00:05:06,790 --> 00:05:09,130
divergence in your system.

98
00:05:09,130 --> 00:05:10,930
This make sense if you
think about it.

99
00:05:10,930 --> 00:05:17,570
If at every time step your
unit sample response is

100
00:05:17,570 --> 00:05:20,810
multiplied by a value that is
greater than 1, then it's

101
00:05:20,810 --> 00:05:21,730
going to increase.

102
00:05:21,730 --> 00:05:26,890
And, in fact, the extent to
which the magnitude of your

103
00:05:26,890 --> 00:05:29,280
dominant pole is greater than
1 is going to determine your

104
00:05:29,280 --> 00:05:34,150
rate of increase and also
determine how fast your

105
00:05:34,150 --> 00:05:35,400
envelope explodes.

106
00:05:40,040 --> 00:05:42,380
Similarly, if the magnitude of
your dominant pole is less

107
00:05:42,380 --> 00:05:48,070
than 1, then in response to a
unit sample input or a delta,

108
00:05:48,070 --> 00:05:49,320
your system's going
to converge.

109
00:05:52,110 --> 00:05:53,470
This also makes sense
intuitively.

110
00:05:53,470 --> 00:05:56,260
If you are progressively
multiplying the values in your

111
00:05:56,260 --> 00:06:00,670
system by a scalar that is less
than 1, then eventually

112
00:06:00,670 --> 00:06:02,030
you're going to end up
converging to 0.

113
00:06:07,730 --> 00:06:10,630
To cover the only category we
haven't talked about, if your

114
00:06:10,630 --> 00:06:14,580
dominant pole is actually equal
in magnitude to 1, then

115
00:06:14,580 --> 00:06:17,350
you're not going to see
convergence or divergence.

116
00:06:17,350 --> 00:06:21,710
And this is one of the places in
which the magnitude of the

117
00:06:21,710 --> 00:06:23,640
scalar that you end up
multiplying your system by can

118
00:06:23,640 --> 00:06:24,990
become relevant.

119
00:06:24,990 --> 00:06:28,170
We're not going to focus on
this situation too much.

120
00:06:28,170 --> 00:06:30,640
But it's good to know what
actually happens when the

121
00:06:30,640 --> 00:06:32,220
magnitude of your dominant
pole is equal to 1.

122
00:06:35,760 --> 00:06:39,770
The other feature that we're
interested in when we're

123
00:06:39,770 --> 00:06:44,380
looking at the dominant pole of
a system is if we were to

124
00:06:44,380 --> 00:06:48,240
represent the dominant pole in
this form, what the angle

125
00:06:48,240 --> 00:06:54,240
associated with that pole is, if
you were to graph that pole

126
00:06:54,240 --> 00:06:56,710
on the complex plane using
polar coordinates.

127
00:07:00,590 --> 00:07:03,720
If your pole stays on the real
axis, or if your pole does not

128
00:07:03,720 --> 00:07:09,180
have a complex component, then
you'll see one of two things.

129
00:07:09,180 --> 00:07:11,490
The first thing that it's
possible for you to see is

130
00:07:11,490 --> 00:07:14,570
that you'll get absolutely
non-alternating behavior.

131
00:07:14,570 --> 00:07:17,660
Your system response stays on
one side of the x-axis and

132
00:07:17,660 --> 00:07:24,540
either converges, diverges,
or remains constant as a

133
00:07:24,540 --> 00:07:29,220
consequence of input
of the unit sample.

134
00:07:29,220 --> 00:07:30,855
And you won't see any
sort of alternating

135
00:07:30,855 --> 00:07:32,950
or oscillating behavior.

136
00:07:32,950 --> 00:07:34,730
This only happens when
your dominant

137
00:07:34,730 --> 00:07:37,640
pole is real and positive.

138
00:07:37,640 --> 00:07:40,910
If you're dominant pole is real
and negative, this also

139
00:07:40,910 --> 00:07:42,980
means that it's still on
the real axis, but

140
00:07:42,980 --> 00:07:44,350
its value is negative.

141
00:07:44,350 --> 00:07:46,600
So if you're looking at polar
coordinates, it's going to

142
00:07:46,600 --> 00:07:48,800
have an angle pi associated
with it.

143
00:07:48,800 --> 00:07:51,550
This means you get alternating
behavior.

144
00:07:51,550 --> 00:07:53,970
And what I mean when I say
alternating behavior is that

145
00:07:53,970 --> 00:07:58,770
your unit sample response is
going to jump across the

146
00:07:58,770 --> 00:08:02,690
x-axis at every time step.

147
00:08:02,690 --> 00:08:04,790
This is also equivalent to
having a period of 2.

148
00:08:07,610 --> 00:08:10,880
The other situation you can run
into is that this angle is

149
00:08:10,880 --> 00:08:12,530
neither 0 nor pi.

150
00:08:12,530 --> 00:08:14,270
And at that point you're going
to be talking about

151
00:08:14,270 --> 00:08:19,060
oscillatory behavior or a
sinusoidal response that

152
00:08:19,060 --> 00:08:21,270
retains its edges at the
envelope of your function.

153
00:08:24,030 --> 00:08:27,070
In order to find the period, or
in order to find the amount

154
00:08:27,070 --> 00:08:31,020
of time it takes for your unit
sample response to complete

155
00:08:31,020 --> 00:08:34,960
one period, you're going to take
the angle associated with

156
00:08:34,960 --> 00:08:39,070
your dominant pole and
divide 2pi by it.

157
00:08:39,070 --> 00:08:43,049
This is the general equation
for a period.

158
00:08:43,049 --> 00:08:47,880
This covers the basics of what
you want to do once you

159
00:08:47,880 --> 00:08:49,270
already have your poles.

160
00:08:49,270 --> 00:08:52,430
Next time I'm actually going
to solve a pole problem and

161
00:08:52,430 --> 00:08:55,370
show you what the long-term
response looks like and also

162
00:08:55,370 --> 00:08:58,100
talk about some things about
poles that I've pretty much

163
00:08:58,100 --> 00:09:00,300
skimmed over.

164
00:09:00,300 --> 00:09:03,330
And at that point you should be
able to solve and look at

165
00:09:03,330 --> 00:09:04,580
poles for yourself.