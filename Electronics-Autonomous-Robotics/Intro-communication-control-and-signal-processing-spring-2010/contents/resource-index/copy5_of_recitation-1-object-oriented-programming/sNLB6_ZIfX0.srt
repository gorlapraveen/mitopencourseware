1
00:00:06,430 --> 00:00:07,260
PROFESSOR: Hi.

2
00:00:07,260 --> 00:00:10,050
Last time we talked about NVCC
method and how to reduce the

3
00:00:10,050 --> 00:00:11,720
number of equations we had
to deal with to solve a

4
00:00:11,720 --> 00:00:12,920
particular circuit.

5
00:00:12,920 --> 00:00:14,660
At this point, we're pretty
well equipped to solve

6
00:00:14,660 --> 00:00:17,150
circuits in the general sense,
but we really haven't talked

7
00:00:17,150 --> 00:00:19,570
about how to use that
information or possibly use

8
00:00:19,570 --> 00:00:22,210
circuits in a particular way.

9
00:00:22,210 --> 00:00:27,040
Before we jump into both that
and abstraction of circuits,

10
00:00:27,040 --> 00:00:28,820
we need to talk about op-amps.

11
00:00:28,820 --> 00:00:31,270
Op-amps is short for Operational
Amplifier.

12
00:00:31,270 --> 00:00:33,840
And it's a tool that we can
use in order to sample

13
00:00:33,840 --> 00:00:36,630
particular voltages from a
subsection of the circuit

14
00:00:36,630 --> 00:00:38,300
without affecting it.

15
00:00:38,300 --> 00:00:39,870
Another thing we can
use op-amps to do

16
00:00:39,870 --> 00:00:41,440
is modify our signal.

17
00:00:41,440 --> 00:00:44,000
Or if we're going to sample a
voltage from a particular

18
00:00:44,000 --> 00:00:47,440
subsection of the circuit, we
can then do stuff to that

19
00:00:47,440 --> 00:00:51,100
voltage without affecting the
circuit, all within the op-amp

20
00:00:51,100 --> 00:00:53,760
or within the op-amp's special
subset of circuitry.

21
00:00:58,530 --> 00:01:01,130
So first of all what is an
operational amplifier?

22
00:01:01,130 --> 00:01:05,900
Well, an operational amplifier
is a giant web of transistors.

23
00:01:05,900 --> 00:01:10,410
But what an operational
amplifier does is act as a

24
00:01:10,410 --> 00:01:12,310
voltage-dependent
voltage source.

25
00:01:12,310 --> 00:01:15,270
It can effectively sample
voltages from an existing

26
00:01:15,270 --> 00:01:19,040
circuit and then use them to
power some other object, for

27
00:01:19,040 --> 00:01:20,480
instance a light bulb.

28
00:01:20,480 --> 00:01:22,685
If you set up this kind of
circuit, you will not actually

29
00:01:22,685 --> 00:01:24,880
be powering this light bulb
with 5 Volts, because the

30
00:01:24,880 --> 00:01:27,240
light bulb itself acts
as a resistor.

31
00:01:27,240 --> 00:01:30,270
And so the voltage drop across
this part of the circuit is

32
00:01:30,270 --> 00:01:32,780
going to be different
from just 5 Volts.

33
00:01:32,780 --> 00:01:36,660
If you want to enable a voltage
drop of 5 Volts across

34
00:01:36,660 --> 00:01:42,140
this light bulb, then you have
to stick up an op-amp.

35
00:01:42,140 --> 00:01:44,670
You have to use an op-amp to
sample the voltage drop at

36
00:01:44,670 --> 00:01:48,540
this component and put it in
between the light bulb and the

37
00:01:48,540 --> 00:01:51,395
rest of the circuit.

38
00:01:51,395 --> 00:01:54,040
When you see an op-amp on a
schematic diagram, it'll

39
00:01:54,040 --> 00:01:56,880
frequently look like this.

40
00:01:56,880 --> 00:01:59,370
You'll have a positive input
voltage, a negative input

41
00:01:59,370 --> 00:02:02,540
voltage, power rails, which are
actually the thing that

42
00:02:02,540 --> 00:02:05,810
determine the range of
expressivity that the op-amp

43
00:02:05,810 --> 00:02:10,419
has, and an output voltage.

44
00:02:10,419 --> 00:02:12,865
In reality, the relationship
between the output voltage and

45
00:02:12,865 --> 00:02:27,910
then input voltages is something
like this, where K

46
00:02:27,910 --> 00:02:31,120
is a very large number.

47
00:02:31,120 --> 00:02:35,550
The effect that this has is
that Vout is going to be

48
00:02:35,550 --> 00:02:40,330
whatever Vout needs
to be, such that

49
00:02:40,330 --> 00:02:41,610
Vplus is equal to Vminus.

50
00:02:45,060 --> 00:02:47,290
That's the basic rule you want
to use when you're interacting

51
00:02:47,290 --> 00:02:48,700
with op-amps.

52
00:02:48,700 --> 00:02:52,760
So in this case, if we wanted to
power this light bulb with

53
00:02:52,760 --> 00:02:55,750
5 Volts, we would do something
like this.

54
00:03:31,760 --> 00:03:34,600
Excuse the sloppiness of
the second diagram.

55
00:03:34,600 --> 00:03:36,710
We still have our 10 Volt
voltage source.

56
00:03:36,710 --> 00:03:37,960
We still have our
voltage divider.

57
00:03:40,400 --> 00:03:46,510
This point samples 5 Volts
from this sub-circuit --

58
00:03:46,510 --> 00:03:51,470
and isolates this part of the
circuit from the light bulb.

59
00:03:57,150 --> 00:04:03,770
Vout has to be whatever value
is necessary such that this

60
00:04:03,770 --> 00:04:07,630
sample point and this sample
point are equal.

61
00:04:07,630 --> 00:04:11,380
Since this value is 5 Volts,
this value will also be driven

62
00:04:11,380 --> 00:04:15,780
to 5 Volts by the op-amp,
which means that

63
00:04:15,780 --> 00:04:17,959
this value is 5 Volts.

64
00:04:17,959 --> 00:04:20,260
And we've successfully
managed to power a

65
00:04:20,260 --> 00:04:21,510
light bulb with 5 Volts.

66
00:04:23,700 --> 00:04:26,910
The other thing you might be
asked to do is to take an

67
00:04:26,910 --> 00:04:31,580
existing schematic, an existing
circuit diagram and

68
00:04:31,580 --> 00:04:34,400
figure out what the operational
amplifier does to

69
00:04:34,400 --> 00:04:38,170
a given signal or possibly what
Vout is or possibly what

70
00:04:38,170 --> 00:04:41,500
Vout is in terms of
the input signal.

71
00:04:41,500 --> 00:04:45,860
So let's practice using
this diagram.

72
00:04:54,970 --> 00:04:56,220
Here's what we're after.

73
00:04:58,720 --> 00:05:02,340
I'm going to figure out where
Vplus is going to be.

74
00:05:02,340 --> 00:05:03,640
This is another voltage
divider.

75
00:05:20,380 --> 00:05:25,280
I'm now interested in Vminus,
in terms of Vout, which is

76
00:05:25,280 --> 00:05:26,530
another voltage divider.

77
00:05:44,870 --> 00:05:47,280
I can set these two equations
equal to one another

78
00:05:47,280 --> 00:05:48,530
and solve for Vout.

79
00:06:41,160 --> 00:06:44,090
I found a new expression for
Vout in the particular case

80
00:06:44,090 --> 00:06:45,530
where V is 10 Volts.

81
00:06:58,480 --> 00:07:03,410
If my input voltage were
previously unspecified, or if

82
00:07:03,410 --> 00:07:13,650
this voltage source were not
specified or just Vin, then I

83
00:07:13,650 --> 00:07:14,900
would be after this
expression.

84
00:07:20,340 --> 00:07:22,150
Some things I'd like to mention,
while we're talking

85
00:07:22,150 --> 00:07:27,420
about op-amps, all the
operational amplifiers we've

86
00:07:27,420 --> 00:07:32,170
been working with so far deal
with Vout in terms of Vin,

87
00:07:32,170 --> 00:07:35,140
where Vin is driven through the
positive terminal, and the

88
00:07:35,140 --> 00:07:37,560
negative terminal is typically
connected to ground.

89
00:07:37,560 --> 00:07:39,150
You can do the opposite
and end up with

90
00:07:39,150 --> 00:07:40,670
some interesting effects.

91
00:07:40,670 --> 00:07:42,010
But it comes at a cost.

92
00:07:42,010 --> 00:07:46,190
It is entirely possible that you
will end up driving your

93
00:07:46,190 --> 00:07:48,800
op-amp to an unstable
equilibrium.

94
00:07:48,800 --> 00:07:50,505
What you need to look at
is this relationship.

95
00:08:02,500 --> 00:08:06,430
There may be a particular point,
in which case your

96
00:08:06,430 --> 00:08:07,550
system is stable.

97
00:08:07,550 --> 00:08:10,110
But if you get any sort of minor
perturbations, you'll

98
00:08:10,110 --> 00:08:12,650
actually end up with
divergence.

99
00:08:12,650 --> 00:08:14,880
If this is the case, then
you'll probably

100
00:08:14,880 --> 00:08:16,380
burn out your op-amp.

101
00:08:16,380 --> 00:08:18,540
You can do this by hooking
it up in this way.

102
00:08:38,000 --> 00:08:40,580
This is expensive and could
possibly burn you.

103
00:08:43,460 --> 00:08:46,350
The other thing to note is that
the power rails on your

104
00:08:46,350 --> 00:08:48,260
op-amp limit its range
of expressivity.

105
00:08:48,260 --> 00:08:49,890
And I think I've said this
before, but it's worth

106
00:08:49,890 --> 00:08:51,560
mentioning again.

107
00:08:51,560 --> 00:08:54,950
If your op-amp is only powered
by 10 Volts, it cannot amplify

108
00:08:54,950 --> 00:08:58,790
your input signal to a final
value greater than 10 Volts.

109
00:08:58,790 --> 00:09:01,660
Likewise, if your input value
is a negative voltage, and

110
00:09:01,660 --> 00:09:05,890
you're working with a
non-inverting amplifier, if

111
00:09:05,890 --> 00:09:08,350
your ground is truly ground or
if your ground is higher

112
00:09:08,350 --> 00:09:12,880
relative than your input
voltage, you cannot actually

113
00:09:12,880 --> 00:09:16,670
express a negative voltage.

114
00:09:16,670 --> 00:09:18,960
The third thing I'd like to
quickly mention is that there

115
00:09:18,960 --> 00:09:21,420
are some terms associated with
op-amps that you might hear

116
00:09:21,420 --> 00:09:24,582
used by the staff or online,
that sort of thing.

117
00:09:24,582 --> 00:09:27,240
A buffer and a voltage follower
are the same thing.

118
00:09:27,240 --> 00:09:30,680
And that's explicitly when you
want to sample a signal or you

119
00:09:30,680 --> 00:09:32,690
want to sample a particular
voltage, and you don't want to

120
00:09:32,690 --> 00:09:36,780
multiply it or add it to
something or do any kind of

121
00:09:36,780 --> 00:09:39,500
LTI operations that we might be
able to do using op-amps in

122
00:09:39,500 --> 00:09:42,290
this course.

123
00:09:42,290 --> 00:09:43,450
You can work with amplifiers.

124
00:09:43,450 --> 00:09:46,060
And the thing we worked with
earlier was an amplifier for a

125
00:09:46,060 --> 00:09:48,060
value less than 1.

126
00:09:48,060 --> 00:09:51,040
You can also use op-amps
to some signals.

127
00:09:51,040 --> 00:09:54,030
And if you look for a voltage
summer amplifier on the

128
00:09:54,030 --> 00:09:56,830
internet, you should be able
to find some information.

129
00:09:56,830 --> 00:09:58,740
In any case, op-amps are
really powerful.

130
00:09:58,740 --> 00:10:02,070
They allow us to both isolate
a particular section of a

131
00:10:02,070 --> 00:10:05,460
circuit and sample a particular
voltage value from

132
00:10:05,460 --> 00:10:08,130
that circuit without affecting
that circuit, and also allow

133
00:10:08,130 --> 00:10:11,640
us to modify that particular
voltage value before using it

134
00:10:11,640 --> 00:10:16,220
in another part of our
overall circuit.

135
00:10:16,220 --> 00:10:19,550
Therefore, we're enabled to
design more complicated and

136
00:10:19,550 --> 00:10:21,240
powerful things.

137
00:10:21,240 --> 00:10:25,680
Next time, I'll talk about
superposition and Thevenin

138
00:10:25,680 --> 00:10:29,010
Norton equivalence, which will
further enable modularity and

139
00:10:29,010 --> 00:10:30,470
abstraction in our
circuit design.