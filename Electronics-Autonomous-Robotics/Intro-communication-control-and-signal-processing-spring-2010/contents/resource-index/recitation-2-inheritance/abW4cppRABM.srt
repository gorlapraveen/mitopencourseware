1
00:00:05,445 --> 00:00:06,790
KENDRA PUGH: Hi.

2
00:00:06,790 --> 00:00:10,930
I'd like to talk to you today
about inheritance as a

3
00:00:10,930 --> 00:00:14,500
fundamental concept in object
oriented programming, its use

4
00:00:14,500 --> 00:00:19,490
in Python, and also tips and
tricks for using inheritance

5
00:00:19,490 --> 00:00:22,320
in Python and in the
object oriented

6
00:00:22,320 --> 00:00:24,690
programming paradigm in 6.01.

7
00:00:24,690 --> 00:00:27,210
First thing I'm going to do is
give a really quick crash

8
00:00:27,210 --> 00:00:30,920
course on inheritance to catch
you up to speed, and also so

9
00:00:30,920 --> 00:00:34,940
that you're clear on what I mean
when I say something like

10
00:00:34,940 --> 00:00:36,130
parent class.

11
00:00:36,130 --> 00:00:42,240
And also, I'm going to address
the sort of nuances of using

12
00:00:42,240 --> 00:00:46,270
inheritance while programming
in an object

13
00:00:46,270 --> 00:00:48,550
oriented fashion in Python.

14
00:00:48,550 --> 00:00:52,720
A lot of the code in 6.01
uses the object oriented

15
00:00:52,720 --> 00:00:53,430
programming paradigm.

16
00:00:53,430 --> 00:00:56,810
And a lot of the code
in 6.01 will

17
00:00:56,810 --> 00:00:58,680
inherit from parent classes.

18
00:00:58,680 --> 00:01:03,030
So this is part of the
motivation for going through a

19
00:01:03,030 --> 00:01:06,450
quick review, and then also
indicating the most common

20
00:01:06,450 --> 00:01:08,760
slip ups, and also the most
significant things that you

21
00:01:08,760 --> 00:01:11,200
may or may not have seen
from other languages.

22
00:01:11,200 --> 00:01:15,080
All right, first, a crash
course on inheritance.

23
00:01:15,080 --> 00:01:16,330
Let's look at the board.

24
00:01:18,420 --> 00:01:24,040
Inheritance is the idea that you
can arrange objects in a

25
00:01:24,040 --> 00:01:29,595
hierarchical fashion such that
very, very generalized or

26
00:01:29,595 --> 00:01:35,050
basic things that are true of
a whole group of objects can

27
00:01:35,050 --> 00:01:36,900
be specified at a
higher level.

28
00:01:36,900 --> 00:01:40,170
And then, you can work your way
down to progressively more

29
00:01:40,170 --> 00:01:41,420
specific levels.

30
00:01:46,720 --> 00:01:49,550
The most formal encounter you've
probably had with this

31
00:01:49,550 --> 00:01:53,960
thing, that approach, is the
biological taxonomy, right?

32
00:01:53,960 --> 00:01:56,840
Kingdom, phylum, class, order,
family, genus, species.

33
00:01:56,840 --> 00:01:58,990
Every species has all
the properties of

34
00:01:58,990 --> 00:02:00,340
that particular genus.

35
00:02:00,340 --> 00:02:04,960
All the genuses of a particular
family have the

36
00:02:04,960 --> 00:02:07,730
properties of that family,
and so on, and so forth.

37
00:02:10,810 --> 00:02:11,870
That's a very concrete
example.

38
00:02:11,870 --> 00:02:13,180
But I find it a little boring.

39
00:02:13,180 --> 00:02:16,540
So I'm going to talk about
dog breeds instead.

40
00:02:16,540 --> 00:02:19,050
You're probably familiar with
the fact that golden

41
00:02:19,050 --> 00:02:22,850
retrievers are a type of
retrievers and that retrievers

42
00:02:22,850 --> 00:02:25,820
are a particular kind of dog.

43
00:02:29,480 --> 00:02:32,200
You can make generalizations
about goldens based on what

44
00:02:32,200 --> 00:02:33,520
you know about dogs, right?

45
00:02:33,520 --> 00:02:34,350
All dogs bark.

46
00:02:34,350 --> 00:02:38,290
All dogs have four legs if they
aren't injured or have

47
00:02:38,290 --> 00:02:41,190
some sort of congenital defect,
that kind of thing.

48
00:02:41,190 --> 00:02:44,620
And goldens also have
all the properties

49
00:02:44,620 --> 00:02:45,570
of retrievers, right?

50
00:02:45,570 --> 00:02:50,140
They are capable of going and
catching game that you've

51
00:02:50,140 --> 00:02:52,110
either shot, or possibly
chase it down and

52
00:02:52,110 --> 00:02:53,660
bring it back to you.

53
00:02:53,660 --> 00:02:56,370
So they're bred to have very
particular properties.

54
00:02:56,370 --> 00:02:59,040
Goldens are also bred to have
very particular properties.

55
00:02:59,040 --> 00:03:02,600
And those that are very specific
to goldens define the

56
00:03:02,600 --> 00:03:05,330
difference between a golden
versus a retriever in the

57
00:03:05,330 --> 00:03:06,490
general sense.

58
00:03:06,490 --> 00:03:09,430
Likewise, when we want to make
objects that have very

59
00:03:09,430 --> 00:03:13,740
particular properties but also
share general properties with

60
00:03:13,740 --> 00:03:17,240
other objects, we're going to
create a new category of

61
00:03:17,240 --> 00:03:23,740
object and put the specifics in
that very specific category

62
00:03:23,740 --> 00:03:26,210
and then take the things that we
can generalize and put them

63
00:03:26,210 --> 00:03:29,510
in more general categories so
we don't end up rewriting a

64
00:03:29,510 --> 00:03:30,150
lot of code.

65
00:03:30,150 --> 00:03:32,830
Or we end up reusing code, but
not copying and pasting it

66
00:03:32,830 --> 00:03:34,730
everywhere because
that's annoying.

67
00:03:34,730 --> 00:03:40,870
The other major advantage of
using inheritance is that code

68
00:03:40,870 --> 00:03:42,220
is more intuitive.

69
00:03:42,220 --> 00:03:44,500
You can make references to
the same piece of code

70
00:03:44,500 --> 00:03:45,860
all over the place.

71
00:03:45,860 --> 00:03:52,400
But it's not as intuitively
accessible to do that over,

72
00:03:52,400 --> 00:03:53,510
and over, and over
again, right?

73
00:03:53,510 --> 00:03:58,250
It's really convenient to think
of the fact that golden

74
00:03:58,250 --> 00:04:02,100
could be a subclass or subtype
of retriever, and that

75
00:04:02,100 --> 00:04:05,430
retriever could be a subclass
or subtype of dog.

76
00:04:05,430 --> 00:04:10,110
When I talk about this
relationship in terms of

77
00:04:10,110 --> 00:04:11,360
object oriented programming--

78
00:04:16,620 --> 00:04:19,500
when I talk about these
categories in terms of object

79
00:04:19,500 --> 00:04:21,520
oriented programming and when
you're actually looking at

80
00:04:21,520 --> 00:04:28,240
code, goldens are a subclass, or
child class, of retrievers.

81
00:04:28,240 --> 00:04:30,580
And retrievers are
a parent class or

82
00:04:30,580 --> 00:04:33,350
super class of goldens.

83
00:04:33,350 --> 00:04:38,520
Likewise, dogs are a parent
class of retrievers.

84
00:04:38,520 --> 00:04:41,400
So now, I've defined my
terminology and also hopefully

85
00:04:41,400 --> 00:04:44,530
given you a very, very, very
quick review of inheritance.

86
00:04:44,530 --> 00:04:47,330
Now, I'm going to talk about
the specifics in Python.

87
00:04:47,330 --> 00:04:50,890
If I turn over here, I've
written up a very short class

88
00:04:50,890 --> 00:04:53,230
definition for dog, right?

89
00:04:53,230 --> 00:04:56,450
Every dog has the class
attribute, cry.

90
00:04:56,450 --> 00:05:00,100
Every dog has an initialization
method that

91
00:05:00,100 --> 00:05:02,850
gives every dog a very specific
name that is passed

92
00:05:02,850 --> 00:05:05,690
in when you initialize
the dog.

93
00:05:05,690 --> 00:05:10,880
And every dog has access to the
class method, greeting,

94
00:05:10,880 --> 00:05:15,360
which returns a string that
says, "I'm," whatever the name

95
00:05:15,360 --> 00:05:21,410
of the dog is, and also the
specific cry, which in this

96
00:05:21,410 --> 00:05:22,680
case, is actually
the class cry.

97
00:05:25,850 --> 00:05:28,630
If you're unfamiliar with using
the plus in terms of

98
00:05:28,630 --> 00:05:31,450
strings, it's just
a concatenator.

99
00:05:31,450 --> 00:05:34,680
So play around with that in
IDLE if you're confused.

100
00:05:34,680 --> 00:05:38,910
I would recommend copying all
of this into IDLE, and then

101
00:05:38,910 --> 00:05:41,655
playing around with a particular
instantiation of

102
00:05:41,655 --> 00:05:43,930
dogs, in this case, Lassie.

103
00:05:43,930 --> 00:05:49,080
If you look at Lassie.name,
you'll end up going after

104
00:05:49,080 --> 00:05:51,590
self.name, which is specified
when your

105
00:05:51,590 --> 00:05:53,040
initialize the object.

106
00:05:53,040 --> 00:05:55,390
So Lassie's name is Lassie.

107
00:05:55,390 --> 00:05:59,465
Likewise, if you were to type
in Lassie.greeting, open

108
00:05:59,465 --> 00:06:03,590
paren, close paren, and hit
Enter, you should get a string

109
00:06:03,590 --> 00:06:09,670
return that says, "I'm Lassie,"
comma, "bark." Mostly

110
00:06:09,670 --> 00:06:12,640
this is to familiarize you with
object oriented in Python

111
00:06:12,640 --> 00:06:13,860
in the general sense.

112
00:06:13,860 --> 00:06:15,730
Now, we're going to look at what
happens when you want to

113
00:06:15,730 --> 00:06:16,980
set up a subclass.

114
00:06:21,460 --> 00:06:25,270
If I set up class Retriever and
I want to inherit from the

115
00:06:25,270 --> 00:06:28,250
super-class, Dog, I'm going
to pass in Dog.

116
00:06:30,840 --> 00:06:33,430
This is in the same syntax that
I would use if it were a

117
00:06:33,430 --> 00:06:35,750
function and I wanted to
pass in a parameter.

118
00:06:35,750 --> 00:06:38,650
If I wanted to inherit from
multiple things or multiple

119
00:06:38,650 --> 00:06:41,700
classes, I would put multiple
classes here.

120
00:06:41,700 --> 00:06:45,840
Right now, we're just going
to inherit from Dog.

121
00:06:45,840 --> 00:06:48,660
Note that I have no code here.

122
00:06:48,660 --> 00:06:50,800
This is pretty much meant to
explicitly specify the fact

123
00:06:50,800 --> 00:06:53,020
that Retriever is not actually
going to introduce any new

124
00:06:53,020 --> 00:06:56,210
properties to dogs.

125
00:06:56,210 --> 00:06:58,260
Their types are going
to be different.

126
00:06:58,260 --> 00:07:01,330
So if I create something that's
a Retriever, it will be

127
00:07:01,330 --> 00:07:04,220
of object type Retriever, versus
if I create something

128
00:07:04,220 --> 00:07:06,800
and say, "Dog," open paren,
close paren, it's going to be

129
00:07:06,800 --> 00:07:07,640
of type Dog.

130
00:07:07,640 --> 00:07:10,240
But what happens when I
create a Retriever--

131
00:07:10,240 --> 00:07:13,420
and as an aside, if you know who
Benji is, I know he's not

132
00:07:13,420 --> 00:07:14,110
a retriever.

133
00:07:14,110 --> 00:07:16,390
But bear with me here.

134
00:07:16,390 --> 00:07:19,780
If I create a Retriever, it's
first going to look for an

135
00:07:19,780 --> 00:07:23,990
initialization in any other
methods or attributes in the

136
00:07:23,990 --> 00:07:29,340
Retriever class definition, run
any code that's here, and

137
00:07:29,340 --> 00:07:36,090
then go to the parent class,
and run all the code here.

138
00:07:40,150 --> 00:07:43,990
So even though Retriever did
not have any explicit code

139
00:07:43,990 --> 00:07:47,020
underneath it, I can still
interact with the object,

140
00:07:47,020 --> 00:07:49,060
Benji, the same way
that I interacted

141
00:07:49,060 --> 00:07:50,100
with the object Lassie.

142
00:07:50,100 --> 00:07:52,040
It has all the same methods and
all the same attributes.

143
00:07:57,230 --> 00:07:58,210
Phew.

144
00:07:58,210 --> 00:07:59,710
So there's basic inheritance.

145
00:07:59,710 --> 00:08:05,280
And I will make another aside
that if you're doing this, you

146
00:08:05,280 --> 00:08:08,000
probably don't need to create a
subclass in the first place.

147
00:08:12,620 --> 00:08:15,000
If you're designing your own
code, and you're trying to

148
00:08:15,000 --> 00:08:18,040
think about what the best way to
organize things is, if you

149
00:08:18,040 --> 00:08:22,470
have to create a subtype or a
subclass and there are no new

150
00:08:22,470 --> 00:08:24,970
methods or attributes or no
different ways of addressing

151
00:08:24,970 --> 00:08:29,940
those methods or attributes,
then this category is probably

152
00:08:29,940 --> 00:08:32,240
actually just this category.

153
00:08:34,940 --> 00:08:36,760
You may want to make a
difference so that you can do

154
00:08:36,760 --> 00:08:38,159
interesting things with
type checking.

155
00:08:38,159 --> 00:08:40,320
I think that's the only thing
I can think of that would

156
00:08:40,320 --> 00:08:40,510
justify it.

157
00:08:40,510 --> 00:08:42,549
And I might be wrong.

158
00:08:42,549 --> 00:08:44,030
Python gurus out there
should correct me.

159
00:08:44,030 --> 00:08:50,300
But a thing to keep in mind.

160
00:08:50,300 --> 00:08:52,280
So we've done the first half
of our inheritance.

161
00:08:52,280 --> 00:08:54,385
We're going to inherit one more
time and create a class

162
00:08:54,385 --> 00:08:55,635
of golden retrievers.

163
00:08:58,340 --> 00:09:02,110
Once again, I've got my class
definition and my indication

164
00:09:02,110 --> 00:09:03,480
that I'm going to inherit
from Retriever.

165
00:09:07,270 --> 00:09:10,710
I don't have any initialization
or attribute

166
00:09:10,710 --> 00:09:11,610
assignments.

167
00:09:11,610 --> 00:09:15,300
I only have a definition
for greeting.

168
00:09:15,300 --> 00:09:17,260
So what happens here?

169
00:09:17,260 --> 00:09:21,430
Well, the first thing we always
do is look for an

170
00:09:21,430 --> 00:09:24,300
initialization method.

171
00:09:24,300 --> 00:09:26,220
Golden doesn't have one, so
it's going to check the

172
00:09:26,220 --> 00:09:27,470
Retriever class.

173
00:09:30,690 --> 00:09:32,640
Retriever doesn't have
one, so it's going to

174
00:09:32,640 --> 00:09:33,890
check the Dog class.

175
00:09:37,480 --> 00:09:39,090
The initialization
method is here.

176
00:09:39,090 --> 00:09:41,700
So when it runs the
initialization method, it's

177
00:09:41,700 --> 00:09:42,950
going to run this code.

178
00:09:50,640 --> 00:09:54,880
The first thing that's going to
happen is any code, or any

179
00:09:54,880 --> 00:09:59,840
attribute assignments, or method
definitions here are

180
00:09:59,840 --> 00:10:03,690
going to be considered the
canon, or the first thing that

181
00:10:03,690 --> 00:10:06,030
any Golden is going
to reference.

182
00:10:06,030 --> 00:10:10,380
So greeting is going to be
executed before greeting used

183
00:10:10,380 --> 00:10:11,630
in any other place.

184
00:10:14,222 --> 00:10:16,100
You notice the only difference
between this greeting and the

185
00:10:16,100 --> 00:10:19,510
Dog greeting is that
"OHAI!" has been

186
00:10:19,510 --> 00:10:21,560
prepended to the phrase.

187
00:10:21,560 --> 00:10:24,960
And the way that we end up doing
that is we refer to-- we

188
00:10:24,960 --> 00:10:28,230
concatenate, and then refer
to the superclass.

189
00:10:28,230 --> 00:10:31,450
And once again, we have to pass
in the explicit argument,

190
00:10:31,450 --> 00:10:34,390
self, when we're talking about
a class definition.

191
00:10:34,390 --> 00:10:38,785
Later, when you actually
instantiate an object and use

192
00:10:38,785 --> 00:10:40,850
your parens, you're not
going to have to

193
00:10:40,850 --> 00:10:42,060
put self as an argument.

194
00:10:42,060 --> 00:10:43,960
It'll get confused.

195
00:10:43,960 --> 00:10:45,210
We'll go over that
in a second.

196
00:10:47,670 --> 00:10:50,385
So let's say I create a golden
retriever, Sidney.

197
00:10:53,240 --> 00:10:54,930
I'm going to pass in one
argument, which is the name.

198
00:10:58,350 --> 00:11:01,500
We're going to consider all the
definitions here first,

199
00:11:01,500 --> 00:11:03,220
which means that goldens are
going to have a method for

200
00:11:03,220 --> 00:11:05,670
greeting that is
specified here.

201
00:11:05,670 --> 00:11:10,900
It's going to use the method for
greeting from Retriever.

202
00:11:10,900 --> 00:11:12,260
And we could put in anything
here, right?

203
00:11:12,260 --> 00:11:13,310
We could put Dog.greeting.

204
00:11:13,310 --> 00:11:18,110
We could put in some other
function that is in the same

205
00:11:18,110 --> 00:11:20,800
environment as class Golden.

206
00:11:20,800 --> 00:11:25,640
But here, we can explicitly
access the superclass that we

207
00:11:25,640 --> 00:11:26,890
defined here.

208
00:11:30,450 --> 00:11:33,360
We're going to head over to
Retriever to see if there any

209
00:11:33,360 --> 00:11:36,300
additional methods or attributes
that are a

210
00:11:36,300 --> 00:11:39,330
consequence of being a subclass
of Retriever that we

211
00:11:39,330 --> 00:11:42,110
need to add to our definition.

212
00:11:42,110 --> 00:11:43,530
Now, we just hit the pass.

213
00:11:43,530 --> 00:11:47,000
On the other hand, Retriever
inherits from Dog.

214
00:11:47,000 --> 00:11:55,410
So once again, we have to jump
over to a super-class and grab

215
00:11:55,410 --> 00:11:57,880
any attributes or methods that
are defined there as well.

216
00:12:01,170 --> 00:12:02,730
So all the way back
over to Sidney.

217
00:12:05,530 --> 00:12:10,670
When I call Sidney.greeting(),
the first thing that happens

218
00:12:10,670 --> 00:12:14,200
is that I look in the most
specific subclass, or whatever

219
00:12:14,200 --> 00:12:17,350
my object type is and
see if there's a

220
00:12:17,350 --> 00:12:18,900
definition for the method.

221
00:12:18,900 --> 00:12:23,110
Because there is, I'm not going
to use Dog.greeting().

222
00:12:23,110 --> 00:12:26,560
I'm going to use
Golden.greeting().

223
00:12:26,560 --> 00:12:32,200
Golden.greeting() says return
a string that says, "OHAI!"

224
00:12:32,200 --> 00:12:36,595
And also append it to whatever
Retriever.greeting() returns.

225
00:12:39,850 --> 00:12:42,830
I go over to Retriever.

226
00:12:42,830 --> 00:12:46,266
It's not here, but I still
have a reference to Dog.

227
00:12:46,266 --> 00:12:47,516
I go over to Dog.

228
00:12:49,760 --> 00:12:52,140
It has a method for greeting.

229
00:12:52,140 --> 00:12:54,240
And it says, "I'm Sidney.

230
00:12:54,240 --> 00:12:57,970
Bark." So the final return
type should be, "OHAI.

231
00:12:57,970 --> 00:12:58,530
I'm Sidney.

232
00:12:58,530 --> 00:13:00,250
Bark."

233
00:13:00,250 --> 00:13:06,470
This concludes my basic overview
of inheritance of

234
00:13:06,470 --> 00:13:09,530
object-oriented programming
in Python for 6.01.

235
00:13:09,530 --> 00:13:12,770
Next time, I'll review some
interesting features in Python

236
00:13:12,770 --> 00:13:18,160
that actually originated in
earlier languages and also

237
00:13:18,160 --> 00:13:20,110
particularly things in aliasing
that people that are

238
00:13:20,110 --> 00:13:22,760
new to Python or people that
are new to programming find

239
00:13:22,760 --> 00:13:24,010
especially confusing.