function ps7_1_2(delta) %Verify that the suboptimal controller of part b
                        %achieves closed loop norm less than g(1,1)+delta.

s=tf('s')          %Define plant
H=1/(s+1);

gbar=sqrt(4-2*sqrt(3)) + delta;   %Define paramters for suboptimal controller
P=gbar^2*(1+sqrt(2-1/gbar^2));
Q=1+sqrt(2-1/gbar^2);
L=sqrt(P-1/Q);
Af=(1-P/gbar^2+P*(1-P/L^2));
Bf=-gbar^2/L;
Cf=-L*(1-P/L^2);
Df=0;

C=tf(ss(Af,Bf,Cf,Df));            %Define controller transfer function

Twz=[C/(1-C*H) C*H/(1-C*H);C*H/(1-C*H) H/(1-C*H)];  %Compute transfer matrix
                                                    %from w to z
gmatlab=norm(Twz,inf);                               %Compute the norm

sprintf('The norm of the resulting closed-loop system is %d.',gmatlab)
sprintf('The corresponding upper bound on the closed-loop norm is %d.',gbar)

