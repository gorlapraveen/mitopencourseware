function ps4_3(a,r)
% function ps4_3(a,r)
%
% solution for Problem 4.3, 6.245/Spring 2004

s=tf('s');                       % a convenient shortcut 

if nargin<1, a=0.01; end
if nargin<2, r=0.00056; end

P0=1/((1+s^2)*(1+4*s^2)*(1+16*s^2));
P1=(1-s)/(1+s);
P2=sqrt(2*a)/(s+a);
d=0.0001;
assignin('base','P0',P0); 
assignin('base','P1',P1); 
assignin('base','P2',P2); 
assignin('base','r',r); 
assignin('base','s',s); 
assignin('base','d',d); 
load_system('ps4_3a');           % load the design model into workspace
[ap,bp,cp,dp]=linmod('ps4_3a');  % extract the LTI model
close_system('ps4_3a');          % close the design model
p=pck(ap,bp,cp,dp);              % re-write plant model in Mu-Tools format
[k,g]=h2syn(p,2,1,2,0);          % design the controller
[ak,bk,ck,dk]=unpck(k);          % get a state space model of the controller
[ag,bg,cg,dg]=unpck(g);          % ss closed loop model
no2co=ss(ag,bg(:,1:2),cg(2,:),dg(2,1:2));
no2te=ss(ag,bg(:,1:2),cg(1,:),dg(1,1:2));
nu=norm(no2co);  % noise-to-control norm
nz=norm(no2te);  % noise-to-tracking error norm
disp(['Mean square control value: ' num2str(nu/r)])
disp(['Mean square tracking error value: ' num2str(nz)])
bode(ss(ag,bg(:,2),cg(1,:),dg(1,2)))