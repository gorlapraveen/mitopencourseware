function H=ps3_2a(B)
% function H=ps3_2(B)
% 
% design for Problem 3.2 of 6.245/Spring 2004

s=tf('s');

[p,q]=butter(10,B,'s');
W=tf(p,q);
P=[s*W 0 -1;W 0.1 0];
P=minreal(ss(P));
[A,B,C,D]=ssdata(P);
plt=pck(A,B,C,D);

[k]=h2syn(plt,1,1);
[a,b,c,d]=unpck(k);
H=tf(ss(a,b,c,d));