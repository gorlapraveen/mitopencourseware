function ps1_1(d)
% function ps1_1(d)
%
% function for 6.245/Spring 2004 PS Problem 1.1

if nargin<1, d=0.001; end        % the default value of d
s=tf('s');                       % a convenient shortcut 
assignin('base','s',s);          % export s to the workspace
assignin('base','d',d);          % export d to the workspace
load_system('ps1_1a');           % load the design model into workspace
[a,b,c,d]=linmod('ps1_1a');     % extract the LTI model
close_system('ps1_1a');          % close the design model
p=pck(a,b,c,d);                  % re-write plant model in Mu-Tools format
[k,g]=h2syn(p,2,1,2,0);              % design the controller
[ak,bk,ck,dk]=unpck(k);          % get a state space model of the controller
K=ss(ak,bk,ck,dk);               % define controller as a standard LTI object
assignin('base','K',K);          % export controller into workspace
load_system('ps1_1b');           % open the testing model into workspace
[ac,bc,cc,dc]=linmod('ps1_1b'); % extract the testing model coefficients
close_system('ps1_1b');          % close the testing model
G=ss(ac,bc,cc,dc);               % calculate the closed loop H2 norm
disp(['True H2 norm:     ' num2str(norm(G))]);   % the actual H2 norm
disp(['Promised H2 norm: ' num2str(h2norm(g))])  % H2 norm promised by h2syn
