function [Ak,Bk,Ck,Dk]=kit()
     % For fixed minimum-phase D1(s), D2(s), find the optimal K which minimizes
     % the H-infinity norm

load_system('ps10b_1')
[A,B,C,D]=linmodv5('ps10b_1');
P=pck(A,B,C,D);
[k,g]=hinfsyn(P,2,1,0,10000,1e-3);
[Ak,Bk,Ck,Dk]=unpck(k);
