[Ak,Bk,Ck,Dk]=kit;        %Find the optimal H-infinity controller for fixed 
                          %D1(s), D2(s).

load_system('ps10b_2')    %This is a SIMULINK model which computes the 
                          %closed-loop dynamics of a modified block diagram
                          %that can be used to find a new D1(s), D2(s) for a 
			  %given K(s).

assignin('base','Ak',Ak);   %give the the SIMULINK model the controller        
assignin('base','Bk',Bk);   %parameters!
assignin('base','Ck',Ck);
assignin('base','Dk',Dk);

[A,B,C,D]=linmodv5('ps10b_2');   %closed-loop state-space description
m=size(A);
n=m(1);

C1=C(1,:);                %Define Ci, Di for each output yi, i=1...8
C2=C(2,:);
C3=C(3,:);
C4=C(4,:);
C5=C(5,:);
C6=C(6,:);
C7=C(7,:);
C8=C(8,:);   

D1=D(1,:);
D2=D(2,:);
D3=D(3,:);
D4=D(4,:);
D5=D(5,:);
D6=D(6,:);
D7=D(7,:);
D8=D(8,:);

abst_init_lmi
a1=symmetric;    %a1,b1 are related to coeffs of numerator of D1(s), a2,b2 are
a2=symmetric;    %related to coeffs of numerator of D2(s)
b1=symmetric;
b2=symmetric;
gamma=symmetric;
P=symmetric(n);
a1>0;
a2>0;
b1>0;
b2>0;
I=eye(2);

J=%%%%%%%%%%%FILL IN HERE%%%%%%%%%%%%%%%%%%
K=%%%%%%%%%%%FILL IN HERE%%%%%%%%%%%%%%%%%%
L=%%%%%%%%%%%FILL IN HERE%%%%%%%%%%%%%%%%%%

[J K;K' L]>0;
lmi_mincx_tbx(%%%%%%%%%%%%%FILL IN HERE%%%%%%%%%%%%%%%%%)

a1opt=value(a1)
a2opt=value(a2)
b1opt=value(b1)
b2opt=value(b2)
gammaopt=value(gamma)


%Now, using the values of a1opt,b1opt,a2opt,b2opt, compute the new D1(s), 
%D2(s).   Plug these into the SIMULINK model ps10b_3.mdl to check the Hinfinity
%norm.
