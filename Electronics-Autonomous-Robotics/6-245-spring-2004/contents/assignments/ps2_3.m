function [Fh2,Fhi,Eh2,Ehi]=ps2_3(b)
% function ps2_3(b)
%
% solution for Problem 2.3 in 6.245/Spring 2004

if nargin<1, b=1; end
assignin('base','b',b);
s=tf('s');
assignin('base','s',s);
load_system('ps2_3a');
[a,b,c,d]=linmod('ps2_3a');
close_system('ps2_3a');
[ar,br,cr,dr]=ssdata(minreal(ss(a,b,c,d)));
p=pck(ar,br,cr,dr);
nmeas=1; 
ncon=1; 
ricmethd=2;
quiet=0;
[kh2,gh2]=h2syn(p,nmeas,ncon,ricmethd,quiet);
[ah2,bh2,ch2,dh2]=unpck(kh2);
[ag,bg,cg,dg]=unpck(gh2);
Kh2=ss(ah2,bh2,ch2,dh2);
G=(s+1)/(s^2+s+1);
disp('H2 controller:')
Fh2=tf(minreal(Kh2*(G-1)+G))
Eh2=tf(minreal(ss(ag,bg,cg,dg)));
gmin=0; 
gmax=norm(Eh2,Inf);
tol=0.01;
epr=1e-10;
epp=1e-6;
[khinf,ghinf]=hinfsyn(p,nmeas,ncon,gmin,gmax,tol,ricmethd,epr,epp,quiet);
[ahi,bhi,chi,dhi]=unpck(khinf);
[ag,bg,cg,dg]=unpck(ghinf);
Ehi=tf(minreal(ss(ag,bg,cg,dg)));
Khi=ss(ahi,bhi,chi,dhi);
disp('H-Infinity controller:')
Fhi=tf(minreal(Khi*(G-1)+G))

