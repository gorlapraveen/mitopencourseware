function p7_1_1(a,e) %Compare the analytical bound on the Hinf norm with that
                     %produced by MATLAB.
if (e<=0)
     disp('epsilon must be positive!')
     return
end

A=-a;          %Define canonical matrices
B1=[0 1/e];
B2=1;
C1=[0;1];
D11=[0 0;0 0];
D12=[e;0];
C2=1;
D21=[1 0];
D22=0;

B=[B1 B2];
C=[C1;C2]
D=[D11 D12;D21 D22];

P=pck(A,B,C,D);
[k,g,gfin]=hinfsyn(P,1,1,0,10*abs(a)*e+10,1e-8);    %Compute minimal Hinf
                                                    %norm in MATLAB

if (a<=0)
     ganalytic=sqrt(2*((a*e)^2+1)+2*sqrt(((a*e)^2+1)^2-1));
     else
     ganalytic=sqrt(2*((a*e)^2+1)-2*sqrt(((a*e)^2+1)^2-1));
end

sprintf('Hinfsyn says that the maximal lower bound for gamma is %d.',gfin)
sprintf('The analytical maximal lower bound on gamma is %d',ganalytic)
