function ps1_2(a,b)
% function ps1_2(a,b)
%
% Solves 6.245/Spring 2004  

s=tf('s');                         % the "Laplace transform" s as a system
if nargin<1, a=1; end              % default a
if nargin<2, b=0.2*a; end          % default b
c=20*b;                            % extra parameter
assignin('base','a',a);            % export variables
assignin('base','b',b);
assignin('base','s',s);
P0=(s-a)/(s+1);                    % plant and shaping filter
H=10*((s/c)^2+sqrt(2)*(s/c)+1)/((s/b)^2+sqrt(2)*(s/b)+1);
assignin('base','P0',P0);          % export variables
assignin('base','H',H);
load_system('ps1_2a');             % generate state space plant model
[a,b,c,d]=linmod('ps1_2a');
close_system('ps1_2a');
d22=d(2,2);                        % remember D22
d(2,2)=0;                          % zero out D22 in the hinfsyn input
p=pck(a,b,c,d);                    % H-Infinity optimization
[k,g,gfin]=hinfsyn(p,1,1,0,1,0.1,2,1e-10,1e-6,0);
if ~isempty(k),                    % if H-Inf norm not exceeding 1 is possible
  [ak,bk,ck,dk]=unpck(k);          % get the controller
  K=ss(ak,bk,ck,dk);
  assignin('base','K',K);          % export controller and D22
  assignin('base','d22',d22);
  load_system('ps1_2b');           % generate closed loop model
  [ac,bc,cc,dc]=linmod('ps1_2b');
  close_system('ps1_2b');
  disp(['gmin:' num2str(gfin)])
  bode(ss(ac,bc,cc,dc))            % check the Bode plot visually
else                               
  disp('Infeasible specifications')
end
  