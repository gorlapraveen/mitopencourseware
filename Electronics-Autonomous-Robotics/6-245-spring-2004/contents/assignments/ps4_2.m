function ps4_2(a2,a1,a0)
% function ps4_2(a2,a1,a0)
%
% testing analytical formula for H2 norm of 1/(s^3+a2*s^2+a1*s+a0)

if nargin<1, a2=1; end
if nargin<2, a1=1; end
if nargin<3, a0=a1*a2/2; end

s=tf('s');
disp(['Numerical:  ' num2str(norm(1/(s^3+a2*s^2+a1*s+a0)))])
disp(['Analytical: ' num2str(sqrt(a2/(2*a0*(a1*a2-a0))))])

