function ps7_3_1(n);    %numerically compute the infinity norm of G-Ga
     
out=[];
w=logspace(-4,4,200);   %look over the frequency range 10^-4 to 10^4
for m=1:length(w)
M=0;    

     for k=0:n-1
        M=M+1/(2+k/n)/(j*w(m)+1+k/n);     %frequency response of Riemann sum.
     end;

out=[out abs(1/(j*w(m)-1)*log(2*(j*w(m)+2)/3/(j*w(m)+1))+1/n*M)];
end
semilogx(w,out);
grid
title('|G(jw)-G_a(jw)|')   %plot the frequency response to check the infinity
       xlabel('w')          %norm
