function ps9_3(d)
% function ps9_3(d)
% 
% code for checking correctness of the solution of 6.245 Problem 9.3

n=10;                      % number of states
N=5000;                    % number of samples
if nargin<1, d=1.1; end    % default argument
D=randn;                            % generate a random system
C=randn(1,n);
B=randn(n,1);
A=randn(n);
A=A-(0.1+max(real(eig(A))))*eye(n);   % adjust to make stable
w=linspace(0,50,N);            % calculate min|G|
g=squeeze(freqresp(ss(A,B,C,D),w));
M=min(abs(g));  
M=min(M,abs(D));
D=D*(d/M);                        % adjust min|G|                         
C=C*(d/M);
if abs(D)<=1,
    disp('D condition not satisfied')
else
    disp('D condition satisfied')
    di=1/(D'*D-1);
    H=[A-B*di*D'*C   B*di*B';C'*(1-D*di*D')*C  -(A-B*di*D'*C)' ];
    minabsre=min(abs(real(eig(H))));
    disp(['minimal absolute value of Re is  ' num2str(minabsre)])
end
close(gcf)
plot(w,abs(g)*(d/M)); grid