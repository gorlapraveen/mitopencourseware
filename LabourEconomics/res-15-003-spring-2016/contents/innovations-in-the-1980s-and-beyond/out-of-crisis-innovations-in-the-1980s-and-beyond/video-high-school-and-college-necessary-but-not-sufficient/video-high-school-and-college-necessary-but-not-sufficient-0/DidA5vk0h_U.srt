1
00:00:10,380 --> 00:00:12,100
Today I want to talk
about the importance

2
00:00:12,100 --> 00:00:15,150
of the basic education
system, and the theme

3
00:00:15,150 --> 00:00:18,630
is going to be that it's a
necessary, but not sufficient

4
00:00:18,630 --> 00:00:21,450
strategy, for education
for the future.

5
00:00:21,450 --> 00:00:25,240
We're all familiar with
the basic education model.

6
00:00:25,240 --> 00:00:28,730
We start in kindergarten
and go through grade school

7
00:00:28,730 --> 00:00:29,870
through high school.

8
00:00:29,870 --> 00:00:32,560
Hopefully go on to a two-
or a four-year college

9
00:00:32,560 --> 00:00:34,240
and maybe even to
graduate school.

10
00:00:34,240 --> 00:00:37,930
Then we enter the labor force
and we start our working career

11
00:00:37,930 --> 00:00:40,570
after we complete our education.

12
00:00:40,570 --> 00:00:43,330
Well that model no longer works.

13
00:00:43,330 --> 00:00:46,230
Today, we have to think
about lifelong learning

14
00:00:46,230 --> 00:00:49,250
because technologies
are changing,

15
00:00:49,250 --> 00:00:52,440
the job market is changing
in unpredictable ways,

16
00:00:52,440 --> 00:00:54,220
and new ways of
working are going

17
00:00:54,220 --> 00:00:55,890
to continue to come along.

18
00:00:55,890 --> 00:00:59,220
They're going to require
us to refresh our skills.

19
00:00:59,220 --> 00:01:01,860
As this graph shows,
the evidence is clear.

20
00:01:01,860 --> 00:01:04,440
High school dropouts
are really doomed

21
00:01:04,440 --> 00:01:07,470
to a lifetime of poverty wages.

22
00:01:07,470 --> 00:01:11,220
On average they earn
about $24,000 a year.

23
00:01:11,220 --> 00:01:15,850
High school graduates get
to about $34,000 a year,

24
00:01:15,850 --> 00:01:19,210
and college graduates
get to about $55,000

25
00:01:19,210 --> 00:01:20,880
a year average income.

26
00:01:20,880 --> 00:01:24,940
So the key lesson of this
graph is, by all means,

27
00:01:24,940 --> 00:01:27,180
every one must
finish high school

28
00:01:27,180 --> 00:01:29,750
and hopefully go beyond that.

29
00:01:29,750 --> 00:01:34,080
But you could say the same thing
for high school graduation.

30
00:01:34,080 --> 00:01:36,030
College still pays off.

31
00:01:36,030 --> 00:01:39,640
This graph shows the
difference in median earnings

32
00:01:39,640 --> 00:01:42,120
of high school and
college graduates,

33
00:01:42,120 --> 00:01:44,690
and you'll see that it's
been growing over time.

34
00:01:44,690 --> 00:01:47,310
That's both for
men and for women.

35
00:01:47,310 --> 00:01:51,250
The gap between high school
and the college degree earnings

36
00:01:51,250 --> 00:01:55,340
has grown, and so there's
increasing returns to college

37
00:01:55,340 --> 00:01:58,430
even given the increased
cost of college.

38
00:01:58,430 --> 00:02:02,470
This graph shows that adjusting
for the increased cost,

39
00:02:02,470 --> 00:02:06,400
that gap is still growing
and the increase in returns

40
00:02:06,400 --> 00:02:09,280
to college, particularly
since around 1980,

41
00:02:09,280 --> 00:02:12,900
have been growing by
very, very large numbers.

42
00:02:12,900 --> 00:02:17,650
And so the message here is
that, yes, college is expensive,

43
00:02:17,650 --> 00:02:21,980
but we've got to be careful
not to underestimate its value.

44
00:02:21,980 --> 00:02:24,970
At the same time, it's
important to recognize

45
00:02:24,970 --> 00:02:28,300
that there's wide variation
in the earnings of college

46
00:02:28,300 --> 00:02:29,040
graduates.

47
00:02:29,040 --> 00:02:32,730
So it's very important to
be careful to choose when,

48
00:02:32,730 --> 00:02:35,260
where, and what kind
of organization,

49
00:02:35,260 --> 00:02:37,620
what kind of
institution you choose

50
00:02:37,620 --> 00:02:39,970
to get your education at.

51
00:02:39,970 --> 00:02:43,080
Ending your education and
then entering the workforce

52
00:02:43,080 --> 00:02:45,030
was a model that
worked in the past,

53
00:02:45,030 --> 00:02:47,140
but it's not going to
work in the future.

54
00:02:47,140 --> 00:02:49,640
So let's look more
carefully at how

55
00:02:49,640 --> 00:02:53,550
we're going to move through
a model of lifelong learning

56
00:02:53,550 --> 00:02:55,350
in the future.