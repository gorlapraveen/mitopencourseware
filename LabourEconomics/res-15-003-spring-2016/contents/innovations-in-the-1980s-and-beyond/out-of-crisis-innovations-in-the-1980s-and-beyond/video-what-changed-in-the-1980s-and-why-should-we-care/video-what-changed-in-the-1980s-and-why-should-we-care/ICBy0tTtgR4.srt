1
00:00:10,290 --> 00:00:12,980
The 1980s turned out
to be a pivotal decade

2
00:00:12,980 --> 00:00:14,900
in the history of work.

3
00:00:14,900 --> 00:00:18,290
So let's take a look at what
happened during that decade

4
00:00:18,290 --> 00:00:21,490
and why we should care
about these developments

5
00:00:21,490 --> 00:00:26,680
because it will affect the
future of work going forward.

6
00:00:26,680 --> 00:00:29,140
The 1970s are going
to go down in history

7
00:00:29,140 --> 00:00:32,280
as a decade of status quo.

8
00:00:32,280 --> 00:00:36,960
The pressures were building, but
managers and workers and unions

9
00:00:36,960 --> 00:00:43,770
and the government behaved as if
the 1960s and 1950s were still

10
00:00:43,770 --> 00:00:44,270
in vogue.

11
00:00:44,270 --> 00:00:45,440
Unions were declining.

12
00:00:45,440 --> 00:00:49,280
New industries were coming along
with new ways of introducing

13
00:00:49,280 --> 00:00:51,130
flexible job designs.

14
00:00:51,130 --> 00:00:55,230
The economy was experiencing
this high rate of inflation

15
00:00:55,230 --> 00:00:57,230
with no growth in wages.

16
00:00:57,230 --> 00:00:59,380
Labor law was getting outdated.

17
00:00:59,380 --> 00:01:01,670
But Congress couldn't
do anything about it.

18
00:01:01,670 --> 00:01:06,370
So the pressures were building
like this proverbial frog that

19
00:01:06,370 --> 00:01:08,630
was in warm water,
stayed in the water

20
00:01:08,630 --> 00:01:10,810
too long until it
started to boil.

21
00:01:10,810 --> 00:01:14,180
Never jumped out because
the challenges were only

22
00:01:14,180 --> 00:01:18,630
being felt incrementally
until it was too late.

23
00:01:18,630 --> 00:01:20,560
So what happened in the 1980s?

24
00:01:20,560 --> 00:01:23,220
The President of
the United States.

25
00:01:23,220 --> 00:01:26,850
Well, Ronald Reagan
got elected president,

26
00:01:26,850 --> 00:01:30,140
a change from a Democrat to
a conservative Republican

27
00:01:30,140 --> 00:01:33,700
with very strong ideas about
how to run the economy.

28
00:01:33,700 --> 00:01:35,590
The second thing
that happened is

29
00:01:35,590 --> 00:01:38,950
it coincided with the
great recession of 1981

30
00:01:38,950 --> 00:01:41,830
to 1983, where the
effort was to try

31
00:01:41,830 --> 00:01:45,560
to break the back of inflation
by the Federal Reserve Board.

32
00:01:45,560 --> 00:01:48,500
The third thing that
came along in coincidence

33
00:01:48,500 --> 00:01:50,590
with these pressures
was the growth

34
00:01:50,590 --> 00:01:53,300
of Japanese and other
global competition.

35
00:01:53,300 --> 00:01:58,200
Now companies in Japan could
build autos and other products

36
00:01:58,200 --> 00:02:02,030
as productively at lower
cost as the United States

37
00:02:02,030 --> 00:02:03,820
and build them in
high qualities so

38
00:02:03,820 --> 00:02:07,180
that they were strong
competitors to this country.

39
00:02:07,180 --> 00:02:09,190
So what did we see happen?

40
00:02:09,190 --> 00:02:12,800
Well, somewhere in
this time period,

41
00:02:12,800 --> 00:02:18,170
that great social contract from
the 1940s through the 1960s

42
00:02:18,170 --> 00:02:19,200
broke down.

43
00:02:19,200 --> 00:02:22,950
This nice coincidence of
wages and productivity

44
00:02:22,950 --> 00:02:25,200
moving together during
this time period,

45
00:02:25,200 --> 00:02:27,940
you can see it began
to fray in the 70s.

46
00:02:27,940 --> 00:02:32,660
And then after 1980, they
depart, never to return again.

47
00:02:32,660 --> 00:02:34,530
So how did this happen?

48
00:02:34,530 --> 00:02:37,620
Well, again, it starts
with Ronald Reagan.

49
00:02:37,620 --> 00:02:41,580
In the August of 1981, the
air traffic controllers

50
00:02:41,580 --> 00:02:44,190
went on strike illegally.

51
00:02:44,190 --> 00:02:46,770
And Ronald Reagan said
this is an illegal strike.

52
00:02:46,770 --> 00:02:48,510
I'm going to take
decisive action.

53
00:02:48,510 --> 00:02:50,850
I'm going to fire them if
they don't come back to work.

54
00:02:50,850 --> 00:02:52,770
He did so.

55
00:02:52,770 --> 00:02:55,720
He broke that strike
and it fundamentally

56
00:02:55,720 --> 00:02:59,360
changed the tenor
of labor relations.

57
00:02:59,360 --> 00:03:02,680
Companies in the private sector
became much more aggressive,

58
00:03:02,680 --> 00:03:06,360
building on the model
of what Reagan did.

59
00:03:06,360 --> 00:03:08,640
They demanded wage
concessions to try

60
00:03:08,640 --> 00:03:10,140
to become more competitive.

61
00:03:10,140 --> 00:03:12,710
They put in what were
called two-tier contracts.

62
00:03:12,710 --> 00:03:16,080
So new workers were starting
that work at a lower wage

63
00:03:16,080 --> 00:03:18,410
rate than more senior workers.

64
00:03:18,410 --> 00:03:21,190
That, in fact, continues
today in many cases.

65
00:03:21,190 --> 00:03:23,320
Auto workers would
understand this

66
00:03:23,320 --> 00:03:27,010
because new hires
come in about 30% less

67
00:03:27,010 --> 00:03:30,780
than workers who are doing the
same work alongside of them.

68
00:03:30,780 --> 00:03:32,600
And so this persisted.

69
00:03:32,600 --> 00:03:36,340
Basically, what happened is
pattern bargaining, that engine

70
00:03:36,340 --> 00:03:39,060
that helped to spread wage
increases across the economy,

71
00:03:39,060 --> 00:03:40,130
broke down.

72
00:03:40,130 --> 00:03:42,840
The bargaining power that
workers got from strikes

73
00:03:42,840 --> 00:03:44,160
was no longer there.

74
00:03:44,160 --> 00:03:46,070
Strikes became
defensive weapons.

75
00:03:46,070 --> 00:03:50,470
And companies like Greyhound
Bus Lines or Eastern Airlines

76
00:03:50,470 --> 00:03:54,510
where strikes occurred, the
companies either won the strike

77
00:03:54,510 --> 00:03:56,990
or in Eastern's case, the
company went out of business.

78
00:03:56,990 --> 00:03:58,930
And all the jobs were lost.

79
00:03:58,930 --> 00:04:01,000
And so this has continued.

80
00:04:01,000 --> 00:04:03,190
Workers lost their
source of power.

81
00:04:03,190 --> 00:04:06,960
They don't have a new source of
bargaining power in place yet.

82
00:04:06,960 --> 00:04:11,020
And so this social contract
continues to be broken.

83
00:04:11,020 --> 00:04:12,780
The challenge that
we face today,

84
00:04:12,780 --> 00:04:16,029
the challenge as next
generation workers you face,

85
00:04:16,029 --> 00:04:19,570
is to figure out what is a
social contract that might work

86
00:04:19,570 --> 00:04:23,560
in the modern economy for
you as a modern workforce

87
00:04:23,560 --> 00:04:25,360
and for society?

88
00:04:25,360 --> 00:04:27,220
We have not figured
that out yet.

89
00:04:27,220 --> 00:04:29,880
That's going to
be your challenge.