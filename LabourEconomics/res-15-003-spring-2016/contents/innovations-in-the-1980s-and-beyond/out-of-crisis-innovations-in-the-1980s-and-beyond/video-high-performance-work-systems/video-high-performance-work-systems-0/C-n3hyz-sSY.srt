1
00:00:10,220 --> 00:00:13,740
Saturn was one of the
most radical innovations

2
00:00:13,740 --> 00:00:16,980
in work organization of
the last several decades,

3
00:00:16,980 --> 00:00:18,540
but it was not alone.

4
00:00:18,540 --> 00:00:21,710
So let's take a look at
a number of companies

5
00:00:21,710 --> 00:00:23,500
that are proof of
concept that you

6
00:00:23,500 --> 00:00:26,480
can compete with what
we call high road

7
00:00:26,480 --> 00:00:29,510
strategies, with good
jobs that provide

8
00:00:29,510 --> 00:00:32,890
good returns to shareholders,
by using what we call

9
00:00:32,890 --> 00:00:35,850
human capital, that is,
essentially workers, skills,

10
00:00:35,850 --> 00:00:38,990
and their social networks
to drive innovation,

11
00:00:38,990 --> 00:00:42,920
good jobs, and high performance
for the organization.

12
00:00:42,920 --> 00:00:45,580
Let's start with one that I
think almost everyone knows,

13
00:00:45,580 --> 00:00:47,250
Southwest Airlines.

14
00:00:47,250 --> 00:00:50,700
This is an airline that
was created in the 1970s,

15
00:00:50,700 --> 00:00:54,750
and it's been an anomaly to
our friends on Wall Street.

16
00:00:54,750 --> 00:00:58,540
Southwest has been one of
the most profitable airlines

17
00:00:58,540 --> 00:01:01,200
in the United States
over the last 30 years.

18
00:01:01,200 --> 00:01:05,090
Yet at the same time, it
breaks a lot of rules.

19
00:01:05,090 --> 00:01:09,220
First of all, it's been roundly
criticized by Wall Street

20
00:01:09,220 --> 00:01:13,400
for not expanding fast enough,
not growing fast enough,

21
00:01:13,400 --> 00:01:17,210
but using its retained
earnings to share them

22
00:01:17,210 --> 00:01:20,070
more equitably with
their employees

23
00:01:20,070 --> 00:01:22,370
than growing the airline.

24
00:01:22,370 --> 00:01:24,730
Secondly, it's highly
unionized airline.

25
00:01:24,730 --> 00:01:27,440
Wall Street thinks
unions are a drag.

26
00:01:27,440 --> 00:01:30,690
Third, it has the
best customer service

27
00:01:30,690 --> 00:01:34,330
of any airline in the country,
and it builds that customer

28
00:01:34,330 --> 00:01:38,510
service by engaging
employees in successful ways.

29
00:01:38,510 --> 00:01:41,970
And it has continued
to be successful

30
00:01:41,970 --> 00:01:46,200
even after the retirement of
its charismatic founder, the CEO

31
00:01:46,200 --> 00:01:47,100
Herb Kelleher.

32
00:01:47,100 --> 00:01:50,840
We're now on the fourth
CEO at Southwest,

33
00:01:50,840 --> 00:01:53,390
and it continues to do well.

34
00:01:53,390 --> 00:01:57,570
And so Southwest does get good
ratings from its employees.

35
00:01:57,570 --> 00:02:01,150
And now, even, it gets good
ratings from Wall Street.

36
00:02:01,150 --> 00:02:03,970
So this is from a
Glassdoor survey

37
00:02:03,970 --> 00:02:06,860
where employees can
voluntarily offer

38
00:02:06,860 --> 00:02:10,340
their assessment of their
companies and their CEO.

39
00:02:10,340 --> 00:02:12,920
And you can see
that Southwest is

40
00:02:12,920 --> 00:02:17,230
ranked number one in the country
in terms of a place to work.

41
00:02:17,230 --> 00:02:18,920
So we can do this.

42
00:02:18,920 --> 00:02:22,060
We can make companies,
design companies that

43
00:02:22,060 --> 00:02:25,190
are successful for employees
by listening to them,

44
00:02:25,190 --> 00:02:27,740
by engaging them, by
engaging in the teamwork

45
00:02:27,740 --> 00:02:30,890
that Southwest
provides, as well as

46
00:02:30,890 --> 00:02:34,000
producing high quality
services and highly profitable

47
00:02:34,000 --> 00:02:35,160
companies.

48
00:02:35,160 --> 00:02:37,120
But Southwest is not alone.

49
00:02:37,120 --> 00:02:39,880
Let's dig a little bit
deeper and understand

50
00:02:39,880 --> 00:02:43,260
what the difference is between
a high road and a low road

51
00:02:43,260 --> 00:02:43,900
company.

52
00:02:43,900 --> 00:02:46,570
Let's do that by first
looking at Walmart,

53
00:02:46,570 --> 00:02:50,620
and then comparing it to
another company called Costco.

54
00:02:50,620 --> 00:02:53,285
As you know, Walmart
is the largest employer

55
00:02:53,285 --> 00:02:54,160
in the United States.

56
00:02:54,160 --> 00:02:58,120
It has over 1.2 million,
quote, "associates."

57
00:02:58,120 --> 00:03:00,170
They like to use
the term associates

58
00:03:00,170 --> 00:03:02,380
to describe their employees.

59
00:03:02,380 --> 00:03:05,190
Walmart has been
successful financially.

60
00:03:05,190 --> 00:03:09,750
Yet Walmart has the tightest
controls on its employees.

61
00:03:09,750 --> 00:03:13,240
It gets its financial
success by having, as you

62
00:03:13,240 --> 00:03:16,860
know, a motto of low
prices all the time.

63
00:03:16,860 --> 00:03:18,580
How does it get
those low prices?

64
00:03:18,580 --> 00:03:22,800
By having very low wage rates,
by controlling employees,

65
00:03:22,800 --> 00:03:25,030
by having high
turnover so employees

66
00:03:25,030 --> 00:03:26,480
don't become more expensive.

67
00:03:26,480 --> 00:03:30,010
It has no unions, and in fact,
it fights unions tooth and nail

68
00:03:30,010 --> 00:03:32,300
to make sure it stays non-union.

69
00:03:32,300 --> 00:03:35,300
So here's a company that
works for shareholders

70
00:03:35,300 --> 00:03:37,990
but is very tough
on its employees.

71
00:03:37,990 --> 00:03:41,120
But then compare that to Costco.

72
00:03:41,120 --> 00:03:43,940
Costco competes with
some of Walmart's stores

73
00:03:43,940 --> 00:03:45,380
that are called Sam Club.

74
00:03:45,380 --> 00:03:48,240
And Costco has a
very different model.

75
00:03:48,240 --> 00:03:52,490
It emphasizes product quality,
it emphasizes customer service,

76
00:03:52,490 --> 00:03:55,710
it pays wages that are
higher than Walmart.

77
00:03:55,710 --> 00:03:58,150
It allows employees
to unionize if that's

78
00:03:58,150 --> 00:03:59,180
what they choose to do.

79
00:03:59,180 --> 00:04:02,160
So some of their stores are
unionized and some are not.

80
00:04:02,160 --> 00:04:06,430
It has a very long
tenure of its employees

81
00:04:06,430 --> 00:04:08,190
because they learn
about the business

82
00:04:08,190 --> 00:04:10,440
and they become more
productive over time.

83
00:04:10,440 --> 00:04:13,170
And so here we have
two alternatives.

84
00:04:13,170 --> 00:04:15,760
We have a high road
employer called Costco.

85
00:04:15,760 --> 00:04:18,970
We have a low road
employer called Walmart.

86
00:04:18,970 --> 00:04:22,050
Both can be reasonably
successful financially,

87
00:04:22,050 --> 00:04:24,810
but only one can be
successful financially

88
00:04:24,810 --> 00:04:27,950
and provide good jobs
and good opportunities

89
00:04:27,950 --> 00:04:30,470
for its employees.

90
00:04:30,470 --> 00:04:33,280
And in fact, this
chart shows that Costco

91
00:04:33,280 --> 00:04:36,940
has outperformed Walmart
on financial grounds

92
00:04:36,940 --> 00:04:38,120
in recent years.

93
00:04:38,120 --> 00:04:41,310
And so there are
two ways to compete.

94
00:04:41,310 --> 00:04:42,810
There's a low road.

95
00:04:42,810 --> 00:04:44,300
There's a high road.

96
00:04:44,300 --> 00:04:45,320
Let's review.

97
00:04:45,320 --> 00:04:47,870
Just how do you get these
high road practices?

98
00:04:47,870 --> 00:04:50,260
How can you build companies
that are successful

99
00:04:50,260 --> 00:04:54,120
for both investors, for
employees, and for customers?

100
00:04:54,120 --> 00:04:56,780
Well, it starts by
selecting employees

101
00:04:56,780 --> 00:05:00,290
both for technical skills
and for teamwork skills

102
00:05:00,290 --> 00:05:02,680
so that they're ready to
work together effectively

103
00:05:02,680 --> 00:05:04,070
with their colleagues.

104
00:05:04,070 --> 00:05:07,350
Secondly, it requires continuous
training and investment

105
00:05:07,350 --> 00:05:11,270
of front line workers
as well middle managers

106
00:05:11,270 --> 00:05:12,440
and supervisors.

107
00:05:12,440 --> 00:05:16,930
It requires us to work together
to build a culture of trust

108
00:05:16,930 --> 00:05:19,290
at the workplace,
to engage employees,

109
00:05:19,290 --> 00:05:22,980
and to get them to want to
cooperate with each other

110
00:05:22,980 --> 00:05:24,700
to get the jobs done.

111
00:05:24,700 --> 00:05:28,360
It requires compensation systems
that align the incentives so

112
00:05:28,360 --> 00:05:31,840
that employees share in the
income and the productivity

113
00:05:31,840 --> 00:05:35,760
that they help to produce by
profit sharing or various kinds

114
00:05:35,760 --> 00:05:37,500
of incentive systems.

115
00:05:37,500 --> 00:05:40,520
It requires, if unionized,
to have labor management

116
00:05:40,520 --> 00:05:43,610
partnerships where labor and
management work together,

117
00:05:43,610 --> 00:05:46,900
and the union leaders are
taken into the confidence

118
00:05:46,900 --> 00:05:51,530
of management representatives
to get the job done together.

119
00:05:51,530 --> 00:05:53,620
And these principles
apply to organizations

120
00:05:53,620 --> 00:05:56,910
not just in the US, but
all around the world.

121
00:05:56,910 --> 00:06:01,590
Recall that we mentioned earlier
companies like Semco in Brazil,

122
00:06:01,590 --> 00:06:04,520
where employees help
design their own jobs

123
00:06:04,520 --> 00:06:06,820
and work in partnership
with management

124
00:06:06,820 --> 00:06:09,190
to improve operations,
and sometimes

125
00:06:09,190 --> 00:06:10,850
to create new businesses.

126
00:06:10,850 --> 00:06:12,830
Or Ikea in Sweden.

127
00:06:12,830 --> 00:06:15,620
Or let's take the
classic example of Toyota

128
00:06:15,620 --> 00:06:18,990
in Japan or in its plants
all around the world.

129
00:06:18,990 --> 00:06:23,580
In fact, Toyota's model of
so-called lean production

130
00:06:23,580 --> 00:06:27,350
is the source of many of
the ideas and practices that

131
00:06:27,350 --> 00:06:30,770
need to be fitted together
in high road firms

132
00:06:30,770 --> 00:06:33,980
to achieve world class
productivity, quality,

133
00:06:33,980 --> 00:06:35,690
and good jobs.

134
00:06:35,690 --> 00:06:37,490
We're going to see
in another video

135
00:06:37,490 --> 00:06:41,250
my colleague Zeynep Ton
talking about a grocery chain

136
00:06:41,250 --> 00:06:43,970
here in the Northeastern
part of the United States

137
00:06:43,970 --> 00:06:47,110
that has been very successful
in building a company that

138
00:06:47,110 --> 00:06:51,030
works for shareholders as well
as customers and employees.

139
00:06:51,030 --> 00:06:53,250
And in fact, the
employees and managers

140
00:06:53,250 --> 00:06:57,570
revolted to save the company
from a change in strategy

141
00:06:57,570 --> 00:07:00,160
in the summer of 2014.

142
00:07:00,160 --> 00:07:02,430
So let's get on with
the task of building

143
00:07:02,430 --> 00:07:05,560
these high road,
good jobs companies,

144
00:07:05,560 --> 00:07:07,620
or, if you're not
in a top management

145
00:07:07,620 --> 00:07:09,710
position at the
moment, demanding

146
00:07:09,710 --> 00:07:12,700
that your employer follow
these principles wherever

147
00:07:12,700 --> 00:07:15,450
you happen to live and work.

148
00:07:15,450 --> 00:07:17,960
That's the challenge
you face, not only

149
00:07:17,960 --> 00:07:22,500
in creating these, but spreading
these to become the norm

150
00:07:22,500 --> 00:07:25,270
across American industry.