1
00:00:10,500 --> 00:00:14,400
One of the reasons that high
road companies like Saturn

2
00:00:14,400 --> 00:00:17,040
and others often
don't get carried on

3
00:00:17,040 --> 00:00:21,220
is that once the founders leave,
the next generation of leaders

4
00:00:21,220 --> 00:00:23,280
don't carry on the
same practices,

5
00:00:23,280 --> 00:00:27,150
and so the culture goes back to
more traditional organizations.

6
00:00:27,150 --> 00:00:29,650
But there are ways
to address this issue

7
00:00:29,650 --> 00:00:33,110
by building commitments
to multiple stakeholders

8
00:00:33,110 --> 00:00:36,150
right into the bylaws
of the corporation.

9
00:00:36,150 --> 00:00:37,950
Let's look at some examples.

10
00:00:37,950 --> 00:00:40,460
We'll consider three
different models.

11
00:00:40,460 --> 00:00:45,050
First, let's talk about what
we call benefit corporations.

12
00:00:45,050 --> 00:00:49,440
Benefit corporations build
right into their charter three

13
00:00:49,440 --> 00:00:50,820
different objectives.

14
00:00:50,820 --> 00:00:53,470
First, they say they
are going to commit

15
00:00:53,470 --> 00:00:58,060
to producing results that are
good for their shareholders,

16
00:00:58,060 --> 00:01:00,560
for their employees,
and for the economy.

17
00:01:00,560 --> 00:01:02,690
Secondly, there are
going to consider

18
00:01:02,690 --> 00:01:06,790
financial and non-financial
considerations when

19
00:01:06,790 --> 00:01:09,280
making key decisions
that allow them

20
00:01:09,280 --> 00:01:13,150
to keep the benefits flowing
to all of these stakeholders.

21
00:01:13,150 --> 00:01:16,440
Third, they agree
to be transparent.

22
00:01:16,440 --> 00:01:19,880
They provide annual reports
on their performance

23
00:01:19,880 --> 00:01:22,280
and their progress in
addressing the interests

24
00:01:22,280 --> 00:01:24,530
of these multiple organizations.

25
00:01:24,530 --> 00:01:27,940
One of the best known companies
that is a benefit corporation

26
00:01:27,940 --> 00:01:29,150
is Patagonia.

27
00:01:29,150 --> 00:01:32,390
Patagonia is this very
famous clothing maker.

28
00:01:32,390 --> 00:01:36,120
And its CEO and
founder, Yvon Chouinard,

29
00:01:36,120 --> 00:01:39,790
says, Patagonia is trying
to build a company that

30
00:01:39,790 --> 00:01:41,530
could last 100 years.

31
00:01:41,530 --> 00:01:45,060
Benefit corporation legislation
creates the legal framework

32
00:01:45,060 --> 00:01:48,560
to enable mission driven
companies like Patagonia

33
00:01:48,560 --> 00:01:52,350
to stay mission driven through
succession, through raising

34
00:01:52,350 --> 00:01:54,810
capital, through
changes in ownership.

35
00:01:54,810 --> 00:01:58,290
In doing so it institutionalizes
the value, the culture,

36
00:01:58,290 --> 00:02:00,530
the processes, and
the high standards

37
00:02:00,530 --> 00:02:02,870
that are put in place
by these founders.

38
00:02:02,870 --> 00:02:05,590
So by doing this we
get some continuity.

39
00:02:05,590 --> 00:02:09,090
In fact, 22 states
now allow companies

40
00:02:09,090 --> 00:02:11,290
to incorporate as
benefit corporations

41
00:02:11,290 --> 00:02:13,000
within their boundaries.

42
00:02:13,000 --> 00:02:17,520
And we have, at this point,
about 786 such companies.

43
00:02:17,520 --> 00:02:19,990
Another option is
employee ownership.

44
00:02:19,990 --> 00:02:24,430
You've probably heard the term
ESOP, employee stock ownership

45
00:02:24,430 --> 00:02:25,320
programs.

46
00:02:25,320 --> 00:02:28,160
Well, employee
ownership companies

47
00:02:28,160 --> 00:02:32,240
build a commitment to being
successful financially

48
00:02:32,240 --> 00:02:36,070
both for traditional investors
and for employee owners right

49
00:02:36,070 --> 00:02:39,040
into the structure and
governance of the firm.

50
00:02:39,040 --> 00:02:40,910
Employees often
have representatives

51
00:02:40,910 --> 00:02:42,560
on the corporate
boards of directors,

52
00:02:42,560 --> 00:02:45,380
but the key to success
of these companies

53
00:02:45,380 --> 00:02:48,870
is to make employees feel
like owners by building

54
00:02:48,870 --> 00:02:52,460
a culture of engagement, of
ownership, of encouraging

55
00:02:52,460 --> 00:02:54,970
employees to make
suggestions for improving

56
00:02:54,970 --> 00:02:56,820
the operations in
the performance

57
00:02:56,820 --> 00:02:59,570
of the organization,
and therefore aligning

58
00:02:59,570 --> 00:03:01,770
the incentives
through condensation

59
00:03:01,770 --> 00:03:04,820
and through the culture
of the organization.

60
00:03:04,820 --> 00:03:08,360
So employee ownership companies
have spread across the country.

61
00:03:08,360 --> 00:03:10,480
There are about 10,000 of them.

62
00:03:10,480 --> 00:03:15,130
And you can find them almost any
industry that you can think of.

63
00:03:15,130 --> 00:03:17,490
The key to employee
ownership, though,

64
00:03:17,490 --> 00:03:20,230
is to make sure
employees are involved,

65
00:03:20,230 --> 00:03:22,230
and to also worry
a bit that they

66
00:03:22,230 --> 00:03:25,190
don't to put all of
their financial resources

67
00:03:25,190 --> 00:03:28,890
for retirement into
the same company.

68
00:03:28,890 --> 00:03:31,610
You also want to
spread your investment

69
00:03:31,610 --> 00:03:35,680
a bit so that you don't come
up short if the company happens

70
00:03:35,680 --> 00:03:38,350
not to be as successful
as you would like.

71
00:03:38,350 --> 00:03:41,750
There are other kinds of
companies call cooperatives.

72
00:03:41,750 --> 00:03:45,040
A cooperative is
a company that has

73
00:03:45,040 --> 00:03:49,440
lots of owners that pool their
resources to get some job they

74
00:03:49,440 --> 00:03:52,250
all need done done together.

75
00:03:52,250 --> 00:03:54,490
So if you happened
to grow up on a farm,

76
00:03:54,490 --> 00:03:56,990
you may have been part
of a milk cooperative

77
00:03:56,990 --> 00:03:59,000
where farmers pool
their resources

78
00:03:59,000 --> 00:04:03,420
and create a dairy to take the
milk from their dairy cows.

79
00:04:03,420 --> 00:04:05,870
There are sawmills in Oregon.

80
00:04:05,870 --> 00:04:10,200
Here in Cambridge we have
the MIT coop and the Harvard

81
00:04:10,200 --> 00:04:15,190
cooperative society, bookstores
that sell books and reference

82
00:04:15,190 --> 00:04:19,339
material and clothing to our
students and to our faculty.

83
00:04:19,339 --> 00:04:21,600
And then there's my
favorite cooperative,

84
00:04:21,600 --> 00:04:25,100
as my tie suggests, it's
the Green Bay Packers.

85
00:04:25,100 --> 00:04:28,410
Green Bay Packers are a
community own football

86
00:04:28,410 --> 00:04:30,880
team started in the 1920s.

87
00:04:30,880 --> 00:04:35,150
And how else could you have
a city, Green Bay, Wisconsin,

88
00:04:35,150 --> 00:04:37,690
with only about
100,000 residents

89
00:04:37,690 --> 00:04:42,460
sustain a very successful,
highly competitive, and often

90
00:04:42,460 --> 00:04:45,750
championship football team
for all of these years?

91
00:04:45,750 --> 00:04:48,690
It's because they are
owned by the community

92
00:04:48,690 --> 00:04:51,550
and they can't be sold
to some higher bidder who

93
00:04:51,550 --> 00:04:53,880
might make a profit off of it.

94
00:04:53,880 --> 00:04:55,690
You'll also see
some cooperatives

95
00:04:55,690 --> 00:04:56,860
in other countries.

96
00:04:56,860 --> 00:05:00,410
Probably the most famous
one is the Mondragon set

97
00:05:00,410 --> 00:05:04,410
of industries, headquartered
in the Basque region of Spain,

98
00:05:04,410 --> 00:05:08,550
with companies that range
from manufacturing, to retail,

99
00:05:08,550 --> 00:05:11,490
to high tech, to
financial institutions.

100
00:05:11,490 --> 00:05:16,040
They have over 81 different
companies at the moment.

101
00:05:16,040 --> 00:05:20,260
So we can build organizations
in different kinds of ways.

102
00:05:20,260 --> 00:05:22,160
And so the takeaway
lesson that I

103
00:05:22,160 --> 00:05:25,250
hope you will have here
from this conversation

104
00:05:25,250 --> 00:05:27,150
is that it's possible.

105
00:05:27,150 --> 00:05:30,000
If you are imaginative,
perhaps you

106
00:05:30,000 --> 00:05:32,780
can invent the new
forms of organizations

107
00:05:32,780 --> 00:05:35,260
for the future that will
meet the needs of all

108
00:05:35,260 --> 00:05:38,840
of their stakeholders,
and will be built to last

109
00:05:38,840 --> 00:05:40,550
for multiple generations.

110
00:05:40,550 --> 00:05:44,070
And in doing so, not only might
you build organizations that

111
00:05:44,070 --> 00:05:48,630
work for investors,
customers, and employees,

112
00:05:48,630 --> 00:05:53,570
but maybe ones that help
to sustain our planet.