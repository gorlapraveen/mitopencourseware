1
00:00:10,230 --> 00:00:13,890
Over the course of six
weeks in the summer of 2014,

2
00:00:13,890 --> 00:00:17,200
Market Basket employees,
joined by their customers,

3
00:00:17,200 --> 00:00:20,980
protested against the
firing of their beloved CEO.

4
00:00:20,980 --> 00:00:24,340
They risked their jobs,
their livelihoods,

5
00:00:24,340 --> 00:00:26,840
and they brought the
business to a halt.

6
00:00:26,840 --> 00:00:31,290
At a time when we see so much
division between the CEOs who

7
00:00:31,290 --> 00:00:35,260
represent the top 1% and
the workers, who represent

8
00:00:35,260 --> 00:00:40,250
by the bottom 99%, this
level of support for a CEO

9
00:00:40,250 --> 00:00:44,090
got the attention of
the entire country.

10
00:00:44,090 --> 00:00:47,740
Arthur Demoulas, he
stands for valuing people.

11
00:00:47,740 --> 00:00:51,250
He always says we are in
the people business first,

12
00:00:51,250 --> 00:00:52,620
grocery business second.

13
00:00:52,620 --> 00:00:54,831
And when the
ownership changed, you

14
00:00:54,831 --> 00:00:56,580
started asking the
questions, what's going

15
00:00:56,580 --> 00:00:57,600
to happen with our benefits?

16
00:00:57,600 --> 00:00:59,160
What's going to
happen with our pay?

17
00:00:59,160 --> 00:01:02,600
And when you start taking
away that factor of focusing

18
00:01:02,600 --> 00:01:06,100
on people, the entire business
model starts to fall apart.

19
00:01:06,100 --> 00:01:08,030
The workers were
fighting for a business

20
00:01:08,030 --> 00:01:10,310
model that works for everyone--

21
00:01:10,310 --> 00:01:14,890
for them, for their customers,
and for their shareholders.

22
00:01:14,890 --> 00:01:17,840
We saw a number of
critical business lessons

23
00:01:17,840 --> 00:01:19,920
that emerged from Market Basket.

24
00:01:19,920 --> 00:01:23,690
Let me share with you
three related lessons.

25
00:01:23,690 --> 00:01:27,710
The first lesson is around
creating customer and employee

26
00:01:27,710 --> 00:01:31,090
loyalty in a pretty
unlikely setting.

27
00:01:31,090 --> 00:01:34,280
Local supermarkets
are not necessarily

28
00:01:34,280 --> 00:01:38,950
known for treating their
employees or customers well.

29
00:01:38,950 --> 00:01:42,600
They are known for offering
bad jobs with poverty level

30
00:01:42,600 --> 00:01:46,960
wages, unpredictable schedules,
and very few opportunities

31
00:01:46,960 --> 00:01:48,760
for success and growth.

32
00:01:48,760 --> 00:01:51,620
They're also known to
frustrate their customers.

33
00:01:51,620 --> 00:01:54,640
The Market Basket
case shows us that it

34
00:01:54,640 --> 00:01:56,770
doesn't have to be that way.

35
00:01:56,770 --> 00:01:59,370
Here's a company that really
puts a focus on building

36
00:01:59,370 --> 00:02:00,520
those relationships.

37
00:02:00,520 --> 00:02:02,269
It's easy to say treat
a customer as you

38
00:02:02,269 --> 00:02:03,310
would like to be treated.

39
00:02:03,310 --> 00:02:04,834
It's a lot different
to say that you

40
00:02:04,834 --> 00:02:06,250
would treat your
customers the way

41
00:02:06,250 --> 00:02:08,083
you would treat your
family or your friends.

42
00:02:08,083 --> 00:02:10,070
Because there's a
lot a loyalty there.

43
00:02:10,070 --> 00:02:13,340
The second lesson that
emerged from Market Basket

44
00:02:13,340 --> 00:02:16,480
is the importance of
employees and what

45
00:02:16,480 --> 00:02:21,260
they do for efficient,
low-cost operations.

46
00:02:21,260 --> 00:02:26,940
You see, Market Basket pays
its employees decent wages.

47
00:02:26,940 --> 00:02:29,450
It offers them
generous benefits.

48
00:02:29,450 --> 00:02:32,250
It treats them with
respect and dignity.

49
00:02:32,250 --> 00:02:36,480
Market Basket designs
the jobs in a way

50
00:02:36,480 --> 00:02:40,510
that allows their employees
to be more motivated, that

51
00:02:40,510 --> 00:02:42,380
allows them to be
more productive,

52
00:02:42,380 --> 00:02:44,630
and that allows them
to contribute more

53
00:02:44,630 --> 00:02:46,870
to the success of their company.

54
00:02:46,870 --> 00:02:48,534
It's not just working
for the paycheck;

55
00:02:48,534 --> 00:02:50,700
it's working for what you're
getting out of the job.

56
00:02:50,700 --> 00:02:52,790
Getting that feeling that you're
providing something of value

57
00:02:52,790 --> 00:02:55,100
that they appreciate, and
appreciating somebody else.

58
00:02:55,100 --> 00:02:56,724
That's a huge motivator for me.

59
00:02:56,724 --> 00:02:58,640
And carrying that through
the company to bring

60
00:02:58,640 --> 00:02:59,580
value to the company.

61
00:02:59,580 --> 00:03:01,160
Who wouldn't want
to work for that?

62
00:03:01,160 --> 00:03:03,260
If you go to a
Market Basket store,

63
00:03:03,260 --> 00:03:05,520
you're going to see a
lot of employees around.

64
00:03:05,520 --> 00:03:08,610
A lot more than what you
see at a local supermarket .

65
00:03:08,610 --> 00:03:11,100
Why is that?

66
00:03:11,100 --> 00:03:15,240
Well, Market Basket does
that so that its employees

67
00:03:15,240 --> 00:03:17,780
can do their jobs
without making errors.

68
00:03:17,780 --> 00:03:19,330
So that they're not rushed.

69
00:03:19,330 --> 00:03:22,050
So that they can treat
their customers well,

70
00:03:22,050 --> 00:03:24,100
help them out when necessary.

71
00:03:24,100 --> 00:03:28,090
So that they have
enough time to engage

72
00:03:28,090 --> 00:03:31,300
in continuous improvement,
come up with suggestions

73
00:03:31,300 --> 00:03:35,940
to reduce cost and increase
customer service and sales.

74
00:03:35,940 --> 00:03:39,010
Appropriate staffing levels
is just one of the job design

75
00:03:39,010 --> 00:03:42,260
decisions that
motivate employees,

76
00:03:42,260 --> 00:03:45,340
make their jobs better,
so make them happier,

77
00:03:45,340 --> 00:03:48,710
that lead to happy
customers, because they

78
00:03:48,710 --> 00:03:50,090
receive better service.

79
00:03:50,090 --> 00:03:52,980
And that also lead
to happier investors

80
00:03:52,980 --> 00:03:56,520
because now the costs are
lower, sales are higher,

81
00:03:56,520 --> 00:03:59,040
and profits are higher.

82
00:03:59,040 --> 00:04:01,990
And that brings me to the
third decision related

83
00:04:01,990 --> 00:04:07,200
to Market Basket, which is
even in a low-cost supermarket

84
00:04:07,200 --> 00:04:12,590
setting, it is not only
possible but highly profitable

85
00:04:12,590 --> 00:04:15,820
to create a business
model that creates

86
00:04:15,820 --> 00:04:21,940
value for customers, employees,
and investors at the same time.

87
00:04:21,940 --> 00:04:24,150
There's a lot of
focus on people.

88
00:04:24,150 --> 00:04:26,080
You have 8 owners
and shareholders,

89
00:04:26,080 --> 00:04:27,811
and then you have
25,000 employees.

90
00:04:27,811 --> 00:04:29,310
And then you have
the further impact

91
00:04:29,310 --> 00:04:31,750
of vendors and customers.

92
00:04:31,750 --> 00:04:33,540
That's a huge base.

93
00:04:33,540 --> 00:04:35,130
Now you're talking
millions of people.

94
00:04:35,130 --> 00:04:38,850
And here's a company that really
puts a focus on everyone who's

95
00:04:38,850 --> 00:04:41,180
involved with our company.

96
00:04:41,180 --> 00:04:44,100
Now, you might wonder, is
Market Basket an exception?

97
00:04:44,100 --> 00:04:45,360
Is it unique?

98
00:04:45,360 --> 00:04:48,740
Well, certainly, what
Market Basket employees

99
00:04:48,740 --> 00:04:51,060
did was pretty extraordinary.

100
00:04:51,060 --> 00:04:55,760
But Market Basket's business
model is hardly special.

101
00:04:55,760 --> 00:04:58,580
In fact, in my
own research, I've

102
00:04:58,580 --> 00:05:01,100
studied four other
low-cost retailers

103
00:05:01,100 --> 00:05:05,150
that do exactly the
same, ranging from Costco

104
00:05:05,150 --> 00:05:10,570
to QuikTrip, a convenience store
chain with gas stations, that

105
00:05:10,570 --> 00:05:12,860
has been in Fortune's
100 best companies

106
00:05:12,860 --> 00:05:16,220
to work for 12 years
in a row and that

107
00:05:16,220 --> 00:05:18,700
is extremely profitable.

108
00:05:18,700 --> 00:05:23,500
If good jobs are possible
at a setting like QuikTrip,

109
00:05:23,500 --> 00:05:26,070
they are possible everywhere.

110
00:05:26,070 --> 00:05:29,880
We just need more companies to
follow the good job strategy.

111
00:05:29,880 --> 00:05:32,490
And everyone can do their part.

112
00:05:32,490 --> 00:05:33,780
You are customers.

113
00:05:33,780 --> 00:05:35,400
You can walk with your feet.

114
00:05:35,400 --> 00:05:39,830
You can decide the shop or dine
at places that offer good jobs.

115
00:05:39,830 --> 00:05:44,290
For those of you who
are employees, managers,

116
00:05:44,290 --> 00:05:46,990
or even investors
in organizations,

117
00:05:46,990 --> 00:05:51,670
then you could be advocates
for a good job strategy.

118
00:05:51,670 --> 00:05:54,710
Finally, I want to leave
you with one last thought.

119
00:05:54,710 --> 00:05:57,710
At some point in
your career, chances

120
00:05:57,710 --> 00:05:59,370
are you'll be a manager.

121
00:05:59,370 --> 00:06:03,990
You might manage just a few
people or hundreds of people.

122
00:06:03,990 --> 00:06:08,520
Remember this, the types
of decisions that you make

123
00:06:08,520 --> 00:06:11,040
are not just business decisions.

124
00:06:11,040 --> 00:06:17,330
They affect the lives of people,
so make those decisions wisely.