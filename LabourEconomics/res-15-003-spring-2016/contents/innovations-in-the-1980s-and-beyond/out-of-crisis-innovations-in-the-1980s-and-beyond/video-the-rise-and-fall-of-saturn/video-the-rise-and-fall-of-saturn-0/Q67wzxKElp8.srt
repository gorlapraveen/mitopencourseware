1
00:00:10,260 --> 00:00:14,140
If you would have been driving
through the hills of Tennessee

2
00:00:14,140 --> 00:00:19,310
about 40 miles south of
Nashville on I-65 in the 1990s,

3
00:00:19,310 --> 00:00:22,380
you would have come across
a very curious exit called

4
00:00:22,380 --> 00:00:23,750
the Saturn Parkway.

5
00:00:23,750 --> 00:00:26,430
And if you would've taken that
exit and gone a little farther,

6
00:00:26,430 --> 00:00:29,150
you would've come across
something called the Donald

7
00:00:29,150 --> 00:00:30,650
Ephlin Parkway.

8
00:00:30,650 --> 00:00:34,200
That was the entrance to
the Saturn Corporation,

9
00:00:34,200 --> 00:00:36,810
one of the boldest,
most innovative,

10
00:00:36,810 --> 00:00:40,390
new experiments in labor
management relations

11
00:00:40,390 --> 00:00:43,750
and organizations of
the past 30 years.

12
00:00:43,750 --> 00:00:47,540
So let's take a look at
what Saturn was all about

13
00:00:47,540 --> 00:00:49,630
and, unfortunately,
why it failed.

14
00:00:49,630 --> 00:00:53,340
Well, Saturn
started in the 1980s

15
00:00:53,340 --> 00:00:56,410
when a number of innovative
labor and management leaders

16
00:00:56,410 --> 00:00:59,750
said, we've got to adjust
the way in which we work.

17
00:00:59,750 --> 00:01:03,000
General Motors came
to Donald Ephlin, who

18
00:01:03,000 --> 00:01:06,330
was the vice president of the
auto workers union, and said,

19
00:01:06,330 --> 00:01:09,510
we can't build a small
car in the United States

20
00:01:09,510 --> 00:01:10,340
competitively.

21
00:01:10,340 --> 00:01:12,680
We've got to
outsource that work.

22
00:01:12,680 --> 00:01:15,590
Don, in his innovative way,
said, well, wait a minute.

23
00:01:15,590 --> 00:01:17,940
Why don't we take a
look if we couldn't

24
00:01:17,940 --> 00:01:20,770
take a clean sheet of paper,
give it to some folks,

25
00:01:20,770 --> 00:01:23,810
and see if they can come up
with a new way to do this.

26
00:01:23,810 --> 00:01:26,050
In short, they
created what became

27
00:01:26,050 --> 00:01:31,740
known as the Committee of 99: 99
people, engineers, accountants,

28
00:01:31,740 --> 00:01:34,010
front line workers,
union representatives,

29
00:01:34,010 --> 00:01:35,350
working together.

30
00:01:35,350 --> 00:01:38,460
The goal was to go
out and find out

31
00:01:38,460 --> 00:01:42,600
what we could do differently to
build a small car competitively

32
00:01:42,600 --> 00:01:43,920
in the United States.

33
00:01:43,920 --> 00:01:48,230
They came together, looking for
who is doing the cutting edge,

34
00:01:48,230 --> 00:01:50,230
both from a
technology standpoint

35
00:01:50,230 --> 00:01:51,570
and from a people standpoint.

36
00:01:51,570 --> 00:01:53,950
They visited Volvo, BMW.

37
00:01:53,950 --> 00:01:55,200
They went to Ford.

38
00:01:55,200 --> 00:01:56,880
They went to Lockheed.

39
00:01:56,880 --> 00:01:59,400
I think they visited
75 different companies

40
00:01:59,400 --> 00:02:01,150
in the US and abroad.

41
00:02:01,150 --> 00:02:05,090
And through all of this
the overpowering thing

42
00:02:05,090 --> 00:02:09,570
was that there's a different
way of working with people.

43
00:02:09,570 --> 00:02:12,380
They created Saturn as
a new kind of company

44
00:02:12,380 --> 00:02:14,250
and a new kind of car.

45
00:02:14,250 --> 00:02:19,410
We came up with five
proposals, and all three teams

46
00:02:19,410 --> 00:02:21,870
have come to a consensus
that they kind of like

47
00:02:21,870 --> 00:02:24,090
proposal number four.

48
00:02:24,090 --> 00:02:28,140
Saturn was built on the basis
of partnership principles

49
00:02:28,140 --> 00:02:29,660
from the bottom up.

50
00:02:29,660 --> 00:02:33,070
They said, we'll have teams
that assemble the cars.

51
00:02:33,070 --> 00:02:35,660
We'll have co-management
on the front lines

52
00:02:35,660 --> 00:02:37,410
at the first lines
of supervision

53
00:02:37,410 --> 00:02:40,320
with a union and a
management co-leader.

54
00:02:40,320 --> 00:02:43,480
We will do that up through
the manufacturing operations:

55
00:02:43,480 --> 00:02:46,940
even product development
and strategy decisions

56
00:02:46,940 --> 00:02:49,760
will be made in a
co-management way.

57
00:02:49,760 --> 00:02:52,770
Well, Saturn had
a lot of critics.

58
00:02:52,770 --> 00:02:55,850
But some of those
critics were silenced

59
00:02:55,850 --> 00:02:57,280
in the first couple of years.

60
00:02:57,280 --> 00:03:00,030
In fact, only about
two or three years

61
00:03:00,030 --> 00:03:03,270
after it rolled the first
car off its assembly lines,

62
00:03:03,270 --> 00:03:06,970
Saturn rose to the top of the
customer satisfaction ratings.

63
00:03:06,970 --> 00:03:09,670
It was as satisfying
to customers,

64
00:03:09,670 --> 00:03:13,420
in terms of sales experience
and initial product quality,

65
00:03:13,420 --> 00:03:16,470
as any other car built
in the United States.

66
00:03:16,470 --> 00:03:18,210
You might go as
far as to say that,

67
00:03:18,210 --> 00:03:21,520
down to the levels
of turning the nuts

68
00:03:21,520 --> 00:03:24,190
and bolts on the car,
that there was passion

69
00:03:24,190 --> 00:03:25,760
in people for their jobs.

70
00:03:25,760 --> 00:03:29,800
Saturn enjoyed in the '90s a
level of quality performance,

71
00:03:29,800 --> 00:03:33,410
a level of brand loyalty, that
the rest of General Motors

72
00:03:33,410 --> 00:03:35,220
had not achieved.

73
00:03:35,220 --> 00:03:36,440
But what happened?

74
00:03:36,440 --> 00:03:39,120
The traditionalists
within General Motors

75
00:03:39,120 --> 00:03:43,270
and within the auto workers
union really didn't like it.

76
00:03:43,270 --> 00:03:45,340
Don Ephlin had
retired from the UAW,

77
00:03:45,340 --> 00:03:47,340
and Steve Yokich
became vice president.

78
00:03:47,340 --> 00:03:49,180
And he was not
philosophically aligned

79
00:03:49,180 --> 00:03:52,770
with any of these
partnership, joint management

80
00:03:52,770 --> 00:03:54,040
ideas whatsoever.

81
00:03:54,040 --> 00:03:57,510
You didn't have a lot of deep
support both within the UAW

82
00:03:57,510 --> 00:04:01,840
or among General Motors
in Detroit for Saturn.

83
00:04:01,840 --> 00:04:05,400
And so Saturn's sales and
productivity began to decline.

84
00:04:05,400 --> 00:04:07,590
Morale of the
workforce declined.

85
00:04:07,590 --> 00:04:11,080
They felt that they weren't
invested in this company

86
00:04:11,080 --> 00:04:12,280
psychologically.

87
00:04:12,280 --> 00:04:14,880
They weren't going to invest
new products in the company.

88
00:04:14,880 --> 00:04:18,300
I think it's fair to say that
the product was outdated.

89
00:04:18,300 --> 00:04:19,820
The way to kill
any car company is

90
00:04:19,820 --> 00:04:21,980
to delay the introduction
of a new product

91
00:04:21,980 --> 00:04:23,706
because the competition
is innovating.

92
00:04:23,706 --> 00:04:25,580
The competition is
bringing out new products.

93
00:04:25,580 --> 00:04:27,020
And without a new
product, Saturn

94
00:04:27,020 --> 00:04:29,810
was going to lose market share
and began to lose market share.

95
00:04:29,810 --> 00:04:33,970
And basic math rule: If you
have an organization that

96
00:04:33,970 --> 00:04:38,640
maintains employment security
while sales are falling,

97
00:04:38,640 --> 00:04:40,629
your overall costs increase.

98
00:04:40,629 --> 00:04:42,670
You can't do it anything
unless you've got money.

99
00:04:42,670 --> 00:04:47,800
And we were being strangled
by a corporation that

100
00:04:47,800 --> 00:04:49,340
didn't know how to handle us.

101
00:04:49,340 --> 00:04:51,580
We were the outlaws,
if you will.

102
00:04:51,580 --> 00:04:54,580
To make a long story
short, Saturn limped along.

103
00:04:54,580 --> 00:04:57,350
It built a new
vehicle at one point

104
00:04:57,350 --> 00:05:00,100
called the Saturn
SUV, called the Vue,

105
00:05:00,100 --> 00:05:01,660
which was very successful.

106
00:05:01,660 --> 00:05:03,570
But Saturn could not survive.

107
00:05:03,570 --> 00:05:07,750
And in 2008, General
Motors closed down

108
00:05:07,750 --> 00:05:11,090
Saturn as part of its
restructuring activities.

109
00:05:11,090 --> 00:05:13,810
So what lessons should we learn?

110
00:05:13,810 --> 00:05:16,430
I think there are
three lessons that

111
00:05:16,430 --> 00:05:19,220
should influence how
we think about bringing

112
00:05:19,220 --> 00:05:22,650
about innovations and new
ways of working going forward.

113
00:05:22,650 --> 00:05:26,390
First, I want us all to
remember that Committee of 99.

114
00:05:26,390 --> 00:05:29,460
They were an early
form of crowdsourcing.

115
00:05:29,460 --> 00:05:31,650
They said, no
individual could come up

116
00:05:31,650 --> 00:05:33,930
with this innovative
design, but if we all

117
00:05:33,930 --> 00:05:36,200
put our heads together,
and we go around the world

118
00:05:36,200 --> 00:05:39,510
and look at best practices,
we can create something new,

119
00:05:39,510 --> 00:05:43,250
something that no one else could
have imagined in ways that we

120
00:05:43,250 --> 00:05:44,230
can work together.

121
00:05:44,230 --> 00:05:45,490
And it was successful.

122
00:05:45,490 --> 00:05:49,370
So remember crowdsourcing
as an option.

123
00:05:49,370 --> 00:05:50,790
But expect resistance.

124
00:05:50,790 --> 00:05:53,090
Remember those
traditionalists who said,

125
00:05:53,090 --> 00:05:55,410
this isn't the way we
do things around here.

126
00:05:55,410 --> 00:05:57,690
And once the original
champions left,

127
00:05:57,690 --> 00:05:59,670
the traditionalists took over.

128
00:05:59,670 --> 00:06:03,280
And they slowly, slowly
starved Saturn for resources.

129
00:06:03,280 --> 00:06:05,430
And the end was inevitable.

130
00:06:05,430 --> 00:06:08,430
And then finally, if you're
going to try new ideas,

131
00:06:08,430 --> 00:06:10,820
and you're going to create
new ways of working together

132
00:06:10,820 --> 00:06:13,500
and new organizations,
build in a strategy

133
00:06:13,500 --> 00:06:16,270
for sustaining that innovation
right from the beginning

134
00:06:16,270 --> 00:06:20,060
so that it will have legs
that goes on into the future.

135
00:06:20,060 --> 00:06:23,130
There'll be opportunities to
invent new ways of working.

136
00:06:23,130 --> 00:06:27,160
Again, as you go forward,
remember these three lessons

137
00:06:27,160 --> 00:06:29,340
from the Saturn Corporation.