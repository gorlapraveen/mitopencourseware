1
00:00:10,540 --> 00:00:13,110
We all know that we
are in a global world.

2
00:00:13,110 --> 00:00:16,730
Companies have operations
all over the planet.

3
00:00:16,730 --> 00:00:19,010
And a big question
we have to face

4
00:00:19,010 --> 00:00:21,590
is how do we keep these
global corporations

5
00:00:21,590 --> 00:00:26,230
accountable for providing good
jobs and decent work in far

6
00:00:26,230 --> 00:00:27,700
flung places?

7
00:00:27,700 --> 00:00:32,070
Let's do it through a company
we all know and love--

8
00:00:32,070 --> 00:00:33,020
Nike.

9
00:00:33,020 --> 00:00:35,860
Nike was a pioneer in this area.

10
00:00:35,860 --> 00:00:39,440
In the 1960s, Nike was one of
the first companies that said

11
00:00:39,440 --> 00:00:42,210
we will design our work here.

12
00:00:42,210 --> 00:00:46,000
We will market and sell our
products around the world,

13
00:00:46,000 --> 00:00:47,840
particularly in
the United States,

14
00:00:47,840 --> 00:00:51,250
but we can find cheaper
places to manufacture them.

15
00:00:51,250 --> 00:00:54,160
First, they started
in Japan, and Korea,

16
00:00:54,160 --> 00:00:59,250
and then Indonesia, and Vietnam,
and China, and Bangladesh.

17
00:00:59,250 --> 00:01:03,660
As they did so, a number
of problems became known.

18
00:01:03,660 --> 00:01:07,400
The Nike products more marketed
well with Michael Jordan

19
00:01:07,400 --> 00:01:11,780
and so on, but Labor
Standards became a problem.

20
00:01:11,780 --> 00:01:15,050
They were famously found
having child labor,

21
00:01:15,050 --> 00:01:17,130
to having unsafe
working conditions,

22
00:01:17,130 --> 00:01:21,020
to having long hours of unpaid
overtime in their factories

23
00:01:21,020 --> 00:01:21,640
abroad.

24
00:01:21,640 --> 00:01:23,530
And what was Nike's response?

25
00:01:23,530 --> 00:01:24,530
Well, wait a minute.

26
00:01:24,530 --> 00:01:27,010
These aren't our problems,
these are our contractors.

27
00:01:27,010 --> 00:01:28,560
These are not our employees.

28
00:01:28,560 --> 00:01:30,440
We don't treat our
employees that way.

29
00:01:30,440 --> 00:01:32,150
Somebody else's problem.

30
00:01:32,150 --> 00:01:38,280
And that lasted for awhile,
but by the middle of the 1990s,

31
00:01:38,280 --> 00:01:41,190
Nike watched its stock
price begin to fall.

32
00:01:41,190 --> 00:01:44,940
It saw the publicity
begin to grow and grow.

33
00:01:44,940 --> 00:01:48,700
And finally, Phil
Knight, the CEO of Nike,

34
00:01:48,700 --> 00:01:50,330
made this famous statement.

35
00:01:50,330 --> 00:01:52,230
He said, Nike's
products have become

36
00:01:52,230 --> 00:01:55,730
synonymous with slave
wages, forced overtime,

37
00:01:55,730 --> 00:01:59,270
and arbitrary abuse, and we
need to do something about it

38
00:01:59,270 --> 00:02:03,030
to change that image, or we're
going to suffer as a company.

39
00:02:03,030 --> 00:02:04,720
So what did Nike do?

40
00:02:04,720 --> 00:02:07,940
Well, to its credit, it
took a whole set of actions.

41
00:02:07,940 --> 00:02:11,340
First, it created a
social responsibility unit

42
00:02:11,340 --> 00:02:13,880
within its organization
and assigned people

43
00:02:13,880 --> 00:02:16,870
the responsibility to
say, fix this problem.

44
00:02:16,870 --> 00:02:18,870
That was a good
start, but not enough.

45
00:02:18,870 --> 00:02:22,560
Second, they created what
are called codes of conduct.

46
00:02:22,560 --> 00:02:26,850
They established
minimum wages, and rules

47
00:02:26,850 --> 00:02:29,590
against sexual
harassment, and rules

48
00:02:29,590 --> 00:02:34,290
for maximum hours and overtime,
and freedom of association.

49
00:02:34,290 --> 00:02:37,740
And they started to apply
that code of conduct

50
00:02:37,740 --> 00:02:40,790
to all factories
that they contracted

51
00:02:40,790 --> 00:02:43,140
with throughout their
global supply chain.

52
00:02:43,140 --> 00:02:46,530
They sent auditors out
there to measure compliance

53
00:02:46,530 --> 00:02:48,180
with its code of conduct.

54
00:02:48,180 --> 00:02:51,190
They took some of their
expert operations managers

55
00:02:51,190 --> 00:02:54,460
and manufacturing experts, sent
them to some of these plants

56
00:02:54,460 --> 00:02:57,460
to help consult, to
help people both build

57
00:02:57,460 --> 00:03:01,320
safe working conditions,
but also efficient ways

58
00:03:01,320 --> 00:03:04,460
of manufacturing products
to improve safety,

59
00:03:04,460 --> 00:03:07,060
improve quality,
improve productivity.

60
00:03:07,060 --> 00:03:10,110
They went on and
worked with a variety

61
00:03:10,110 --> 00:03:12,050
of non-governmental
organizations,

62
00:03:12,050 --> 00:03:15,250
or NGOs in these countries
who are advocating

63
00:03:15,250 --> 00:03:18,430
for improved employment
conditions, safety standards,

64
00:03:18,430 --> 00:03:22,160
and in some cases, environmental
standards in these factories.

65
00:03:22,160 --> 00:03:25,560
They shared this audit data
with academic researchers,

66
00:03:25,560 --> 00:03:28,800
including a team of
researchers here at MIT,

67
00:03:28,800 --> 00:03:32,700
where they started to analyze
the data and learn what works

68
00:03:32,700 --> 00:03:33,810
and what didn't.

69
00:03:33,810 --> 00:03:36,650
They also created a website,
and to their credit,

70
00:03:36,650 --> 00:03:39,560
you can go on this
website and you can find

71
00:03:39,560 --> 00:03:41,360
all their factories listed.

72
00:03:41,360 --> 00:03:43,600
You can find the
audit scores, and you

73
00:03:43,600 --> 00:03:45,810
can find an analysis
of what is working

74
00:03:45,810 --> 00:03:50,020
and a very frank analysis
of what isn't working.

75
00:03:50,020 --> 00:03:53,930
They met here at MIT,
and later, at Stanford

76
00:03:53,930 --> 00:03:58,860
with multiple stakeholders,
NGOs, academics, competitors,

77
00:03:58,860 --> 00:04:03,190
suppliers, in forming what
they call a just supply chain

78
00:04:03,190 --> 00:04:05,560
project and set of practices.

79
00:04:05,560 --> 00:04:08,570
The research findings
from our MIT project,

80
00:04:08,570 --> 00:04:10,800
led by my colleague
Richard Locke,

81
00:04:10,800 --> 00:04:13,500
reached a number of conclusions.

82
00:04:13,500 --> 00:04:15,590
First, they found
that NGO pressure

83
00:04:15,590 --> 00:04:18,010
was really important to
get this thing going.

84
00:04:18,010 --> 00:04:20,800
Nike and other companies
like Hewlett Packard,

85
00:04:20,800 --> 00:04:25,260
and other electronics firms,
and other consumer companies

86
00:04:25,260 --> 00:04:27,130
that followed in
Nike's footsteps

87
00:04:27,130 --> 00:04:30,420
did it because there was
transparency and pressure that

88
00:04:30,420 --> 00:04:33,570
was being brought to bear
to get them to do so.

89
00:04:33,570 --> 00:04:36,120
The codes of conduct were
successful and the audits

90
00:04:36,120 --> 00:04:38,360
we're successful,
but only partially.

91
00:04:38,360 --> 00:04:43,460
They seem to bring the
practices up to about 50% to 60%

92
00:04:43,460 --> 00:04:45,830
of the overall goals
that were established

93
00:04:45,830 --> 00:04:47,380
in the standards that were set.

94
00:04:47,380 --> 00:04:49,820
So there's lots of
room for improvement.

95
00:04:49,820 --> 00:04:53,270
The standard that was
most frequently violated

96
00:04:53,270 --> 00:04:56,560
was around working hours
and overtime hours.

97
00:04:56,560 --> 00:04:57,810
Why is that?

98
00:04:57,810 --> 00:04:59,860
It's important for us
to know, because we

99
00:04:59,860 --> 00:05:01,060
are part of the problem.

100
00:05:01,060 --> 00:05:03,860
As we are fickle consumers,
and all of a sudden

101
00:05:03,860 --> 00:05:08,240
we want the latest logo,
or the latest scarf,

102
00:05:08,240 --> 00:05:11,300
or the latest shirt,
and so the company

103
00:05:11,300 --> 00:05:13,220
puts pressure on
the supply chain

104
00:05:13,220 --> 00:05:15,170
to get those
products in a hurry.

105
00:05:15,170 --> 00:05:17,860
And the contractor there out
in Asia or somewhere else

106
00:05:17,860 --> 00:05:21,430
says to employees, I need those,
I need them today and get them

107
00:05:21,430 --> 00:05:24,080
done no matter what, or we're
going to lose this contract.

108
00:05:24,080 --> 00:05:27,310
And so they work long hours, and
they're forced to work overtime

109
00:05:27,310 --> 00:05:29,950
and all kinds of bad
things happen after that.

110
00:05:29,950 --> 00:05:32,340
But the scores of
suppliers that got better

111
00:05:32,340 --> 00:05:34,690
had three key ingredients.

112
00:05:34,690 --> 00:05:38,360
First, they got management help
from Nike and other companies.

113
00:05:38,360 --> 00:05:40,200
Secondly, they were
in countries that

114
00:05:40,200 --> 00:05:43,700
had stronger rules of law,
less corrupt governments,

115
00:05:43,700 --> 00:05:47,380
and they had stronger rules
governing human rights

116
00:05:47,380 --> 00:05:49,160
and commercial practices.

117
00:05:49,160 --> 00:05:51,890
And so the environment
of the country matters.

118
00:05:51,890 --> 00:05:55,650
So this is all great progress.

119
00:05:55,650 --> 00:05:57,860
Companies have learned
how to do this.

120
00:05:57,860 --> 00:06:00,510
But what about us?

121
00:06:00,510 --> 00:06:02,890
If we're part of the
problem, we ought

122
00:06:02,890 --> 00:06:04,600
to be part of the solution.

123
00:06:04,600 --> 00:06:07,300
There are a number of things
that have been very positive.

124
00:06:07,300 --> 00:06:09,540
There's an organization
on college campuses

125
00:06:09,540 --> 00:06:13,140
around the country called United
Students Against Sweatshops,

126
00:06:13,140 --> 00:06:15,500
and they have put pressure
on their universities--

127
00:06:15,500 --> 00:06:17,600
universities like
Cornell and Brown

128
00:06:17,600 --> 00:06:20,020
and Wisconsin and
others to make sure

129
00:06:20,020 --> 00:06:23,820
that they're only buying
their university logo apparel

130
00:06:23,820 --> 00:06:26,340
and athletic wear
from companies that

131
00:06:26,340 --> 00:06:31,110
have these kinds of just supply
chain practices in place.

132
00:06:31,110 --> 00:06:34,150
So we can do this, but
it takes a lot of energy,

133
00:06:34,150 --> 00:06:35,940
and a lot of collective effort.

134
00:06:35,940 --> 00:06:40,160
And it has to be done by all
the parties working together.

135
00:06:40,160 --> 00:06:41,810
Companies can't do it alone.

136
00:06:41,810 --> 00:06:43,960
NGOs can't do it alone.

137
00:06:43,960 --> 00:06:46,460
Local governments
can't do it alone.

138
00:06:46,460 --> 00:06:50,190
But if they all work together,
and build a sustained effort

139
00:06:50,190 --> 00:06:52,520
to improve the
conditions, and then

140
00:06:52,520 --> 00:06:55,750
produce the data that
makes it all transparent

141
00:06:55,750 --> 00:06:58,760
for us as consumers, we
can make a difference

142
00:06:58,760 --> 00:07:03,360
in improving the conditions
of very low wage workers

143
00:07:03,360 --> 00:07:06,040
in many developing countries.

144
00:07:06,040 --> 00:07:09,390
So I'd like to ask
you two questions.

145
00:07:09,390 --> 00:07:13,940
Apple, our computer
friends, and iPhone friends,

146
00:07:13,940 --> 00:07:17,600
and iPod friends are about
two decades behind Nike.

147
00:07:17,600 --> 00:07:21,960
They have experienced terrible
problems with their contractors

148
00:07:21,960 --> 00:07:23,490
in China and elsewhere.

149
00:07:23,490 --> 00:07:26,690
You may have heard about
Foxconn with suicides

150
00:07:26,690 --> 00:07:30,880
of workers frustrated with
terrible working conditions.

151
00:07:30,880 --> 00:07:33,960
If you were to write
to the CEO of Apple,

152
00:07:33,960 --> 00:07:37,550
what should Apple learn
from Nike's experiences?

153
00:07:37,550 --> 00:07:40,230
And then, more
broadly, what can you

154
00:07:40,230 --> 00:07:44,360
do to hold companies accountable
for their global operations?

155
00:07:44,360 --> 00:07:47,510
How can we take
responsibility for making sure

156
00:07:47,510 --> 00:07:52,110
that every shirt, every tie,
every sweatshirt that we buy

157
00:07:52,110 --> 00:07:56,050
is made in a just supply
chain, and keep asking,

158
00:07:56,050 --> 00:07:59,670
and be willing to pay for
products that are made

159
00:07:59,670 --> 00:08:01,410
under fair working conditions?

160
00:08:01,410 --> 00:08:03,810
We can do that
individually as consumers,

161
00:08:03,810 --> 00:08:08,130
and we can collectively set
the norms that essentially

162
00:08:08,130 --> 00:08:11,050
say, to any company, if
you want our business,

163
00:08:11,050 --> 00:08:14,360
then you've got to demonstrate
that you are providing

164
00:08:14,360 --> 00:08:17,500
safe and healthy working
conditions, and fair wages

165
00:08:17,500 --> 00:08:21,300
for people wherever they
make these products.