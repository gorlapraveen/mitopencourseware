1
00:00:17,070 --> 00:00:18,100
Welcome.

2
00:00:18,100 --> 00:00:21,950
You are about to do
something very special, which

3
00:00:21,950 --> 00:00:25,020
is to negotiate
the future of work.

4
00:00:25,020 --> 00:00:27,880
This video is
designed to help you

5
00:00:27,880 --> 00:00:30,330
know what you need
to know to navigate

6
00:00:30,330 --> 00:00:32,610
the software and the
tools and methods

7
00:00:32,610 --> 00:00:36,460
to help you be successful
in this process.

8
00:00:36,460 --> 00:00:40,010
What you see on the screen
are a series of slides.

9
00:00:40,010 --> 00:00:42,580
We'll spend more time with
some, less with others.

10
00:00:42,580 --> 00:00:46,530
All of them will be available
to you on the course website.

11
00:00:46,530 --> 00:00:48,580
Here's what you need to
know at the beginning.

12
00:00:48,580 --> 00:00:52,110
Once you agree to be part
of this negotiations,

13
00:00:52,110 --> 00:00:54,920
we want you to sign up
and we want you to commit.

14
00:00:54,920 --> 00:00:57,210
Because in the
negotiations there

15
00:00:57,210 --> 00:01:00,620
will be yourself and
three or four other people

16
00:01:00,620 --> 00:01:04,510
in appropriate roles, and
they'll all be depending on you

17
00:01:04,510 --> 00:01:06,670
to stay through to agreement.

18
00:01:06,670 --> 00:01:09,630
And, frankly, that's
how you'll all learn.

19
00:01:09,630 --> 00:01:10,690
What are the roles?

20
00:01:10,690 --> 00:01:13,650
The roles in the negotiation
match key stakeholders

21
00:01:13,650 --> 00:01:14,680
in society.

22
00:01:14,680 --> 00:01:18,500
Labor, management,
government, and education.

23
00:01:18,500 --> 00:01:21,420
And essentially, each of you
will be taking on one of these

24
00:01:21,420 --> 00:01:24,230
roles-- they'll be assigned
through the course platform--

25
00:01:24,230 --> 00:01:26,920
and that will tell
you who you're

26
00:01:26,920 --> 00:01:31,190
going to represent in figuring
out the future of work.

27
00:01:31,190 --> 00:01:33,350
Now, once you're
assigned to your role,

28
00:01:33,350 --> 00:01:37,890
you'll come to the website
platform for this negotiation.

29
00:01:37,890 --> 00:01:39,830
That's what you see
on the screen here.

30
00:01:39,830 --> 00:01:42,300
It, again, reiterates what
are the different roles

31
00:01:42,300 --> 00:01:45,060
in the exercise and
gives you some background

32
00:01:45,060 --> 00:01:46,410
about the assignment.

33
00:01:46,410 --> 00:01:48,640
And then for all
four of the roles--

34
00:01:48,640 --> 00:01:50,750
there's two here and two more--

35
00:01:50,750 --> 00:01:53,790
you'll see some
additional information

36
00:01:53,790 --> 00:01:56,060
to help you know
what you need to know

37
00:01:56,060 --> 00:01:58,524
to be effective in your role.

38
00:01:58,524 --> 00:01:59,940
You'll see there's
a Next button--

39
00:01:59,940 --> 00:02:01,523
I just checked it--
that will move you

40
00:02:01,523 --> 00:02:03,250
on to the next
screen, which is where

41
00:02:03,250 --> 00:02:06,250
you start to fill out some of
the key information we need.

42
00:02:06,250 --> 00:02:09,229
There's different universities
that are participating in this.

43
00:02:09,229 --> 00:02:11,030
Check which one
you're coming from.

44
00:02:11,030 --> 00:02:12,830
You will be assigned
a group number

45
00:02:12,830 --> 00:02:14,250
which you want to assign.

46
00:02:14,250 --> 00:02:16,440
And then there's other
demographic information

47
00:02:16,440 --> 00:02:19,120
that you'll fill
out on education,

48
00:02:19,120 --> 00:02:22,010
whether you've been a
member of a union, age,

49
00:02:22,010 --> 00:02:23,380
and other factors.

50
00:02:23,380 --> 00:02:28,430
They're all required except the
gender item which is optional.

51
00:02:28,430 --> 00:02:31,020
But again, once you
finish filling out

52
00:02:31,020 --> 00:02:33,620
those different items,
you'll click Next

53
00:02:33,620 --> 00:02:38,180
and you go on to a screen
that now asks you to tell us

54
00:02:38,180 --> 00:02:40,420
about your preferences
or priorities

55
00:02:40,420 --> 00:02:43,330
before negotiations begins.

56
00:02:43,330 --> 00:02:47,090
You'll see that there's
five different issues

57
00:02:47,090 --> 00:02:49,500
about workforce
capability, all of which

58
00:02:49,500 --> 00:02:51,660
could be subjects
of negotiation,

59
00:02:51,660 --> 00:02:55,220
and we want you to rank order
them from your top priority

60
00:02:55,220 --> 00:02:57,390
to your second, third,
fourth, and fifth.

61
00:02:57,390 --> 00:02:58,400
No ties here.

62
00:02:58,400 --> 00:03:01,480
You have to pick one,
two, three, four or five

63
00:03:01,480 --> 00:03:03,510
for each of the five items.

64
00:03:03,510 --> 00:03:06,500
There's also some questions
about representation,

65
00:03:06,500 --> 00:03:10,840
about organizational
performance, about compensation

66
00:03:10,840 --> 00:03:13,420
and rewards and
incentives for managers,

67
00:03:13,420 --> 00:03:16,660
as well as work family
kinds of issues.

68
00:03:16,660 --> 00:03:21,230
All of these are questions that
indicate your priorities going

69
00:03:21,230 --> 00:03:23,410
in, and, frankly,
that get you thinking

70
00:03:23,410 --> 00:03:27,220
about what's at stake as you
think about the future of work.

71
00:03:27,220 --> 00:03:29,530
Finally, before we go
on to the next screen,

72
00:03:29,530 --> 00:03:31,850
in each of the five
big categories--

73
00:03:31,850 --> 00:03:34,080
each of which had
five sub questions--

74
00:03:34,080 --> 00:03:36,190
we want you to rank
order those in terms

75
00:03:36,190 --> 00:03:39,220
of what's important in the
role that you're playing.

76
00:03:39,220 --> 00:03:43,220
And again, you'll press Next
and go on to the next screen.

77
00:03:43,220 --> 00:03:46,540
Here there's a couple of
short attitude questions

78
00:03:46,540 --> 00:03:48,910
about cooperation
and competition.

79
00:03:48,910 --> 00:03:52,450
Again, these will be quick and
they give us your attitudes

80
00:03:52,450 --> 00:03:55,560
before we begin the bargaining.

81
00:03:55,560 --> 00:03:56,660
That's the setup.

82
00:03:56,660 --> 00:03:58,650
Now we're getting
ready to bargain.

83
00:03:58,650 --> 00:04:01,100
You'll see that there's
a slide that tells you

84
00:04:01,100 --> 00:04:03,930
and a set of materials
about preparation.

85
00:04:03,930 --> 00:04:05,970
The key thing about
preparation is

86
00:04:05,970 --> 00:04:09,440
to not just think about your
own priorities, and frankly,

87
00:04:09,440 --> 00:04:12,910
your underlying interests, but
to think about the priorities

88
00:04:12,910 --> 00:04:13,870
and the interest--

89
00:04:13,870 --> 00:04:15,120
what's really at stake--

90
00:04:15,120 --> 00:04:17,470
for the people in
the other roles.

91
00:04:17,470 --> 00:04:21,890
And so the preparation
materials will get you up

92
00:04:21,890 --> 00:04:24,380
to speed on moving
through it quickly.

93
00:04:24,380 --> 00:04:27,790
But there's a lot here to
read that will help you.

94
00:04:27,790 --> 00:04:30,770
In negotiations, we say
the three things that

95
00:04:30,770 --> 00:04:33,920
are most important are
preparation, preparation

96
00:04:33,920 --> 00:04:34,920
and preparation.

97
00:04:34,920 --> 00:04:38,000
Just like in real estate,
location, location,

98
00:04:38,000 --> 00:04:38,740
and location.

99
00:04:38,740 --> 00:04:41,590
This time is time
that's well spent.

100
00:04:41,590 --> 00:04:44,580
At this point now you've
committed to a bargain,

101
00:04:44,580 --> 00:04:47,660
you've got your role, you've
done your preparation,

102
00:04:47,660 --> 00:04:50,340
and we're ready to
begin negotiations.

103
00:04:50,340 --> 00:04:52,730
I'm going to say a little
bit about the negotiations

104
00:04:52,730 --> 00:04:57,300
and then we're going to talk
about what happens afterwards.

105
00:04:57,300 --> 00:05:00,310
You'll see that there's a
link to the settlement form--

106
00:05:00,310 --> 00:05:04,710
which I'll show you in a minute,
but there's also, I'm sorry.

107
00:05:04,710 --> 00:05:07,440
There's also a page
that's not on here

108
00:05:07,440 --> 00:05:10,100
but that will come
on your screen that

109
00:05:10,100 --> 00:05:13,720
will let you print out all
of your preparation choices

110
00:05:13,720 --> 00:05:17,070
either in a PDF or
in a printed document

111
00:05:17,070 --> 00:05:19,540
so that you've got
that in front of you.

112
00:05:19,540 --> 00:05:22,040
When it comes to the
actual bargaining,

113
00:05:22,040 --> 00:05:25,230
I'm going to just give
you a very simple example.

114
00:05:25,230 --> 00:05:27,790
Let's say in the
opening statement--

115
00:05:27,790 --> 00:05:30,120
and you'll all make
opening statements--

116
00:05:30,120 --> 00:05:32,190
and this is, of course,
virtual negotiations

117
00:05:32,190 --> 00:05:35,190
so you'll be writing it
out online for the others

118
00:05:35,190 --> 00:05:36,800
in your group to see--

119
00:05:36,800 --> 00:05:40,010
the folks who are in the
worker's role might say,

120
00:05:40,010 --> 00:05:41,430
there ought to be
more government

121
00:05:41,430 --> 00:05:43,120
grants for skills training.

122
00:05:43,120 --> 00:05:46,350
The person in the
government role may say,

123
00:05:46,350 --> 00:05:49,340
we're willing to provide some
funding but only in areas that

124
00:05:49,340 --> 00:05:51,620
are skill shortages in society.

125
00:05:51,620 --> 00:05:54,810
Management might say,
yes, but the grant should

126
00:05:54,810 --> 00:05:57,920
be to the employer since they
know what the skills are not

127
00:05:57,920 --> 00:05:59,600
just to the individuals.

128
00:05:59,600 --> 00:06:01,930
The education
providers say, this

129
00:06:01,930 --> 00:06:05,220
needs to be part of an
integrated education system

130
00:06:05,220 --> 00:06:09,060
and so these funds for
curriculum development

131
00:06:09,060 --> 00:06:11,500
ought to come to the
educational institutions

132
00:06:11,500 --> 00:06:15,210
so that they can provide the
new training that's needed.

133
00:06:15,210 --> 00:06:17,640
Government may say,
that's all great

134
00:06:17,640 --> 00:06:21,450
but, we need to have
a funding mechanism.

135
00:06:21,450 --> 00:06:24,870
Let's say, $.01 per hour
for people that work.

136
00:06:24,870 --> 00:06:27,530
Management, as you see
here, might say, well,

137
00:06:27,530 --> 00:06:29,270
we're education
providers so we should

138
00:06:29,270 --> 00:06:31,430
be able to get
development funds too.

139
00:06:31,430 --> 00:06:34,760
Ultimately, the management,
the workers, and the education

140
00:06:34,760 --> 00:06:37,520
providers might come
together and say,

141
00:06:37,520 --> 00:06:40,370
we need to provide governance
and insight and input

142
00:06:40,370 --> 00:06:42,340
into this government initiative.

143
00:06:42,340 --> 00:06:46,120
And together they might then
outline what an agreement is.

144
00:06:46,120 --> 00:06:49,800
But as you can see, there's a
series of iterative proposals

145
00:06:49,800 --> 00:06:53,420
and counter proposals that are
all part of the negotiations.

146
00:06:53,420 --> 00:06:56,660
And in the end, we're not
looking for a simple sentence,

147
00:06:56,660 --> 00:06:59,750
we had an agreement on
government funded education.

148
00:06:59,750 --> 00:07:01,890
We're really looking for
two or three paragraphs

149
00:07:01,890 --> 00:07:04,630
that says who does what,
what are they agreeing to,

150
00:07:04,630 --> 00:07:08,530
how will it be funded, and
what are the outcomes involved.

151
00:07:08,530 --> 00:07:14,310
Now, we've done this many times,
and it turns out this issue--

152
00:07:14,310 --> 00:07:16,350
support for worker training--

153
00:07:16,350 --> 00:07:18,480
is a common issue
that people pick.

154
00:07:18,480 --> 00:07:23,730
We want you to push the
envelope about what's needed

155
00:07:23,730 --> 00:07:26,590
in the future of work, so we're
going to say that at least

156
00:07:26,590 --> 00:07:30,480
for your first two agreements
they cannot be agreements that

157
00:07:30,480 --> 00:07:34,180
are on this issue of
government funded education,

158
00:07:34,180 --> 00:07:37,900
but on other issues that are
among the many 25 issues that

159
00:07:37,900 --> 00:07:40,250
we've highlighted
in five categories.

160
00:07:40,250 --> 00:07:42,740
And then if you want
to come back to this

161
00:07:42,740 --> 00:07:44,680
as part of a package, great.

162
00:07:44,680 --> 00:07:48,910
But the point is, push the
envelope, do something new.

163
00:07:48,910 --> 00:07:50,960
Once you have an
agreement you'll

164
00:07:50,960 --> 00:07:53,060
then follow the URL
to a screen that

165
00:07:53,060 --> 00:07:54,340
looks like what you see here.

166
00:07:54,340 --> 00:07:56,050
It says it's part two.

167
00:07:56,050 --> 00:07:59,020
And again, it'll ask you what
group number you're at, what

168
00:07:59,020 --> 00:08:00,750
institution are you doing this.

169
00:08:00,750 --> 00:08:04,330
And then you'll find that
there's a question about what

170
00:08:04,330 --> 00:08:07,970
role are you in, how many people
are part of this agreement,

171
00:08:07,970 --> 00:08:10,360
and what were their roles?

172
00:08:10,360 --> 00:08:13,550
So obviously, if you are
an education institution,

173
00:08:13,550 --> 00:08:16,940
you would check management,
labor, and federal government

174
00:08:16,940 --> 00:08:19,970
and leave education
blank on question five.

175
00:08:19,970 --> 00:08:22,460
But at that point you
go to the next screen

176
00:08:22,460 --> 00:08:25,520
and this is your chance to
indicate what you actually

177
00:08:25,520 --> 00:08:26,500
agreed to.

178
00:08:26,500 --> 00:08:30,040
You'll click, let's say,
fair treatment and a screen

179
00:08:30,040 --> 00:08:34,110
will open up saying, write
into this what you agreed upon.

180
00:08:34,110 --> 00:08:35,789
This is important.

181
00:08:35,789 --> 00:08:40,049
Each of you will be entering
the agreement separately.

182
00:08:40,049 --> 00:08:42,140
So if there's four people
in the negotiations,

183
00:08:42,140 --> 00:08:44,370
there will be four
entries of the text.

184
00:08:44,370 --> 00:08:46,650
It's perfectly OK
for you to each cut

185
00:08:46,650 --> 00:08:49,570
and splice the same language.

186
00:08:49,570 --> 00:08:51,360
But the key thing--
and by the way,

187
00:08:51,360 --> 00:08:53,370
if you put in
different language,

188
00:08:53,370 --> 00:08:56,470
that's evidence that it might
not be a very stable agreement.

189
00:08:56,470 --> 00:08:59,910
But the point is, you
each put in your language

190
00:08:59,910 --> 00:09:04,160
because we also ask you to
each rate how important is this

191
00:09:04,160 --> 00:09:07,590
in terms of your role
and how satisfied are you

192
00:09:07,590 --> 00:09:09,630
with the process, not
just the substance,

193
00:09:09,630 --> 00:09:11,100
to get to this agreement.

194
00:09:11,100 --> 00:09:13,300
And this way we can
hear from each of you

195
00:09:13,300 --> 00:09:14,477
in the negotiations.

196
00:09:14,477 --> 00:09:16,060
And of course, there's
a space for you

197
00:09:16,060 --> 00:09:19,030
to say if there's anything
else that you want to put.

198
00:09:19,030 --> 00:09:21,450
And that completes the process.

199
00:09:21,450 --> 00:09:24,600
There's a link here that
will let you come back

200
00:09:24,600 --> 00:09:26,940
and enter a second
or third agreement.

201
00:09:26,940 --> 00:09:28,750
And when you're done--

202
00:09:28,750 --> 00:09:31,810
when you're done-- you will
have done something very special

203
00:09:31,810 --> 00:09:34,270
which is you will have
negotiated an agreement

204
00:09:34,270 --> 00:09:36,400
about the future of work.

205
00:09:36,400 --> 00:09:40,710
People who are in positions
of power and authority

206
00:09:40,710 --> 00:09:42,590
are interested to
see what comes out

207
00:09:42,590 --> 00:09:44,800
of the negotiations
in this course.

208
00:09:44,800 --> 00:09:47,110
We will try to sum
up the agreements

209
00:09:47,110 --> 00:09:50,360
and look for insights and
lessons around which there

210
00:09:50,360 --> 00:09:52,670
might be the kind of
alignment that could actually

211
00:09:52,670 --> 00:09:55,780
change things in
society and ensure

212
00:09:55,780 --> 00:09:59,170
that the institutions of
work are better designed

213
00:09:59,170 --> 00:10:01,790
to serve the needs of
yourself and others

214
00:10:01,790 --> 00:10:03,620
in the future generations.

215
00:10:03,620 --> 00:10:05,880
Thank you very much.