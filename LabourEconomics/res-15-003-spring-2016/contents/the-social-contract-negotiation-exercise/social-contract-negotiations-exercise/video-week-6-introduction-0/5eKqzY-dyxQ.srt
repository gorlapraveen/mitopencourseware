1
00:00:05,480 --> 00:00:07,270
Welcome to our week six.

2
00:00:07,270 --> 00:00:11,190
It's time to negotiate the next
generation social contract.

3
00:00:11,190 --> 00:00:14,540
So I'm looking forward to a
really exciting and fruitful

4
00:00:14,540 --> 00:00:15,320
week.

5
00:00:15,320 --> 00:00:20,060
All the instructions are now
on the platform on week six,

6
00:00:20,060 --> 00:00:22,270
so all you need to
do is to go to it.

7
00:00:22,270 --> 00:00:26,550
You'll see instructions
for how to find your team,

8
00:00:26,550 --> 00:00:28,430
your role assignments.

9
00:00:28,430 --> 00:00:32,650
Then you go through, and
you'll complete the preparation

10
00:00:32,650 --> 00:00:36,700
survey, you'll negotiate
online with your counterparts,

11
00:00:36,700 --> 00:00:39,970
hopefully in the same time
zone where you are located.

12
00:00:39,970 --> 00:00:43,710
And then as you complete
the survey form at the end,

13
00:00:43,710 --> 00:00:45,990
to record your
settlements, those of you

14
00:00:45,990 --> 00:00:50,090
who are taking the course for
certification and for a grade

15
00:00:50,090 --> 00:00:53,640
will get some extra credit
as described on the platform.

16
00:00:53,640 --> 00:00:56,030
So we're excited about it.

17
00:00:56,030 --> 00:00:59,020
We're looking forward to
hearing what you have in mind

18
00:00:59,020 --> 00:01:01,970
for shaping the future of work.

19
00:01:01,970 --> 00:01:04,819
As a bonus, we will
provide a facilitator.

20
00:01:04,819 --> 00:01:09,560
One of our MBA students, what
we call our community TAs,

21
00:01:09,560 --> 00:01:11,560
will be assigned to your group.

22
00:01:11,560 --> 00:01:13,100
What is a facilitator?

23
00:01:13,100 --> 00:01:15,890
A facilitator is just what
that term sounds like.

24
00:01:15,890 --> 00:01:18,400
It's a neutral
person, it's a person

25
00:01:18,400 --> 00:01:20,880
who helps move you
through the process

26
00:01:20,880 --> 00:01:23,520
without influencing
your proposals

27
00:01:23,520 --> 00:01:27,370
or the particular terms of
the agreement that you reach.

28
00:01:27,370 --> 00:01:31,930
So this individual will
help you in scheduling times

29
00:01:31,930 --> 00:01:35,080
or answering questions
and be a liaison back

30
00:01:35,080 --> 00:01:36,800
to the rest of us as needed.

31
00:01:36,800 --> 00:01:39,940
So use the facilitator
at your choice,

32
00:01:39,940 --> 00:01:42,470
and we'll see how
all of this works.

33
00:01:42,470 --> 00:01:49,640
The deadline is Thursday at the
end of the day UTC time 11:59

34
00:01:49,640 --> 00:01:50,390
PM.

35
00:01:50,390 --> 00:01:53,910
So it's important that
you and your teammates

36
00:01:53,910 --> 00:01:56,840
work on an agreement
on when to negotiate,

37
00:01:56,840 --> 00:02:00,570
engage in the course
discussion as instructed.

38
00:02:00,570 --> 00:02:06,500
And we'll see what we
learn from this experience.

39
00:02:06,500 --> 00:02:09,449
Next week is the grand
finale of this course,

40
00:02:09,449 --> 00:02:11,009
and we have a special event.

41
00:02:11,009 --> 00:02:14,100
It's called a live video
event or video chat,

42
00:02:14,100 --> 00:02:15,690
whatever one wants to call it.

43
00:02:15,690 --> 00:02:20,560
On Tuesday, May 10th
at UTC time 8:00 PM,

44
00:02:20,560 --> 00:02:23,560
we will have an hour and
a half live interaction

45
00:02:23,560 --> 00:02:26,290
with all of you, where
we will summarize

46
00:02:26,290 --> 00:02:30,020
what we have taken away as the
key lessons from this course.

47
00:02:30,020 --> 00:02:32,950
And we'll incorporate
the materials

48
00:02:32,950 --> 00:02:35,300
not only from the
course but also

49
00:02:35,300 --> 00:02:39,740
your materials from the
polls that you completed,

50
00:02:39,740 --> 00:02:43,960
the surveys, the assignments
that you did, the discussions

51
00:02:43,960 --> 00:02:47,940
that you've had on the forum,
all of this will come together.

52
00:02:47,940 --> 00:02:50,330
And with your
engagement, directly

53
00:02:50,330 --> 00:02:53,510
by passing on questions
to us or comments

54
00:02:53,510 --> 00:02:55,660
as we go along, for
an hour and a half

55
00:02:55,660 --> 00:02:59,140
we will see what did we learn
from this, where are we going,

56
00:02:59,140 --> 00:03:01,430
and how can we use
all of this material

57
00:03:01,430 --> 00:03:03,770
effectively, not
only for yourselves,

58
00:03:03,770 --> 00:03:07,460
but for external audiences who
are also interested in shaping

59
00:03:07,460 --> 00:03:08,750
the future of work.

60
00:03:08,750 --> 00:03:11,310
So please put this
on your calendar,

61
00:03:11,310 --> 00:03:13,600
be ready with
questions or comments.

62
00:03:13,600 --> 00:03:15,900
We'll send you
information next Monday,

63
00:03:15,900 --> 00:03:17,970
the day before
this event, on how

64
00:03:17,970 --> 00:03:21,930
to tune in, where to link
in for this interesting,

65
00:03:21,930 --> 00:03:25,320
I hope interesting,
and informative wrap-up

66
00:03:25,320 --> 00:03:27,730
for the course.

67
00:03:27,730 --> 00:03:29,770
Let me make a special
request of you

68
00:03:29,770 --> 00:03:33,250
to make sure that we bring
your voices into this process,

69
00:03:33,250 --> 00:03:36,900
one segment of the summary
will ask the question

70
00:03:36,900 --> 00:03:40,920
"What is our key message
to the future leaders who

71
00:03:40,920 --> 00:03:42,810
will shape the future of work?"

72
00:03:42,810 --> 00:03:45,430
Business leaders and
HR professionals,

73
00:03:45,430 --> 00:03:47,540
labor leaders and
their advocates,

74
00:03:47,540 --> 00:03:50,260
government policymakers,
and those of us

75
00:03:50,260 --> 00:03:51,720
in the education field.

76
00:03:51,720 --> 00:03:55,650
So make sure that, on the
discussion board this week,

77
00:03:55,650 --> 00:03:58,990
where we will post
questions or a place

78
00:03:58,990 --> 00:04:02,190
where you can add your thoughts
to each of these stakeholders,

79
00:04:02,190 --> 00:04:04,540
that you bring your
voice into this process.

80
00:04:04,540 --> 00:04:08,400
And we'll incorporate that
into the discussion next week,

81
00:04:08,400 --> 00:04:08,900
Tuesday.

82
00:04:08,900 --> 00:04:12,400
So I look forward to seeing
what you have on your mind

83
00:04:12,400 --> 00:04:14,440
and what message
you want to deliver

84
00:04:14,440 --> 00:04:18,440
to these groups who will help
to shape the future of work.

85
00:04:18,440 --> 00:04:23,510
Last week we had a very, very
rich, and engaging discussion,

86
00:04:23,510 --> 00:04:26,210
and, in fact, throughout
this course, many of you

87
00:04:26,210 --> 00:04:28,740
commented on the
subject for last week

88
00:04:28,740 --> 00:04:31,160
and that is: "What's
the future of Labor?"

89
00:04:31,160 --> 00:04:34,180
Is there still a need
for labor organizations,

90
00:04:34,180 --> 00:04:36,710
for unions as we've called
them over many years,

91
00:04:36,710 --> 00:04:39,530
and in fact over
several centuries?

92
00:04:39,530 --> 00:04:42,580
Is there a new form
of representation?

93
00:04:42,580 --> 00:04:46,070
Are there new ways in which
workers can get a voice at work

94
00:04:46,070 --> 00:04:49,910
and assert their interests
and work with their employers

95
00:04:49,910 --> 00:04:52,420
to shape the future of work?

96
00:04:52,420 --> 00:04:55,660
Most of you do see
the need for some form

97
00:04:55,660 --> 00:04:59,580
of labor representation,
maybe not for everyone,

98
00:04:59,580 --> 00:05:02,210
but for the people
who really want

99
00:05:02,210 --> 00:05:04,860
to have a collective
voice at the workplace.

100
00:05:04,860 --> 00:05:08,170
But you also see the need for
change, and for innovation,

101
00:05:08,170 --> 00:05:09,880
to build on the
strengths of what

102
00:05:09,880 --> 00:05:12,210
unions have done
historically for so

103
00:05:12,210 --> 00:05:13,910
many workers around the world.

104
00:05:13,910 --> 00:05:16,160
You talked about
the need to expand

105
00:05:16,160 --> 00:05:17,840
more labor-management
partnerships,

106
00:05:17,840 --> 00:05:20,910
like the one out at Kaiser
Permanente that we discussed.

107
00:05:20,910 --> 00:05:24,530
Many of you focused, in
a very rich dialogue,

108
00:05:24,530 --> 00:05:27,250
over the need for what
you call digital unions.

109
00:05:27,250 --> 00:05:31,700
That is using information
technology, social media,

110
00:05:31,700 --> 00:05:35,640
and related mechanisms
to convey your ideas

111
00:05:35,640 --> 00:05:38,620
and to communicate with
each other across the world.

112
00:05:38,620 --> 00:05:41,980
And indeed you talked about
the need for global unions,

113
00:05:41,980 --> 00:05:45,970
because obviously today,
where people work, how people

114
00:05:45,970 --> 00:05:49,040
are affected by jobs
that are created

115
00:05:49,040 --> 00:05:53,890
by companies around the world,
is not simply a national issue.

116
00:05:53,890 --> 00:05:55,310
Some of you talked
about the need

117
00:05:55,310 --> 00:05:58,990
to return to these interesting
organizations called guilds,

118
00:05:58,990 --> 00:06:02,050
where workers came
together cooperatively

119
00:06:02,050 --> 00:06:06,010
to enhance their profession
or their particular occupation

120
00:06:06,010 --> 00:06:09,410
and to work to enhance
their professionalism.

121
00:06:09,410 --> 00:06:12,230
You talked about
the need for apps

122
00:06:12,230 --> 00:06:15,030
to share information
on where good jobs are

123
00:06:15,030 --> 00:06:17,240
and good employers
are, along the lines

124
00:06:17,240 --> 00:06:21,230
that we used in the good
jobs survey that some of you

125
00:06:21,230 --> 00:06:21,820
have done.

126
00:06:21,820 --> 00:06:24,060
And then many of you
talked about the need

127
00:06:24,060 --> 00:06:28,620
for new forms of advocates
for low-wage workers

128
00:06:28,620 --> 00:06:31,590
in the United States, in
Europe, in developing countries

129
00:06:31,590 --> 00:06:33,700
all around the
world, particularly

130
00:06:33,700 --> 00:06:37,260
focusing on the role that
NGOs are now playing.

131
00:06:37,260 --> 00:06:39,950
So I think the bottom
line of all of this:

132
00:06:39,950 --> 00:06:42,460
don't just observe these trends.

133
00:06:42,460 --> 00:06:44,600
Get involved,
whether it's working

134
00:06:44,600 --> 00:06:48,230
on the side of
advocates for labor,

135
00:06:48,230 --> 00:06:51,170
whether it's enriching
and strengthening

136
00:06:51,170 --> 00:06:54,390
the role that HR plays
in our organizations,

137
00:06:54,390 --> 00:06:57,630
whether it's thinking
about the future of labor

138
00:06:57,630 --> 00:07:00,220
policy in our countries,
or whether you're

139
00:07:00,220 --> 00:07:02,710
like me working in
the education sphere,

140
00:07:02,710 --> 00:07:06,310
either on the university
level with the outside world,

141
00:07:06,310 --> 00:07:11,440
all of you in this
global-based online course,

142
00:07:11,440 --> 00:07:14,870
or are school students in high
schools and community colleges.

143
00:07:14,870 --> 00:07:16,590
We all have a role to play.

144
00:07:16,590 --> 00:07:19,570
We've emphasized that
throughout this course, now

145
00:07:19,570 --> 00:07:21,800
it's time for us to
put our ideas to work.

146
00:07:21,800 --> 00:07:24,510
So I look forward to a
very invigorating week

147
00:07:24,510 --> 00:07:26,270
of negotiations.

148
00:07:26,270 --> 00:07:29,330
We'll summarize the
settlements that

149
00:07:29,330 --> 00:07:32,990
are achieved next week
when we come together,

150
00:07:32,990 --> 00:07:36,260
and we'll see if we can't
really summarize and bring

151
00:07:36,260 --> 00:07:38,120
together what did
we learn, what are

152
00:07:38,120 --> 00:07:40,430
the key takeaways
from our efforts

153
00:07:40,430 --> 00:07:43,130
to shape the future of work.