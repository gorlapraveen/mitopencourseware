1
00:00:05,160 --> 00:00:08,070
We're speaking today with Heidi
Shierholz, the chief economist

2
00:00:08,070 --> 00:00:09,810
at the US Department of Labor.

3
00:00:09,810 --> 00:00:14,630
Heidi follows the job trends
and occupational opportunities

4
00:00:14,630 --> 00:00:15,690
around the country.

5
00:00:15,690 --> 00:00:17,160
And so this is a
good opportunity

6
00:00:17,160 --> 00:00:19,650
to discuss where some
of the good jobs are.

7
00:00:19,650 --> 00:00:21,470
So Heidi, thank you
for joining us today.

8
00:00:21,470 --> 00:00:22,178
It's my pleasure.

9
00:00:22,178 --> 00:00:24,140
I wonder if you could
start by telling

10
00:00:24,140 --> 00:00:27,200
us what do you see as
some of the good job

11
00:00:27,200 --> 00:00:30,800
opportunities for young people
entering the labor force today?

12
00:00:30,800 --> 00:00:33,200
I think it's important
to know that there

13
00:00:33,200 --> 00:00:35,060
are a lot of good
jobs being created.

14
00:00:35,060 --> 00:00:36,660
There are a lot of
good opportunities.

15
00:00:36,660 --> 00:00:39,790
And if you look, our
Bureau of Labor Statistics

16
00:00:39,790 --> 00:00:41,520
does these employment
projections,

17
00:00:41,520 --> 00:00:44,160
where you can look and
see what kind of jobs

18
00:00:44,160 --> 00:00:46,770
are growing strongly
for the next 10 years,

19
00:00:46,770 --> 00:00:49,360
what kind of wages
will those jobs have.

20
00:00:49,360 --> 00:00:51,447
And there's some
trends that come out

21
00:00:51,447 --> 00:00:52,530
of that, that you can see.

22
00:00:52,530 --> 00:00:54,970
So some of the
fastest growing jobs

23
00:00:54,970 --> 00:00:58,140
are jobs in the health
sector, particularly nurses--

24
00:00:58,140 --> 00:01:00,820
really strong wages and
really strong job growth.

25
00:01:00,820 --> 00:01:03,680
We also see good job
growth in the tech sector,

26
00:01:03,680 --> 00:01:06,660
so software engineering,
software development,

27
00:01:06,660 --> 00:01:09,200
those kinds of jobs
are growing quickly.

28
00:01:09,200 --> 00:01:13,600
You also see projected
growth in some jobs

29
00:01:13,600 --> 00:01:15,230
in professional and
business services

30
00:01:15,230 --> 00:01:18,550
like accountant jobs
and auditing jobs,

31
00:01:18,550 --> 00:01:20,200
sort of those old standbys.

32
00:01:20,200 --> 00:01:22,930
We're going to always need
them, and they're really

33
00:01:22,930 --> 00:01:24,210
decent paying jobs.

34
00:01:24,210 --> 00:01:26,570
So that's at the high
end of the labor force.

35
00:01:26,570 --> 00:01:29,242
How about at the middle
tier of the labor force.

36
00:01:29,242 --> 00:01:30,200
What's happening there?

37
00:01:30,200 --> 00:01:32,580
One of the phenomenons
that we're seeing there

38
00:01:32,580 --> 00:01:34,860
is there's this
big group of people

39
00:01:34,860 --> 00:01:37,630
who are hitting retirement
age, the baby boomers.

40
00:01:37,630 --> 00:01:40,020
And as that continues
to happen, there's

41
00:01:40,020 --> 00:01:42,550
going to be a lot of
job turnover in some

42
00:01:42,550 --> 00:01:45,800
of these really good
middle class jobs

43
00:01:45,800 --> 00:01:53,460
like carpenter jobs, electrician
jobs, utility workers, welders.

44
00:01:53,460 --> 00:01:57,200
Those kinds of jobs
that historically people

45
00:01:57,200 --> 00:01:59,230
got a lot of training
in those kind of jobs

46
00:01:59,230 --> 00:02:02,620
through their unions and
apprenticeship programs.

47
00:02:02,620 --> 00:02:04,790
As we have this
group of people that

48
00:02:04,790 --> 00:02:08,280
was trained in that way
hitting retirement age,

49
00:02:08,280 --> 00:02:10,380
we have to really
figure out how we're

50
00:02:10,380 --> 00:02:11,940
going to refill those jobs.

51
00:02:11,940 --> 00:02:14,440
One of the things the
Department of Labor is doing

52
00:02:14,440 --> 00:02:17,760
is really doing everything we
can to support apprenticeship

53
00:02:17,760 --> 00:02:19,900
programs so they can
help train up people

54
00:02:19,900 --> 00:02:21,900
to get into those jobs,
because those are really

55
00:02:21,900 --> 00:02:25,380
good middle class jobs that
are going to be very in demand

56
00:02:25,380 --> 00:02:26,790
going forward.

57
00:02:26,790 --> 00:02:28,690
Well that's great,
because apprenticeships

58
00:02:28,690 --> 00:02:31,820
have such a positive
record, so rebuilding those

59
00:02:31,820 --> 00:02:34,630
is so critical to our economy
and the future workforce.

60
00:02:34,630 --> 00:02:38,130
Research shows apprenticeship
has the highest rate of return

61
00:02:38,130 --> 00:02:41,780
as far as investment in workers,
what pays off for the economy,

62
00:02:41,780 --> 00:02:42,950
for their future earnings.

63
00:02:42,950 --> 00:02:46,390
So it's a really good
route for our country

64
00:02:46,390 --> 00:02:49,460
to invest in and for people to
take advantage of as they're

65
00:02:49,460 --> 00:02:51,500
thinking about what
their careers are going

66
00:02:51,500 --> 00:02:53,640
to look like going forward.

67
00:02:53,640 --> 00:02:55,610
And what about at
the lower end of

68
00:02:55,610 --> 00:02:57,270
the occupational distribution?

69
00:02:57,270 --> 00:02:59,920
The projections show
that we are expected

70
00:02:59,920 --> 00:03:02,970
to add a lot of jobs
that, at this point,

71
00:03:02,970 --> 00:03:06,770
are low wage jobs: home
health aides, food prep

72
00:03:06,770 --> 00:03:11,110
workers in restaurants and bars,
child care workers, janitors,

73
00:03:11,110 --> 00:03:12,530
maids and housekeeping cleaners.

74
00:03:12,530 --> 00:03:15,140
So some of these
lower end jobs are

75
00:03:15,140 --> 00:03:18,050
projected to see strong
growth going forward.

76
00:03:18,050 --> 00:03:21,680
So I think that's a really
important segment of the labor

77
00:03:21,680 --> 00:03:23,080
market to think about.

78
00:03:23,080 --> 00:03:25,320
And that's where
policy can come in

79
00:03:25,320 --> 00:03:28,190
to help make sure that
those are better jobs.

80
00:03:28,190 --> 00:03:32,440
So things like making sure we
have a decent minimum wage--

81
00:03:32,440 --> 00:03:33,950
increasing the minimum wage.

82
00:03:33,950 --> 00:03:37,270
Making sure people have
overtime protections.

83
00:03:37,270 --> 00:03:39,430
Making sure that
people have paid leave.

84
00:03:39,430 --> 00:03:42,470
The president just
announced initiatives

85
00:03:42,470 --> 00:03:45,430
to bolster paid leave
in the United States.

86
00:03:45,430 --> 00:03:50,390
Those kinds of policies
can help ensure

87
00:03:50,390 --> 00:03:53,480
that we have high
quality jobs for people

88
00:03:53,480 --> 00:03:56,220
at all levels of
education and training.

89
00:03:56,220 --> 00:03:58,530
Certainly important
that we really

90
00:03:58,530 --> 00:04:00,640
help to improve
those low wage jobs

91
00:04:00,640 --> 00:04:03,750
and provide career opportunities
for growth, and so on.

92
00:04:03,750 --> 00:04:05,720
What else would you
say that young people

93
00:04:05,720 --> 00:04:09,700
need to do to prepare for
the good jobs of the future?

94
00:04:09,700 --> 00:04:13,080
Thinking about investments in
your own training and education

95
00:04:13,080 --> 00:04:16,990
over the course of your
career is a really smart idea.

96
00:04:16,990 --> 00:04:18,910
One of the things
people should be

97
00:04:18,910 --> 00:04:22,620
doing is asking their employers
for what opportunities there

98
00:04:22,620 --> 00:04:24,920
are for advancement,
asking your union

99
00:04:24,920 --> 00:04:29,510
what can you do to help support
these kind of career ladders,

100
00:04:29,510 --> 00:04:33,540
looking into things like joint
union and management programs

101
00:04:33,540 --> 00:04:35,520
that support these
kind of career ladders.

102
00:04:35,520 --> 00:04:37,650
For example, there's
programs that

103
00:04:37,650 --> 00:04:41,130
will help people go from a
nursing assistant, to an LPN,

104
00:04:41,130 --> 00:04:42,580
to a registered nurse.

105
00:04:42,580 --> 00:04:45,620
And if you can
think of supporting

106
00:04:45,620 --> 00:04:48,120
those kinds of
trajectories, you can really

107
00:04:48,120 --> 00:04:51,230
carve out a career
that really let's

108
00:04:51,230 --> 00:04:54,190
you punch the ticket
to the middle class.

109
00:04:54,190 --> 00:04:56,520
I think that's good advice.

110
00:04:56,520 --> 00:04:59,200
Young people have
lots of opportunities

111
00:04:59,200 --> 00:05:01,770
and they have to
create new ones along

112
00:05:01,770 --> 00:05:03,789
with the partnership of
the Department of Labor,

113
00:05:03,789 --> 00:05:05,830
and good employers, and
good labor organizations.

114
00:05:05,830 --> 00:05:07,210
So thank you very much.

115
00:05:07,210 --> 00:05:09,520
It's been my pleasure.