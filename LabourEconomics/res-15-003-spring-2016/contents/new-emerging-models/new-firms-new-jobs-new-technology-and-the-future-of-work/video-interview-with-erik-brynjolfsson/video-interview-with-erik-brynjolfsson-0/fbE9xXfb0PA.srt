1
00:00:06,950 --> 00:00:09,910
We're speaking today with Eric
Brynjolfsson, a professor here

2
00:00:09,910 --> 00:00:11,950
at MIT Sloan School
of Management,

3
00:00:11,950 --> 00:00:15,520
the director of the MIT
Initiative on the Digital

4
00:00:15,520 --> 00:00:18,520
Economy, and co-author
of a bestselling book,

5
00:00:18,520 --> 00:00:21,510
The Second Machine
Age, a book that really

6
00:00:21,510 --> 00:00:23,270
has gotten the
conversation going

7
00:00:23,270 --> 00:00:27,290
about how important
today's digital economy is,

8
00:00:27,290 --> 00:00:29,480
how the technologies
that are coming along

9
00:00:29,480 --> 00:00:32,350
are going to have an enormous
effect on the future of work.

10
00:00:32,350 --> 00:00:34,260
So Eric, thank you
for joining us.

11
00:00:34,260 --> 00:00:35,260
It's a real pleasure.

12
00:00:35,260 --> 00:00:35,770
Good.

13
00:00:35,770 --> 00:00:36,810
Let's get started.

14
00:00:36,810 --> 00:00:39,880
Why don't you tell
us why you think

15
00:00:39,880 --> 00:00:43,780
the current technological
wave of innovation

16
00:00:43,780 --> 00:00:47,530
is as important as the
first Industrial Revolution,

17
00:00:47,530 --> 00:00:50,035
with the steam engine and all
the things that went with it.

18
00:00:50,035 --> 00:00:51,410
Well, you know,
technologies make

19
00:00:51,410 --> 00:00:53,576
a huge difference in the
living standards of people.

20
00:00:53,576 --> 00:00:56,010
For centuries, living standards
were essentially stagnant

21
00:00:56,010 --> 00:00:58,870
until James Watt and others
developed a better steam

22
00:00:58,870 --> 00:01:01,340
engine, and that ignited
the Industrial Revolution.

23
00:01:01,340 --> 00:01:03,330
And ever since then,
living standards

24
00:01:03,330 --> 00:01:07,710
have been growing about 2% per
year, which adds up to a lot.

25
00:01:07,710 --> 00:01:09,810
What those
technologies really did

26
00:01:09,810 --> 00:01:12,650
was they augmented
and automated a lot

27
00:01:12,650 --> 00:01:17,280
of muscle power from humans
and animals to machines.

28
00:01:17,280 --> 00:01:19,320
What we're seeing
now is that machines

29
00:01:19,320 --> 00:01:21,400
are beginning to be able
to do the same thing

30
00:01:21,400 --> 00:01:23,370
for our brains and our minds.

31
00:01:23,370 --> 00:01:26,240
We have computers and
software and big data

32
00:01:26,240 --> 00:01:29,300
that are learning how to
help us make decisions,

33
00:01:29,300 --> 00:01:31,490
how to extend our
mental capacity.

34
00:01:31,490 --> 00:01:36,260
And that's at least as profound
a difference as what the steam

35
00:01:36,260 --> 00:01:38,230
engine and, later, the
internal combustion

36
00:01:38,230 --> 00:01:40,650
engine and other technologies
did for our muscles.

37
00:01:40,650 --> 00:01:46,040
Well, it's very clear that
technological changes like this

38
00:01:46,040 --> 00:01:49,650
are very good for the economy,
but they also affect work.

39
00:01:49,650 --> 00:01:53,150
Tell us a little bit about
how technologies affect

40
00:01:53,150 --> 00:01:55,970
work in what ways-- in positive
ways, in negative ways--

41
00:01:55,970 --> 00:01:57,521
and then we can
go on from there.

42
00:01:57,521 --> 00:01:58,020
Sure.

43
00:01:58,020 --> 00:01:59,510
Well, the big headline
is the first one

44
00:01:59,510 --> 00:02:01,500
you mentioned, that the
pie is getting bigger.

45
00:02:01,500 --> 00:02:03,830
And so we have to
keep sight of that,

46
00:02:03,830 --> 00:02:06,040
that basically these are
technologies that create

47
00:02:06,040 --> 00:02:08,090
more abundance, more wealth.

48
00:02:08,090 --> 00:02:10,990
And there's more wealth now than
there ever has been in history,

49
00:02:10,990 --> 00:02:12,690
and global poverty
is going down.

50
00:02:12,690 --> 00:02:13,960
So that's the good news.

51
00:02:13,960 --> 00:02:16,400
But the reality is that
there's no economic law that

52
00:02:16,400 --> 00:02:19,030
says that everyone's going
to benefit evenly from this.

53
00:02:19,030 --> 00:02:22,210
It's possible for some
people to be made worse off.

54
00:02:22,210 --> 00:02:24,219
In fact, there's
no economic reason

55
00:02:24,219 --> 00:02:26,010
that you couldn't have
a majority of people

56
00:02:26,010 --> 00:02:29,654
be made worse off, even as other
people get much, much better.

57
00:02:29,654 --> 00:02:31,070
Through most of
history, there was

58
00:02:31,070 --> 00:02:34,380
a rising tide that lifted
most boats, almost all boats.

59
00:02:34,380 --> 00:02:37,240
But in the past 20
years or so, there's

60
00:02:37,240 --> 00:02:40,310
been what we call a
decoupling, where productivity

61
00:02:40,310 --> 00:02:43,700
has continued to grow, GDP per
capita has continued to grow,

62
00:02:43,700 --> 00:02:47,160
but the median income, the 50th
percentile and those below it,

63
00:02:47,160 --> 00:02:50,320
have seen their incomes
stagnate or even fall.

64
00:02:50,320 --> 00:02:53,060
And that reflects the fact
that these technologies

65
00:02:53,060 --> 00:02:55,770
don't affect everybody evenly.

66
00:02:55,770 --> 00:02:57,300
So let's play this out.

67
00:02:57,300 --> 00:03:00,560
How do you see these
technologies affecting work

68
00:03:00,560 --> 00:03:02,100
in the future?

69
00:03:02,100 --> 00:03:04,834
Well, the technologies
continue to advance,

70
00:03:04,834 --> 00:03:07,000
and they continue to affect
different kinds of jobs.

71
00:03:07,000 --> 00:03:09,680
But what really matters is
how we go about using them.

72
00:03:09,680 --> 00:03:12,580
What we've seen so far is
that many of the technologies

73
00:03:12,580 --> 00:03:14,910
have been used to
automate a lot of routine

74
00:03:14,910 --> 00:03:19,410
information-processing work,
things like bank tellers,

75
00:03:19,410 --> 00:03:22,160
bookkeepers, a lot
of middle managers.

76
00:03:22,160 --> 00:03:25,070
In fact, about 60% of
Americans primarily

77
00:03:25,070 --> 00:03:27,850
do information-processing
work, so that's a big category.

78
00:03:27,850 --> 00:03:29,992
And machines are very
good at automating

79
00:03:29,992 --> 00:03:31,450
a lot of these,
especially the more

80
00:03:31,450 --> 00:03:34,090
repetitive, routine
information-processing tasks.

81
00:03:34,090 --> 00:03:37,632
It turns out that the people
who do those jobs are typically

82
00:03:37,632 --> 00:03:39,340
in the middle of the
income distribution,

83
00:03:39,340 --> 00:03:41,600
and what they've seen is
that as machines get better

84
00:03:41,600 --> 00:03:44,010
at those tasks, there's
less demand for humans

85
00:03:44,010 --> 00:03:45,290
to do the identical tasks.

86
00:03:45,290 --> 00:03:47,579
Or if they do still
have jobs, they

87
00:03:47,579 --> 00:03:49,620
have to compete with
machines that are doing them

88
00:03:49,620 --> 00:03:50,840
more and more cheaply.

89
00:03:50,840 --> 00:03:53,160
And that his pushed down wages.

90
00:03:53,160 --> 00:03:54,980
It's led to fewer
job opportunities

91
00:03:54,980 --> 00:03:56,750
in those categories.

92
00:03:56,750 --> 00:03:59,810
At the same time, there's been
growth in job opportunities

93
00:03:59,810 --> 00:04:02,170
at the other ends of
the spectrum, what

94
00:04:02,170 --> 00:04:06,010
David Autor and other people
call job polarization.

95
00:04:06,010 --> 00:04:09,210
At the high end, you
get creative people

96
00:04:09,210 --> 00:04:11,294
or entrepreneurs are
making, in some cases,

97
00:04:11,294 --> 00:04:12,460
more money than ever before.

98
00:04:12,460 --> 00:04:13,990
It's probably the
best time in history.

99
00:04:13,990 --> 00:04:15,650
If you have some kind
of special talent

100
00:04:15,650 --> 00:04:17,470
that could be
replicated digitally,

101
00:04:17,470 --> 00:04:19,640
you might end up
being a billionaire.

102
00:04:19,640 --> 00:04:22,670
On the other hand, the
other end of the spectrum

103
00:04:22,670 --> 00:04:24,930
has also seen some
growth, and those

104
00:04:24,930 --> 00:04:29,150
are jobs that require a lot
of dexterity or physical tasks

105
00:04:29,150 --> 00:04:31,300
or interpersonal skills,
things that machines

106
00:04:31,300 --> 00:04:32,320
aren't very good at.

107
00:04:32,320 --> 00:04:34,720
The thing is, often those
jobs don't pay very well.

108
00:04:34,720 --> 00:04:36,770
And just to be specific,
those are the kinds

109
00:04:36,770 --> 00:04:40,860
of jobs like a
waiter or a gardener

110
00:04:40,860 --> 00:04:43,080
or a janitor, those
kinds of tasks.

111
00:04:43,080 --> 00:04:46,140
So what is your advice,
then, to workers

112
00:04:46,140 --> 00:04:48,940
facing this world of
changing technology?

113
00:04:48,940 --> 00:04:51,150
What should they
do in order to be

114
00:04:51,150 --> 00:04:53,530
able to prosper in this world?

115
00:04:53,530 --> 00:04:55,750
Well, what you'd
like to try to do

116
00:04:55,750 --> 00:04:59,180
is strengthen those
areas where humans have

117
00:04:59,180 --> 00:05:00,306
an advantage over machines.

118
00:05:00,306 --> 00:05:02,596
You don't really want to be
competing against machines.

119
00:05:02,596 --> 00:05:04,580
Ideally, you'd like to
do things were machines

120
00:05:04,580 --> 00:05:06,120
can leverage your talents.

121
00:05:06,120 --> 00:05:07,944
Data scientists
are more in demand

122
00:05:07,944 --> 00:05:09,360
than ever before,
because machines

123
00:05:09,360 --> 00:05:11,250
have created an
abundance of data,

124
00:05:11,250 --> 00:05:12,580
but they can't analyze it.

125
00:05:12,580 --> 00:05:15,380
They can't even ask the
right questions themselves.

126
00:05:15,380 --> 00:05:16,870
You need humans to do that.

127
00:05:16,870 --> 00:05:18,990
And so if you have
those kinds of skills,

128
00:05:18,990 --> 00:05:20,390
you're more valuable than ever.

129
00:05:20,390 --> 00:05:22,650
And Silicon Valley,
and now companies

130
00:05:22,650 --> 00:05:24,550
throughout the
country and the world,

131
00:05:24,550 --> 00:05:27,830
are greatly demanding
those kinds of workers.

132
00:05:27,830 --> 00:05:29,960
And so there's a set of
categories around that.

133
00:05:29,960 --> 00:05:32,501
I mentioned earlier, if you have
some kind of special talents

134
00:05:32,501 --> 00:05:35,180
or creativity, machines
aren't very creative.

135
00:05:35,180 --> 00:05:39,790
People who can sing well or
who can invent new businesses,

136
00:05:39,790 --> 00:05:44,470
as entrepreneurs, many kinds
of scientists, artists,

137
00:05:44,470 --> 00:05:46,940
if you're a great
writer or write books,

138
00:05:46,940 --> 00:05:48,990
those are things
that are probably

139
00:05:48,990 --> 00:05:50,870
more valuable now than before.

140
00:05:50,870 --> 00:05:53,670
Another big category that's
gotten to be more valuable is--

141
00:05:53,670 --> 00:05:55,480
I touched on it earlier-- is
some of these interpersonal

142
00:05:55,480 --> 00:05:56,120
skills--

143
00:05:56,120 --> 00:05:58,310
nurturing and caring.

144
00:05:58,310 --> 00:06:01,750
I don't think it would
have been very motivating

145
00:06:01,750 --> 00:06:04,930
if, at the half time of the
football games last weekend,

146
00:06:04,930 --> 00:06:06,760
the coach had been
a robot coach.

147
00:06:06,760 --> 00:06:08,830
You need a human to do that.

148
00:06:08,830 --> 00:06:11,770
And that means that
leadership, team work,

149
00:06:11,770 --> 00:06:12,860
those kinds of things.

150
00:06:12,860 --> 00:06:15,500
We still connect with
other people a lot better,

151
00:06:15,500 --> 00:06:17,030
and we will for some time.

152
00:06:17,030 --> 00:06:19,780
And I think all those skills,
from the creative ones

153
00:06:19,780 --> 00:06:22,650
to the interpersonal, the
teamwork, the nurturing skills,

154
00:06:22,650 --> 00:06:23,890
they can all be developed.

155
00:06:23,890 --> 00:06:26,090
And I would encourage
people to work harder

156
00:06:26,090 --> 00:06:27,990
at developing those
kinds of skills.

157
00:06:27,990 --> 00:06:31,110
Become a better salesperson,
negotiator, nurturing,

158
00:06:31,110 --> 00:06:34,180
caring for people, nursing.

159
00:06:34,180 --> 00:06:36,310
Those are all things
that I expect to grow,

160
00:06:36,310 --> 00:06:37,940
even as machines
take care of more

161
00:06:37,940 --> 00:06:41,210
and more of the routine,
repetitive kinds of tasks.

162
00:06:41,210 --> 00:06:45,030
So that's great advice
to the workforce.

163
00:06:45,030 --> 00:06:46,910
Let's talk a bit about
the people who are

164
00:06:46,910 --> 00:06:48,820
designing these technologies.

165
00:06:48,820 --> 00:06:54,420
How can we encourage
more technological design

166
00:06:54,420 --> 00:06:57,940
that complements work, that
utilizes these skills, that

167
00:06:57,940 --> 00:07:00,220
thinks about ways
to enhance them

168
00:07:00,220 --> 00:07:04,910
or to work with them
to drive productivity

169
00:07:04,910 --> 00:07:06,246
and economic performance?

170
00:07:06,246 --> 00:07:07,870
Well, I'm glad you
asked that question,

171
00:07:07,870 --> 00:07:09,286
because most people
don't even get

172
00:07:09,286 --> 00:07:10,706
as far as asking that question.

173
00:07:10,706 --> 00:07:12,330
I think there's a
widespread assumption

174
00:07:12,330 --> 00:07:14,150
out there that
technology just happens,

175
00:07:14,150 --> 00:07:16,600
and there's nothing you can
do to shape the path of it.

176
00:07:16,600 --> 00:07:19,300
The reality is that you
can encourage people

177
00:07:19,300 --> 00:07:23,580
to have technologies that create
more inclusive innovation, that

178
00:07:23,580 --> 00:07:25,800
helps people more
broadly, or that

179
00:07:25,800 --> 00:07:28,940
mostly focuses on substituting
for people, for that matter.

180
00:07:28,940 --> 00:07:31,670
And we, as a society
and as individuals,

181
00:07:31,670 --> 00:07:33,490
can shape the
direction of technology

182
00:07:33,490 --> 00:07:36,160
to quite a significant extent.

183
00:07:36,160 --> 00:07:39,810
One of the things we can do is
change around our tax policy

184
00:07:39,810 --> 00:07:42,130
and our government policies.

185
00:07:42,130 --> 00:07:44,250
One of the oldest
rules of economics

186
00:07:44,250 --> 00:07:47,350
is if you want less of
something, you tax it.

187
00:07:47,350 --> 00:07:49,350
If you want more of
something, you subsidize it.

188
00:07:49,350 --> 00:07:52,650
Right now, ironically,
we are taxing work.

189
00:07:52,650 --> 00:07:57,400
If two entrepreneurs come up
with a billion-dollar idea,

190
00:07:57,400 --> 00:07:59,510
and one of them involves
employing lots of people

191
00:07:59,510 --> 00:08:01,630
and one of them doesn't,
our current tax system

192
00:08:01,630 --> 00:08:04,620
will put more taxes on the
one that employs more people,

193
00:08:04,620 --> 00:08:06,510
and that's probably
not we want to do.

194
00:08:06,510 --> 00:08:09,919
Conversely, we don't tax
pollution, carbon, congestion,

195
00:08:09,919 --> 00:08:11,460
things that we'd
like to see less of.

196
00:08:11,460 --> 00:08:12,918
So we could rejigger
the tax system

197
00:08:12,918 --> 00:08:15,130
to tax the things that
we don't want more,

198
00:08:15,130 --> 00:08:22,460
and then we'll have less of
them, and lower the taxes,

199
00:08:22,460 --> 00:08:24,410
or even subsidize, work.

200
00:08:24,410 --> 00:08:26,510
And I think that would
guide entrepreneurs

201
00:08:26,510 --> 00:08:28,120
to be more creative
about inventing

202
00:08:28,120 --> 00:08:31,650
things that have less pollution
and more widespread labor.

203
00:08:31,650 --> 00:08:35,210
The Earned Income Tax Credit is
a good example, but too small.

204
00:08:35,210 --> 00:08:36,816
Could be expanded at that.

205
00:08:36,816 --> 00:08:38,190
Another kind of
thing we could do

206
00:08:38,190 --> 00:08:42,030
is just recognize reward
and motivate people.

207
00:08:42,030 --> 00:08:45,030
One of the things that I've
learned over the past few years

208
00:08:45,030 --> 00:08:47,330
is how much people
can be motivated

209
00:08:47,330 --> 00:08:49,490
by having a prize and a
goal in front of them.

210
00:08:49,490 --> 00:08:51,660
I've been watching
the DARPA Robotics

211
00:08:51,660 --> 00:08:53,930
Challenge, the DARPA
Grand Challenge that

212
00:08:53,930 --> 00:08:55,640
led to the driverless car.

213
00:08:55,640 --> 00:08:57,620
And I see my colleagues
in engineering,

214
00:08:57,620 --> 00:09:00,412
grad students, professors
working nights and weekends

215
00:09:00,412 --> 00:09:02,870
to solve this challenge that
has been put in front of them.

216
00:09:02,870 --> 00:09:05,170
And many times,
they do come around

217
00:09:05,170 --> 00:09:07,550
to making huge strides in that.

218
00:09:07,550 --> 00:09:09,920
Why don't we try to
reward and motivate

219
00:09:09,920 --> 00:09:13,090
business leaders and
economists to reinvent

220
00:09:13,090 --> 00:09:15,150
the businesses and
economy the same way

221
00:09:15,150 --> 00:09:17,820
that technologists have been
reinventing the technology?

222
00:09:17,820 --> 00:09:20,140
And in fact, here at MIT,
we're launching something

223
00:09:20,140 --> 00:09:23,140
called the Inclusive
Innovation Competition, which

224
00:09:23,140 --> 00:09:25,690
is designed specifically
to recognize those business

225
00:09:25,690 --> 00:09:29,060
models that use technology
to create broad,

226
00:09:29,060 --> 00:09:30,190
shared prosperity.

227
00:09:30,190 --> 00:09:34,370
We call it shared prosperity
for the many, not just the few.

228
00:09:34,370 --> 00:09:36,070
It's just getting
launched, but we

229
00:09:36,070 --> 00:09:39,720
think this is going to be a good
way to recognize and highlight

230
00:09:39,720 --> 00:09:40,830
people who have done that.

231
00:09:40,830 --> 00:09:42,280
I think that's a
great idea, and I

232
00:09:42,280 --> 00:09:45,130
hope that it creates
a lot of attention,

233
00:09:45,130 --> 00:09:49,380
and that we get lots of
people really thinking along

234
00:09:49,380 --> 00:09:55,630
these ways, because I think
it's the genius of people--

235
00:09:55,630 --> 00:09:59,560
using technologies to address
important problems that

236
00:09:59,560 --> 00:10:01,940
will help make progress.

237
00:10:01,940 --> 00:10:03,550
Yeah, and we all have choices.

238
00:10:03,550 --> 00:10:05,560
We can shape the future
in that direction.

239
00:10:05,560 --> 00:10:06,990
And why don't we
encourage people

240
00:10:06,990 --> 00:10:09,370
who are doing it in a way
that does create this shared

241
00:10:09,370 --> 00:10:10,960
prosperity?

242
00:10:10,960 --> 00:10:13,640
So let me ask this final
question, because it's

243
00:10:13,640 --> 00:10:15,260
obviously on everybody's mind.

244
00:10:15,260 --> 00:10:18,560
And that is, from time
to time in history,

245
00:10:18,560 --> 00:10:21,160
as you know better than
I, people have worried

246
00:10:21,160 --> 00:10:23,490
that technology is
going to replace work,

247
00:10:23,490 --> 00:10:24,870
it's going to be
the end of work,

248
00:10:24,870 --> 00:10:26,500
we're not going to
have enough jobs.

249
00:10:26,500 --> 00:10:30,010
And that's an issue on
people's mind today.

250
00:10:30,010 --> 00:10:32,140
How do you think about
that problem, given

251
00:10:32,140 --> 00:10:34,720
all of the innovation
that's going on

252
00:10:34,720 --> 00:10:36,620
and that could
come down the road?

253
00:10:36,620 --> 00:10:39,760
Well, down the road,
I could certainly

254
00:10:39,760 --> 00:10:42,611
imagine technology creating
kind of a Star Trek economy.

255
00:10:42,611 --> 00:10:44,610
And I don't think that's
necessarily a bad thing

256
00:10:44,610 --> 00:10:47,440
if technology's able to take
care of all of our basic needs

257
00:10:47,440 --> 00:10:48,000
for us.

258
00:10:48,000 --> 00:10:50,090
But the more
immediate issue is not

259
00:10:50,090 --> 00:10:51,640
that technology is
going to eliminate

260
00:10:51,640 --> 00:10:54,330
all the jobs, but the types
of jobs that are affected.

261
00:10:54,330 --> 00:10:58,090
The reality is that technology's
always been destroying jobs.

262
00:10:58,090 --> 00:10:59,450
It's always been creating jobs.

263
00:10:59,450 --> 00:11:01,770
But most importantly, it's
been changing the mix.

264
00:11:01,770 --> 00:11:04,450
And what we're seeing recently,
as I touched on earlier,

265
00:11:04,450 --> 00:11:07,504
is that mix is changing in
a way that a lot of people

266
00:11:07,504 --> 00:11:09,420
are being made worse
off, and other people are

267
00:11:09,420 --> 00:11:11,000
being made much better off.

268
00:11:11,000 --> 00:11:14,180
And we want to try to shape the
direction of the technology.

269
00:11:14,180 --> 00:11:16,980
We want to also give people
the skills and education

270
00:11:16,980 --> 00:11:18,790
so they can adapt to
the new kinds of jobs

271
00:11:18,790 --> 00:11:19,710
that are available.

272
00:11:19,710 --> 00:11:21,560
And if we do that,
I think we're going

273
00:11:21,560 --> 00:11:24,474
to be able to have that kind
of world of shared prosperity.

274
00:11:24,474 --> 00:11:26,390
The technology will march
on, and we shouldn't

275
00:11:26,390 --> 00:11:27,770
try to stop the technology.

276
00:11:27,770 --> 00:11:29,400
That's how we get
that bigger pie,

277
00:11:29,400 --> 00:11:30,820
that growth that I mentioned.

278
00:11:30,820 --> 00:11:32,400
But we can direct it.

279
00:11:32,400 --> 00:11:34,590
In the earlier
industrial revolutions,

280
00:11:34,590 --> 00:11:37,710
when the steam engine and
other technologies came along,

281
00:11:37,710 --> 00:11:39,640
we adapted our institutions.

282
00:11:39,640 --> 00:11:41,760
We invented mass
public education,

283
00:11:41,760 --> 00:11:43,690
first at the primary
level, and then later

284
00:11:43,690 --> 00:11:44,940
at the secondary level.

285
00:11:44,940 --> 00:11:48,220
We invented social security and
a whole bunch of other policies

286
00:11:48,220 --> 00:11:51,240
that helped smooth
that transition

287
00:11:51,240 --> 00:11:53,200
to a new kind of work
force and cushioned

288
00:11:53,200 --> 00:11:55,580
the people who otherwise
would have been left behind.

289
00:11:55,580 --> 00:11:57,580
We're going to have to
reinvent education again.

290
00:11:57,580 --> 00:11:58,996
And this time,
we're going to have

291
00:11:58,996 --> 00:12:01,490
to do our best to shape
the technology that

292
00:12:01,490 --> 00:12:03,590
is aligned with our values.

293
00:12:03,590 --> 00:12:06,120
Ultimately, it's not what
the technology does to us.

294
00:12:06,120 --> 00:12:07,840
Technology is a tool.

295
00:12:07,840 --> 00:12:10,940
It's always been a tool, whether
it's a hammer or an enterprise

296
00:12:10,940 --> 00:12:12,380
resource planning system.

297
00:12:12,380 --> 00:12:15,770
We have more powerful tools
now than we ever had before,

298
00:12:15,770 --> 00:12:18,470
and that means we have more
power to shape the future

299
00:12:18,470 --> 00:12:19,690
than we ever did before.

300
00:12:19,690 --> 00:12:22,300
But it starts with understanding
that we have that power,

301
00:12:22,300 --> 00:12:25,181
and aligning our
actions with our values.

302
00:12:25,181 --> 00:12:27,430
Well, Eric, I think that's
a really important message,

303
00:12:27,430 --> 00:12:31,530
and one that I'm delighted that
we can deliver in this class

304
00:12:31,530 --> 00:12:33,760
and in other settings
where people can really

305
00:12:33,760 --> 00:12:34,510
make a difference.

306
00:12:34,510 --> 00:12:35,886
So thanks for joining us today.

307
00:12:35,886 --> 00:12:37,010
It's a tremendous pleasure.

308
00:12:37,010 --> 00:12:39,010
Thanks a lot, Tom.