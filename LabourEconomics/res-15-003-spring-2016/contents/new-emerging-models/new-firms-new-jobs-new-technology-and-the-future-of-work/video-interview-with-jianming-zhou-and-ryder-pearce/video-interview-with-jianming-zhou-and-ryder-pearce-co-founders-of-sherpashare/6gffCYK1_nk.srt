1
00:00:05,500 --> 00:00:07,770
We're talking today
with Jianming Zhou,

2
00:00:07,770 --> 00:00:10,270
and, Ryder Pearce
the co-founders

3
00:00:10,270 --> 00:00:11,950
of a very interesting
organization

4
00:00:11,950 --> 00:00:13,630
called SherpaShare.

5
00:00:13,630 --> 00:00:15,960
It works in the ride
service business,

6
00:00:15,960 --> 00:00:18,650
and it provides
services both to drivers

7
00:00:18,650 --> 00:00:23,360
and to other people companies
interested in this business.

8
00:00:23,360 --> 00:00:25,920
So we're very interested
in learning more about

9
00:00:25,920 --> 00:00:28,830
what you do, why you
founded your organization,

10
00:00:28,830 --> 00:00:29,840
and where it's going.

11
00:00:29,840 --> 00:00:31,920
So welcome, thank you
for joining us today.

12
00:00:31,920 --> 00:00:32,420
Thank you.

13
00:00:32,420 --> 00:00:33,620
Thanks for having us.

14
00:00:33,620 --> 00:00:35,970
So tell me what is SherpaShare?

15
00:00:35,970 --> 00:00:38,290
What is it that you do?

16
00:00:38,290 --> 00:00:42,620
Yeah so SherpaShare is
financial management,

17
00:00:42,620 --> 00:00:46,030
and a communication platform
for on-demand workers.

18
00:00:46,030 --> 00:00:50,312
And we basically
offer them to access--

19
00:00:50,312 --> 00:00:52,270
basically offer them an
independent and neutral

20
00:00:52,270 --> 00:00:55,030
platform to access
information, utility,

21
00:00:55,030 --> 00:00:59,530
and work opportunities
to maximize their work.

22
00:00:59,530 --> 00:01:01,170
And tell me a
little bit about why

23
00:01:01,170 --> 00:01:02,780
you formed this organization.

24
00:01:02,780 --> 00:01:08,400
What led you to do it?

25
00:01:08,400 --> 00:01:11,460
The fundamental reason
is we see there's

26
00:01:11,460 --> 00:01:15,260
a fundamental conflict
between on-demand services

27
00:01:15,260 --> 00:01:17,890
and the workers And the
workers are typically

28
00:01:17,890 --> 00:01:20,310
working for multiple
services, and they

29
00:01:20,310 --> 00:01:23,610
have growing challenges
to manage their work,

30
00:01:23,610 --> 00:01:26,280
and understand that there
are all other work options.

31
00:01:26,280 --> 00:01:30,430
And they don't have
loyalty to any services.

32
00:01:30,430 --> 00:01:33,890
At the same time a company
competing for the same supply

33
00:01:33,890 --> 00:01:37,080
pool, and they have very
high acquisition cost.

34
00:01:37,080 --> 00:01:40,240
And so we think
a sort of putting

35
00:01:40,240 --> 00:01:42,870
in a buffer layer in between
can benefit both sides.

36
00:01:42,870 --> 00:01:47,600
So that's why we position
us to sell that need.

37
00:01:47,600 --> 00:01:53,800
And so who do you define as your
customers for your services?

38
00:01:53,800 --> 00:01:56,980
So typically it's been
ride share drivers

39
00:01:56,980 --> 00:01:58,470
is who we've started with.

40
00:01:58,470 --> 00:02:03,040
And those typically,
Uber, Lyft, formerly

41
00:02:03,040 --> 00:02:06,140
Sidecar, and delivery
companies as well

42
00:02:06,140 --> 00:02:11,500
although we'd like to expand
to other independent drivers.

43
00:02:11,500 --> 00:02:14,760
And do the companies
themselves come to you

44
00:02:14,760 --> 00:02:18,330
with ideas for your
services or ask you

45
00:02:18,330 --> 00:02:24,500
for either information that's
helpful to them as well?

46
00:02:24,500 --> 00:02:25,380
Some, yeah.

47
00:02:25,380 --> 00:02:27,670
Especially for smaller players.

48
00:02:27,670 --> 00:02:30,390
So basically, if you're
looking for a channel

49
00:02:30,390 --> 00:02:32,670
to acquire supplies--

50
00:02:32,670 --> 00:02:35,739
but actually for the
workers, they also

51
00:02:35,739 --> 00:02:37,280
are looking for a
third party channel

52
00:02:37,280 --> 00:02:39,790
to exchange information
as well, yeah.

53
00:02:39,790 --> 00:02:45,150
You know what I find fascinating
is you characterize SherpaShare

54
00:02:45,150 --> 00:02:49,670
an intermediary, not as a
representative or an advocate

55
00:02:49,670 --> 00:02:52,190
for one party or the other.

56
00:02:52,190 --> 00:02:54,170
Tell me more about
how that works

57
00:02:54,170 --> 00:02:59,120
and why you're positioning
Sherpa share in this kind

58
00:02:59,120 --> 00:03:00,260
of middle category?

59
00:03:00,260 --> 00:03:02,990
What do each of the
different stakeholders

60
00:03:02,990 --> 00:03:05,970
get from your Services

61
00:03:05,970 --> 00:03:10,100
Yeah so, as I said, like
most workers actually

62
00:03:10,100 --> 00:03:13,500
working for multiple
services, so because they

63
00:03:13,500 --> 00:03:15,210
are working for
multiple services,

64
00:03:15,210 --> 00:03:17,210
they need tools to
manage your earnings

65
00:03:17,210 --> 00:03:21,130
from different sources,
check their personal expenses

66
00:03:21,130 --> 00:03:23,130
understand that
their tax liability,

67
00:03:23,130 --> 00:03:27,480
and navigate increasingly
more working options.

68
00:03:27,480 --> 00:03:31,020
And no single on-demand
services can't actually

69
00:03:31,020 --> 00:03:33,900
provide the necessary
tools to help them.

70
00:03:33,900 --> 00:03:39,110
And other side is on-demand
services are very powerful,

71
00:03:39,110 --> 00:03:41,070
and workers are actually
on the weak side.

72
00:03:41,070 --> 00:03:45,770
And they don't-- they are
forced into a Silo Situation.

73
00:03:45,770 --> 00:03:49,690
So that's why we
think like SherpaShare

74
00:03:49,690 --> 00:03:52,830
as a third party can
provide value on both sides,

75
00:03:52,830 --> 00:03:56,600
make the transaction more
smooth and efficient.

76
00:03:56,600 --> 00:04:01,110
Yeah, for example,
so that the workers

77
00:04:01,110 --> 00:04:04,440
don't have loyalty,
as J.Z. Said,

78
00:04:04,440 --> 00:04:07,890
and so workers who
need a place where they

79
00:04:07,890 --> 00:04:10,520
can manage their
multiple work options

80
00:04:10,520 --> 00:04:14,670
and also communicate with each
other and exchange information.

81
00:04:14,670 --> 00:04:18,339
And the problem the
companies are having is,

82
00:04:18,339 --> 00:04:20,820
they basically don't
have a good way

83
00:04:20,820 --> 00:04:24,556
to demand loyalty,
or command loyalty.

84
00:04:24,556 --> 00:04:26,180
And that's one of
things we see they're

85
00:04:26,180 --> 00:04:27,346
struggling with quite a bit.

86
00:04:27,346 --> 00:04:29,990
How do they actually
convince a driver,

87
00:04:29,990 --> 00:04:32,980
who has multiple options,
to work for them.

88
00:04:32,980 --> 00:04:36,210
And that's in many ways to
challenge of the on demand

89
00:04:36,210 --> 00:04:40,040
economy where you don't have
a direct employer and employee

90
00:04:40,040 --> 00:04:44,880
relationship in place, so
that the interaction is

91
00:04:44,880 --> 00:04:47,160
a little bit more fluid.

92
00:04:47,160 --> 00:04:49,990
So I think that's a
very interesting point.

93
00:04:49,990 --> 00:04:55,700
So your services
come via an app.

94
00:04:55,700 --> 00:04:57,650
Take me through the app.

95
00:04:57,650 --> 00:05:00,280
If I'm a driver, what do I do?

96
00:05:00,280 --> 00:05:01,800
How do I get access to it?

97
00:05:01,800 --> 00:05:05,740
What will I learn
by using your app?

98
00:05:05,740 --> 00:05:08,530
Yeah, so, also it's actually--

99
00:05:08,530 --> 00:05:09,640
have a mobile app.

100
00:05:09,640 --> 00:05:11,020
We also have a website.

101
00:05:11,020 --> 00:05:16,150
So we provide a tool to
help you to consolidate all

102
00:05:16,150 --> 00:05:17,720
your earnings into one place.

103
00:05:17,720 --> 00:05:20,250
We also use a mobile app
to track your expense

104
00:05:20,250 --> 00:05:21,230
automatically.

105
00:05:21,230 --> 00:05:23,490
Now since [INAUDIBLE]
focus on the drivers,

106
00:05:23,490 --> 00:05:26,420
so the biggest expense for
them actually is mileage.

107
00:05:26,420 --> 00:05:29,240
So we track the mileage
for them automatically.

108
00:05:29,240 --> 00:05:34,640
We also have so-called heat
map, which is across map,

109
00:05:34,640 --> 00:05:37,830
and show where the hot spot
for the drivers in real time.

110
00:05:37,830 --> 00:05:39,970
So they can, based
on the heat map

111
00:05:39,970 --> 00:05:42,770
decide when and where to
work for each service.

112
00:05:42,770 --> 00:05:46,750
And we also have a
so-called chat feature,

113
00:05:46,750 --> 00:05:50,850
so kind of chat room,
so drivers can get help,

114
00:05:50,850 --> 00:05:53,410
get support from other drivers.

115
00:05:53,410 --> 00:05:57,460
Yeah that's basically
what we have now.

116
00:05:57,460 --> 00:05:59,530
Today we are
launching a new app.

117
00:05:59,530 --> 00:06:00,690
As of last week.

118
00:06:00,690 --> 00:06:04,200
We launched a brand new app that
basically took the chat feature

119
00:06:04,200 --> 00:06:06,220
and gave it its own home.

120
00:06:06,220 --> 00:06:09,200
So when we launched the
chat in our existing

121
00:06:09,200 --> 00:06:12,430
app we found that, drivers--

122
00:06:12,430 --> 00:06:15,350
it was a strong need for drivers
to communicate with each other,

123
00:06:15,350 --> 00:06:19,240
to ask questions, to
give status updates.

124
00:06:19,240 --> 00:06:21,650
What's it like down time
right now, for example.

125
00:06:21,650 --> 00:06:23,160
When does the game get out?

126
00:06:23,160 --> 00:06:25,700
And so last week, we
launched a new app

127
00:06:25,700 --> 00:06:28,660
dedicated to basically
allowing drivers

128
00:06:28,660 --> 00:06:30,350
to exchange information.

129
00:06:30,350 --> 00:06:32,200
Call it SherpaShare Plus.

130
00:06:32,200 --> 00:06:33,590
Real time exchange [INAUDIBLE].

131
00:06:33,590 --> 00:06:36,630
For example, when I
came in this morning,

132
00:06:36,630 --> 00:06:38,850
I posted something
saying, hey, we're

133
00:06:38,850 --> 00:06:42,497
going to be in Boston
talking about driver issues,

134
00:06:42,497 --> 00:06:43,830
anything you want to talk about?

135
00:06:43,830 --> 00:06:45,880
And we had several
drivers right away

136
00:06:45,880 --> 00:06:50,970
respond, a couple the
points where repeat--

137
00:06:50,970 --> 00:06:52,660
Uber should add
tips for drivers,

138
00:06:52,660 --> 00:06:53,880
so that was one that drivers

139
00:06:53,880 --> 00:06:54,580
And tips.

140
00:06:54,580 --> 00:06:55,790
And temps it was the one.

141
00:06:55,790 --> 00:07:00,069
And a few others, basically
saying that drivers

142
00:07:00,069 --> 00:07:01,110
want to communicate more.

143
00:07:01,110 --> 00:07:03,394
So if companies can
do more or if we

144
00:07:03,394 --> 00:07:05,060
can do more to help
drivers communicate,

145
00:07:05,060 --> 00:07:06,170
it's a strong lead.

146
00:07:06,170 --> 00:07:09,030
That's very interesting
that drivers are--

147
00:07:09,030 --> 00:07:11,740
they operate
independently, separately,

148
00:07:11,740 --> 00:07:13,890
they're all in their
own vehicle, And so on.

149
00:07:13,890 --> 00:07:16,310
But they're looking for
some sense of community,

150
00:07:16,310 --> 00:07:17,690
and communications.

151
00:07:17,690 --> 00:07:20,700
That's and that's a service
that you can provide.

152
00:07:20,700 --> 00:07:21,700
That's very interesting.

153
00:07:21,700 --> 00:07:24,950
Yeah, there's like two
fundamental needs drivers need.

154
00:07:24,950 --> 00:07:27,380
The number one is
always financial need.

155
00:07:27,380 --> 00:07:29,980
The second need, we
actually call social need.

156
00:07:29,980 --> 00:07:32,500
So basically, they
need a channel

157
00:07:32,500 --> 00:07:34,110
to talk about their career.

158
00:07:34,110 --> 00:07:36,970
And so basically they talk
to passengers every day.

159
00:07:36,970 --> 00:07:38,820
But they need to talk
to other drivers,

160
00:07:38,820 --> 00:07:43,160
talk about their company, talk
about their so-called career,

161
00:07:43,160 --> 00:07:44,550
in some way.

162
00:07:44,550 --> 00:07:47,190
So we want to
become that channel.

163
00:07:47,190 --> 00:07:50,150
Well that's a
fascinating opportunity

164
00:07:50,150 --> 00:07:52,230
for people to do
it in a modern way.

165
00:07:52,230 --> 00:07:55,570
Do you know if this creates
any social networking where

166
00:07:55,570 --> 00:07:58,010
people drivers
actually physically get

167
00:07:58,010 --> 00:08:02,030
together, and form communities
or is it all online?

168
00:08:02,030 --> 00:08:02,850
Both, actually.

169
00:08:02,850 --> 00:08:04,770
Yeah, there is some
offline we see more.

170
00:08:04,770 --> 00:08:07,950
I mean, online is the
big one, because everyone

171
00:08:07,950 --> 00:08:10,990
has different schedules
and so it's hard to align.

172
00:08:10,990 --> 00:08:13,300
But traditional
employment of course,

173
00:08:13,300 --> 00:08:17,190
had lunch, break, the break
room, the water cooler which

174
00:08:17,190 --> 00:08:19,510
doesn't exist for the drivers.

175
00:08:19,510 --> 00:08:24,300
We would like to replicate
the online chat as the water

176
00:08:24,300 --> 00:08:25,180
cooler area.

177
00:08:25,180 --> 00:08:28,240
We do see in some cities where
there are a lot of drivers,

178
00:08:28,240 --> 00:08:29,950
the drivers will coordinate.

179
00:08:29,950 --> 00:08:33,140
Hey, I'm taking a break
at this coffee shop

180
00:08:33,140 --> 00:08:36,900
now and trying to get
a few people together.

181
00:08:36,900 --> 00:08:38,730
So it's kind [INAUDIBLE]
of our platform.

182
00:08:38,730 --> 00:08:42,070
Drivers can organize
whatever they want,

183
00:08:42,070 --> 00:08:45,570
whether that's discussions
or offline meet-ups.

184
00:08:45,570 --> 00:08:48,840
Another name for
on-demand industry

185
00:08:48,840 --> 00:08:52,300
is some people call O2O,
which is online to offline,

186
00:08:52,300 --> 00:08:53,670
or offline to online.

187
00:08:53,670 --> 00:08:55,900
Actually, our community
is very interesting we

188
00:08:55,900 --> 00:08:58,400
develop a community
from offline to online.

189
00:08:58,400 --> 00:09:00,200
But eventually we
also want to bring

190
00:09:00,200 --> 00:09:03,280
online to offline in some way.

191
00:09:03,280 --> 00:09:05,300
Well there's all
kinds of opportunities

192
00:09:05,300 --> 00:09:07,670
for that kind of interaction.

193
00:09:07,670 --> 00:09:11,110
Well let's talk more
specifically about the drivers

194
00:09:11,110 --> 00:09:13,600
and how they are
doing, financially.

195
00:09:13,600 --> 00:09:16,400
You're providing
both information

196
00:09:16,400 --> 00:09:19,130
for helping them build
a social network,

197
00:09:19,130 --> 00:09:23,520
but also to assess their
own financial situation.

198
00:09:23,520 --> 00:09:25,600
What are you hearing
from drivers?

199
00:09:25,600 --> 00:09:30,460
What can you say about how
they are doing financially now,

200
00:09:30,460 --> 00:09:32,870
and over time?

201
00:09:32,870 --> 00:09:36,910
Yeah the biggest thing we
found at a fundamental level

202
00:09:36,910 --> 00:09:40,600
was a lot of these drivers are
first time independent workers.

203
00:09:40,600 --> 00:09:46,000
And so understanding
expenses, mileage tracking,

204
00:09:46,000 --> 00:09:48,700
wasn't really there so I
didn't have a basis for that.

205
00:09:48,700 --> 00:09:50,240
And so what that
meant was, whatever

206
00:09:50,240 --> 00:09:53,450
Uber and Lyft and other
companies marketed to them.

207
00:09:53,450 --> 00:09:56,000
They say $35 an hour.

208
00:09:56,000 --> 00:09:57,800
Drivers didn't have
a good sense of what

209
00:09:57,800 --> 00:09:59,520
their actual net earnings was.

210
00:09:59,520 --> 00:10:02,160
And that's why we started,
that's how we started basically

211
00:10:02,160 --> 00:10:04,610
showing them, here's what
you're actually making.

212
00:10:04,610 --> 00:10:11,230
Another issue we saw was
that basically drivers didn't

213
00:10:11,230 --> 00:10:13,260
know how to
optimize, their work,

214
00:10:13,260 --> 00:10:15,960
and so there's a couple things
that we've helped them with.

215
00:10:15,960 --> 00:10:18,470
And so we put out
reports every once

216
00:10:18,470 --> 00:10:21,400
in a while that show
trends in different cities.

217
00:10:21,400 --> 00:10:25,460
And we do see that
earnings per trip, I mean,

218
00:10:25,460 --> 00:10:26,820
they do fluctuate.

219
00:10:26,820 --> 00:10:29,330
In some cities they go
up, some they go down.

220
00:10:29,330 --> 00:10:32,120
Of course, people's schedules
are vastly different.

221
00:10:32,120 --> 00:10:35,840
So it's always been
very difficult to point

222
00:10:35,840 --> 00:10:40,370
to a trend in either direction.

223
00:10:40,370 --> 00:10:43,970
But what I will say
is that we noticed

224
00:10:43,970 --> 00:10:46,690
that the vast majority
of these workers

225
00:10:46,690 --> 00:10:48,530
are doing this for
supplemental income.

226
00:10:48,530 --> 00:10:52,660
And so this might be 10 hours
a week or 20 hours a week.

227
00:10:52,660 --> 00:10:54,880
And that's in the range of--

228
00:10:54,880 --> 00:10:56,840
they might make a couple
hundred dollars more,

229
00:10:56,840 --> 00:11:00,260
in addition to full time work

230
00:11:00,260 --> 00:11:02,260
What do you hear from
the drivers themselves?

231
00:11:02,260 --> 00:11:04,220
What would they like
SherpaShare to do?

232
00:11:06,830 --> 00:11:09,410
They will definitely like--

233
00:11:09,410 --> 00:11:11,510
always want to make more money.

234
00:11:11,510 --> 00:11:18,260
Yeah, and so whatever we
do can increase the income,

235
00:11:18,260 --> 00:11:19,840
they will love it.

236
00:11:19,840 --> 00:11:23,810
So we basically organize all
our service in in three buckets.

237
00:11:23,810 --> 00:11:28,320
The first one is help them
to sort of like manage--

238
00:11:28,320 --> 00:11:29,690
count their money, right?

239
00:11:29,690 --> 00:11:31,320
Manage their money.

240
00:11:31,320 --> 00:11:35,470
Second is basically how to help
them to optimize their income.

241
00:11:35,470 --> 00:11:37,120
The third bucket
is basically how

242
00:11:37,120 --> 00:11:38,680
to help them make more money.

243
00:11:38,680 --> 00:11:41,740
So if you look at our
service, basically, we

244
00:11:41,740 --> 00:11:43,890
provide us some
personal utility tools

245
00:11:43,890 --> 00:11:45,840
to help them to
count their money.

246
00:11:45,840 --> 00:11:47,130
There will be a community.

247
00:11:47,130 --> 00:11:50,160
So with some cross
[INAUDIBLE] information, so

248
00:11:50,160 --> 00:11:52,270
they can make better
decisions every day.

249
00:11:52,270 --> 00:11:56,550
Hopefully they can optimize
their work and maximize income.

250
00:11:56,550 --> 00:11:58,850
The third thing we
eventually will do

251
00:11:58,850 --> 00:12:02,040
is connect them with a
new working opportunity,

252
00:12:02,040 --> 00:12:03,720
so they can make more money.

253
00:12:03,720 --> 00:12:05,480
Yeah.

254
00:12:05,480 --> 00:12:09,650
And so that's what the
drivers get from you.

255
00:12:09,650 --> 00:12:11,980
What's your revenue model?

256
00:12:11,980 --> 00:12:17,170
How do you survive,
and you have investors

257
00:12:17,170 --> 00:12:20,140
know as a startup
organization, but you

258
00:12:20,140 --> 00:12:22,840
are a for profit business?

259
00:12:22,840 --> 00:12:27,390
So what's the model
for generating revenue?

260
00:12:27,390 --> 00:12:30,760
Yeah, so we are an
intermediate layer

261
00:12:30,760 --> 00:12:33,650
between the on-demand
services and workers.

262
00:12:33,650 --> 00:12:39,780
And so we're also a
layer between workers

263
00:12:39,780 --> 00:12:42,700
and, other work
related services,

264
00:12:42,700 --> 00:12:44,920
and worker and consumer, right.

265
00:12:44,920 --> 00:12:49,190
So we eventually will make
money from every relationship.

266
00:12:49,190 --> 00:12:52,630
So we will start with some
premise of this on the worker

267
00:12:52,630 --> 00:12:56,920
side, then try to make some
money on the company side,

268
00:12:56,920 --> 00:13:01,000
through some marketing and
[INAUDIBLE] kind of things.

269
00:13:01,000 --> 00:13:03,980
So marketing opportunities
for companies,

270
00:13:03,980 --> 00:13:07,330
and pay for services,
from drivers,

271
00:13:07,330 --> 00:13:11,830
for more premium kind of
information and material.

272
00:13:11,830 --> 00:13:12,420
Exactly.

273
00:13:12,420 --> 00:13:16,710
So an example of that would be
what's most valuable to drivers

274
00:13:16,710 --> 00:13:19,740
now is tracking
mileage, because that's

275
00:13:19,740 --> 00:13:23,680
a direct number they can look
at and use for a tax direction,

276
00:13:23,680 --> 00:13:26,500
and so that's something where
a lot of other utility services

277
00:13:26,500 --> 00:13:29,440
charge for, and drivers
are willing to pay for.

278
00:13:29,440 --> 00:13:32,280
So that's one example
on the worker side

279
00:13:32,280 --> 00:13:34,430
they'll pay for that utility.

280
00:13:34,430 --> 00:13:37,400
And the company
side, they'll see

281
00:13:37,400 --> 00:13:39,910
that there's a
network of drivers

282
00:13:39,910 --> 00:13:42,280
who have a certain level of
experience in this space.

283
00:13:42,280 --> 00:13:46,070
And of course, those
are potential drivers

284
00:13:46,070 --> 00:13:47,430
that they want to target.

285
00:13:47,430 --> 00:13:49,982
So that's, of course, they're
interested in both acquiring

286
00:13:49,982 --> 00:13:51,440
those drivers and
then once they've

287
00:13:51,440 --> 00:13:54,580
acquired them, convincing
them to work on a certain day.

288
00:13:54,580 --> 00:13:59,990
Yeah, so in some respects, you
can be a recruiting agency,

289
00:13:59,990 --> 00:14:04,660
or help them to identify the
pool of potential drivers

290
00:14:04,660 --> 00:14:05,160
for them.

291
00:14:05,160 --> 00:14:07,120
So that's a really
important service

292
00:14:07,120 --> 00:14:09,190
because that must
be very difficult,

293
00:14:09,190 --> 00:14:12,570
what do you find is-- how
stable are these drivers?

294
00:14:12,570 --> 00:14:13,850
How long do they stay?

295
00:14:13,850 --> 00:14:15,700
Do they turn over a lot?

296
00:14:15,700 --> 00:14:19,427
What's your experience
telling you?

297
00:14:19,427 --> 00:14:20,260
[INTERPOSING VOICES]

298
00:14:20,260 --> 00:14:22,870
There's a natural turn
for this industry.

299
00:14:22,870 --> 00:14:28,130
Yeah, and sometimes we use
a metaphor, travel industry,

300
00:14:28,130 --> 00:14:30,730
so more like the
travel industry.

301
00:14:30,730 --> 00:14:35,154
Like every family, they want
travel two to three times

302
00:14:35,154 --> 00:14:36,320
a year, something like that.

303
00:14:36,320 --> 00:14:38,236
Before this industry
extreme, more like people

304
00:14:38,236 --> 00:14:41,280
work on and off kind of thing.

305
00:14:41,280 --> 00:14:44,100
So for us it's more like
a we provide a platform

306
00:14:44,100 --> 00:14:46,700
with data and information
that, whenever

307
00:14:46,700 --> 00:14:49,630
they decide to do
something in this industry,

308
00:14:49,630 --> 00:14:51,280
they will go to SherpaShare.

309
00:14:51,280 --> 00:14:55,100
And hopefully, with
the entire industry

310
00:14:55,100 --> 00:14:58,390
become more mature,
average higher,

311
00:14:58,390 --> 00:15:00,420
I think the people will
find that this is more

312
00:15:00,420 --> 00:15:04,190
a legit viable work option
for them to make a living.

313
00:15:04,190 --> 00:15:07,730
So an example of
that would be someone

314
00:15:07,730 --> 00:15:11,040
who was on boards with
Uber, for example,

315
00:15:11,040 --> 00:15:13,820
might only really want
that income for a month,

316
00:15:13,820 --> 00:15:16,340
or a week even, to have
some supplemental income,

317
00:15:16,340 --> 00:15:18,170
and then leave for six months.

318
00:15:18,170 --> 00:15:20,350
And that's a pattern we
see, where people really

319
00:15:20,350 --> 00:15:22,457
want someone come for
a fixed period of time,

320
00:15:22,457 --> 00:15:23,540
and then they might leave.

321
00:15:23,540 --> 00:15:26,300
And the question that we're
always wondering is at--

322
00:15:26,300 --> 00:15:27,800
and we believe at
some point they'll

323
00:15:27,800 --> 00:15:30,280
come back, because
they'll want extra income.

324
00:15:30,280 --> 00:15:35,410
It's a question of when is
the next phase of their life

325
00:15:35,410 --> 00:15:37,866
where they'll need
supplemental income?

326
00:15:37,866 --> 00:15:40,240
So it could be students is a
good example, where students

327
00:15:40,240 --> 00:15:42,700
will-- on our way
over here we had

328
00:15:42,700 --> 00:15:44,710
an Uber driver who was
also a full time student,

329
00:15:44,710 --> 00:15:45,850
and a full time driver.

330
00:15:45,850 --> 00:15:48,090
And he fits in his driving
when he's a student,

331
00:15:48,090 --> 00:15:50,842
but after he's done,
after he graduates,

332
00:15:50,842 --> 00:15:52,300
he'll probably find
a full time job

333
00:15:52,300 --> 00:15:55,470
and scale back on the driving.

334
00:15:55,470 --> 00:16:00,380
So you fit it into sort of the
life courses, people's life

335
00:16:00,380 --> 00:16:03,920
changes, and responsibilities
change, and time allocations.

336
00:16:03,920 --> 00:16:04,420
Right.

337
00:16:04,420 --> 00:16:08,769
And then the other end
is when people are older,

338
00:16:08,769 --> 00:16:10,310
and they have more
free time, they'll

339
00:16:10,310 --> 00:16:13,780
also become drivers as well.

340
00:16:13,780 --> 00:16:16,740
There are a lot of
controversies in this industry,

341
00:16:16,740 --> 00:16:21,350
as you know, questions
about whether drivers

342
00:16:21,350 --> 00:16:24,810
should be independent
contractors or employees.

343
00:16:24,810 --> 00:16:34,450
Concerns on some chat rooms
and various online linking

344
00:16:34,450 --> 00:16:38,785
mechanisms that Uber doesn't
really care about the drivers.

345
00:16:38,785 --> 00:16:39,660
What are you hearing?

346
00:16:39,660 --> 00:16:41,530
Where are you positioned?

347
00:16:41,530 --> 00:16:43,630
Do you hear these
kinds of things?

348
00:16:43,630 --> 00:16:45,720
Do you see them as credible?

349
00:16:45,720 --> 00:16:48,660
What's your view of
some of the critiques

350
00:16:48,660 --> 00:16:52,610
of this industry at the moment?

351
00:16:52,610 --> 00:16:57,030
I think some concerns
are true in some degree,

352
00:16:57,030 --> 00:17:00,700
and we are kind of
happy that we see

353
00:17:00,700 --> 00:17:03,430
some other groups
supporting workers as well,

354
00:17:03,430 --> 00:17:07,190
and we are honored to be a very
important channel for drivers

355
00:17:07,190 --> 00:17:10,130
to voice out their concerns.

356
00:17:10,130 --> 00:17:12,990
We actually want to take a
neutral, independent position.

357
00:17:12,990 --> 00:17:15,541
I think that the entire
on-demand industry

358
00:17:15,541 --> 00:17:16,540
needs protection, right?

359
00:17:16,540 --> 00:17:19,170
So they would definitely
need to protect workers,

360
00:17:19,170 --> 00:17:22,020
but we'll also need to
protect the service side.

361
00:17:22,020 --> 00:17:25,569
So I think the better
way is, collect data,

362
00:17:25,569 --> 00:17:29,200
build some channels,
allow them to communicate,

363
00:17:29,200 --> 00:17:34,530
I think eventually they will
find a balance somewhere, yeah.

364
00:17:34,530 --> 00:17:38,110
Well I think this role
of providing information

365
00:17:38,110 --> 00:17:44,200
as a basis for fostering more
communications between all

366
00:17:44,200 --> 00:17:46,300
of the parties who have
a stake in this industry

367
00:17:46,300 --> 00:17:50,070
as an important service
that you're providing.

368
00:17:50,070 --> 00:17:54,630
So help me envision
where you will

369
00:17:54,630 --> 00:17:58,620
be five years from now
what will SherpaShare look

370
00:17:58,620 --> 00:17:59,890
like if you are successful?

371
00:18:02,650 --> 00:18:04,380
We want to become the
number one support

372
00:18:04,380 --> 00:18:09,040
platform for on-demand
workers around the globe.

373
00:18:09,040 --> 00:18:13,690
And so currently we are only
focused on the driver vertical

374
00:18:13,690 --> 00:18:17,660
only US bucket, but we want
to expand to other verticals

375
00:18:17,660 --> 00:18:19,880
and other regions as well.

376
00:18:19,880 --> 00:18:24,400
So that's we will hope in
the next five years, yes.

377
00:18:24,400 --> 00:18:27,760
So basically all those workers
will be exchanging information

378
00:18:27,760 --> 00:18:30,500
on our platform, and
so whatever service

379
00:18:30,500 --> 00:18:33,100
they want to work for
whatever opinion they have,

380
00:18:33,100 --> 00:18:35,510
they're use SherpaShare
as like they're

381
00:18:35,510 --> 00:18:37,740
in the exchange for
that information.

382
00:18:37,740 --> 00:18:40,202
So your point earlier
about giving them a voice

383
00:18:40,202 --> 00:18:41,910
is something we really
want to emphasize.

384
00:18:41,910 --> 00:18:45,050
So that we don't necessarily
care what the voice is,

385
00:18:45,050 --> 00:18:49,200
but we give you the channel
to basically express yourself.

386
00:18:49,200 --> 00:18:54,060
The ultimate vision, I'm
thinking, is it's more like a--

387
00:18:54,060 --> 00:18:56,760
drivers say, I have
eight hours for work.

388
00:18:56,760 --> 00:19:01,070
And SherpaShare put up a
working portfolio for him,

389
00:19:01,070 --> 00:19:04,360
so he can hit his financial
goal on a daily basis.

390
00:19:04,360 --> 00:19:09,550
So they can look at where their
optimal opportunities are,

391
00:19:09,550 --> 00:19:13,160
what times of day,
what days of the week,

392
00:19:13,160 --> 00:19:15,830
and how to balance that
with all of their other

393
00:19:15,830 --> 00:19:19,410
responsibilities, and
be efficient in making

394
00:19:19,410 --> 00:19:20,986
those choices.

395
00:19:20,986 --> 00:19:22,110
Well it's very interesting.

396
00:19:22,110 --> 00:19:24,410
It's a very creative
organization.

397
00:19:24,410 --> 00:19:27,830
We appreciate your sharing
your insights with us

398
00:19:27,830 --> 00:19:29,020
for this course.

399
00:19:29,020 --> 00:19:31,480
We wish you all
the best, and thank

400
00:19:31,480 --> 00:19:33,550
you very much for
taking the time

401
00:19:33,550 --> 00:19:35,240
to come across the country.

402
00:19:35,240 --> 00:19:37,740
You don't have an Uber
service in the area.

403
00:19:37,740 --> 00:19:41,210
So I guess you had to
go by commercial airline

404
00:19:41,210 --> 00:19:43,249
until you land, and
then you could--

405
00:19:43,249 --> 00:19:44,540
We didn't take Uber helicopter.

406
00:19:44,540 --> 00:19:46,070
You didn't take Uber helicopter.

407
00:19:46,070 --> 00:19:49,640
Well maybe some day that will
be part of all of our future.

408
00:19:49,640 --> 00:19:50,841
So, thank you very much.

409
00:19:50,841 --> 00:19:51,340
Thank you.

410
00:19:51,340 --> 00:19:52,890
Thanks.