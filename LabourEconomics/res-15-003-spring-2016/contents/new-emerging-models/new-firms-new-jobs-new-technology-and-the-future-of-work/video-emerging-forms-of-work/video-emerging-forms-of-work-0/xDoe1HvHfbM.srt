1
00:00:10,210 --> 00:00:13,520
The way we have talked about
the rise of the social contract

2
00:00:13,520 --> 00:00:15,710
following the Second
World War might

3
00:00:15,710 --> 00:00:17,590
make it seem a little
bit more utopian

4
00:00:17,590 --> 00:00:19,260
than was actually the case.

5
00:00:19,260 --> 00:00:22,650
Wages did rise in tandem
with productivity,

6
00:00:22,650 --> 00:00:26,200
and this meant a high standard
of living for many Americans.

7
00:00:26,200 --> 00:00:29,250
There was also less
emphasis on shareholders

8
00:00:29,250 --> 00:00:31,140
and immediate
short term returns.

9
00:00:31,140 --> 00:00:33,700
There was also high
levels of job stability.

10
00:00:33,700 --> 00:00:36,960
People often came into a company
without very much experience

11
00:00:36,960 --> 00:00:39,030
at all and expected
to work their way up

12
00:00:39,030 --> 00:00:41,880
through promotions over time.

13
00:00:41,880 --> 00:00:45,410
But there we're also downsides
to employment over this period.

14
00:00:45,410 --> 00:00:47,700
The trade off of job
security, of having

15
00:00:47,700 --> 00:00:50,980
a stable career with a
predictable career trajectory

16
00:00:50,980 --> 00:00:54,230
was often the risk of
boredom and monotonous work.

17
00:00:54,230 --> 00:00:57,320
Corporations, in
the 1950s, were very

18
00:00:57,320 --> 00:00:58,950
hierarchical and conformist.

19
00:00:58,950 --> 00:01:03,080
Intellectual diversity and
creativity were discouraged.

20
00:01:03,080 --> 00:01:04,720
These corporations,
of course, we're

21
00:01:04,720 --> 00:01:06,530
also dominated by white men.

22
00:01:06,530 --> 00:01:08,290
There wasn't very
much diversity,

23
00:01:08,290 --> 00:01:10,640
and there were very
sizable gaps in pay

24
00:01:10,640 --> 00:01:12,440
based on gender and race.

25
00:01:12,440 --> 00:01:15,230
So while we can learn
a lot from this period,

26
00:01:15,230 --> 00:01:19,570
it's not exactly a golden age
to which we should return.

27
00:01:19,570 --> 00:01:22,590
We've made a lot of progress
since this period as well.

28
00:01:22,590 --> 00:01:25,250
There's more diversity in the
workforce than ever before.

29
00:01:25,250 --> 00:01:27,640
And pay gaps between
demographic groups,

30
00:01:27,640 --> 00:01:30,560
though they still exist,
are becoming smaller.

31
00:01:30,560 --> 00:01:33,340
With the rise of the
knowledge economy moreover--

32
00:01:33,340 --> 00:01:37,910
an economy that encourages ideas
and creativity and innovation--

33
00:01:37,910 --> 00:01:40,680
workers are being given
more discretion in how

34
00:01:40,680 --> 00:01:43,580
they go about their work and
even the types of projects

35
00:01:43,580 --> 00:01:45,590
on which they work.

36
00:01:45,590 --> 00:01:47,820
Some organizations
are even trying

37
00:01:47,820 --> 00:01:51,660
to make themselves look and feel
as non-corporate-y as possible

38
00:01:51,660 --> 00:01:53,860
as a way to attract
and retain talent,

39
00:01:53,860 --> 00:01:55,300
and there's evidence
that this can

40
00:01:55,300 --> 00:01:57,040
be good for the bottom line.

41
00:01:57,040 --> 00:02:00,230
In addition, due in
large part to advances

42
00:02:00,230 --> 00:02:03,700
in information
technology, new technology

43
00:02:03,700 --> 00:02:06,870
mediated work arrangements
are becoming more popular

44
00:02:06,870 --> 00:02:10,340
and facilitating various
forms of freelance work.

45
00:02:10,340 --> 00:02:12,200
Freelancing is
not new of course.

46
00:02:12,200 --> 00:02:14,760
The word actually dates
back to medieval times

47
00:02:14,760 --> 00:02:18,430
where one could be a
freelance mercenary for hire.

48
00:02:18,430 --> 00:02:20,850
Various new websites
and applications

49
00:02:20,850 --> 00:02:23,200
are springing up
throughout the economy that

50
00:02:23,200 --> 00:02:25,150
connect people--
buyers and sellers--

51
00:02:25,150 --> 00:02:27,030
in unique and innovative ways.

52
00:02:27,030 --> 00:02:29,300
So for software,
for example, there's

53
00:02:29,300 --> 00:02:31,420
companies like
freelancer.com where

54
00:02:31,420 --> 00:02:34,610
someone can go on and post a
project, for example, solicit

55
00:02:34,610 --> 00:02:36,800
help coding a new
website, and people

56
00:02:36,800 --> 00:02:39,160
will bid on those projects.

57
00:02:39,160 --> 00:02:43,070
I actually used one of these
sites not for a website per se,

58
00:02:43,070 --> 00:02:45,800
but to help me learn a new
coding language called Python.

59
00:02:45,800 --> 00:02:46,910
I went on the site.

60
00:02:46,910 --> 00:02:48,440
I posted my project.

61
00:02:48,440 --> 00:02:51,490
And I found an undergraduate
student based in California--

62
00:02:51,490 --> 00:02:54,610
a very bright young man--
who stayed up with me

63
00:02:54,610 --> 00:02:56,760
into the early hours of
the morning helping me

64
00:02:56,760 --> 00:02:58,380
learn how to code.

65
00:02:58,380 --> 00:03:01,700
And there are plenty of other
examples of technology mediated

66
00:03:01,700 --> 00:03:02,220
work--

67
00:03:02,220 --> 00:03:03,910
Mechanical Turk,
for example, where

68
00:03:03,910 --> 00:03:07,360
buyers can pay people to do
basic, often routine tasks

69
00:03:07,360 --> 00:03:08,730
like taking surveys.

70
00:03:08,730 --> 00:03:12,430
Taskrabbit.com, where people
can find people to do basic life

71
00:03:12,430 --> 00:03:13,330
chores for them.

72
00:03:13,330 --> 00:03:15,540
And care.com, where
people can locate,

73
00:03:15,540 --> 00:03:19,210
for example, house and pet
sitters for when they are away.

74
00:03:19,210 --> 00:03:21,290
These alternative
modes of employment

75
00:03:21,290 --> 00:03:24,350
have distinct advantages
in terms of flexibility

76
00:03:24,350 --> 00:03:25,700
and individual expression.

77
00:03:25,700 --> 00:03:28,340
People don't
typically have a boss

78
00:03:28,340 --> 00:03:31,430
looking over their shoulder, at
least not in a physical sense.

79
00:03:31,430 --> 00:03:34,170
They have flexibility
to work different hours,

80
00:03:34,170 --> 00:03:38,230
often from various locations,
including their own home.

81
00:03:38,230 --> 00:03:40,540
But there are common
reoccurring disadvantages

82
00:03:40,540 --> 00:03:42,190
to these modes of work as well.

83
00:03:42,190 --> 00:03:44,940
There are high levels of
uncertainty and insecurity.

84
00:03:44,940 --> 00:03:46,730
You don't know whether
you'll have work

85
00:03:46,730 --> 00:03:48,170
from one month to the next.

86
00:03:48,170 --> 00:03:50,114
You don't know who
your next boss will be,

87
00:03:50,114 --> 00:03:52,280
whether they'll be fair and
ethical, whether they'll

88
00:03:52,280 --> 00:03:54,227
pay in a timely way.

89
00:03:54,227 --> 00:03:55,810
If you have an issue
with an employer,

90
00:03:55,810 --> 00:03:57,720
for example, withholding
promised pay,

91
00:03:57,720 --> 00:04:00,610
it's often difficult to
resolve these issues.

92
00:04:00,610 --> 00:04:03,490
There's broad, often
global competition,

93
00:04:03,490 --> 00:04:06,060
which could put downward
pressure on wages.

94
00:04:06,060 --> 00:04:09,360
So for example, when
I hired this coder

95
00:04:09,360 --> 00:04:11,120
to help me with this
freelancing project,

96
00:04:11,120 --> 00:04:14,922
he was competing against people
located in China and India.

97
00:04:14,922 --> 00:04:16,380
There are other
trade offs as well.

98
00:04:16,380 --> 00:04:19,380
There's isolation, a lack
of pension and retirement,

99
00:04:19,380 --> 00:04:22,060
lack of paid vacation
and time off,

100
00:04:22,060 --> 00:04:25,430
the need for continuous
learning that can accommodate

101
00:04:25,430 --> 00:04:28,360
the diverse and changing
needs of employers,

102
00:04:28,360 --> 00:04:30,700
and needing to finance
the education out

103
00:04:30,700 --> 00:04:31,790
of your own pocket.

104
00:04:31,790 --> 00:04:34,090
In addition, many
of these employees

105
00:04:34,090 --> 00:04:36,870
lack the protections
that are traditionally

106
00:04:36,870 --> 00:04:42,330
afforded to workers under more
stable employment contracts.