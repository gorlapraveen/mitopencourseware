1
00:00:10,660 --> 00:00:13,350
In the last video, we talked
about freelancing work,

2
00:00:13,350 --> 00:00:13,620
including new
technology-mediated work

3
00:00:13,620 --> 00:00:14,161
arrangements.

4
00:00:14,161 --> 00:00:16,280
In this video, we're going
to take a look at Uber

5
00:00:16,280 --> 00:00:17,340
as a case study.

6
00:00:17,340 --> 00:00:19,890
Many of you have probably
heard of Uber by now.

7
00:00:19,890 --> 00:00:28,310
Their main business
model is to connect

8
00:00:28,310 --> 00:00:30,280
people who need a
ride with drivers

9
00:00:30,280 --> 00:00:32,000
who are willing to
drive them, using

10
00:00:32,000 --> 00:00:33,130
their own personal vehicle.

11
00:00:33,130 --> 00:00:36,040
And if this sounds suspiciously
like a cab company,

12
00:00:36,040 --> 00:00:36,720
you're right.

13
00:00:36,720 --> 00:00:40,020
Uber competes directly with
traditional cab companies.

14
00:00:40,020 --> 00:00:43,360
In contrast, however, Uber
drivers use their own vehicle.

15
00:00:43,360 --> 00:00:45,750
And this means they're
responsible for the costs

16
00:00:45,750 --> 00:00:46,570
associated with maintenance
and repair, the wear and tear

17
00:00:46,570 --> 00:00:48,150
on their own
vehicle, and largely

18
00:00:48,150 --> 00:00:49,820
fueling their own vehicle.

19
00:00:49,820 --> 00:00:52,050
The main service
provided by Uber

20
00:00:52,050 --> 00:00:56,940
is this technology, which
connects people and facilitates

21
00:00:56,940 --> 00:00:57,960
automatic payments.

22
00:01:04,410 --> 00:01:06,510
Early on, Uber and
companies like it,

23
00:01:06,510 --> 00:01:08,800
were seen as ushering in
a new and very exciting

24
00:01:08,800 --> 00:01:09,410
model of work.

25
00:01:09,410 --> 00:01:10,820
There was high flexibility
in terms of when you worked,

26
00:01:10,820 --> 00:01:12,300
and where you worked.

27
00:01:12,300 --> 00:01:15,340
Uber even claimed early on that
the median salary for a driver,

28
00:01:15,340 --> 00:01:18,130
working at least 40 hours
a week in New York City,

29
00:01:18,130 --> 00:01:20,500
was $90,000 a year.

30
00:01:20,500 --> 00:01:23,540
Since then, people have begun
to challenge these figures.

31
00:01:23,540 --> 00:01:56,871
And darker aspects of work at
Uber have begun to surface.

32
00:01:56,871 --> 00:01:58,870
Uber started out with
higher fares, for example.

33
00:01:58,870 --> 00:02:01,016
But has cut them on
multiple occasions,

34
00:02:01,016 --> 00:02:02,390
in order to try
to better compete

35
00:02:02,390 --> 00:02:03,681
with traditional cab companies.

36
00:02:03,681 --> 00:02:06,710
In fact, there are reports
that UberX drivers, UberX

37
00:02:06,710 --> 00:02:09,750
being the most commonly
used, lowest-cost service,

38
00:02:09,750 --> 00:02:12,630
make only a little bit more
than traditional cab drivers.

39
00:02:12,630 --> 00:02:15,100
And again, these drivers are
responsible for the costs

40
00:02:15,100 --> 00:02:23,480
associated with using
their own vehicle

41
00:02:23,480 --> 00:02:24,810
over extended periods of time.

42
00:02:24,810 --> 00:02:26,860
And there's some evidence
that Uber drivers

43
00:02:26,860 --> 00:02:28,740
are becoming
increasingly disgruntled,

44
00:02:28,740 --> 00:02:30,420
because of this price
cutting, because

45
00:02:30,420 --> 00:02:36,660
of verbal abuse by passengers,
and a perception of apathy

46
00:02:36,660 --> 00:02:39,220
on the part of Uber to
do anything about it.

47
00:02:39,220 --> 00:02:42,050
They're frustrated, as well,
because of the ranking system,

48
00:02:42,050 --> 00:02:43,640
which will essentially
make them lose

49
00:02:43,640 --> 00:02:47,430
their job if their
passenger rating falls

50
00:02:47,430 --> 00:02:50,370
below a certain level.

51
00:02:50,370 --> 00:02:53,540
And some workers in some
areas like California,

52
00:02:53,540 --> 00:02:57,100
have begun to try to organize
and collectively bargain.

53
00:02:57,100 --> 00:02:59,880
So, for example, in
California drivers

54
00:02:59,880 --> 00:03:05,179
formed CADA, the California
App-based Drivers Association.

55
00:03:05,179 --> 00:03:07,720
But these drivers face a tough
and very difficult road ahead.

56
00:03:07,720 --> 00:03:11,414
The US law does not
require Uber to negotiate,

57
00:03:11,414 --> 00:03:13,330
because these drivers
are currently considered

58
00:03:13,330 --> 00:03:14,740
independent contractors.

59
00:03:14,740 --> 00:03:17,810
In other words,
they're not employees

60
00:03:17,810 --> 00:03:20,498
from Uber's point of view.

61
00:03:32,450 --> 00:03:34,346
They're not their problem.

62
00:03:37,920 --> 00:03:39,440
So as it stands,
this association,

63
00:03:39,440 --> 00:03:42,120
the California App-based
Drivers Association,

64
00:03:42,120 --> 00:03:44,680
is trying to bring publicity
to some of these issues.

65
00:03:44,680 --> 00:03:48,220
But it doesn't have very
much leverage beyond that.

66
00:03:48,220 --> 00:04:20,940
So the question that
I would pose to you

67
00:04:20,940 --> 00:04:23,350
as a class is, what
do you recommend

68
00:04:23,350 --> 00:04:27,620
that Uber drivers do to improve
their employment circumstances?

69
00:04:27,620 --> 00:04:37,350
I encourage you to
join in discussion

70
00:04:37,350 --> 00:04:39,940
with your fellow classmates
in the next segment

71
00:04:39,940 --> 00:04:41,860
of this course.