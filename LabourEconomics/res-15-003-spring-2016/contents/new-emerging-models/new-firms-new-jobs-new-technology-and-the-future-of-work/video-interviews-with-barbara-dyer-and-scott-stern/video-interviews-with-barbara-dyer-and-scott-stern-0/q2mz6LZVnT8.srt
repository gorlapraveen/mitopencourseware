1
00:00:10,400 --> 00:00:13,330
We're talking today about the
role that entrepreneurs play

2
00:00:13,330 --> 00:00:15,660
in shaping the future of work.

3
00:00:15,660 --> 00:00:18,690
We're talking with Professor
Scott Stern, the Chair

4
00:00:18,690 --> 00:00:22,210
of our Sloan School's
Technological Innovation,

5
00:00:22,210 --> 00:00:24,210
Entrepreneurship
and Strategy Group,

6
00:00:24,210 --> 00:00:27,470
and with Barbara Dyer,
the President of Hitachi

7
00:00:27,470 --> 00:00:28,710
Foundation.

8
00:00:28,710 --> 00:00:31,370
The foundation works
with entrepreneurs

9
00:00:31,370 --> 00:00:33,760
to build both good
companies that

10
00:00:33,760 --> 00:00:36,770
are financially successful,
as well as ones that

11
00:00:36,770 --> 00:00:40,280
provide good job opportunities.

12
00:00:40,280 --> 00:00:43,100
Can entrepreneurs build
a good job strategy

13
00:00:43,100 --> 00:00:45,600
creating good opportunities
for the full workforce

14
00:00:45,600 --> 00:00:47,110
in from the beginning?

15
00:00:47,110 --> 00:00:50,190
So I think one of the
greatest misconceptions

16
00:00:50,190 --> 00:00:52,860
is that being a
good entrepreneur

17
00:00:52,860 --> 00:00:57,190
and building a company that
really scales is actually

18
00:00:57,190 --> 00:01:00,670
in opposition to
building a company that

19
00:01:00,670 --> 00:01:01,920
creates great jobs.

20
00:01:01,920 --> 00:01:06,440
In fact, I'd claim that
exactly the reverse is true.

21
00:01:06,440 --> 00:01:09,000
To attract the
really best people,

22
00:01:09,000 --> 00:01:13,720
to really create meaningful,
innovative value for consumers,

23
00:01:13,720 --> 00:01:16,630
to really be in a situation
where ultimately you

24
00:01:16,630 --> 00:01:19,950
are able to charge a
price well above your cost

25
00:01:19,950 --> 00:01:22,330
and create that
kind of value, means

26
00:01:22,330 --> 00:01:25,610
that you need to be
attracting, nurturing,

27
00:01:25,610 --> 00:01:28,680
investing in employees
in meaningful ways.

28
00:01:28,680 --> 00:01:31,550
If everyone who shows up
to your company is simply,

29
00:01:31,550 --> 00:01:34,350
all it is about cost,
well, then, there's

30
00:01:34,350 --> 00:01:35,770
not much of a company at all.

31
00:01:35,770 --> 00:01:38,180
There's not much of
an organization there.

32
00:01:38,180 --> 00:01:40,770
What we've observed at
the nearly 100 companies

33
00:01:40,770 --> 00:01:44,000
that we've looked at across
health care and manufacturing

34
00:01:44,000 --> 00:01:49,150
is at the businesses that
create great quality jobs don't

35
00:01:49,150 --> 00:01:52,870
begin with the premise that
this is a good HR strategy.

36
00:01:52,870 --> 00:01:56,540
They start with,
what is excellence?

37
00:01:56,540 --> 00:02:02,310
And they very quickly figure out
that the workforce piece of it

38
00:02:02,310 --> 00:02:05,710
is a critical part
of operations.

39
00:02:05,710 --> 00:02:07,840
They have to invest
in their people,

40
00:02:07,840 --> 00:02:10,289
and they have to
create systems that

41
00:02:10,289 --> 00:02:15,620
provide ongoing support, ongoing
learning, ongoing incentives

42
00:02:15,620 --> 00:02:20,300
so that their people are
really delivering in the best

43
00:02:20,300 --> 00:02:22,230
possible way.

44
00:02:22,230 --> 00:02:27,400
We hear that many investors have
a mindset that labor is a cost

45
00:02:27,400 --> 00:02:30,960
and it ought to be minimized,
and put a lot of pressure

46
00:02:30,960 --> 00:02:32,680
on entrepreneurs.

47
00:02:32,680 --> 00:02:34,910
How can we overcome
that resistance?

48
00:02:34,910 --> 00:02:38,090
We have to do the hard work
of persuading investors

49
00:02:38,090 --> 00:02:42,990
that it's not a choice
between lower profits,

50
00:02:42,990 --> 00:02:50,010
but a better workforce versus
higher profits and a cheaper

51
00:02:50,010 --> 00:02:51,080
workforce.

52
00:02:51,080 --> 00:02:57,030
Instead, the choice is for
equally viable businesses,

53
00:02:57,030 --> 00:03:01,200
we get to choose the kind
of organizations we build.

54
00:03:01,200 --> 00:03:03,340
Successful venture
capitalists who

55
00:03:03,340 --> 00:03:06,400
have built successful
companies, they meaningfully

56
00:03:06,400 --> 00:03:09,910
talk about the great
organizations they've built.

57
00:03:09,910 --> 00:03:14,890
Entrepreneurs are uncomfortable
talking to investors

58
00:03:14,890 --> 00:03:17,470
about a workforce strategy.

59
00:03:17,470 --> 00:03:20,610
They are concerned
that investors

60
00:03:20,610 --> 00:03:26,810
will view this as excess, as
having too high a burn rate.

61
00:03:26,810 --> 00:03:30,890
They don't have fully at their
grasp the economics of it.

62
00:03:30,890 --> 00:03:32,910
They can't really
make the business case

63
00:03:32,910 --> 00:03:34,990
as well as they might
like to as to why

64
00:03:34,990 --> 00:03:37,950
it is that building
a strategy like this

65
00:03:37,950 --> 00:03:42,410
will end up achieving the
numbers in the next two, five,

66
00:03:42,410 --> 00:03:43,700
ten years.

67
00:03:43,700 --> 00:03:48,400
There is a tendency to expect
that at the startup phase,

68
00:03:48,400 --> 00:03:52,050
the founders will certainly
work for below market rates,

69
00:03:52,050 --> 00:03:53,750
and that the people
that are hired

70
00:03:53,750 --> 00:03:55,520
will work for
below market rates.

71
00:03:55,520 --> 00:03:56,950
They'll work around the clock.

72
00:03:56,950 --> 00:04:01,860
There should be no expectation
of salary increases or benefits

73
00:04:01,860 --> 00:04:04,670
because the company hasn't
started generating a profit.

74
00:04:04,670 --> 00:04:05,860
Now you're a scholar.

75
00:04:05,860 --> 00:04:07,580
You know that the
research tells us

76
00:04:07,580 --> 00:04:10,380
that if you build into
the DNA of a company

77
00:04:10,380 --> 00:04:12,680
from the very start
this kind of commitment,

78
00:04:12,680 --> 00:04:18,399
this kind of strategy, it'll
be the driving framework

79
00:04:18,399 --> 00:04:20,110
for the company over time.

80
00:04:20,110 --> 00:04:23,220
And so that's where
all the evidence

81
00:04:23,220 --> 00:04:25,050
from other
organizations plays in.

82
00:04:25,050 --> 00:04:27,400
If you can help them
to make that business

83
00:04:27,400 --> 00:04:30,072
case, to understand
the potential,

84
00:04:30,072 --> 00:04:31,030
then there's some hope.

85
00:04:31,030 --> 00:04:33,940
And so we have to find
ways to deal with that.

86
00:04:33,940 --> 00:04:36,810
Could you give us an example
of an organization or two

87
00:04:36,810 --> 00:04:39,330
that has really
invested in this way?

88
00:04:39,330 --> 00:04:41,000
Take for example, Beepi.

89
00:04:41,000 --> 00:04:44,490
Beepi is an online
car marketplace

90
00:04:44,490 --> 00:04:48,210
that's really kind of fixing the
lemons problem for used cars.

91
00:04:48,210 --> 00:04:50,740
Basically, right now you can
think of the used car industry

92
00:04:50,740 --> 00:04:54,570
as definitely not associated
with a good job strategy, not

93
00:04:54,570 --> 00:04:56,500
even a good service.

94
00:04:56,500 --> 00:04:58,506
One of the key choices
that Ale Resnik, one

95
00:04:58,506 --> 00:04:59,880
of the founders
of this firm, had

96
00:04:59,880 --> 00:05:02,550
to make was whether or not the
technicians, the people who

97
00:05:02,550 --> 00:05:07,060
would be inspecting the
cars, was going to be hiring

98
00:05:07,060 --> 00:05:09,700
a series of
contractor inspectors,

99
00:05:09,700 --> 00:05:11,680
or whether or not
he'd be investing

100
00:05:11,680 --> 00:05:14,160
in long-term employees.

101
00:05:14,160 --> 00:05:16,400
He got different advice
from different investors

102
00:05:16,400 --> 00:05:18,380
about that choice.

103
00:05:18,380 --> 00:05:23,000
He ultimately decided that
the strength and the quality

104
00:05:23,000 --> 00:05:24,820
and the innovations
of his company

105
00:05:24,820 --> 00:05:31,360
could best be made by
investing in employees.

106
00:05:31,360 --> 00:05:34,120
All those employees
get extensive training.

107
00:05:34,120 --> 00:05:35,810
They're upping
their skill level.

108
00:05:35,810 --> 00:05:37,750
They get some equity
in the company.

109
00:05:37,750 --> 00:05:39,380
And as that company
has grown, all

110
00:05:39,380 --> 00:05:43,890
of a sudden they are the
single most visible and single

111
00:05:43,890 --> 00:05:47,190
most memorable and single
best advocates for the company

112
00:05:47,190 --> 00:05:51,270
as it scaled across
the United States.

113
00:05:51,270 --> 00:05:54,780
Jessamyn Rodriguez, who is the
founder of Hot Bread Kitchens

114
00:05:54,780 --> 00:05:56,650
started with a long-term vision.

115
00:05:56,650 --> 00:06:03,630
The idea was to enable primarily
immigrant, but other women

116
00:06:03,630 --> 00:06:06,830
and men, who have
an ethnic bread

117
00:06:06,830 --> 00:06:11,520
to take that to the
market, to create

118
00:06:11,520 --> 00:06:15,410
real economic opportunity
through baking bread,

119
00:06:15,410 --> 00:06:19,640
and to enable them to
build their own businesses

120
00:06:19,640 --> 00:06:22,970
around their particular craft.

121
00:06:22,970 --> 00:06:27,010
And so Hot Bread Kitchen, now
a very successful operation

122
00:06:27,010 --> 00:06:32,300
in Harlem, is incubating bread
businesses throughout New York

123
00:06:32,300 --> 00:06:33,120
and beyond.

124
00:06:33,120 --> 00:06:37,180
So it's a bakery, yes, but
it's far more than that.

125
00:06:37,180 --> 00:06:40,730
And to help individuals
to kind of build

126
00:06:40,730 --> 00:06:43,310
math skills while
they're creating breads,

127
00:06:43,310 --> 00:06:46,250
to build business skills
while they're creating breads,

128
00:06:46,250 --> 00:06:49,110
to build marketing skills
while they're creating breads,

129
00:06:49,110 --> 00:06:51,770
was a brilliant touch.

130
00:06:51,770 --> 00:06:56,130
The bakery is a great example,
a place I should clearly visit.

131
00:06:56,130 --> 00:06:58,070
How about other examples?

132
00:06:58,070 --> 00:07:01,530
Recently we were
supporting a sector

133
00:07:01,530 --> 00:07:04,110
of energy entrepreneurs
looking to address

134
00:07:04,110 --> 00:07:05,780
issues of climate change.

135
00:07:05,780 --> 00:07:09,560
And one company, Rebound, is
a remarkable refrigeration

136
00:07:09,560 --> 00:07:14,350
company that has the potential
of saving supermarkets

137
00:07:14,350 --> 00:07:17,270
enormous amount of money
in refrigeration costs

138
00:07:17,270 --> 00:07:21,010
and dramatically reducing
the amount of CO2.

139
00:07:21,010 --> 00:07:26,440
The founder of that company is
eager to build a business that

140
00:07:26,440 --> 00:07:31,320
is focused on good people,
great jobs, pathways

141
00:07:31,320 --> 00:07:32,480
of upward mobility.

142
00:07:32,480 --> 00:07:37,300
It's who he is, it's where
he wants to take his company.

143
00:07:37,300 --> 00:07:40,760
Entrepreneurship can be
used to provide good jobs.

144
00:07:40,760 --> 00:07:43,230
How do we get this
to become the norm?

145
00:07:43,230 --> 00:07:45,540
People need to vote
with their feet.

146
00:07:45,540 --> 00:07:48,760
When you think about joining a
company, when you think about--

147
00:07:48,760 --> 00:07:51,130
say you're in a big company
thinking about which kind

148
00:07:51,130 --> 00:07:52,630
of companies to partner with--

149
00:07:52,630 --> 00:07:55,980
when you think about,
even as an investor,

150
00:07:55,980 --> 00:07:58,119
make sure you're having
that conversation,

151
00:07:58,119 --> 00:07:59,660
and understanding
as you're selecting

152
00:07:59,660 --> 00:08:04,220
among different groups to work
with, different firms to work

153
00:08:04,220 --> 00:08:06,400
with, even different
portfolio companies

154
00:08:06,400 --> 00:08:10,190
to invest in, go one layer
beyond the simple value

155
00:08:10,190 --> 00:08:13,040
creation, value capture logic,
and get to the organization

156
00:08:13,040 --> 00:08:14,070
and the team.

157
00:08:14,070 --> 00:08:17,570
You are more likely
to be investing

158
00:08:17,570 --> 00:08:23,970
in a long-term play that really
delivers fundamental value

159
00:08:23,970 --> 00:08:26,740
and really delivers
strong returns

160
00:08:26,740 --> 00:08:29,710
if you're investing
in the kind of company

161
00:08:29,710 --> 00:08:32,220
that people are going
to be attracted to

162
00:08:32,220 --> 00:08:34,480
and allows people
to nurture and grow.

163
00:08:34,480 --> 00:08:37,370
Well Scott, I think this
is an important message

164
00:08:37,370 --> 00:08:42,250
for entrepreneurs, for
investors, and for young people

165
00:08:42,250 --> 00:08:46,310
who really do want to build
great companies that address

166
00:08:46,310 --> 00:08:49,040
big problems in society,
that are successful,

167
00:08:49,040 --> 00:08:51,310
and that provide
good jobs and career

168
00:08:51,310 --> 00:08:52,602
opportunities to their peers.

169
00:08:52,602 --> 00:08:53,560
So thank you very much.

170
00:08:53,560 --> 00:08:55,010
Thank you very much as well.

171
00:08:55,010 --> 00:08:55,830
Thank you so much.

172
00:08:55,830 --> 00:08:57,080
Thank you for your time today.

173
00:08:57,080 --> 00:08:58,690
You're welcome.