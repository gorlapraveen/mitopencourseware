1
00:00:06,210 --> 00:00:07,970
What a busy week we had.

2
00:00:07,970 --> 00:00:11,550
Because we covered so
much ground this past week

3
00:00:11,550 --> 00:00:14,930
and because we're almost at
the midpoint of this course,

4
00:00:14,930 --> 00:00:17,130
I thought it would be
useful to summarize

5
00:00:17,130 --> 00:00:18,980
some of the key
lessons from the week

6
00:00:18,980 --> 00:00:22,590
and really one of the key
lessons from this course.

7
00:00:22,590 --> 00:00:24,460
That lesson is very simple.

8
00:00:24,460 --> 00:00:29,500
It is that our economies need to
continue to encourage and grow

9
00:00:29,500 --> 00:00:32,000
what we call high
road firms that

10
00:00:32,000 --> 00:00:35,640
are both good for
shareholders and for employees

11
00:00:35,640 --> 00:00:38,570
if we are really to shape
the future of work that

12
00:00:38,570 --> 00:00:42,230
works for all the
stakeholders in our economy.

13
00:00:42,230 --> 00:00:44,130
How can organizations do this?

14
00:00:44,130 --> 00:00:48,220
Well, it starts by having a
debate over the basic values

15
00:00:48,220 --> 00:00:48,950
of the firm.

16
00:00:48,950 --> 00:00:50,440
And you did that this week.

17
00:00:50,440 --> 00:00:53,600
You took on the question
of whether this emphasis

18
00:00:53,600 --> 00:00:57,910
on shareholder value, or what
we called financialization

19
00:00:57,910 --> 00:01:02,000
view of the firm, is really
the only way in which firms

20
00:01:02,000 --> 00:01:02,980
can compete.

21
00:01:02,980 --> 00:01:05,019
And you answered
it very clearly.

22
00:01:05,019 --> 00:01:07,810
You endorsed firms
that should be

23
00:01:07,810 --> 00:01:12,160
accountable to multiple
stakeholders, to employees,

24
00:01:12,160 --> 00:01:16,130
to societies, as well
as to their investors

25
00:01:16,130 --> 00:01:17,560
and their owners.

26
00:01:17,560 --> 00:01:21,940
And you noticed that there's a
wide variety of different forms

27
00:01:21,940 --> 00:01:25,280
that organizations take, both
here in the United States

28
00:01:25,280 --> 00:01:26,250
and around the world.

29
00:01:26,250 --> 00:01:28,870
And in fact, the US is
one of the few countries

30
00:01:28,870 --> 00:01:32,490
that emphasizes shareholder
value so heavily.

31
00:01:32,490 --> 00:01:36,770
You also noted that it not
only varies across countries,

32
00:01:36,770 --> 00:01:39,790
but even in the US
we didn't always

33
00:01:39,790 --> 00:01:42,550
put such a heavy emphasis
on shareholder value.

34
00:01:42,550 --> 00:01:45,200
There was a point in
time when we recognized

35
00:01:45,200 --> 00:01:51,450
and that CEOs recognized that
they need to have organizations

36
00:01:51,450 --> 00:01:54,620
that address both the interests
of the people who invest

37
00:01:54,620 --> 00:01:56,810
in them, who own
them, but also they

38
00:01:56,810 --> 00:01:59,550
were held accountable
to their employees,

39
00:01:59,550 --> 00:02:01,860
to their communities,
and societies.

40
00:02:01,860 --> 00:02:04,080
So we had this debate.

41
00:02:04,080 --> 00:02:05,870
We have to keep
that debate going.

42
00:02:05,870 --> 00:02:08,410
In the poll that you
answered, a vast majority [?

43
00:02:08,410 --> 00:02:11,570
of you ?] endorsed the
stakeholder view of the firm.

44
00:02:11,570 --> 00:02:13,510
So let's get on with it.

45
00:02:13,510 --> 00:02:16,400
But you also noted that
there's no magic bullet.

46
00:02:16,400 --> 00:02:20,280
There's no single practice
that high road firms can follow

47
00:02:20,280 --> 00:02:22,150
to achieve these outcomes.

48
00:02:22,150 --> 00:02:26,540
Instead, what we saw is that
there's a bundle of practices,

49
00:02:26,540 --> 00:02:29,190
or what we call a
system of practices,

50
00:02:29,190 --> 00:02:32,160
of employment practices,
operational features

51
00:02:32,160 --> 00:02:34,640
of the organization,
business practices that

52
00:02:34,640 --> 00:02:36,750
have to fit together
and be fitted

53
00:02:36,750 --> 00:02:39,820
to the particular industries
and particular organizational

54
00:02:39,820 --> 00:02:42,900
settings, particular
cultures and countries

55
00:02:42,900 --> 00:02:44,790
in which they are embedded.

56
00:02:44,790 --> 00:02:48,550
And this is just a list of the
basic things that we discussed.

57
00:02:48,550 --> 00:02:52,830
That, yes, it starts by hiring
good, strong employees who

58
00:02:52,830 --> 00:02:55,130
have good technical
skills and good people

59
00:02:55,130 --> 00:02:58,680
in organizational skills so
they can work together in teams

60
00:02:58,680 --> 00:03:00,540
and communicate with each other.

61
00:03:00,540 --> 00:03:02,880
Then we invest in
training and development

62
00:03:02,880 --> 00:03:06,980
and make sure that we create
a culture of respect and trust

63
00:03:06,980 --> 00:03:08,170
at the workplace.

64
00:03:08,170 --> 00:03:11,000
Provide employees with
a good opportunity

65
00:03:11,000 --> 00:03:13,720
to improve operations
in the organization,

66
00:03:13,720 --> 00:03:16,210
to bring their voice
into the discussion

67
00:03:16,210 --> 00:03:18,270
so that their
interests are reflected

68
00:03:18,270 --> 00:03:21,000
in organizational
practices themselves.

69
00:03:21,000 --> 00:03:23,480
And then recognize
that you all want

70
00:03:23,480 --> 00:03:26,330
good, flexible organizational
arrangements that

71
00:03:26,330 --> 00:03:30,230
allow you to mix your personal
and your family and your work

72
00:03:30,230 --> 00:03:34,020
lives, but to do it in creative
ways with modern technology,

73
00:03:34,020 --> 00:03:36,750
with fair wages that are
linked, in some ways,

74
00:03:36,750 --> 00:03:39,660
to productivity, to
performance, to the economy

75
00:03:39,660 --> 00:03:41,240
of your organizations.

76
00:03:41,240 --> 00:03:43,950
And where there are
unions involved,

77
00:03:43,950 --> 00:03:46,150
we need to have
strong partnerships

78
00:03:46,150 --> 00:03:48,440
between labor and
management, not

79
00:03:48,440 --> 00:03:50,660
the traditional
adversarial relations.

80
00:03:50,660 --> 00:03:53,590
When we do those things,
we get this bundle.

81
00:03:53,590 --> 00:03:56,620
We get this system of
reinforcing practices

82
00:03:56,620 --> 00:03:58,630
between the business
strategies that

83
00:03:58,630 --> 00:04:02,760
are followed to create high road
organizations, the employment

84
00:04:02,760 --> 00:04:06,200
systems, the operational
practices, and the workforce

85
00:04:06,200 --> 00:04:06,790
culture.

86
00:04:06,790 --> 00:04:10,200
So we know something
about how to do this.

87
00:04:10,200 --> 00:04:15,440
And we know that this can
happen not only in traditional

88
00:04:15,440 --> 00:04:18,660
profit-making firms or
public corporations,

89
00:04:18,660 --> 00:04:21,769
but we discussed a whole range
of different organizational

90
00:04:21,769 --> 00:04:22,580
forms--

91
00:04:22,580 --> 00:04:25,260
family businesses
like Market Basket,

92
00:04:25,260 --> 00:04:28,090
public corporations,
as we mentioned,

93
00:04:28,090 --> 00:04:31,290
benefit corporations
like Patagonia,

94
00:04:31,290 --> 00:04:35,520
employee-owned firms like my two
favorite breweries, New Belgium

95
00:04:35,520 --> 00:04:39,660
and Harpoon, or in other
countries, IDEO in Sweden

96
00:04:39,660 --> 00:04:40,660
and around the world.

97
00:04:40,660 --> 00:04:43,480
There are examples of
employee-owned firms.

98
00:04:43,480 --> 00:04:48,080
Community ownership-- Mondragon
in Spain and other countries.

99
00:04:48,080 --> 00:04:51,350
And then my favorite football
team, the Green Bay Packers,

100
00:04:51,350 --> 00:04:54,630
owned by the community
and maintaining

101
00:04:54,630 --> 00:04:58,160
long-standing good relationships
with their stakeholders

102
00:04:58,160 --> 00:04:59,460
and their environments.

103
00:04:59,460 --> 00:05:01,720
We also discussed
whether it's possible

104
00:05:01,720 --> 00:05:06,520
for global corporations to
hold to high road standards

105
00:05:06,520 --> 00:05:07,860
in their supply chains.

106
00:05:07,860 --> 00:05:10,190
We've got a lot of work
to do in this area.

107
00:05:10,190 --> 00:05:12,310
It's clear that we
have a long way to go.

108
00:05:12,310 --> 00:05:15,490
But companies and their
suppliers and NGOs

109
00:05:15,490 --> 00:05:16,720
are all working on it.

110
00:05:16,720 --> 00:05:20,070
So we recognize we can do
this in a variety of ways.

111
00:05:20,070 --> 00:05:23,220
But I was particularly
excited and pleased

112
00:05:23,220 --> 00:05:26,140
with the examples of high
road and low road firms

113
00:05:26,140 --> 00:05:29,250
that you talked about in your
own assignments this week.

114
00:05:29,250 --> 00:05:32,400
And you brought your
own personal experiences

115
00:05:32,400 --> 00:05:36,290
as employees and sometimes as
customers into this process.

116
00:05:36,290 --> 00:05:41,110
And here's just an example of
firms in the retail industry--

117
00:05:41,110 --> 00:05:43,230
a family-owned firm, Wegmans.

118
00:05:43,230 --> 00:05:45,520
That's a local one here
in the United States.

119
00:05:45,520 --> 00:05:49,190
Whole Foods, which is known very
well, was used as an example

120
00:05:49,190 --> 00:05:52,780
and compared with some other
organizations, one well known

121
00:05:52,780 --> 00:05:55,760
and another one where we
just disguised the name a bit

122
00:05:55,760 --> 00:06:00,290
in another country that
talked about the low wages

123
00:06:00,290 --> 00:06:04,870
and lack of a living wage, not
particularly paying attention

124
00:06:04,870 --> 00:06:07,550
to employee training
and development.

125
00:06:07,550 --> 00:06:11,730
But also noting that even
in government agencies,

126
00:06:11,730 --> 00:06:15,690
you had some very low road
firms or organizations

127
00:06:15,690 --> 00:06:18,660
that favored their
certain employees

128
00:06:18,660 --> 00:06:21,320
and didn't provide the kind
of feedback and opportunities

129
00:06:21,320 --> 00:06:21,990
for growth.

130
00:06:21,990 --> 00:06:25,080
But then there were some
libraries, hospitals,

131
00:06:25,080 --> 00:06:27,990
government agencies that you
rated as high road firms.

132
00:06:27,990 --> 00:06:32,520
The key point here is there's
no single organizational form,

133
00:06:32,520 --> 00:06:35,250
and there's no single
model that says

134
00:06:35,250 --> 00:06:38,840
you can only do this in some
sectors of the economy and not

135
00:06:38,840 --> 00:06:39,380
others.

136
00:06:39,380 --> 00:06:43,460
So I think we have brought
home this very key lesson

137
00:06:43,460 --> 00:06:45,840
about the importance
of high road firms.

138
00:06:45,840 --> 00:06:47,710
Our challenge now
is to make sure

139
00:06:47,710 --> 00:06:51,050
that we figure out how to
spread them to larger numbers.

140
00:06:51,050 --> 00:06:55,810
So this next week, it's time
to go on to the next subject.

141
00:06:55,810 --> 00:06:58,460
We're going to take up
two critical issues--

142
00:06:58,460 --> 00:07:01,870
entrepreneurship and
the role of technology.

143
00:07:01,870 --> 00:07:04,300
And you've already started
those conversations

144
00:07:04,300 --> 00:07:05,780
on the discussion forum.

145
00:07:05,780 --> 00:07:09,780
So we'll continue them with some
really good videos and guest

146
00:07:09,780 --> 00:07:13,500
interviews and material from
people all over the world,

147
00:07:13,500 --> 00:07:18,780
talking about how we can use
technology to augment work,

148
00:07:18,780 --> 00:07:21,880
not just to substitute
for work or displace work.

149
00:07:21,880 --> 00:07:23,380
Yes, that's going to go on.

150
00:07:23,380 --> 00:07:26,250
But pay particular
attention to what

151
00:07:26,250 --> 00:07:31,650
our experts say we can do to use
technology to good advantage,

152
00:07:31,650 --> 00:07:35,370
to augment and to support
good work in the future.

153
00:07:35,370 --> 00:07:38,720
And then let's also discuss
how we can build high road

154
00:07:38,720 --> 00:07:41,450
firms right from the beginning.

155
00:07:41,450 --> 00:07:43,790
As entrepreneurs,
what do we need

156
00:07:43,790 --> 00:07:46,730
to do to make sure that we
understand what it takes

157
00:07:46,730 --> 00:07:49,150
to have a good
business model, to be

158
00:07:49,150 --> 00:07:51,510
responsive to the
investors who are providing

159
00:07:51,510 --> 00:07:55,220
the capital for these firms, but
also to make sure that we make

160
00:07:55,220 --> 00:07:59,830
the choices about how we build
good jobs and careers for all

161
00:07:59,830 --> 00:08:03,350
of the employees in
these new enterprises.

162
00:08:03,350 --> 00:08:04,890
And let me make a final offer.

163
00:08:04,890 --> 00:08:08,360
And that is, we're
almost about, as I said,

164
00:08:08,360 --> 00:08:10,440
at the midpoint of the course.

165
00:08:10,440 --> 00:08:15,040
And I'd like to use this weekend
to offer an open forum, sort

166
00:08:15,040 --> 00:08:17,530
of office hours on the
discussion board, where

167
00:08:17,530 --> 00:08:22,610
my team here for the course and
I will be available to answer

168
00:08:22,610 --> 00:08:24,940
questions, to engage
in commentary,

169
00:08:24,940 --> 00:08:28,750
to continue the dialogue
with you more directly.

170
00:08:28,750 --> 00:08:31,750
So please take a look
at the discussion board,

171
00:08:31,750 --> 00:08:34,210
participate with us,
ask us questions.

172
00:08:34,210 --> 00:08:37,860
Let's use this as an opportunity
to see what we have learned

173
00:08:37,860 --> 00:08:40,840
so far, to continue to
debate, even with us

174
00:08:40,840 --> 00:08:41,940
if you disagree with us.

175
00:08:41,940 --> 00:08:43,120
That's fine, too.

176
00:08:43,120 --> 00:08:45,490
Let's have a good
discussion this weekend.

177
00:08:45,490 --> 00:08:49,140
I'll make sure that I check the
discussion board periodically

178
00:08:49,140 --> 00:08:52,720
over the next several days
and see what's on your mind.

179
00:08:52,720 --> 00:08:54,970
And then, finally,
this weekend, we

180
00:08:54,970 --> 00:08:58,810
will give you a short
evaluation to do us a favor

181
00:08:58,810 --> 00:09:02,210
and give us a mid-course
evaluation on how

182
00:09:02,210 --> 00:09:03,540
we are doing in the course.

183
00:09:03,540 --> 00:09:07,120
So I look forward to continuing
the conversations this weekend

184
00:09:07,120 --> 00:09:09,325
and in the weeks ahead.