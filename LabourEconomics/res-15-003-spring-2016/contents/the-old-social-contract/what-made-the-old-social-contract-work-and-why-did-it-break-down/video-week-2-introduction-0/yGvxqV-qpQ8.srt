1
00:00:05,460 --> 00:00:07,350
Welcome to the second
week of our class

2
00:00:07,350 --> 00:00:08,850
on shaping the future of work.

3
00:00:08,850 --> 00:00:11,090
We got off to a
great start last week

4
00:00:11,090 --> 00:00:14,800
with lots of good discussions
on the future of work,

5
00:00:14,800 --> 00:00:16,830
on your hopes, your aspirations.

6
00:00:16,830 --> 00:00:19,750
A lot of discussion about
the future of technology

7
00:00:19,750 --> 00:00:22,420
and how it's going to affect
work and some of the worries

8
00:00:22,420 --> 00:00:24,380
that many of you
have about this.

9
00:00:24,380 --> 00:00:26,540
So we'll continue
these discussions

10
00:00:26,540 --> 00:00:28,660
we'll go into more
depth on technology

11
00:00:28,660 --> 00:00:33,040
and work down the way in about
the fourth week of class.

12
00:00:33,040 --> 00:00:36,060
But to give you a preview,
I posted a new note

13
00:00:36,060 --> 00:00:39,730
on the discussion board
about a big new MIT project

14
00:00:39,730 --> 00:00:44,730
on the future of the technology
in textiles, of all industries.

15
00:00:44,730 --> 00:00:46,800
We are about to
start a big project

16
00:00:46,800 --> 00:00:49,390
to see if we can put some
of the ideas of this course

17
00:00:49,390 --> 00:00:53,840
to work in practice, where we
shape the future of the textile

18
00:00:53,840 --> 00:00:56,660
and fabric industry
with high technology

19
00:00:56,660 --> 00:00:58,890
jobs, hopefully,
good quality jobs,

20
00:00:58,890 --> 00:01:00,490
with new training programs.

21
00:01:00,490 --> 00:01:02,230
So take a look at that post.

22
00:01:02,230 --> 00:01:06,070
See if you have thoughts on how
we can shape the future of work

23
00:01:06,070 --> 00:01:08,010
in that particular sector.

24
00:01:08,010 --> 00:01:10,060
But this week, we'll
do three things.

25
00:01:10,060 --> 00:01:13,590
First, the videos that
are posted for this week

26
00:01:13,590 --> 00:01:17,310
will give you a bit of a history
of how the social contract

27
00:01:17,310 --> 00:01:20,710
played out and worked well in
the United States and, in fact,

28
00:01:20,710 --> 00:01:23,650
in some other countries
around the world in the past.

29
00:01:23,650 --> 00:01:27,900
We do this not to try to repeat
history but to spring forward

30
00:01:27,900 --> 00:01:30,970
on the basis of a good
understanding of where we've

31
00:01:30,970 --> 00:01:34,180
come from and what we
need to do to build

32
00:01:34,180 --> 00:01:36,110
a new social contract--

33
00:01:36,110 --> 00:01:39,090
a good foundation for
building a new social contract

34
00:01:39,090 --> 00:01:40,440
in the future.

35
00:01:40,440 --> 00:01:42,860
Second, this week we're
going to introduce

36
00:01:42,860 --> 00:01:46,380
some new regional
discussion facilitators.

37
00:01:46,380 --> 00:01:50,460
Our MBAs from the class
I'm teaching here on campus

38
00:01:50,460 --> 00:01:52,270
are from all over the world.

39
00:01:52,270 --> 00:01:56,220
From China, from South
Asia, from Africa,

40
00:01:56,220 --> 00:01:58,490
from northern
Europe, from Brazil.

41
00:01:58,490 --> 00:02:01,000
They will host
discussions about how

42
00:02:01,000 --> 00:02:03,230
the issues we're
talking about play out

43
00:02:03,230 --> 00:02:05,300
in your regions of
the world, and we

44
00:02:05,300 --> 00:02:07,790
ask those of you who
are from those areas

45
00:02:07,790 --> 00:02:10,130
to join that conversation.

46
00:02:10,130 --> 00:02:12,740
We'll even have some new
experts from Australia.

47
00:02:12,740 --> 00:02:15,280
Some colleagues of mine in
the universities over there

48
00:02:15,280 --> 00:02:17,330
who study and work
on these issues.

49
00:02:17,330 --> 00:02:21,010
So I think we'll have a full
body discussion about how

50
00:02:21,010 --> 00:02:23,440
the changing nature
of work is playing out

51
00:02:23,440 --> 00:02:26,760
in different parts
of our planet.

52
00:02:26,760 --> 00:02:31,190
To get things started on
your personal career process,

53
00:02:31,190 --> 00:02:33,740
this week, there's
an assignment that

54
00:02:33,740 --> 00:02:36,790
will take you step by
step through a process

55
00:02:36,790 --> 00:02:39,440
of identifying the
kinds of issues

56
00:02:39,440 --> 00:02:42,570
that you're interested
in, you have some aptitude

57
00:02:42,570 --> 00:02:45,370
toward, and then the kind
of educational levels

58
00:02:45,370 --> 00:02:48,920
that are available to get
access to the jobs in fields

59
00:02:48,920 --> 00:02:49,940
of interest to you.

60
00:02:49,940 --> 00:02:53,740
So get started by going
to the assignment page,

61
00:02:53,740 --> 00:02:55,250
start that process.

62
00:02:55,250 --> 00:02:57,700
It'll carry through over
the next several weeks,

63
00:02:57,700 --> 00:03:00,430
and it's accompanied
by several videos

64
00:03:00,430 --> 00:03:02,700
around the role
of education, it's

65
00:03:02,700 --> 00:03:04,880
changing role in
lifelong learning,

66
00:03:04,880 --> 00:03:08,680
and a special video by our
colleague, Lee Dyer, who

67
00:03:08,680 --> 00:03:12,040
talks about the skills
and competencies needed

68
00:03:12,040 --> 00:03:14,920
for the jobs of the future.

69
00:03:14,920 --> 00:03:17,590
So that's what
we'll do this week.

70
00:03:17,590 --> 00:03:20,410
But before we finish
here, let's take

71
00:03:20,410 --> 00:03:24,020
a look at what you told us
last week about your goals

72
00:03:24,020 --> 00:03:26,560
and your aspirations for work.

73
00:03:26,560 --> 00:03:30,040
Here's the results
of the poll that you

74
00:03:30,040 --> 00:03:32,370
filled in about
your top priority

75
00:03:32,370 --> 00:03:34,990
for your work and your career.

76
00:03:34,990 --> 00:03:37,420
Notice that the
top item comes out

77
00:03:37,420 --> 00:03:40,300
to be having a good
work life balance,

78
00:03:40,300 --> 00:03:43,250
followed very closely
by having a big impact

79
00:03:43,250 --> 00:03:46,610
on problems that are important
to you and to society.

80
00:03:46,610 --> 00:03:49,370
And then in third place
but not too far behind

81
00:03:49,370 --> 00:03:54,450
is the desire to have a
good living from your work

82
00:03:54,450 --> 00:03:55,710
experiences.

83
00:03:55,710 --> 00:03:59,120
These are the same top three
items that your colleagues

84
00:03:59,120 --> 00:04:02,110
last year indicated were
their top priorities,

85
00:04:02,110 --> 00:04:04,880
although this year,
the work life balance

86
00:04:04,880 --> 00:04:07,850
became a little bit
higher in the ranking.

87
00:04:07,850 --> 00:04:11,960
So it's very clear that
you want many things out

88
00:04:11,960 --> 00:04:14,120
of your jobs and your careers.

89
00:04:14,120 --> 00:04:15,940
Unlike your parents,
as many of you

90
00:04:15,940 --> 00:04:19,079
told us who focused over
here on earning a good living

91
00:04:19,079 --> 00:04:21,250
and having a stable
job, you want

92
00:04:21,250 --> 00:04:24,690
to be able to both have
a big impact on work.

93
00:04:24,690 --> 00:04:26,750
You want to have a
good work life balance

94
00:04:26,750 --> 00:04:30,000
so you can have a
good opportunity

95
00:04:30,000 --> 00:04:33,870
to raise your children and
be a good spouse at home

96
00:04:33,870 --> 00:04:37,730
or a partner at home and
also earn a good living.

97
00:04:37,730 --> 00:04:40,380
So let's focus on
how we do that.

98
00:04:40,380 --> 00:04:42,870
Let me give you a
couple of examples

99
00:04:42,870 --> 00:04:45,130
of the rich way in
which you embodied

100
00:04:45,130 --> 00:04:50,230
these rankings in the range
of comments in the exercise

101
00:04:50,230 --> 00:04:53,390
around comparing your goals
with those of your parents.

102
00:04:53,390 --> 00:04:56,300
This one, I think,
captures the essence

103
00:04:56,300 --> 00:04:58,210
of what the poll told us.

104
00:04:58,210 --> 00:05:02,530
As one of you said, "I dream
of a well-balanced work family

105
00:05:02,530 --> 00:05:05,600
environment, where I
can develop my skills

106
00:05:05,600 --> 00:05:07,970
put them to service
for my community,

107
00:05:07,970 --> 00:05:10,050
and as well get a
wage that allows

108
00:05:10,050 --> 00:05:12,600
me to have a decent
life so that I

109
00:05:12,600 --> 00:05:15,330
can provide for my
family and my offspring."

110
00:05:15,330 --> 00:05:18,220
Or this one, "My
dream is to change

111
00:05:18,220 --> 00:05:21,100
the world by contributing
something meaningful to it.

112
00:05:21,100 --> 00:05:23,810
While maintaining a
balanced lifestyle,

113
00:05:23,810 --> 00:05:25,940
I consider myself
a global citizen."

114
00:05:25,940 --> 00:05:28,430
This is something that a
number of you mentioned.

115
00:05:28,430 --> 00:05:32,350
That you wanted to work and
live in a culturally diverse yet

116
00:05:32,350 --> 00:05:35,250
an integrated workplace
and lifestyle,

117
00:05:35,250 --> 00:05:39,840
and that you want to push
your children to be curious

118
00:05:39,840 --> 00:05:44,090
about their world and to
push the limits of what

119
00:05:44,090 --> 00:05:46,230
they can do as they grow up.

120
00:05:46,230 --> 00:05:49,400
Here's another one
that says, particularly

121
00:05:49,400 --> 00:05:51,520
about young millennials
and their interest

122
00:05:51,520 --> 00:05:54,750
in working with the
digital revolution.

123
00:05:54,750 --> 00:05:58,230
"My dream is to be part of
the digital revolution driven

124
00:05:58,230 --> 00:06:01,410
by millennials working
at innovative companies,"

125
00:06:01,410 --> 00:06:05,180
and "to make the world a
better place to live and work."

126
00:06:05,180 --> 00:06:06,840
That theme comes through again.

127
00:06:06,840 --> 00:06:09,930
And then finally, "I want
to earn a living wage

128
00:06:09,930 --> 00:06:13,300
and not have to miss my
children growing up."

129
00:06:13,300 --> 00:06:16,470
This is the world of
work that you aspire to.

130
00:06:16,470 --> 00:06:18,120
This is the world
of work that we

131
00:06:18,120 --> 00:06:20,540
have to create moving forward.

132
00:06:20,540 --> 00:06:22,190
So let's get on with the task.

133
00:06:22,190 --> 00:06:26,270
This week, we'll focus
on your responsibilities

134
00:06:26,270 --> 00:06:29,370
to make sure that you
are prepared to have

135
00:06:29,370 --> 00:06:31,620
the right educational
levels to achieve

136
00:06:31,620 --> 00:06:34,060
your goals and your
aspirations, and then we'll

137
00:06:34,060 --> 00:06:37,280
also look at what
made the world of work

138
00:06:37,280 --> 00:06:41,060
more effective in sometimes
in the past in our countries

139
00:06:41,060 --> 00:06:43,530
so that we can spring
forward to think

140
00:06:43,530 --> 00:06:45,960
about how we build
a new foundation

141
00:06:45,960 --> 00:06:48,950
for the future of work.