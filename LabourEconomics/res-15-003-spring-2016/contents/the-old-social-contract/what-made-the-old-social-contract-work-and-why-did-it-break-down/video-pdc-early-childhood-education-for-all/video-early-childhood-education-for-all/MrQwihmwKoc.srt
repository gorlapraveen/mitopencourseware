1
00:00:10,530 --> 00:00:12,880
Education, education, education.

2
00:00:12,880 --> 00:00:15,540
Some people say that's
the key to success

3
00:00:15,540 --> 00:00:18,040
in this knowledge-based
economy that we're in.

4
00:00:18,040 --> 00:00:19,230
And they're right.

5
00:00:19,230 --> 00:00:21,980
But the reality is
the education model

6
00:00:21,980 --> 00:00:24,870
that we have inherited
today was built

7
00:00:24,870 --> 00:00:27,940
for the farming economy and
maybe the industrial economy

8
00:00:27,940 --> 00:00:29,320
of the last century.

9
00:00:29,320 --> 00:00:30,950
And so we're going
to have to adapt

10
00:00:30,950 --> 00:00:34,540
it to meet the needs of the
next generation workforce.

11
00:00:34,540 --> 00:00:37,640
And you, as members of the
next generation workforce,

12
00:00:37,640 --> 00:00:40,030
are going to have to
have your own strategy

13
00:00:40,030 --> 00:00:44,670
for addressing how,
when, where, and how much

14
00:00:44,670 --> 00:00:47,230
over the course of
your life of education

15
00:00:47,230 --> 00:00:49,480
you are going to invest in.

16
00:00:49,480 --> 00:00:51,880
Let's start with early
childhood education.

17
00:00:51,880 --> 00:00:53,390
And I'll start with a story.

18
00:00:53,390 --> 00:00:54,970
Several months
ago, I was sitting

19
00:00:54,970 --> 00:00:58,620
at a lunch with international
students here at MIT.

20
00:00:58,620 --> 00:01:00,570
And they were
mid-career students.

21
00:01:00,570 --> 00:01:03,370
And so I was sitting between
two mothers, one from Portugal

22
00:01:03,370 --> 00:01:04,550
and one from China.

23
00:01:04,550 --> 00:01:07,060
And I asked them, when
did your children start

24
00:01:07,060 --> 00:01:09,090
school in your countries?

25
00:01:09,090 --> 00:01:12,720
And one said, right after
I finished maternity leave.

26
00:01:12,720 --> 00:01:15,420
And the other
said, at age three.

27
00:01:15,420 --> 00:01:19,170
They were amazed to hear from
me that basically our children

28
00:01:19,170 --> 00:01:22,170
in this country start in
kindergarten at age five,

29
00:01:22,170 --> 00:01:24,760
unless there's some
private alternative.

30
00:01:24,760 --> 00:01:28,270
Well, about 70% of the
industrialized world

31
00:01:28,270 --> 00:01:30,320
starts education earlier.

32
00:01:30,320 --> 00:01:32,170
So why is this so important?

33
00:01:32,170 --> 00:01:33,940
Well, let's take a look.

34
00:01:33,940 --> 00:01:36,660
This is Professor James
Heckman from the University

35
00:01:36,660 --> 00:01:39,440
of Chicago, a Nobel
Prize-winning economist.

36
00:01:39,440 --> 00:01:42,690
And he is the world's
expert on the economics

37
00:01:42,690 --> 00:01:44,390
of early childhood education.

38
00:01:44,390 --> 00:01:46,360
He has studied this
in experiments,

39
00:01:46,360 --> 00:01:49,910
he's studied it in Chicago, he's
studied it around the world.

40
00:01:49,910 --> 00:01:52,810
And what he finds is
that there's a 7% to 10%

41
00:01:52,810 --> 00:01:57,440
rate of return for society for
every year of early childhood

42
00:01:57,440 --> 00:01:58,550
education.

43
00:01:58,550 --> 00:02:02,020
That return is measured not
only in lifetime earnings.

44
00:02:02,020 --> 00:02:06,550
It's measured in lower
dropout rates, less violence

45
00:02:06,550 --> 00:02:09,500
and less criminal
records, the ability

46
00:02:09,500 --> 00:02:13,970
to achieve one's objectives
within one's career.

47
00:02:13,970 --> 00:02:18,620
A whole set of data tell us that
getting children into school

48
00:02:18,620 --> 00:02:22,790
early is very, very
important to their success.

49
00:02:22,790 --> 00:02:26,590
Well, it's even more important
for children who are at risk.

50
00:02:26,590 --> 00:02:29,620
Inner city students from
disadvantaged neighborhoods

51
00:02:29,620 --> 00:02:34,070
have even higher rates of return
for early childhood education.

52
00:02:34,070 --> 00:02:36,600
So it's very clear
that if we want

53
00:02:36,600 --> 00:02:40,990
to have the next generation's
children well educated

54
00:02:40,990 --> 00:02:44,830
so they can move on to achieve
their dreams in the workforce,

55
00:02:44,830 --> 00:02:46,710
then we have to
get on with this.

56
00:02:46,710 --> 00:02:50,890
The challenge is that
it's very expensive.

57
00:02:50,890 --> 00:02:54,900
The good news is that mayors
and governors around the country

58
00:02:54,900 --> 00:02:57,060
are beginning to
recognize the importance.

59
00:02:57,060 --> 00:03:01,380
So from Seattle to New York to
Boston and many other cities,

60
00:03:01,380 --> 00:03:03,740
we're seeing mayors
take the lead in saying,

61
00:03:03,740 --> 00:03:07,560
let's figure out how we invest
in early childhood education,

62
00:03:07,560 --> 00:03:09,690
and let's get on with that task.

63
00:03:09,690 --> 00:03:12,940
For individual parents,
it is very expensive.

64
00:03:12,940 --> 00:03:16,750
And so we have to work
together as a community

65
00:03:16,750 --> 00:03:19,430
to figure out how we
can fund early childhood

66
00:03:19,430 --> 00:03:21,150
education for all.

67
00:03:21,150 --> 00:03:23,750
And for every
parent, it's critical

68
00:03:23,750 --> 00:03:27,510
that you think as
you can about what

69
00:03:27,510 --> 00:03:29,990
do you need, what do your
children need, what's

70
00:03:29,990 --> 00:03:34,410
appropriate for different age
groups, from early childhood

71
00:03:34,410 --> 00:03:39,050
right through elementary
school, what can you afford,

72
00:03:39,050 --> 00:03:42,430
and how can you make it fit
into your work and other family

73
00:03:42,430 --> 00:03:43,740
responsibilities.

74
00:03:43,740 --> 00:03:46,530
But if we don't
start early, then we

75
00:03:46,530 --> 00:04:02,030
will pay the price later.