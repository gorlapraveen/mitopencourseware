1
00:00:05,120 --> 00:00:07,730
Today we're speaking with
Liz Shuler, the secretary

2
00:00:07,730 --> 00:00:10,090
treasurer of AFL-CIO.

3
00:00:10,090 --> 00:00:13,530
Liz is not only the
chief financial officer

4
00:00:13,530 --> 00:00:16,370
for the AFL-CIO, she
has a special interest

5
00:00:16,370 --> 00:00:18,790
and responsibility in
working with young people

6
00:00:18,790 --> 00:00:20,860
to introduce the
Labor Movement to them

7
00:00:20,860 --> 00:00:22,770
and to learn from
them about what

8
00:00:22,770 --> 00:00:25,330
they would like to see the Labor
Movement of the future look

9
00:00:25,330 --> 00:00:25,830
like.

10
00:00:31,330 --> 00:00:33,420
So I'm the secretary
treasurer, which

11
00:00:33,420 --> 00:00:37,220
is the chief financial
of the AFL-CIO.

12
00:00:37,220 --> 00:00:39,610
We have 57 affiliated unions.

13
00:00:39,610 --> 00:00:41,310
And I actually
came up through one

14
00:00:41,310 --> 00:00:42,910
of those unions,
the International

15
00:00:42,910 --> 00:00:44,400
Brotherhood of
Electrical Workers,

16
00:00:44,400 --> 00:00:46,220
as a rank and file member.

17
00:00:46,220 --> 00:00:50,530
And my responsibilities
here are to manage the money

18
00:00:50,530 --> 00:00:53,440
and make sure that we're
using the dues money wisely

19
00:00:53,440 --> 00:00:57,140
and building
program, making sure

20
00:00:57,140 --> 00:00:59,200
that our finances are in order.

21
00:00:59,200 --> 00:01:00,970
But in addition to
that finance hat,

22
00:01:00,970 --> 00:01:06,720
I also work to engage,
educate, mobilize young people

23
00:01:06,720 --> 00:01:10,610
in our movement, particularly
focused, as a woman officer,

24
00:01:10,610 --> 00:01:15,500
in making sure that we are
engaging women in our Labor

25
00:01:15,500 --> 00:01:18,550
Movement and really speaking
out on issues that affect women

26
00:01:18,550 --> 00:01:22,480
in the workplace, and then
in terms of innovation,

27
00:01:22,480 --> 00:01:25,410
what can we be doing
as a labor movement

28
00:01:25,410 --> 00:01:28,780
to change as the workplace
changes and evolves?

29
00:01:28,780 --> 00:01:31,570
And that can include anything
from workforce development

30
00:01:31,570 --> 00:01:36,810
and training to issues
around the on-demand economy.

31
00:01:36,810 --> 00:01:39,405
There's so much happening
in the world of work.

32
00:01:45,100 --> 00:01:47,200
Well, the young people
in the Labor Movement--

33
00:01:47,200 --> 00:01:50,140
I was a young person when
I first started in my union

34
00:01:50,140 --> 00:01:51,600
at 23 years old--

35
00:01:51,600 --> 00:01:54,280
often feel that they
don't have as much

36
00:01:54,280 --> 00:01:56,010
of a voice in our
Labor Movement.

37
00:01:56,010 --> 00:02:00,190
And I wanted to figure out a
way to change that and make

38
00:02:00,190 --> 00:02:06,700
their ideas and opinions and
thoughts and approaches woven

39
00:02:06,700 --> 00:02:09,160
into the fabric of
what we do every day.

40
00:02:09,160 --> 00:02:12,380
So we started a young worker
program we call NextUP

41
00:02:12,380 --> 00:02:13,890
about six years ago.

42
00:02:13,890 --> 00:02:16,850
And the intent was
to figure out ways

43
00:02:16,850 --> 00:02:20,490
to get young people engaged
in their own unions,

44
00:02:20,490 --> 00:02:23,870
but also making connections with
young people outside the Labor

45
00:02:23,870 --> 00:02:26,280
Movement so that we're
all working together

46
00:02:26,280 --> 00:02:29,540
to advance the interests of
young people in this economy.

47
00:02:29,540 --> 00:02:33,530
And we've been building
young worker groups

48
00:02:33,530 --> 00:02:36,720
all across the country
through our AFL-CIO structure.

49
00:02:36,720 --> 00:02:39,840
So in most major cities and
pretty much every state,

50
00:02:39,840 --> 00:02:42,360
we have a young
worker group that's

51
00:02:42,360 --> 00:02:44,380
giving voice to young people.

52
00:02:44,380 --> 00:02:48,990
They're leading in
politics, in legislation,

53
00:02:48,990 --> 00:02:53,020
in organizing, and
really being a voice

54
00:02:53,020 --> 00:02:54,900
and a presence for
the Labor Movement

55
00:02:54,900 --> 00:02:56,790
in their local communities.

56
00:02:56,790 --> 00:02:58,320
I'm really proud
of the work they've

57
00:02:58,320 --> 00:03:03,100
done to put together a young
worker economic platform

58
00:03:03,100 --> 00:03:05,060
so that they can
hold politicians

59
00:03:05,060 --> 00:03:08,110
accountable on the issues
that matter to young people

60
00:03:08,110 --> 00:03:09,200
in the economy.

61
00:03:09,200 --> 00:03:12,220
And of course, front and
center is job opportunities,

62
00:03:12,220 --> 00:03:15,620
making sure that when people
graduate from high school

63
00:03:15,620 --> 00:03:17,510
or even graduate from
college that we have

64
00:03:17,510 --> 00:03:19,620
good jobs waiting for them.

65
00:03:19,620 --> 00:03:22,770
And often, we know young
people are grappling

66
00:03:22,770 --> 00:03:25,230
with a lot of student
loan debt when they've

67
00:03:25,230 --> 00:03:27,770
gone through college
and how are our elected

68
00:03:27,770 --> 00:03:31,720
officials responding to that
and really connecting the Labor

69
00:03:31,720 --> 00:03:34,950
Movement to the issues that
matter to young people?

70
00:03:40,490 --> 00:03:43,900
Well, the course is a real
opportunity for young people

71
00:03:43,900 --> 00:03:48,660
to connect on what is the future
of work going to look like?

72
00:03:48,660 --> 00:03:53,790
What are we doing as working
people to influence that?

73
00:03:53,790 --> 00:03:56,680
And in the labor movement,
of course, front and center

74
00:03:56,680 --> 00:03:59,070
is collective bargaining
and how can we

75
00:03:59,070 --> 00:04:01,420
strengthen collective
bargaining and how

76
00:04:01,420 --> 00:04:05,120
does the role of collective
bargaining influence

77
00:04:05,120 --> 00:04:06,720
work in general?

78
00:04:06,720 --> 00:04:10,610
We know the unionized
workforce has been shrinking,

79
00:04:10,610 --> 00:04:14,820
but in a time of the greatest
inequality we've seen,

80
00:04:14,820 --> 00:04:17,750
it's more important than ever
that we grow the Labor Movement

81
00:04:17,750 --> 00:04:21,690
and get our fair share of
that growth in the economy.

82
00:04:21,690 --> 00:04:23,620
And so I think for
young people who

83
00:04:23,620 --> 00:04:26,160
are looking at what can I
do to influence, what can I

84
00:04:26,160 --> 00:04:30,020
do to help, taking a course
like this, educating themselves,

85
00:04:30,020 --> 00:04:31,870
but then also taking
that education

86
00:04:31,870 --> 00:04:33,930
and moving it into action.

87
00:04:33,930 --> 00:04:35,800
And I think when I
talk to young people,

88
00:04:35,800 --> 00:04:39,210
they often feel
powerless and don't

89
00:04:39,210 --> 00:04:41,140
see the Labor
Movement as a vehicle

90
00:04:41,140 --> 00:04:43,280
for that kind of action.

91
00:04:43,280 --> 00:04:46,570
And so I think the
message is that yeah,

92
00:04:46,570 --> 00:04:48,040
there are opportunities.

93
00:04:48,040 --> 00:04:51,290
There are places you can
go to exert your power.

94
00:04:51,290 --> 00:04:54,330
And the power of coming
together in the power

95
00:04:54,330 --> 00:04:57,690
of collective action, nowhere
is that better on display

96
00:04:57,690 --> 00:04:58,950
than with the Labor Movement.

97
00:05:05,040 --> 00:05:06,500
Young people in
the Labor Movement

98
00:05:06,500 --> 00:05:09,500
have a very unique perspective.

99
00:05:09,500 --> 00:05:13,600
They often are some of the
only young people in their peer

100
00:05:13,600 --> 00:05:15,570
group that have
a decent job that

101
00:05:15,570 --> 00:05:19,030
pays benefits, that gives
them the training they

102
00:05:19,030 --> 00:05:21,260
need without going into debt.

103
00:05:21,260 --> 00:05:26,760
So I think that they can do a
service to other young people

104
00:05:26,760 --> 00:05:28,880
who may not know those
opportunities exist

105
00:05:28,880 --> 00:05:32,620
by lifting up the possibilities
of what the Labor Movement can

106
00:05:32,620 --> 00:05:33,660
offer.

107
00:05:33,660 --> 00:05:36,530
They also have a
unique perspective

108
00:05:36,530 --> 00:05:39,090
on what's happening
in the economy.

109
00:05:39,090 --> 00:05:43,820
As we know, this generation
has suffered disproportionately

110
00:05:43,820 --> 00:05:45,820
with the lack of opportunity.

111
00:05:45,820 --> 00:05:49,440
The unemployment numbers
have hit them, I think,

112
00:05:49,440 --> 00:05:53,890
at double the rate of the
average who's out of work.

113
00:05:53,890 --> 00:05:58,320
And we want to come together
as a society to figure out,

114
00:05:58,320 --> 00:06:02,710
what kind of jobs do we
want to see in this country

115
00:06:02,710 --> 00:06:04,380
as we go forward in the future?

116
00:06:04,380 --> 00:06:07,690
Do we want low-road,
f jobs or do

117
00:06:07,690 --> 00:06:12,090
we want opportunities to
really support your family

118
00:06:12,090 --> 00:06:13,850
and put food on the table?

119
00:06:13,850 --> 00:06:17,810
And I think young people are
feeling that so acutely and so

120
00:06:17,810 --> 00:06:20,770
they have ideas and
thoughts to share

121
00:06:20,770 --> 00:06:22,380
on how we can
rebuild the economy.

122
00:06:22,380 --> 00:06:24,380
So I think that this
is a great opportunity

123
00:06:24,380 --> 00:06:28,260
to bring people together,
have their voices heard,

124
00:06:28,260 --> 00:06:33,380
make sure that we're getting our
politicians' attention on this

125
00:06:33,380 --> 00:06:35,060
because those
elected officials are

126
00:06:35,060 --> 00:06:38,620
the ones that write the
rules and pass the policies.

127
00:06:38,620 --> 00:06:41,740
And so if we're going to rewrite
the rules of the economy,

128
00:06:41,740 --> 00:06:43,586
we need to get more
and more young voices

129
00:06:43,586 --> 00:06:44,460
in that conversation.

130
00:06:50,470 --> 00:06:52,900
I think there is
so much opportunity

131
00:06:52,900 --> 00:06:54,950
at this moment in
time as, of course,

132
00:06:54,950 --> 00:06:57,300
the workplace is
changing and evolving.

133
00:06:57,300 --> 00:06:59,870
So is the Labor
Movement and so is

134
00:06:59,870 --> 00:07:02,500
the progressive
infrastructure in the country.

135
00:07:02,500 --> 00:07:05,690
And I think the message
is that we don't just

136
00:07:05,690 --> 00:07:08,680
have to sit back and
wait for change to happen

137
00:07:08,680 --> 00:07:13,450
and feel like we can't
be a part of that change.

138
00:07:13,450 --> 00:07:16,260
I think it's important that
you see the Labor Movement

139
00:07:16,260 --> 00:07:20,120
as a vehicle for that
possibility of bringing

140
00:07:20,120 --> 00:07:22,160
people's voices together.

141
00:07:22,160 --> 00:07:25,860
And as an institution,
we can do that.

142
00:07:25,860 --> 00:07:27,600
But we can't do it alone.

143
00:07:27,600 --> 00:07:30,970
We need to be working
together in collaboration

144
00:07:30,970 --> 00:07:34,420
and in partnership with
all kinds of organizations.

145
00:07:34,420 --> 00:07:37,840
And the more people we bring
together, the more powerful

146
00:07:37,840 --> 00:07:38,820
our voices are.

147
00:07:38,820 --> 00:07:41,000
So I think for young
people who are looking

148
00:07:41,000 --> 00:07:44,280
for an avenue and a way to
make change, certainly look

149
00:07:44,280 --> 00:07:46,690
to the Labor Movement, but
look in your communities.

150
00:07:46,690 --> 00:07:49,650
Look at all the
different organizations

151
00:07:49,650 --> 00:07:53,040
that are lifting up
voices in different ways.

152
00:07:53,040 --> 00:07:55,970
Look at your local
businesses, for example.

153
00:07:55,970 --> 00:07:58,780
We often think that
labor and management are

154
00:07:58,780 --> 00:08:00,790
at odds with each
other, but could we

155
00:08:00,790 --> 00:08:03,350
be more powerful if we
were working together

156
00:08:03,350 --> 00:08:08,680
in collaboration on the
things that matter to creating

157
00:08:08,680 --> 00:08:13,020
better jobs, high-road,
high-wage jobs?

158
00:08:13,020 --> 00:08:15,580
And so I think that
young people will

159
00:08:15,580 --> 00:08:19,740
play an integral and important
role in making that happen.