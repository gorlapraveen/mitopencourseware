1
00:00:07,580 --> 00:00:09,800
Let's boil the
3,000 year history

2
00:00:09,800 --> 00:00:12,500
of work down to three minutes.

3
00:00:12,500 --> 00:00:14,620
First, there were
hunters and gatherers.

4
00:00:14,620 --> 00:00:16,450
Everyone contributed,
and their business

5
00:00:16,450 --> 00:00:18,630
was basically to survive.

6
00:00:18,630 --> 00:00:24,290
Eventually they began to
tame plants and animals,

7
00:00:24,290 --> 00:00:26,130
opening up new kinds of work.

8
00:00:26,130 --> 00:00:28,960
And, if you were
lucky, pleasure.

9
00:00:28,960 --> 00:00:32,310
In early civilizations,
powerful ruling classes

10
00:00:32,310 --> 00:00:37,350
emerged for the first time.

11
00:00:37,350 --> 00:00:39,490
They could outsource work
to slaves and servants

12
00:00:39,490 --> 00:00:47,520
because that mundane,
hard work was considered

13
00:00:47,520 --> 00:00:49,919
beneath their dignity,
even though it

14
00:00:49,919 --> 00:00:51,210
made their lifestyles possible.

15
00:00:51,210 --> 00:00:53,126
In the Middle Ages, new
religious philosophies

16
00:00:53,126 --> 00:00:54,450
changed that.

17
00:00:54,450 --> 00:00:57,360
Working hard became synonymous
with doing good, pleasing God,

18
00:00:57,360 --> 00:00:58,990
and repenting for one's sins.

19
00:00:58,990 --> 00:01:04,620
Then came the rise
of the family farm.

20
00:01:04,620 --> 00:01:07,010
Working the land took
on a sense of community.

21
00:01:07,010 --> 00:01:08,780
All family members
literally pitched in,

22
00:01:08,780 --> 00:01:11,570
depended on each other, and the
farmer's work was never done.

23
00:01:11,570 --> 00:01:16,190
But as civilization ticked
on, more and more people

24
00:01:16,190 --> 00:01:17,860
moved to cities.

25
00:01:17,860 --> 00:01:20,220
In those cities, artisans
specialized more and more,

26
00:01:20,220 --> 00:01:23,410
and along came the
craft stage of work,

27
00:01:23,410 --> 00:01:25,910
like shoemakers and
carpenters, all very skilled

28
00:01:25,910 --> 00:01:27,670
and specialized.

29
00:01:27,670 --> 00:01:33,110
Then the Industrial Revolution
came along and brought with it

30
00:01:33,110 --> 00:01:33,840
mass production.

31
00:01:33,840 --> 00:01:34,570
Humans adapted.

32
00:01:34,570 --> 00:01:36,410
New kinds of factories
created new kinds

33
00:01:36,410 --> 00:01:39,650
of workers eager to earn a
living however they could

34
00:01:39,650 --> 00:01:42,500
in a rapidly changing world.

35
00:01:42,500 --> 00:01:44,280
Some workers banded together.

36
00:01:44,280 --> 00:01:47,040
They took historic stands
for humane work conditions

37
00:01:47,040 --> 00:01:49,800
and demanded all kinds of
fundamental rights including

38
00:01:49,800 --> 00:01:52,440
the 40 hour work week.

39
00:01:52,440 --> 00:01:54,700
The development of computers,
the World Wide Web,

40
00:01:54,700 --> 00:01:56,810
and widespread
education ushered us

41
00:01:56,810 --> 00:02:04,970
into the
knowledge-based economy.

42
00:02:09,780 --> 00:02:11,630
As fewer people
and fewer hours are

43
00:02:11,630 --> 00:02:13,570
needed to do the
work of yesterday,

44
00:02:13,570 --> 00:02:14,680
people are adapting again.

45
00:02:14,680 --> 00:02:17,740
Today some do work that didn't
even exist five years ago.

46
00:02:17,740 --> 00:02:18,850
So here we are.

47
00:02:18,850 --> 00:02:20,571
Is this the end of
work's evolution?

48
00:02:20,571 --> 00:02:21,070
No.

49
00:02:21,070 --> 00:02:24,110
How you live and work
will invent the future.

50
00:02:24,110 --> 00:02:25,740
You'll work to eat.

51
00:02:25,740 --> 00:02:29,060
You'll eat by outsourcing food
production to other workers.

52
00:02:29,060 --> 00:02:32,970
You'll tap into a deep human
drive to work on something

53
00:02:32,970 --> 00:02:35,790
and to make a genuine
difference doing it.

54
00:02:35,790 --> 00:02:38,510
You'll rely on your networks
and connections to help you out,

55
00:02:38,510 --> 00:02:40,600
and you will help
others in turn.

56
00:02:40,600 --> 00:02:42,270
You'll specialize
in something no one

57
00:02:42,270 --> 00:02:44,310
has thought of before
using technology

58
00:02:44,310 --> 00:02:45,980
no one's seen before.

59
00:02:45,980 --> 00:02:49,680
You'll wonder how to better tip
the balance of work and life

60
00:02:49,680 --> 00:02:52,390
to the needs of a new world.

61
00:02:52,390 --> 00:02:54,750
So keep on working,
creating, and inventing

62
00:02:54,750 --> 00:03:05,190
the future of work.