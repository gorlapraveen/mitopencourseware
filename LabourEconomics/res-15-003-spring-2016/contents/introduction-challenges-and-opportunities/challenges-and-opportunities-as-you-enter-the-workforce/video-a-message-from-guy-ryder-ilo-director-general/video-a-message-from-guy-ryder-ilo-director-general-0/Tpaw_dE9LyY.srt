1
00:00:10,920 --> 00:00:13,120
I'm pleased to know that
our colleagues at MIT

2
00:00:13,120 --> 00:00:15,790
are reaching out to
people all over the world

3
00:00:15,790 --> 00:00:19,340
to engage them in this important
discussion about the future

4
00:00:19,340 --> 00:00:20,240
of work.

5
00:00:20,240 --> 00:00:23,230
We are launching our own
future of work initiative

6
00:00:23,230 --> 00:00:24,610
here at the ILO.

7
00:00:24,610 --> 00:00:27,850
And this course will fit very
well with our initiative.

8
00:00:27,850 --> 00:00:29,930
Look, I think that our
future of work initiative

9
00:00:29,930 --> 00:00:32,259
responds to two
essential things.

10
00:00:32,259 --> 00:00:33,820
One's got to do with need.

11
00:00:33,820 --> 00:00:35,830
One's got to do
with opportunity.

12
00:00:35,830 --> 00:00:37,960
The need comes from
the fact, and we're all

13
00:00:37,960 --> 00:00:40,120
conscious of this in
our everyday lives,

14
00:00:40,120 --> 00:00:42,300
that there is massive
change taking place

15
00:00:42,300 --> 00:00:46,420
in the world of work,
transformative change at a pace

16
00:00:46,420 --> 00:00:48,320
and at a depth that
we don't really

17
00:00:48,320 --> 00:00:50,200
have experience of in the past.

18
00:00:50,200 --> 00:00:52,700
So we think that
we need to study

19
00:00:52,700 --> 00:00:57,010
these big changes to understand
them better as a way of working

20
00:00:57,010 --> 00:01:01,080
out what we should do about
them in order to make sure

21
00:01:01,080 --> 00:01:03,570
that the world of work
responds to the ILO's

22
00:01:03,570 --> 00:01:07,504
ideas of social justice
fairness of work.

23
00:01:07,504 --> 00:01:08,920
And then there is
the opportunity.

24
00:01:08,920 --> 00:01:10,544
And the opportunity
comes from the fact

25
00:01:10,544 --> 00:01:14,410
that in three years
time, in 2019, the ILO

26
00:01:14,410 --> 00:01:17,840
will celebrate its 100th
birthday, its centenary.

27
00:01:17,840 --> 00:01:19,540
So what better opportunity--

28
00:01:19,540 --> 00:01:21,910
they don't come round
that frequently--

29
00:01:21,910 --> 00:01:23,970
what better
opportunity than that

30
00:01:23,970 --> 00:01:27,350
to have this in depth
reflection about the direction

31
00:01:27,350 --> 00:01:31,160
that the world of work is taking
and what we can do to shape it,

32
00:01:31,160 --> 00:01:35,080
to move it, towards these
ideals of social justice.

33
00:01:35,080 --> 00:01:38,340
We are proposing
to amend the states

34
00:01:38,340 --> 00:01:41,450
that they structure their
national dialogues around four

35
00:01:41,450 --> 00:01:42,600
conversations.

36
00:01:42,600 --> 00:01:46,810
And very quickly, one is to
do with work and society.

37
00:01:46,810 --> 00:01:48,460
Sounds a bit
philosophical, but I

38
00:01:48,460 --> 00:01:50,880
think there are some really
deep questions out there

39
00:01:50,880 --> 00:01:53,940
about what work means to
each and every one of us

40
00:01:53,940 --> 00:01:56,580
in terms of, not only
of earning our living,

41
00:01:56,580 --> 00:02:01,330
but also of self-realization,
of what our lives mean,

42
00:02:01,330 --> 00:02:03,490
and how we want to
proceed with them.

43
00:02:03,490 --> 00:02:06,170
Secondly, and it's a most
frequently asked question

44
00:02:06,170 --> 00:02:08,039
that I at least get
about the future

45
00:02:08,039 --> 00:02:11,650
is decent work of tomorrow,
the decent jobs of tomorrow.

46
00:02:11,650 --> 00:02:13,760
Where are those
jobs coming from?

47
00:02:13,760 --> 00:02:17,210
We're having to create 40
million jobs every year

48
00:02:17,210 --> 00:02:20,910
just to absorb the new people
coming onto the labor markets.

49
00:02:20,910 --> 00:02:23,950
And frankly, we're not able
to do it at the moment.

50
00:02:23,950 --> 00:02:28,570
So we have to be able to discuss
the nature and the origin

51
00:02:28,570 --> 00:02:30,600
of work in the future.

52
00:02:30,600 --> 00:02:34,070
The third item is about the
organization, the organization

53
00:02:34,070 --> 00:02:35,440
of work in production.

54
00:02:35,440 --> 00:02:38,490
We're seeing the digital
economy take off.

55
00:02:38,490 --> 00:02:41,300
We're seeing the way that
the internet is mediating

56
00:02:41,300 --> 00:02:43,860
work in entirely new ways.

57
00:02:43,860 --> 00:02:45,980
Maybe, the enterprise
is going to look

58
00:02:45,980 --> 00:02:49,210
very different in the future
from what it looks like today.

59
00:02:49,210 --> 00:02:52,060
We're seeing the fragmentation
and globalization

60
00:02:52,060 --> 00:02:55,690
of production systems
in global supply chains.

61
00:02:55,690 --> 00:02:59,060
We need to understand what
the full implications are

62
00:02:59,060 --> 00:03:03,417
of this new organization and
global division of labor.

63
00:03:03,417 --> 00:03:05,750
And the fourth point-- and
this is the culminating point

64
00:03:05,750 --> 00:03:07,570
of the whole thing.

65
00:03:07,570 --> 00:03:10,660
We have to say, well,
what are the consequences

66
00:03:10,660 --> 00:03:15,490
of these changes for the ILO's
mandate for social justice?

67
00:03:15,490 --> 00:03:19,260
How can we shape, manage
all of these changes,

68
00:03:19,260 --> 00:03:22,930
so that we keep the world
moving in this direction

69
00:03:22,930 --> 00:03:24,120
of social justice?

70
00:03:24,120 --> 00:03:26,480
So let me end by
encouraging everyone

71
00:03:26,480 --> 00:03:30,480
to join us in using the ideas
and the information presented

72
00:03:30,480 --> 00:03:33,690
in this course to improve
the future of work

73
00:03:33,690 --> 00:03:36,820
for people all around the world.