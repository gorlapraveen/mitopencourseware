1
00:00:10,230 --> 00:00:13,150
Let's talk about the current
opportunities and challenges

2
00:00:13,150 --> 00:00:16,230
that you'll face as you
enter the labor market.

3
00:00:16,230 --> 00:00:18,880
My first question is,
are these the best

4
00:00:18,880 --> 00:00:22,670
of times, or the worst of
times to be looking for a job

5
00:00:22,670 --> 00:00:24,000
and starting your career?

6
00:00:24,000 --> 00:00:26,880
Well, let's take a look at some
of the labor market realities

7
00:00:26,880 --> 00:00:27,980
that you face.

8
00:00:27,980 --> 00:00:31,770
You all recall what we referred
to as the Great Recession

9
00:00:31,770 --> 00:00:34,530
of 2007 and 2009?

10
00:00:34,530 --> 00:00:37,390
This chart shows
just how many jobs we

11
00:00:37,390 --> 00:00:38,760
lost during that time period.

12
00:00:38,760 --> 00:00:44,130
Well, the good news is
that, in fact, 2015--

13
00:00:44,130 --> 00:00:48,140
finally-- was a great year
for job growth in the US.

14
00:00:48,140 --> 00:00:51,190
We made a big dent in
closing the jobs deficit,

15
00:00:51,190 --> 00:00:54,030
by creating nearly 2
and 1/2 million jobs.

16
00:00:54,030 --> 00:00:57,230
So now we only need
another 3 million more

17
00:00:57,230 --> 00:00:59,700
to close the full job deficit.

18
00:00:59,700 --> 00:01:03,010
Let's hope we keep making
progress like we did last year.

19
00:01:03,010 --> 00:01:06,850
No guarantees, given the
shaky world economy we're in.

20
00:01:06,850 --> 00:01:09,510
So we have to keep pressing
government and business

21
00:01:09,510 --> 00:01:13,250
leaders to keep the job
growth going strong.

22
00:01:13,250 --> 00:01:15,370
That's challenge number one.

23
00:01:15,370 --> 00:01:17,880
But that's only the
beginning of the challenge.

24
00:01:17,880 --> 00:01:20,440
Recall what the
Occupy movement said,

25
00:01:20,440 --> 00:01:24,970
that 1% of the top earners
are getting all of the income?

26
00:01:24,970 --> 00:01:26,510
Well, they were right.

27
00:01:26,510 --> 00:01:30,530
Over 50% of income
growth in the last decade

28
00:01:30,530 --> 00:01:32,790
has come to the top 1%.

29
00:01:32,790 --> 00:01:36,610
And we have to go way
back here, to 1928,

30
00:01:36,610 --> 00:01:41,530
to have a situation where we get
that much unequal distribution

31
00:01:41,530 --> 00:01:42,430
of income.

32
00:01:42,430 --> 00:01:44,790
So we do have an income problem.

33
00:01:44,790 --> 00:01:46,810
But it wasn't always this way.

34
00:01:46,810 --> 00:01:49,860
From the end of World War II--

35
00:01:49,860 --> 00:01:55,270
down here about 1947,
1948-- all the way through

36
00:01:55,270 --> 00:01:59,310
till about 1980,
we saw an economy

37
00:01:59,310 --> 00:02:03,380
where wages, and productivity,
and profits moved together.

38
00:02:03,380 --> 00:02:05,260
Later on we're going
to talk about this

39
00:02:05,260 --> 00:02:09,380
as the era of the post-World
War II social contract.

40
00:02:09,380 --> 00:02:10,830
But something happened here.

41
00:02:10,830 --> 00:02:16,630
Note, after 1980s, productivity
continued to grow by over 80%,

42
00:02:16,630 --> 00:02:20,030
but wages and
compensation flat-lined.

43
00:02:20,030 --> 00:02:22,930
The average worker in
the United States economy

44
00:02:22,930 --> 00:02:28,000
has only seen about a 9%
wage increase since 1980.

45
00:02:28,000 --> 00:02:30,550
And in fact, if you don't
have a college degree

46
00:02:30,550 --> 00:02:33,950
your wages have gone down,
relative to the cost of living

47
00:02:33,950 --> 00:02:35,580
during this time period.

48
00:02:35,580 --> 00:02:38,250
So while in the
early stage, we had

49
00:02:38,250 --> 00:02:42,630
what President Kennedy said, "a
rising tide lifts all boats."

50
00:02:42,630 --> 00:02:45,650
Everybody worked hard
and shared in the gains.

51
00:02:45,650 --> 00:02:48,820
We have seen something
happen since 1980

52
00:02:48,820 --> 00:02:50,730
that has changed that.

53
00:02:50,730 --> 00:02:53,680
Let's now focus a little
bit more carefully on just

54
00:02:53,680 --> 00:02:55,720
what happens if you
are entering the labor

55
00:02:55,720 --> 00:02:58,320
market after finishing
high school or college

56
00:02:58,320 --> 00:02:59,810
at this point in time.

57
00:02:59,810 --> 00:03:02,970
Many of you will start
off as unpaid interns,

58
00:03:02,970 --> 00:03:05,710
or in part time
jobs, or in jobs that

59
00:03:05,710 --> 00:03:08,200
don't use the full
education that you've

60
00:03:08,200 --> 00:03:09,820
worked so hard to achieve.

61
00:03:09,820 --> 00:03:12,680
You'll be what we
call underemployed.

62
00:03:12,680 --> 00:03:16,850
And it's not surprising that
given that, many of your cohort

63
00:03:16,850 --> 00:03:20,390
are experiencing very low
levels of job satisfaction.

64
00:03:20,390 --> 00:03:23,620
In fact, the lowest levels that
we have seen in many years.

65
00:03:23,620 --> 00:03:26,960
Back in the 1980s,
this red line shows,

66
00:03:26,960 --> 00:03:30,540
that people under the age
of 25 were like others--

67
00:03:30,540 --> 00:03:34,320
more than 50% were
satisfied with their wages,

68
00:03:34,320 --> 00:03:36,420
their hours of work,
and their opportunities

69
00:03:36,420 --> 00:03:37,800
to learn on the job.

70
00:03:37,800 --> 00:03:42,740
But now, these numbers have
declined to a little over 35%.

71
00:03:42,740 --> 00:03:46,050
And so only about one
third of young people

72
00:03:46,050 --> 00:03:48,330
are satisfied with their jobs.

73
00:03:48,330 --> 00:03:49,880
How does this really affect you?

74
00:03:49,880 --> 00:03:52,940
Well, the economic
evidence is very clear--

75
00:03:52,940 --> 00:03:56,660
if you start off as many of
you are experiencing now,

76
00:03:56,660 --> 00:04:00,670
in a low-income jobs that
don't use your full skills that

77
00:04:00,670 --> 00:04:04,520
you've been educated for, or
you start off as a paid or even

78
00:04:04,520 --> 00:04:09,290
an unpaid intern, or you start
off in part-time work in hotels

79
00:04:09,290 --> 00:04:13,270
or restaurants, or
other retail areas,

80
00:04:13,270 --> 00:04:17,620
you're going to experience
about 10 years-- or even more--

81
00:04:17,620 --> 00:04:22,220
of suppressed income and lack
of opportunities for growth.

82
00:04:22,220 --> 00:04:24,140
Does it have to be this way?

83
00:04:24,140 --> 00:04:26,230
I don't believe
that for a moment.

84
00:04:26,230 --> 00:04:28,330
There is no iron
law of economics

85
00:04:28,330 --> 00:04:29,980
that produce these results.

86
00:04:29,980 --> 00:04:32,250
It's the results
of bad policies,

87
00:04:32,250 --> 00:04:34,880
of our institutions and
the way in which we govern.

88
00:04:34,880 --> 00:04:37,270
We're failing to catch
up with the changing

89
00:04:37,270 --> 00:04:39,510
workforce, and the
changing nature of work.

90
00:04:39,510 --> 00:04:41,540
And we can do better.

91
00:04:41,540 --> 00:04:44,150
And in fact, many
firms are doing better.

92
00:04:44,150 --> 00:04:48,310
Some companies are way ahead
in providing good jobs.

93
00:04:48,310 --> 00:04:51,650
A lot of innovation is
happening at the local level

94
00:04:51,650 --> 00:04:55,060
in start-ups, in
nonprofit organizations,

95
00:04:55,060 --> 00:04:56,611
in state and local governments.

96
00:04:56,611 --> 00:04:58,610
We're going to look at
some of those innovations

97
00:04:58,610 --> 00:05:02,550
in our next video, and ask how
you can join this process so

98
00:05:02,550 --> 00:05:08,700
that we can improve the work
life of everyone going forward.