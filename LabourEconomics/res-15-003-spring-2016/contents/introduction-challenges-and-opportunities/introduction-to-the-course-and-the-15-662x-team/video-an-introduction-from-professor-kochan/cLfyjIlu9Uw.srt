1
00:00:10,130 --> 00:00:13,080
We can't think of a more
interesting and exciting time

2
00:00:13,080 --> 00:00:14,740
to launch this course.

3
00:00:14,740 --> 00:00:17,890
Yes, there are challenges
that you and your peers

4
00:00:17,890 --> 00:00:21,220
will face because my baby
boom generation didn't

5
00:00:21,220 --> 00:00:25,120
do a great job of keeping
our policies and practices up

6
00:00:25,120 --> 00:00:27,250
to date with the changing
economy and the changing

7
00:00:27,250 --> 00:00:28,240
workforce.

8
00:00:28,240 --> 00:00:32,549
But at the same time, we're in
an extremely interesting era

9
00:00:32,549 --> 00:00:34,100
of innovation.

10
00:00:34,100 --> 00:00:36,790
Around the world we're
seeing new organizations

11
00:00:36,790 --> 00:00:39,010
getting formed
using crowd sourcing

12
00:00:39,010 --> 00:00:42,170
and other modern techniques,
organizations we couldn't even

13
00:00:42,170 --> 00:00:44,270
have imagined several years ago.

14
00:00:44,270 --> 00:00:47,930
There are new worker advocacy
organizations coming along

15
00:00:47,930 --> 00:00:51,410
that will help empower you
to find your way in the labor

16
00:00:51,410 --> 00:00:52,110
market.

17
00:00:52,110 --> 00:00:56,030
There are new apps that allow
you to navigate this labor

18
00:00:56,030 --> 00:00:58,700
market and to figure out
where the good jobs are

19
00:00:58,700 --> 00:01:01,770
and distinguish between
good and bad employers.

20
00:01:01,770 --> 00:01:04,470
And then there are courses
like this exploding

21
00:01:04,470 --> 00:01:07,690
all around our universities
here in the United States

22
00:01:07,690 --> 00:01:08,720
and in other countries.

23
00:01:08,720 --> 00:01:12,070
We rise and fall on the
strength of our middle class--

24
00:01:12,070 --> 00:01:15,060
These online courses
allow you to gain access

25
00:01:15,060 --> 00:01:16,890
to some of the world's
leading experts

26
00:01:16,890 --> 00:01:18,350
in your particular field.

27
00:01:18,350 --> 00:01:22,160
The role of the corporation in
the US has changed quite a bit.

28
00:01:22,160 --> 00:01:25,380
So it's time to really
bring all of this together

29
00:01:25,380 --> 00:01:28,880
and see if we can't help really
shape the future of work.

30
00:01:28,880 --> 00:01:31,390
We're going to introduce
you to lots of companies

31
00:01:31,390 --> 00:01:33,440
that are breaking new ground.

32
00:01:33,440 --> 00:01:37,060
In every industry we
can find great companies

33
00:01:37,060 --> 00:01:41,940
that are doing well financially,
serving their customers well,

34
00:01:41,940 --> 00:01:45,560
focused on a mission that is
really important for society

35
00:01:45,560 --> 00:01:47,220
and providing good jobs.

36
00:01:47,220 --> 00:01:49,840
One of the things we want
to do is to highlight those

37
00:01:49,840 --> 00:01:52,220
and to learn more about
your own experiences

38
00:01:52,220 --> 00:01:54,960
with companies and
organizations like this.

39
00:01:54,960 --> 00:01:56,840
And so as we go
along, we're going

40
00:01:56,840 --> 00:01:58,820
to learn from your
experiences, and you're

41
00:01:58,820 --> 00:02:00,910
going to meet a
whole set of peers

42
00:02:00,910 --> 00:02:05,210
who have good jobs and maybe
not such good jobs, some mentors

43
00:02:05,210 --> 00:02:08,419
who are ready to work with
you to provide advice,

44
00:02:08,419 --> 00:02:12,430
and all of that will be a
learning process together.

45
00:02:12,430 --> 00:02:15,150
And let me say a word about
another piece of this course

46
00:02:15,150 --> 00:02:17,620
that I'm especially
excited about.

47
00:02:17,620 --> 00:02:19,210
In every week,
we're going to have

48
00:02:19,210 --> 00:02:21,780
something called the
personal development corner.

49
00:02:21,780 --> 00:02:24,810
This is designed to
provide you with a path

50
00:02:24,810 --> 00:02:27,880
forward so that you can be
empowered to take control

51
00:02:27,880 --> 00:02:29,370
of your own career.

52
00:02:29,370 --> 00:02:31,540
So there'll be
readings and videos

53
00:02:31,540 --> 00:02:34,070
and exercises and
assignments that allow

54
00:02:34,070 --> 00:02:37,830
you to understand what are
some of your capabilities, what

55
00:02:37,830 --> 00:02:41,230
are some of your interests,
what jobs might fit well

56
00:02:41,230 --> 00:02:43,800
with your background,
and where you would like

57
00:02:43,800 --> 00:02:46,670
to go with your own
career; and how will

58
00:02:46,670 --> 00:02:50,310
you develop your own personal
career plan to make sure

59
00:02:50,310 --> 00:02:52,170
that you are ready
to take advantage

60
00:02:52,170 --> 00:02:55,160
of the opportunities
that are out there.

61
00:02:55,160 --> 00:02:58,790
So let's start exploring this
evolving and interesting world

62
00:02:58,790 --> 00:02:59,700
of work.

63
00:02:59,700 --> 00:03:02,010
Let's do it with our eyes open.

64
00:03:02,010 --> 00:03:04,720
Let's understand and
learn from our history

65
00:03:04,720 --> 00:03:07,880
about what worked in
the past, what's broken,

66
00:03:07,880 --> 00:03:10,340
what needs to be fixed, and why.

67
00:03:10,340 --> 00:03:12,340
And then let's
see if together we

68
00:03:12,340 --> 00:03:15,620
can't shape the future of
work for your generation.

69
00:03:15,620 --> 00:03:18,160
So let's get started.