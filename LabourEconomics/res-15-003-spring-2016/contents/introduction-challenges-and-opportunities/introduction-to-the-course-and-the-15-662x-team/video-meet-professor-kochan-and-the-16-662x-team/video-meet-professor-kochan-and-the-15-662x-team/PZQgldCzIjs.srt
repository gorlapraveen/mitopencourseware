1
00:00:10,330 --> 00:00:14,550
I'm the head
instructor for 15.662,

2
00:00:14,550 --> 00:00:17,800
How to Secure the American
Dream for the Next Generation

3
00:00:17,800 --> 00:00:19,040
Workforce.

4
00:00:19,040 --> 00:00:20,840
I'd like to take a
little time to tell

5
00:00:20,840 --> 00:00:22,650
you a little bit
about myself and why

6
00:00:22,650 --> 00:00:24,760
I'm offering this course.

7
00:00:24,760 --> 00:00:27,170
I've been studying,
and teaching,

8
00:00:27,170 --> 00:00:29,930
and working on issues
about the nature

9
00:00:29,930 --> 00:00:32,140
of work for the last 40 years.

10
00:00:32,140 --> 00:00:35,460
I've never been more concerned
about the future of work

11
00:00:35,460 --> 00:00:37,220
than I am right now.

12
00:00:37,220 --> 00:00:41,440
I'm concerned about the need
for more jobs, better jobs,

13
00:00:41,440 --> 00:00:45,240
for you and for all people
in the next generation.

14
00:00:45,240 --> 00:00:48,600
And so I think we can work
together in this course

15
00:00:48,600 --> 00:00:52,280
to explore how you can
address these issues

16
00:00:52,280 --> 00:00:55,170
and secure your future as well.

17
00:00:55,170 --> 00:00:57,960
So let me tell you a little
bit about my background

18
00:00:57,960 --> 00:01:01,310
so you can understand
where I'm coming from.

19
00:01:01,310 --> 00:01:05,209
I grew up on a small
family farm in Wisconsin.

20
00:01:05,209 --> 00:01:08,180
We learned at a very
early age the importance

21
00:01:08,180 --> 00:01:11,410
of working together,
the value of hard work,

22
00:01:11,410 --> 00:01:16,140
and the satisfaction that comes
from doing a job very well.

23
00:01:16,140 --> 00:01:18,910
My father only had an
eighth grade education,

24
00:01:18,910 --> 00:01:22,420
but he told all of us, my
brother and my sisters,

25
00:01:22,420 --> 00:01:25,270
get as much education
as you can so that you

26
00:01:25,270 --> 00:01:29,190
can get an occupation and a
career that is better than what

27
00:01:29,190 --> 00:01:31,150
farming will be in the future.

28
00:01:31,150 --> 00:01:34,460
That was clearly the best
advice I could have ever gotten,

29
00:01:34,460 --> 00:01:35,470
and it worked.

30
00:01:35,470 --> 00:01:39,280
Here I am, many years
later, an MIT professor,

31
00:01:39,280 --> 00:01:42,340
thanks to a good education
from the schools in Wisconsin

32
00:01:42,340 --> 00:01:44,720
and from the University
of Wisconsin.

33
00:01:44,720 --> 00:01:48,440
So in my professional
life I've been

34
00:01:48,440 --> 00:01:52,050
fortunate to work with a
wide variety of business,

35
00:01:52,050 --> 00:01:55,560
and labor, and government
organizations over the years,

36
00:01:55,560 --> 00:01:57,450
and I've been a
constant advocate

37
00:01:57,450 --> 00:01:59,690
for improving relations
at the workplace,

38
00:01:59,690 --> 00:02:01,600
for getting people
to work together,

39
00:02:01,600 --> 00:02:04,230
to think about how the
workforce is changing,

40
00:02:04,230 --> 00:02:06,760
and how they need
to change to meet

41
00:02:06,760 --> 00:02:09,500
the conditions of the
economy, and the society,

42
00:02:09,500 --> 00:02:12,500
and the workforce that
we find ourselves in.

43
00:02:12,500 --> 00:02:14,760
And so that's what I want
to do in this course:

44
00:02:14,760 --> 00:02:19,220
to engage you, the next
generation workers, and those

45
00:02:19,220 --> 00:02:22,200
of us in the baby boom
generation who still care

46
00:02:22,200 --> 00:02:24,110
about these issues
and want to make

47
00:02:24,110 --> 00:02:26,810
a contribution to
a discussion of how

48
00:02:26,810 --> 00:02:29,350
we can improve the workplace.

49
00:02:29,350 --> 00:02:32,900
And I want to especially
learn and listen from you.

50
00:02:32,900 --> 00:02:35,710
I'm engaged in writing
a book on this topic,

51
00:02:35,710 --> 00:02:39,260
and I want to get your
ideas so you help shape

52
00:02:39,260 --> 00:02:42,040
how we're going to change,
how we're going to adapt,

53
00:02:42,040 --> 00:02:46,640
and how we're going to create
the workplace of the future.

54
00:02:46,640 --> 00:02:50,160
I'm Barbara Dyer, president and
CEO of the Hitachi Foundation

55
00:02:50,160 --> 00:02:53,720
and senior lecturer at MIT
Sloan School of Management.

56
00:02:53,720 --> 00:02:56,130
The Hitachi Foundation
focuses on the role

57
00:02:56,130 --> 00:02:59,190
of business in society,
with a particular emphasis

58
00:02:59,190 --> 00:03:04,120
on how businesses create
social and economic value

59
00:03:04,120 --> 00:03:06,120
in the pursuit of profits.

60
00:03:06,120 --> 00:03:11,080
We'll be talking about start up
companies and their motivation

61
00:03:11,080 --> 00:03:14,170
to create a real
benefit for society,

62
00:03:14,170 --> 00:03:16,750
and we'll be talking some
about traditional businesses,

63
00:03:16,750 --> 00:03:20,840
more long term businesses, and
the ways in which they create

64
00:03:20,840 --> 00:03:23,520
social value and
environmental value,

65
00:03:23,520 --> 00:03:26,950
and the lessons that can be
learned across the spectrum,

66
00:03:26,950 --> 00:03:31,070
from early stage businesses
to later stage businesses.

67
00:03:31,070 --> 00:03:32,340
I'm Zeynep Ton.

68
00:03:32,340 --> 00:03:34,550
I'm an adjunct
associate professor

69
00:03:34,550 --> 00:03:37,710
at MIT Sloan in the
Operations Management group.

70
00:03:37,710 --> 00:03:41,170
I will tell you about how
we can create companies

71
00:03:41,170 --> 00:03:44,160
that deliver great value
to their investors,

72
00:03:44,160 --> 00:03:46,990
good jobs for their
employees, and great value

73
00:03:46,990 --> 00:03:50,540
to their customers
all at the same time.

74
00:03:50,540 --> 00:03:54,300
I'm Christine Riordan, a Ph.D.
Student at MIT's Institute

75
00:03:54,300 --> 00:03:56,480
for Work and
Employment Research.

76
00:03:56,480 --> 00:03:59,350
I am going to be talking to
you about how labor unions are

77
00:03:59,350 --> 00:04:04,070
reinventing themselves, and how
new forms of worker advocacy

78
00:04:04,070 --> 00:04:07,360
can help support your
aspirations for work.

79
00:04:07,360 --> 00:04:11,189
I'm Francesca Cicileo,
an undergraduate at MIT.

80
00:04:11,189 --> 00:04:12,730
I'm going to tell
you more about what

81
00:04:12,730 --> 00:04:15,510
our generation, the
so-called millennials,

82
00:04:15,510 --> 00:04:18,130
say are their goals and
aspirations for work,

83
00:04:18,130 --> 00:04:20,930
and their version of
the American dream.

84
00:04:20,930 --> 00:04:22,230
Hello, my name's John McCarthy.

85
00:04:22,230 --> 00:04:25,250
I'm a postdoctoral fellow here
at MIT's Institute for Work

86
00:04:25,250 --> 00:04:26,690
and Employment Research.

87
00:04:26,690 --> 00:04:28,690
I'm going to be talking
to you about freelancing

88
00:04:28,690 --> 00:04:31,610
work, including new
technology-mediated work

89
00:04:31,610 --> 00:04:34,330
arrangements that are springing
up across the economy.

90
00:04:34,330 --> 00:04:36,830
We're going to look at some
of the positive and negative

91
00:04:36,830 --> 00:04:38,720
aspects of these
work arrangements,

92
00:04:38,720 --> 00:04:42,130
and also talk about how work
institutions might emerge

93
00:04:42,130 --> 00:04:45,130
to ameliorate some of the
negative aspects of these work

94
00:04:45,130 --> 00:04:46,280
arrangements.

95
00:04:46,280 --> 00:04:49,120
So I hope we will learn
together, and then

96
00:04:49,120 --> 00:04:52,890
let's take our lessons and
our message to the broader

97
00:04:52,890 --> 00:04:55,970
audiences, to future
leaders, in this country

98
00:04:55,970 --> 00:04:58,020
and around the world.