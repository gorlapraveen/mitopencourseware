1
00:00:05,370 --> 00:00:07,350
Welcome to the
launch of our course

2
00:00:07,350 --> 00:00:09,210
on shaping the future of work.

3
00:00:09,210 --> 00:00:11,400
Together, over the
next seven weeks,

4
00:00:11,400 --> 00:00:14,830
we'll explore how to expand
the number of good jobs,

5
00:00:14,830 --> 00:00:17,130
career opportunities,
and workplaces

6
00:00:17,130 --> 00:00:20,490
that help us all realize our
dreams, our aspirations we

7
00:00:20,490 --> 00:00:21,410
have for work.

8
00:00:21,410 --> 00:00:23,490
For our family lives
and for building

9
00:00:23,490 --> 00:00:26,230
sustainable successful
enterprises,

10
00:00:26,230 --> 00:00:28,300
and for our societies.

11
00:00:28,300 --> 00:00:31,380
But before we talk
about that, let's talk

12
00:00:31,380 --> 00:00:33,500
about some of the
difficulties we've

13
00:00:33,500 --> 00:00:35,670
experienced in the
last week or so.

14
00:00:35,670 --> 00:00:38,270
I want to begin with the
statement of solidarity

15
00:00:38,270 --> 00:00:40,830
for our colleagues in
Europe and in Asia,

16
00:00:40,830 --> 00:00:44,860
in light of the terrible
tragedies that have occurred

17
00:00:44,860 --> 00:00:47,150
in Brussels and in Pakistan.

18
00:00:47,150 --> 00:00:50,700
Today, we are all
sharing our concern

19
00:00:50,700 --> 00:00:53,420
that there have been
over the past 18

20
00:00:53,420 --> 00:00:57,060
months, 75 bombings
in over 20 countries,

21
00:00:57,060 --> 00:00:59,330
killing over 1,300 people.

22
00:00:59,330 --> 00:01:03,370
We think about the grief and the
pain that so many nationalities

23
00:01:03,370 --> 00:01:06,060
and religions and people
of different cultures

24
00:01:06,060 --> 00:01:08,870
all over the world
have been experiencing.

25
00:01:08,870 --> 00:01:11,370
And we are with them together.

26
00:01:11,370 --> 00:01:14,780
My fellow of course team
members here at MIT and I

27
00:01:14,780 --> 00:01:19,060
feel profoundly sad at
yet, more acts of violence.

28
00:01:19,060 --> 00:01:20,550
We condemn them.

29
00:01:20,550 --> 00:01:23,890
We are committed to addressing
the causes of violence

30
00:01:23,890 --> 00:01:26,470
in our societies and
in our workplaces,

31
00:01:26,470 --> 00:01:28,670
and to doing everything
we can to help

32
00:01:28,670 --> 00:01:32,710
resolve these differences before
they devolve into terrorism.

33
00:01:32,710 --> 00:01:36,010
Above all, this course
rests on the principle

34
00:01:36,010 --> 00:01:41,010
of respect for diversity in
our world, in our work places.

35
00:01:41,010 --> 00:01:42,480
We had planned to
show a bit later

36
00:01:42,480 --> 00:01:46,080
in the course, a 20 minute
video interview with Mary Rowe,

37
00:01:46,080 --> 00:01:48,770
one of the world's leaders
in managing diversity

38
00:01:48,770 --> 00:01:50,250
and workplace conflicts.

39
00:01:50,250 --> 00:01:52,460
Mary outlines the
ways that we can

40
00:01:52,460 --> 00:01:54,780
address these
tensions and the anger

41
00:01:54,780 --> 00:02:00,130
that they show in our society,
and that sometimes do spill

42
00:02:00,130 --> 00:02:02,200
over to our places of work.

43
00:02:02,200 --> 00:02:05,130
But given the events
in Europe and Asia

44
00:02:05,130 --> 00:02:06,970
and other parts of
the world, we'll

45
00:02:06,970 --> 00:02:09,199
post that interview this
week so we can start

46
00:02:09,199 --> 00:02:11,160
discussing these issues now.

47
00:02:11,160 --> 00:02:13,800
So we invite you to
watch Mary's interview

48
00:02:13,800 --> 00:02:16,500
and use it in the
discussion forum

49
00:02:16,500 --> 00:02:20,150
to comment on how issues
of diversity, inclusion,

50
00:02:20,150 --> 00:02:22,040
and the management
of inequities can

51
00:02:22,040 --> 00:02:25,800
be addressed in your workplace
and in your job experiences.

52
00:02:25,800 --> 00:02:28,660
We can design a
better future as we

53
00:02:28,660 --> 00:02:32,310
help each other shape this
aspect of our workplaces

54
00:02:32,310 --> 00:02:34,250
in a positive way.

55
00:02:34,250 --> 00:02:36,700
I invite you also to
get started by sampling

56
00:02:36,700 --> 00:02:40,660
the rich array of other videos
that we have posted this week.

57
00:02:40,660 --> 00:02:44,300
We have welcoming comments from
national and internationally

58
00:02:44,300 --> 00:02:47,420
renowned leaders from
all over the world.

59
00:02:47,420 --> 00:02:49,870
The US secretary of
labor, Tom Perez,

60
00:02:49,870 --> 00:02:52,190
the director general of
the International Labor

61
00:02:52,190 --> 00:02:53,920
Organization, Guy Ryder.

62
00:02:53,920 --> 00:02:56,630
The secretary treasurer
of America's largest union

63
00:02:56,630 --> 00:02:58,530
federation, Liz Schuler.

64
00:02:58,530 --> 00:03:01,510
And our course team's human
resource management expert,

65
00:03:01,510 --> 00:03:02,440
Lee Dyer.

66
00:03:02,440 --> 00:03:05,680
We include this
diversity of voices

67
00:03:05,680 --> 00:03:07,960
to show how all of
these communities

68
00:03:07,960 --> 00:03:10,840
need to work together to
shape the future of work

69
00:03:10,840 --> 00:03:14,760
for you and for all the parties
that have a stake in it.

70
00:03:14,760 --> 00:03:18,150
And don't miss our three
minute animated video

71
00:03:18,150 --> 00:03:21,190
summarizing 3,000 years
of the history of work.

72
00:03:21,190 --> 00:03:22,940
I think you'll enjoy it.

73
00:03:22,940 --> 00:03:26,850
Then we move on to other
videos and readings

74
00:03:26,850 --> 00:03:29,740
that discuss the challenges
and exciting opportunities,

75
00:03:29,740 --> 00:03:32,410
innovations occurring
in our workplaces today.

76
00:03:32,410 --> 00:03:36,680
These set the stage for a much
more in-depth conversation

77
00:03:36,680 --> 00:03:40,310
about these issues
in the weeks ahead.

78
00:03:40,310 --> 00:03:43,290
We also note in your
discussion forum

79
00:03:43,290 --> 00:03:45,880
that you already
started, that some of you

80
00:03:45,880 --> 00:03:48,780
are concerned about
how the issues of jobs

81
00:03:48,780 --> 00:03:50,720
and the future of work
are being discussed

82
00:03:50,720 --> 00:03:54,160
in the presidential campaign
in the United States.

83
00:03:54,160 --> 00:03:57,430
Some of you have lamented
that yes, they talk about jobs

84
00:03:57,430 --> 00:03:59,780
but don't have a vision
for the future of work.

85
00:03:59,780 --> 00:04:02,640
Well, maybe it's our
job to bring our voices

86
00:04:02,640 --> 00:04:06,350
into these conversations as
we will try to do, and really

87
00:04:06,350 --> 00:04:09,480
lay out a vision for
what work could be,

88
00:04:09,480 --> 00:04:11,460
what the real needs
of workers are,

89
00:04:11,460 --> 00:04:14,240
what the real needs of
businesses are today.

90
00:04:14,240 --> 00:04:18,519
So maybe we can add our
voices to this discussion.

91
00:04:18,519 --> 00:04:20,829
So welcome, join us.

92
00:04:20,829 --> 00:04:23,960
Look forward to having
you engage with us

93
00:04:23,960 --> 00:04:27,950
and helping to see if we can use
our collective efforts to shape

94
00:04:27,950 --> 00:04:30,250
the future of work.