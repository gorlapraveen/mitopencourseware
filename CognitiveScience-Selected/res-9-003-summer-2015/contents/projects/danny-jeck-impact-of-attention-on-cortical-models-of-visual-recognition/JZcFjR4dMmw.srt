1
00:00:04,810 --> 00:00:10,110
[MUSIC PLAYS]

2
00:00:10,110 --> 00:00:11,490
DANNY JECK: Hi, I'm Danny Jeck.

3
00:00:11,490 --> 00:00:14,040
I'm a fifth year grad
student getting my PhD

4
00:00:14,040 --> 00:00:16,500
at Johns Hopkins in
biomedical engineering.

5
00:00:16,500 --> 00:00:19,110
My project, what I'm
trying to look into

6
00:00:19,110 --> 00:00:28,360
is how attention is related
to models of visual cortex.

7
00:00:28,360 --> 00:00:31,440
The first lecture
was by Jim DiCarlo.

8
00:00:31,440 --> 00:00:35,400
He gave a talk about how the
models of object recognition

9
00:00:35,400 --> 00:00:37,920
seem to match pretty
well with the behavior

10
00:00:37,920 --> 00:00:42,090
of inferotemporal
cortex in macaques.

11
00:00:42,090 --> 00:00:44,880
We know that also in
macaques earlier areas

12
00:00:44,880 --> 00:00:48,330
of visual processing are
modulated by attention.

13
00:00:48,330 --> 00:00:49,680
So the question is, well, OK.

14
00:00:49,680 --> 00:00:52,282
We have this model, let's
say we add some modulation

15
00:00:52,282 --> 00:00:53,740
due to attention,
what does that do

16
00:00:53,740 --> 00:00:57,900
downstream as that information
propagates through the network?

17
00:00:57,900 --> 00:01:01,440
I'm building a model
in Python right now.

18
00:01:01,440 --> 00:01:02,550
And it's running.

19
00:01:02,550 --> 00:01:04,920
The main goal of
the model is to see

20
00:01:04,920 --> 00:01:07,620
how some modulation
in earlier cortex

21
00:01:07,620 --> 00:01:11,130
would propagate through a
model like what we believe is

22
00:01:11,130 --> 00:01:12,530
happening in the brain already.

23
00:01:12,530 --> 00:01:17,400
A boring finding would be
that a 10% modulation results

24
00:01:17,400 --> 00:01:20,760
in a 10% modulation downstream.

25
00:01:20,760 --> 00:01:23,070
I'm expecting that that's
not the case, because there's

26
00:01:23,070 --> 00:01:25,830
a whole bunch of nonlinearities
and normalization

27
00:01:25,830 --> 00:01:28,920
that happens that should
propagate through this network.

28
00:01:28,920 --> 00:01:31,200
The question is what is
the magnitude of that,

29
00:01:31,200 --> 00:01:36,870
how does that affect things
if the 10% modulation is not

30
00:01:36,870 --> 00:01:41,100
actually the right number
because of some measurements

31
00:01:41,100 --> 00:01:43,320
or the way I'm interpreting
the measurements that

32
00:01:43,320 --> 00:01:46,140
have been made already, what
would different numbers allow

33
00:01:46,140 --> 00:01:47,190
for.

34
00:01:47,190 --> 00:01:50,850
Or perhaps the modulations
we found downstream are all

35
00:01:50,850 --> 00:01:53,160
due to other feedback
from other areas

36
00:01:53,160 --> 00:01:56,400
rather than this going
back to the beginning

37
00:01:56,400 --> 00:01:58,410
and propagating all
the way through.

38
00:01:58,410 --> 00:02:01,000
So the idea came about
from Ethan Meyers.

39
00:02:01,000 --> 00:02:03,990
He was originally
interested in trying

40
00:02:03,990 --> 00:02:08,021
to do this kind of two
passes through a network, one

41
00:02:08,021 --> 00:02:09,479
in which you sort
of try and figure

42
00:02:09,479 --> 00:02:11,770
out the location of an object,
and another in which you

43
00:02:11,770 --> 00:02:12,960
try to recognize it.

44
00:02:12,960 --> 00:02:15,210
I kind of took that in
a different direction

45
00:02:15,210 --> 00:02:18,660
because I was more interested
in the neurophysiology side

46
00:02:18,660 --> 00:02:19,200
of things.

47
00:02:19,200 --> 00:02:21,840
In my current lab, I wouldn't
have had time to do something

48
00:02:21,840 --> 00:02:24,030
like this because
I wasn't planning

49
00:02:24,030 --> 00:02:25,980
on investing a lot
of time understanding

50
00:02:25,980 --> 00:02:27,030
what deep networks were.

51
00:02:27,030 --> 00:02:31,380
So really, having the time to
sort of work on a free project

52
00:02:31,380 --> 00:02:32,260
has been really nice.

53
00:02:32,260 --> 00:02:34,550
[MUSIC PLAYS]