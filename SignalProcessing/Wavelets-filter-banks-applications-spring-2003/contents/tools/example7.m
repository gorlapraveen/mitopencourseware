% Example 3a: Compute the samples of the biorthogonal scaling functions
% and wavelets.

[s,a]= biorwavf('bior4.4');           % 9/7 filters
[h0,h1,f0,f1] = biorfilt(a, s);

[x,phi,phitilde,psi,psitilde] = biphivals(h0,h1,f0,f1,5);

plot(x,phi,'-',x,psi,'-.')
legend('Primary scaling function', 'Primary wavelet')
pause

plot(x,phitilde,'--',x,psitilde,':')
legend('Dual scaling function', 'Dual wavelet')
