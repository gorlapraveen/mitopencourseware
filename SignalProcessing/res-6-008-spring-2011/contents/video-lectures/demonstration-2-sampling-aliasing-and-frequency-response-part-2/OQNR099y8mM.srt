1
00:00:00,090 --> 00:00:02,490
The following content is
provided under a Creative

2
00:00:02,490 --> 00:00:04,030
Commons license.

3
00:00:04,030 --> 00:00:06,330
Your support will help
MIT OpenCourseWare

4
00:00:06,330 --> 00:00:10,720
continue to offer high quality
educational resources for free.

5
00:00:10,720 --> 00:00:13,320
To make a donation or
view additional materials

6
00:00:13,320 --> 00:00:17,280
from hundreds of MIT courses,
visit MIT OpenCourseWare

7
00:00:17,280 --> 00:00:18,450
at ocw.mit.edu.

8
00:00:23,462 --> 00:00:25,406
[MUSIC PLAYING]

9
00:00:49,542 --> 00:00:51,500
ALAN OPPENHEIM: Here we
demonstrate the effects

10
00:00:51,500 --> 00:00:53,390
of sampling and
aliasing by using

11
00:00:53,390 --> 00:00:55,430
this non-recursive
digital filter.

12
00:00:55,430 --> 00:00:57,500
Where as a digital
filter, it's simply

13
00:00:57,500 --> 00:01:00,020
set up as an identity system.

14
00:01:00,020 --> 00:01:02,060
But we take
advantage of the fact

15
00:01:02,060 --> 00:01:04,849
that it has a
sampler for the input

16
00:01:04,849 --> 00:01:08,620
and a de-sampler for the output.

17
00:01:08,620 --> 00:01:13,090
To demonstrate the sampling
and aliasing effect,

18
00:01:13,090 --> 00:01:14,770
we'll use a sinusoidal input.

19
00:01:14,770 --> 00:01:19,330
And so on the oscilloscope,
what we have are two traces.

20
00:01:19,330 --> 00:01:22,090
The top trace is
the input sinusoid.

21
00:01:22,090 --> 00:01:25,360
And the bottom trace
is the output sinusoid.

22
00:01:25,360 --> 00:01:31,180
And we know that we expect
that as the input sinusoidal

23
00:01:31,180 --> 00:01:34,900
frequency is increased, the
output sinusoidal frequency

24
00:01:34,900 --> 00:01:39,820
will likewise increase until we
get into the vicinity of half

25
00:01:39,820 --> 00:01:41,880
the sampling frequency.

26
00:01:41,880 --> 00:01:45,310
In the vicinity of half the
sampling frequency, because

27
00:01:45,310 --> 00:01:48,910
of the fact that the low pass
filter is not an ideal low pass

28
00:01:48,910 --> 00:01:52,000
filter, we have
a beating effect.

29
00:01:52,000 --> 00:01:54,640
And we see the beating
effect evident here.

30
00:01:54,640 --> 00:01:59,110
Now if we were to increase
the input frequency

31
00:01:59,110 --> 00:02:02,080
past the half the
sampling frequency,

32
00:02:02,080 --> 00:02:05,470
even though the input
frequency would increase,

33
00:02:05,470 --> 00:02:08,710
the output frequency would
decrease due to aliasing.

34
00:02:08,710 --> 00:02:12,400
And let's illustrate that
by first moving back down

35
00:02:12,400 --> 00:02:14,380
toward DC.

36
00:02:14,380 --> 00:02:17,080
And then using an
automatic sweep,

37
00:02:17,080 --> 00:02:21,700
to sweep us from DC through
half the sampling frequency up

38
00:02:21,700 --> 00:02:23,587
to the sampling frequency.

39
00:02:27,000 --> 00:02:29,310
And so here we get
in the vicinity

40
00:02:29,310 --> 00:02:30,800
of half the sampling frequency.

41
00:02:30,800 --> 00:02:32,510
We see the beating effect.

42
00:02:32,510 --> 00:02:34,950
Past that, the output
frequency decreases,

43
00:02:34,950 --> 00:02:38,130
even though the input
frequency is increasing.

44
00:02:38,130 --> 00:02:41,520
And now, let's illustrate
that once more.

45
00:02:41,520 --> 00:02:44,820
But in this case, let's
listen to the output

46
00:02:44,820 --> 00:02:47,306
as well as watching the output.

47
00:02:47,306 --> 00:02:48,740
[HIGH PITCH FREQUENCY]

48
00:02:48,740 --> 00:02:49,696
We hear it increase.

49
00:02:53,050 --> 00:02:56,218
And then we hear the
output frequency decrease.

50
00:02:56,218 --> 00:02:58,166
[FREQUENCY STOPS]

51
00:03:07,910 --> 00:03:13,970
What we see here is the impulse
response of the overall system.

52
00:03:13,970 --> 00:03:17,900
And we observe, for one thing,
that it's a symmetrical impulse

53
00:03:17,900 --> 00:03:18,530
response.

54
00:03:18,530 --> 00:03:22,170
In other words, corresponds
to a linear phase filter.

55
00:03:22,170 --> 00:03:24,320
We can also look at
the impulse response

56
00:03:24,320 --> 00:03:26,820
before the de-sampling
low pass filter.

57
00:03:26,820 --> 00:03:30,980
Lets take out the de-sampling
low pass filter slowly.

58
00:03:30,980 --> 00:03:35,000
And what we observe is
basically the output

59
00:03:35,000 --> 00:03:37,490
of the digital to
analog converter.

60
00:03:37,490 --> 00:03:41,390
Which of course, is a staircase
or boxcar function, not

61
00:03:41,390 --> 00:03:42,740
an impulse train.

62
00:03:42,740 --> 00:03:45,560
In the real world, the
output of a D to A converter

63
00:03:45,560 --> 00:03:48,510
generally is a boxcar
type of function.

64
00:03:48,510 --> 00:03:51,440
We can put the de-sampling
filter back in now.

65
00:03:51,440 --> 00:03:55,070
And notice that the effect
of the de-sampling filter

66
00:03:55,070 --> 00:04:01,130
is basically to smooth out
the rough edges in the boxcar

67
00:04:01,130 --> 00:04:02,720
output from D to A converter.

68
00:04:18,640 --> 00:04:21,910
Now what we'd like to illustrate
is the frequency response

69
00:04:21,910 --> 00:04:25,280
of the equivalent
continuous time filter.

70
00:04:25,280 --> 00:04:27,730
And we can do that by
sweeping the filter

71
00:04:27,730 --> 00:04:29,590
with a sinusoidal input.

72
00:04:29,590 --> 00:04:33,070
So what we'll see in
this demonstration is

73
00:04:33,070 --> 00:04:36,320
on the upper trace, the input
sinusoid, on the lower trace,

74
00:04:36,320 --> 00:04:38,500
the output sinusoid.

75
00:04:38,500 --> 00:04:42,100
Using a 20 kilohertz sampling
rate and a sweep from 0

76
00:04:42,100 --> 00:04:44,350
to 10 kilohertz, in
other words, a sweep

77
00:04:44,350 --> 00:04:50,150
from 0 to effectively pi in
terms of the digital filter.

78
00:04:50,150 --> 00:04:52,930
So what we'll
observe as the input

79
00:04:52,930 --> 00:04:56,620
frequency increases is
that the output sinusoid

80
00:04:56,620 --> 00:05:00,310
will have essentially constant
amplitude up to the cutoff

81
00:05:00,310 --> 00:05:03,520
frequency of the filter,
and then approximately zero

82
00:05:03,520 --> 00:05:04,900
amplitude past.

83
00:05:04,900 --> 00:05:08,190
So let's now sweep the
filter frequency response.

84
00:05:13,040 --> 00:05:17,400
And there is the filter
cutoff frequency.

85
00:05:22,160 --> 00:05:25,660
Now we can also observe the
filter frequency responds

86
00:05:25,660 --> 00:05:27,650
in several other ways.

87
00:05:27,650 --> 00:05:29,110
One way in which
we can observe it

88
00:05:29,110 --> 00:05:33,220
is by looking also
at the amplitude

89
00:05:33,220 --> 00:05:37,420
of the output sinusoid as
a function of frequency

90
00:05:37,420 --> 00:05:39,800
rather than as a
function of time.

91
00:05:39,800 --> 00:05:43,180
And so we'll observe that
on the left hand scope.

92
00:05:43,180 --> 00:05:44,680
While on the right
hand scope, we'll

93
00:05:44,680 --> 00:05:48,460
have the same trace that we
just saw, namely two traces.

94
00:05:48,460 --> 00:05:50,860
The upper trace is
the input sinusoid.

95
00:05:50,860 --> 00:05:53,530
The lower trace is
the output sinusoid.

96
00:05:53,530 --> 00:05:57,160
And in addition to observing
the frequency response,

97
00:05:57,160 --> 00:06:00,310
let's also listen to
the output sinusoid

98
00:06:00,310 --> 00:06:03,820
and observe the
attenuation in the output

99
00:06:03,820 --> 00:06:06,160
as we go from the filter
passband to the filter

100
00:06:06,160 --> 00:06:07,240
stopband.

101
00:06:07,240 --> 00:06:09,490
Again, a 20 kilohertz
sampling rate.

102
00:06:09,490 --> 00:06:12,852
And a sweep range from
0 to 10 kilohertz.

103
00:06:12,852 --> 00:06:15,262
[HIGH PITCH FREQUENCY]

104
00:06:20,007 --> 00:06:21,840
Now of course, we're
in the filter stopband.

105
00:06:25,060 --> 00:06:31,000
Now if we increase the sweep
range from 10 kilohertz to 20

106
00:06:31,000 --> 00:06:34,540
kilohertz so that the sweep
range is equal to the sampling

107
00:06:34,540 --> 00:06:37,330
frequency, in essence
that corresponds

108
00:06:37,330 --> 00:06:41,830
to sweeping out the digital
filter from 0 to 2 pi.

109
00:06:41,830 --> 00:06:44,050
And in that case,
we'll begin to see

110
00:06:44,050 --> 00:06:46,960
some of the periodicity in
the digital filter frequency

111
00:06:46,960 --> 00:06:48,020
response.

112
00:06:48,020 --> 00:06:52,300
So let's do that now with a
20 kilohertz sampling rate

113
00:06:52,300 --> 00:06:56,338
and a sweep range of
0 to 20 kilohertz.

114
00:06:56,338 --> 00:06:59,134
[HIGH PITCH FREQUENCY]

115
00:06:59,134 --> 00:07:07,860
And now we come near 2 pi we
get back into the passband.

116
00:07:07,860 --> 00:07:12,330
And finally back to a 0
to 10 kilohertz sweep, so

117
00:07:12,330 --> 00:07:16,620
that we're again sweeping
only from 0 to pi with regard

118
00:07:16,620 --> 00:07:18,518
to the digital filter.

119
00:07:18,518 --> 00:07:20,474
[HIGH PITCH FREQUENCY]

120
00:07:29,780 --> 00:07:32,670
OK, now what we would
like to demonstrate

121
00:07:32,670 --> 00:07:35,950
is the effect of changing
the sampling frequency.

122
00:07:35,950 --> 00:07:41,280
And we know that the effective
filter cutoff frequency is

123
00:07:41,280 --> 00:07:44,550
tied to the sampling frequency.

124
00:07:44,550 --> 00:07:47,910
And for this particular
filter, corresponds to a tenth

125
00:07:47,910 --> 00:07:50,100
of the sampling frequency.

126
00:07:50,100 --> 00:07:52,680
Consequently, if we double
the sampling frequency,

127
00:07:52,680 --> 00:07:56,880
we should double the effective
filter passband width

128
00:07:56,880 --> 00:08:00,120
or double the filter
cutoff frequency.

129
00:08:00,120 --> 00:08:02,460
And so let's do that now.

130
00:08:02,460 --> 00:08:06,960
Again, a 0 to 10 kilohertz
sweep range, but a 40 kilohertz

131
00:08:06,960 --> 00:08:09,870
sampling frequency.

132
00:08:09,870 --> 00:08:11,710
[HIGH PITCH FREQUENCY]

133
00:08:12,630 --> 00:08:16,160
And we should observe that the
filter cutoff frequency has now

134
00:08:16,160 --> 00:08:18,290
doubled to 4 kilohertz.

135
00:08:20,940 --> 00:08:25,380
Now let's begin to decrease
the filter sampling frequency.

136
00:08:25,380 --> 00:08:28,010
So from 40, let's change
the sampling frequency

137
00:08:28,010 --> 00:08:30,240
to 20 kilohertz.

138
00:08:30,240 --> 00:08:34,929
We should see the cutoff
frequency cut in half.

139
00:08:34,929 --> 00:08:37,429
[HIGH PITCH FREQUENCY]

140
00:08:43,429 --> 00:08:45,070
Now we can go even further.

141
00:08:45,070 --> 00:08:48,010
We can cut the sampling
frequency down to 10 kilohertz.

142
00:08:48,010 --> 00:08:51,150
And remember that the sweep
range is zero to 10 kilohertz.

143
00:08:51,150 --> 00:08:54,660
So now we'll be
sweeping from 0 to 2 pi.

144
00:08:54,660 --> 00:08:56,560
[HIGH PITCH FREQUENCY]

145
00:09:01,310 --> 00:09:06,650
So as we get close to 2 pi
we'll see the passband again.

146
00:09:06,650 --> 00:09:11,330
And now let's cut down the
sampling frequency even further

147
00:09:11,330 --> 00:09:14,147
to 5 kilohertz.

148
00:09:14,147 --> 00:09:16,492
[SHORT HIGH PITCH FREQUENCY]

149
00:09:18,370 --> 00:09:20,210
Here we are at 2 pi.

150
00:09:20,210 --> 00:09:22,410
[HIGH PITCH FREQUENCY]

151
00:09:22,410 --> 00:09:24,240
And then at 4 pi.

152
00:09:26,780 --> 00:09:29,390
All right, so that
illustrates the effect

153
00:09:29,390 --> 00:09:31,970
of changing the
sampling frequency.

154
00:09:31,970 --> 00:09:34,460
Now let's conclude
this demonstration

155
00:09:34,460 --> 00:09:37,280
of the effect of the sampling
frequency on the filter cutoff

156
00:09:37,280 --> 00:09:42,200
frequency by carrying out some
filtering on some live audio.

157
00:09:42,200 --> 00:09:48,530
What we'll watch, in this case,
is the output audio waveform

158
00:09:48,530 --> 00:09:52,370
as a function of time on
the single trace scope.

159
00:09:52,370 --> 00:09:55,610
And also, we'll
listen to the output.

160
00:09:55,610 --> 00:09:58,850
We'll begin it with a 40
kilohertz sampling rate,

161
00:09:58,850 --> 00:10:01,790
then reduce that to 20
kilohertz, 10 kilohertz,

162
00:10:01,790 --> 00:10:03,500
and then 5 kilohertz.

163
00:10:03,500 --> 00:10:06,380
And in each of those cases,
the effective filter cutoff

164
00:10:06,380 --> 00:10:10,170
frequency then is cut
in half from 4 kilohertz

165
00:10:10,170 --> 00:10:14,580
to 2 kilohertz to 1 kilohertz
and then to 500 cycles.

166
00:10:14,580 --> 00:10:17,480
So let's begin with a 40
kilohertz sampling frequency,

167
00:10:17,480 --> 00:10:22,559
or an effective filter cutoff
frequency of 4 kilohertz.

168
00:10:22,559 --> 00:10:27,390
[MUSIC - SCOTT JOPLIN, "MAPLE
 LEAF RAG"]

169
00:10:27,390 --> 00:10:29,625
Now let's reduce
that to 20 kilohertz

170
00:10:29,625 --> 00:10:32,630
sampling frequency or
a 2 kilohertz filter.

171
00:10:38,080 --> 00:10:40,400
Then a 10 kilohertz
sampling frequency.

172
00:10:45,740 --> 00:10:48,410
And finally, a 5 kilohertz
sampling frequency

173
00:10:48,410 --> 00:10:53,120
corresponding to a 500 cycle
equivalent analog filter.

174
00:10:58,220 --> 00:11:00,040
All right, now let's
finally conclude

175
00:11:00,040 --> 00:11:03,850
by returning to a little
higher quality ragtime

176
00:11:03,850 --> 00:11:08,820
by changing the sampling
frequency back to 40 kilohertz.