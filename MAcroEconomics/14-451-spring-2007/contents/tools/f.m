function f = f(k)        

global alpha delta A

f = A*k.^alpha + (1-delta)*k;


